#include <vtkObjectFactory.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkInformationVector.h>
#include <vtkInformation.h>
#include <vtkDataObject.h>
#include <vtkSmartPointer.h>
#include <vtkTriangleFilter.h>
#include <vtkCellArray.h>
#include <vtkPolyData.h>
#include <vtkPLYReader.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkOBJReader.h>
#include <vtkProperty.h>
#include <vtkCellData.h>
#include <vtkAxesActor.h>
#include <vtkPointData.h>
#include <vtkTransform.h>
#include <vtkSphereSource.h>
#include <vtkCallbackCommand.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkCellPicker.h>
#include <vtkViewport.h>
#include <vtkCamera.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkpolydataalgorithm.h>

#include <Eigen>
#include <Sparse>
#include <SparseQR>

#ifdef _MSC_VER
#include <Windows.h>
#define SG_QPC_PRESENT 1
#endif

#include <vector>
#include <string>
#include <chrono>
#include <algorithm>