///
/// @file SkinnedMesh.cpp
/// @author Michal Zak
/// @brief Contains implementation and interface of mesh-skinning.
///

#include "stdafx.h"
#include "SkinnedMesh.h"

// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
CSkinnedMesh::CSkinnedMesh(void)
{
  this->SetNumberOfInputPorts(0);
  this->SetNumberOfOutputPorts(1);
}

// ------------------------------------------------------------------------------
CSkinnedMesh::~CSkinnedMesh(void)
{
}

// ------------------------------------------------------------------------------
/// Overridden function, this method does the process.
int CSkinnedMesh::RequestData(vtkInformation* vtkNotUsed(request), vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{
  // get the info objects
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the ouptut
  vtkPolyData *output = vtkPolyData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  if (output)
    Execute(output);
  return 1;
}

// ------------------------------------------------------------------------------
void CSkinnedMesh::SetMeshData(vtkPolyData* meshData)
{
  assert(meshData);
  vtkPoints* points = meshData->GetPoints();
  UINT32 vertexCount = points->GetNumberOfPoints();
  UINT32 triangleCount = meshData->GetNumberOfCells();

  vertices.resize(vertexCount);
  for (UINT32 i = 0; i < vertexCount; i++)
  {
    double xyz[3];
    points->GetPoint(i, xyz);
    vertices[i].x = xyz[0];
    vertices[i].y = xyz[1];
    vertices[i].z = xyz[2];
  }

  meshData->BuildCells();
  indices.resize(triangleCount * 3);
  for (UINT32 i = 0; i < triangleCount; i++)
  {
    vtkIdType* inds;
    vtkIdType numPts;
    meshData->GetCellPoints((vtkIdType)i, numPts, inds);

    if (numPts >= 3)
    {
      indices[i * 3] = inds[0];
      indices[i * 3 + 1] = inds[1];
      indices[i * 3 + 2] = inds[2];
    }
  }
}

// ------------------------------------------------------------------------------
bool CSkinnedMesh::ComputeWeights(vtkPolyData* meshData_LaplaceSmoothed)
{
  assert(meshData_LaplaceSmoothed);
  vtkPoints* points = meshData_LaplaceSmoothed->GetPoints();
  UINT32 vertexCount = points->GetNumberOfPoints();
  //UINT32 vertexCount = vertices.size();
  const UINT32 boneCount = skeletonRestPose.bones.size();

  if (vertexCount != vertices.size() || boneCount == 0)
    return false;
    
  std::vector<double> weightsVertToBone;
  weightsVertToBone.resize(boneCount);

  // generate weights for each vertex
  for (UINT32 i = 0; i < vertexCount; i++)
  {
    double xyz[3];
    //xyz[0] = vertices[i].x;
    //xyz[1] = vertices[i].y;
    //xyz[2] = vertices[i].z;
    points->GetPoint(i, xyz);
    Eigen::Vector3d pos(xyz[0], xyz[1], xyz[2]);
    
    // query all bones
    for (UINT32 j = 0; j < boneCount; j++)
    {
      const CSkeletonBone& currentBone = skeletonRestPose.bones[j];
      const CSkeletonJoint& node1 = skeletonRestPose.nodes[currentBone.nodeHead];
      const CSkeletonJoint& node2 = skeletonRestPose.nodes[currentBone.nodeTail];
      const Eigen::Vector3d p1(node1.x, node1.y, node1.z);
      const Eigen::Vector3d p2(node2.x, node2.y, node2.z);
      weightsVertToBone[j] = 1 / (0.00001 + distanceFromLineSegmentSq(pos, p1, p2));
    }

    for (UINT32 k = 0; k < BONES_PER_VERTEX; k++)
    {
      vertices[i].boneIndices[k] = 0;
      vertices[i].boneWeights[k] = 0;
    }

    // collect most significant
    double weightsum = 0;
    for (UINT32 k = 0; k < BONES_PER_VERTEX; k++)
    {
      int bestJ = -1;
      double bestW = 0;
      for (UINT32 j = 0; j < boneCount; j++)
      {
        if (weightsVertToBone[j] > bestW)
        {
          bestJ = j;
          bestW = weightsVertToBone[j];
        }
      }
      weightsVertToBone[bestJ] = 0;

      //const auto& bestbone = skeletonRestPose.nodes[skeletonRestPose.bones[bestJ].nodeHead];
      //vertices[i].x = bestbone.x; +0.1 * rand() / RAND_MAX;
      //vertices[i].y = bestbone.y; +0.1 * rand() / RAND_MAX;
      //vertices[i].z = bestbone.z; +0.1 * rand() / RAND_MAX;

      vertices[i].boneIndices[k] = bestJ;
      vertices[i].boneWeights[k] = bestW;
      weightsum += bestW;
    }

    // normalize weights
    double invweightsum = 1.0 / weightsum;
    for (UINT32 k = 0; k < BONES_PER_VERTEX; k++)
      vertices[i].boneWeights[k] *= invweightsum;
  }

  //DEBUG
  //for (UINT32 j = 0; j < boneCount; j++)
  //{
  //  const CSkeletonJoint& node1 = skeletonRestPose.nodes[skeletonRestPose.bones[j].nodeHead];
  //  const CSkeletonJoint& node2 = skeletonRestPose.nodes[skeletonRestPose.bones[j].nodeTail];
  //  const Eigen::Vector3d p1(node1.x, node1.y, node1.z);
  //  const Eigen::Vector3d p2(node2.x, node2.y, node2.z);

  //  indices.push_back(vertices.size());
  //  indices.push_back(vertices.size());
  //  indices.push_back(vertices.size());

  //  vertices.push_back(TMeshVertex());
  //  vertices.back().x = node2.x;
  //  vertices.back().y = node2.y;
  //  vertices.back().z = node2.z;
  //}

  //for (UINT32 i = 0; i < vertexCount; i++)
  //{
  //  double xyz[3];
  //  //xyz[0] = vertices[i].x;
  //  //xyz[1] = vertices[i].y;
  //  //xyz[2] = vertices[i].z;
  //  points->GetPoint(i, xyz);

  //  indices.push_back(vertices.size());
  //  indices.push_back(vertices.size());
  //  indices.push_back(vertices.size());

  //  vertices.push_back(TMeshVertex());
  //  vertices.back().x = xyz[0];
  //  vertices.back().y = xyz[1];
  //  vertices.back().z = xyz[2];
  //}

  return true;
}

// ------------------------------------------------------------------------------
void CSkinnedMesh::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

// ------------------------------------------------------------------------------
void CSkinnedMesh::Execute(vtkPolyData* toPolydata)
{
  unsigned char colorWhite[] = { 255, 255, 255 };
  assert(toPolydata);
  UINT32 vertexCount = vertices.size();
  UINT32 indexCount = indices.size();
  vtkSmartPointer<vtkPoints> points;
  
  points = toPolydata->GetPoints() ? vtkSmartPointer<vtkPoints>(toPolydata->GetPoints()) : vtkSmartPointer<vtkPoints>::New();
  points->SetNumberOfPoints(vertexCount);

  vtkSmartPointer<vtkUnsignedCharArray> colors =
    vtkSmartPointer<vtkUnsignedCharArray>::New();
  colors->SetNumberOfComponents(3);
  colors->SetName("Colors");

  const auto& restBones = skeletonRestPose.bones;
  const auto& poseBones = skeletonCurrPose.bones;

  for (UINT32 i = 0; i < vertexCount; i++)
  {
    const TMeshVertex& vert = vertices[i];
    Eigen::Vector4d posw(vert.x, vert.y, vert.z, 1);
    Eigen::Vector4d poswSkinned(0, 0, 0, 1);
   
    for (UINT32 k = 0; k < BONES_PER_VERTEX; k++)
    {
      UINT32 boneIndex = vert.boneIndices[k];
      poswSkinned += vert.boneWeights[k] * (poseBones[boneIndex].worldTransform * (restBones[boneIndex].worldTransformInverted * posw));
    }

    //poswSkinned = posw;
    points->SetPoint(i, poswSkinned.x(), poswSkinned.y(), poswSkinned.z());

    if (ColorizeSegmentation)
      colors->InsertNextTupleValue(&colortable[(vert.boneIndices[0] * 3) % sizeof(colortable)]);
    else
      colors->InsertNextTupleValue(colorWhite);
  }

  toPolydata->SetPoints(points);
  //if (toPolydata->GetNumberOfCells() != indexCount)
  {
    toPolydata->DeleteCells();
    toPolydata->Allocate(indexCount / 3);
    for (UINT32 i = 0; i < indexCount; i += 3)
    {
      vtkIdType pts[3];
      pts[0] = (vtkIdType)indices[i];
      pts[1] = (vtkIdType)indices[i + 1];
      pts[2] = (vtkIdType)indices[i + 2];
      toPolydata->InsertNextCell(VTK_TRIANGLE, 3, pts);
    }
    toPolydata->BuildCells();
  }

  toPolydata->GetPointData()->SetScalars(colors);
}

// ------------------------------------------------------------------------------
vtkStandardNewMacro(CSkinnedMesh);