///
/// @file Assignment.h
/// @author Michal Zak
/// @brief Skeleton and its assignment.
///

#pragma once
#include "Utils.h"

/// Contraint of rotation (defined as rotation aroud x,y,z in rads).
class CSkeletonJointConstraint
{
public:
  double minx = -M_PI, maxx = M_PI;
  double miny = -M_PI, maxy = M_PI;
  double minz = -M_PI, maxz = M_PI;
};

/// Skeleton joint (node).
class CSkeletonJoint
{
public:
  Eigen::Matrix4d matrixWorld_Parent;         //!< temporary storage for bone matrix
  CSkeletonJointConstraint constraint_Parent; //!< joint movement constraints

  int index;                                  //!< joint index
  double x, y, z;                             //!< position coordinates
  std::vector<int> links;                     //!< array of links to child joints and parent joint

  int parentIndex = -1;                       //!< index of parent

  /// Default constructor.
  inline CSkeletonJoint() : x(0), y(0), z(0) { }
};

class CSkeleton;

/// One skeleton bone.
class CSkeletonBone
{
private:
  friend class CSkeleton;
public:
  Eigen::Matrix4d   worldTransform;         //!< transformation in world coordinates
  Eigen::Matrix4d   worldTransformInverted; //!< inverse transformation in world coordinates

  Eigen::Vector3d   axisAngles;                 //!< current rotation described by axis angles
  double            stretch = 1;                //!< bone stretching (multiplication of former length)

  CSkeleton* skeleton;  //!< pointer to skeleton
  int parentBone;       //!< index of parent bone
  int nodeHead;   //!< index of bone head 
  int nodeTail;   //!< index of bone tail

  /// Default constructor.
  inline CSkeletonBone() : axisAngles(0, 0, 0) {  }
};

/// Skeleton, collection of nodes (joints) and bones.
class CSkeleton
{
private:
  /// Traverse tree and set parent indices.
  void Traverse(int currIndex, int cameFrom);
public:
  CBoundBox boundbox;      //!< axis oriented bounding box of skeleton, used during skeleton matching
  std::vector<CSkeletonJoint, aligned_allocator<CSkeletonJoint, 16> > nodes;    //!< stored joints in aligned "vector" array
  std::vector<CSkeletonBone, aligned_allocator<CSkeletonBone, 16> > bones;      //!< stored skeleton bones in aligned "vector" array

  Eigen::MatrixXd distanceMatrix; //!< distance matrix (between nodes=vertices, bones=edges)
  int headIndex = -1;      //!< humanoid head - index of node
  int rootIndex = -1;      //!< root of skeleton, assumed to be "chest"
  double longestPath = 0;  //!< longest path in skeletal graph

  /// Default constructor.
  inline CSkeleton() { }
  /// Copy constructor.
  CSkeleton(const CSkeleton& copyFrom) : boundbox(copyFrom.boundbox), nodes(copyFrom.nodes), bones(copyFrom.bones),
    distanceMatrix(copyFrom.distanceMatrix), headIndex(copyFrom.headIndex), rootIndex(copyFrom.rootIndex), longestPath(copyFrom.longestPath)
  {
    for (CSkeletonBone& bone : bones)
      bone.skeleton = this;
  }
  /// Assignment operator.
  CSkeleton& operator= (const CSkeleton& copyFrom)
  {
    boundbox = copyFrom.boundbox;
    nodes = copyFrom.nodes;
    bones = copyFrom.bones;
    distanceMatrix = copyFrom.distanceMatrix;
    headIndex = copyFrom.headIndex;
    rootIndex = copyFrom.rootIndex;
    longestPath = copyFrom.longestPath;
    for (CSkeletonBone& bone : bones)
      bone.skeleton = this;

    return *this;
  }

  /// Clear all joints.
  inline void Clear() { boundbox = CBoundBox(); nodes.clear(); distanceMatrix.resize(0, 0); headIndex = -1; longestPath = 0; }
  /// Load skeleton from file.
  bool Load(const char* filename);
  /// Compute distance matrix (geodesic distances between nodes, store them inside distanceMatrix).
  void ComputeDistanceMatrix();

  /// Add new node (joint), return index of it.
  int AddNode(double x, double y, double z);
  /// Connect two joints (make bone).
  void Connect(int node1, int node2);
  /// Find node representing head, reorder rest. Head is the highest node. Distance matrix must be already prepared!
  int FindHeadAndReorder();
  /// Rescale graph positions. Also affects distance matrix.
  void Rescale(double scalingFactor, double newCenterX, double newCenterY, double newCenterZ, bool centerIsNodeHavingMaxDegree);

  /// Compute best value of DWT using rows from two matrices mat1, mat2.
  static double ComputeValueDWT(Eigen::MatrixXd& mat1, Eigen::MatrixXd& mat2, int row1, int row2);

  /// Create new skeleton that is topologically same as skeletonTemplate but inserted in positions of skeletonGenerated.
  static CSkeleton Match(const CSkeleton& skeletonGenerated, const CSkeleton& skeletonTemplate);

  /// Prepare array of bones.
  void PrepareBones();
  /// Mark specified node as root and recompute "parent" indices.
  void MakeRootNode(int index);

  /// Compute transformation matrix of bone in "relative rest pose" to "current parent pose".
  static Eigen::Matrix4d RelativeRestPose(const CSkeleton& skeletonMeshPose, const CSkeleton& skeletonMeshRest, const int boneIndex);
};

/// CSkeletonSource generates polydata from given skeleton.
class CSkeletonSource : public vtkPolyDataAlgorithm
{
public:
private:
  /// Not implemented.
  CSkeletonSource(const CSkeletonSource&) : vtkPolyDataAlgorithm() { }
  /// Not implemented.
  void operator=(const CSkeletonSource&) { }

protected:
  CSkeleton skeleton;   //!< local copy of skeleton

  /// Execution method.
  virtual void Execute(vtkPolyData* toPolydata);

  /// Default constructor.
  CSkeletonSource(void);
  /// Default destructor.
  virtual ~CSkeletonSource(void);
  /// Overridden function, this method does the process.
  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
public:
  /// Copy skeleton to internal storage for future rendering.
  inline void SetSkeleton(const CSkeleton& skeletonRef) { skeleton = skeletonRef; }
  /// Get ref. to internally stored skeleton.
  const CSkeleton& GetSkeleton() const { return skeleton; }

  /// VTK macro.
  vtkTypeMacro(CSkeletonSource, vtkPolyDataAlgorithm);
  /// Print info about this object.
  void PrintSelf(ostream& os, vtkIndent indent);
  /// Create new instance.
  static CSkeletonSource* New();
};

/// Helper class - visualize skeleton from source using internal actors.
class CSkeletonVisualization
{
public:
  std::vector<vtkSmartPointer<vtkAxesActor> > axesBoneActors;                                    //!< local axes shows at joints
  vtkSmartPointer<CSkeletonSource> skeletonSource = vtkSmartPointer<CSkeletonSource>::New();     //!< source used for generating skeletal polydata 
  vtkSmartPointer<vtkActor> skeletonActor = vtkSmartPointer<vtkActor>::New();                    //!< skeleton actor (placing polydata on scene)
  vtkSmartPointer<vtkPolyDataMapper> mapperSkeleton = vtkSmartPointer<vtkPolyDataMapper>::New(); //!< polydata mapper
  bool refreshed = false;        //!< flag: have we drawn it before?
  bool axesVisible = false;      //!< show or hide axes in joints
  bool skeletonVisible = true;   //!< show or hide bones

  /// Default constructor.
  inline CSkeletonVisualization()
  {
    mapperSkeleton->SetInputConnection(skeletonSource->GetOutputPort());
    skeletonActor->SetMapper(mapperSkeleton);
    skeletonActor->GetProperty()->SetLineWidth(2);
    skeletonActor->GetProperty()->SetPointSize(10);
    skeletonActor->GetProperty()->LightingOff();
    skeletonActor->GetProperty()->SetColor(0, 0, 0);
    skeletonActor->PickableOff();
  }

  /// Refresh visualization.
  inline void Refresh(vtkSmartPointer<vtkRenderer> renderer, const CSkeleton& skeleton)
  {
    skeletonSource->SetSkeleton(skeleton);
    skeletonSource->Modified();
    skeletonSource->Update();

    if (!refreshed)
    {
      renderer->AddActor(skeletonActor);
      for (const CSkeletonBone& bone : skeletonSource->GetSkeleton().bones)
      {
        vtkSmartPointer<vtkAxesActor> axesBone = vtkSmartPointer<vtkAxesActor>::New();
        renderer->AddActor(axesBone);
        axesBone->SetAxisLabels(0);
        axesBoneActors.push_back(axesBone);
      }
    }

    for (size_t i = 0; i < axesBoneActors.size(); i++)
    {
      const CSkeletonBone& bone = skeletonSource->GetSkeleton().bones[i];
      vtkSmartPointer<vtkAxesActor>& axesBone = axesBoneActors[i];
      vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();

      double matval[16];
      for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
          matval[i * 4 + j] = bone.worldTransform.coeff(i, j);

      transform->SetMatrix(matval);
      double axesScale = skeletonSource->GetSkeleton().longestPath * 0.05;
      transform->Scale(axesScale, axesScale, axesScale);
      axesBone->SetUserTransform(transform);
      axesBone->SetVisibility(axesVisible);
    }

    skeletonActor->SetVisibility(skeletonVisible);
    refreshed = true;
  }
};