///
/// @file MeshEmptyFilter.cpp
/// @author Michal Zak
/// @brief MeshEmptyFilter class, empty filter with internal structures
///

#include "stdafx.h"
#include "MeshEmptyFilter.h"

// ------------------------------------------------------------------------------
MeshEmptyFilter::MeshEmptyFilter(void)
{
  numberVertexes = 0;
  numberTriangles = 0;
  this->SetNumberOfInputPorts(1);
  this->SetNumberOfOutputPorts(1);
}

// ------------------------------------------------------------------------------
MeshEmptyFilter::~MeshEmptyFilter(void)
{
}

// ------------------------------------------------------------------------------
int MeshEmptyFilter::RequestData(vtkInformation* vtkNotUsed(request), vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the input and ouptut
  vtkPolyData *input = vtkPolyData::SafeDownCast(
      inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkPolyData *output = vtkPolyData::SafeDownCast(
      outInfo->Get(vtkDataObject::DATA_OBJECT()));

  vtkSmartPointer<vtkTriangleFilter> triangle_mesh = vtkTriangleFilter::New();
  triangle_mesh->SetInputData(input);
  triangle_mesh->PassVertsOff();
  triangle_mesh->PassLinesOff();
  triangle_mesh->Update();

  InitMesh(triangle_mesh->GetOutput());
  Execute();

  DoneMesh(output);
  return 1;
}

// ------------------------------------------------------------------------------
void MeshEmptyFilter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
void MeshEmptyFilter::InitMesh(vtkPolyData* fromPolydata)
{
  aabb = CBoundBox();
  vertexes.clear();
  edges.clear();
  triangles.clear();
  numberVertexes = 0;
  numberTriangles = 0;
  numberEdges = 0;

  int i;
  double *pCoord;
  int n;
  vtkIdType *pts;

  vtkstd::vector<CVertex>::pointer  vertex;
  vtkstd::vector<CTriangle>::pointer	triangle;

  numberVertexes = fromPolydata->GetNumberOfPoints();
  numberTriangles = fromPolydata->GetNumberOfCells();

  if (numberVertexes == 0 || numberTriangles == 0)
    return;

  vertexes.resize(numberVertexes);
  triangles.resize(numberTriangles);
  vertex = numberVertexes ? &(vertexes[0]) : NULL;
  triangle = numberTriangles ? &(triangles[0]) : NULL;

  vtkDataArray *points = fromPolydata->GetPoints()->GetData();

  for(i=0;i<numberVertexes;i++,vertex++)
  {
    pCoord = points->GetTuple(i);
    vertex->SetCoord(pCoord);
    aabb.Join(vertex->dCoord[0], vertex->dCoord[1], vertex->dCoord[2]);
  }

  fromPolydata->BuildCells();
  for(i=0; i<numberTriangles; i++,triangle++)
  {   
    fromPolydata->GetCellPoints(i,n,pts);
    triangle->SetVertex(pts);
  }

  BuildConnection();
}

// ------------------------------------------------------------------------------
void MeshEmptyFilter::DoneMesh(vtkPolyData* toPolydata)
{
  if (!numberTriangles)
    return;

  int i;
  // set up polydata object and data arrays
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();  

  vtkstd::vector<CVertex>::const_pointer	  vertex;  
  vertex = &(vertexes[0]);
  points->Allocate(numberVertexes);
  for(i = 0; i < numberVertexes; i++, vertex++)
  {
    points->SetPoint(i, vertex->dCoord[0], vertex->dCoord[1], vertex->dCoord[2]);    
  }
  points->SetNumberOfPoints(numberVertexes);
  
  toPolydata->SetPoints(points);

  toPolydata->Allocate(numberTriangles);
  vtkstd::vector<CTriangle>::const_pointer	  triangle;  
  triangle = &(triangles[0]);
  for(i = 0; i < numberTriangles; i++, triangle++)
  {    
    vtkIdType pts[3];
    pts[0] = (vtkIdType)triangle->aVertex[0];
    pts[1] = (vtkIdType)triangle->aVertex[1];
    pts[2] = (vtkIdType)triangle->aVertex[2];
    toPolydata->InsertNextCell(VTK_TRIANGLE, 3, pts);
  }
  
  vertexes.clear();
  triangles.clear();
  edges.clear();

  numberVertexes = 0;
  numberTriangles = 0;
  numberEdges = 0;
}


// ------------------------------------------------------------------------------
void MeshEmptyFilter::BuildConnection()
{
  int i,j,t,tright;
  int sv1,sv2,sv3,sv4;
  int dv2;
  int size,k;
  bool bflag;

  int     *pVertexIndex,*pVertexIndex2,*pEdgeIndex;

  vtkstd::vector<CVertex>::pointer      startVertex,pVertex1,pVertex2;
  vtkstd::vector<CEdge>::pointer			  startEdge,pEdge;
  vtkstd::vector<CTriangle>::pointer	  startTriangle,pTriangle;

  vtkstd::vector<int>::const_pointer	  neight;

  startVertex = &(vertexes[0]);
  startTriangle = &(triangles[0]);
  for( t=0,pTriangle=startTriangle; t<numberTriangles; t++,pTriangle++)
  {
    pVertexIndex = pTriangle->aVertex;
    //each triangle has three vertex;
    for(i=0;i<3;i++)
    {
      //add the triangle to the vertex as an first degree ring triangle.
      (startVertex+pVertexIndex[i])->OneRingTriangle.push_back(t);
    }
  }

  numberEdges = 0;
  // each mesh has 3*numberTriangles edges at most.
  edges.resize(3*numberTriangles);

  startEdge = &(edges[0]);
  for(t=0,pTriangle=startTriangle; t<numberTriangles; t++,pTriangle++)
  {
    pEdgeIndex = pTriangle->aEdge;
    //each triangle has three vertex;
    for(i=0;i<3;i++)
    {
      if( pEdgeIndex[i] >= 0 )				continue;
      else  pEdgeIndex[i] = numberEdges;      

      pVertexIndex = pTriangle->aVertex;
      pEdge = startEdge + numberEdges ;

      sv1 = pVertexIndex[i];
      sv2 = pVertexIndex[(i+1)%3];
      sv3 = pVertexIndex[(i+2)%3];
      sv4 = -1;
      tright = -1;

      pVertex1 = startVertex+sv1;   
      pVertex2 = startVertex+sv2;   

      pVertex1->degree++;
      pVertex2->degree++;

      bflag = true;
      size = pVertex1->OneRingTriangle.size();
      neight = &(pVertex1->OneRingTriangle[0]);
      for( k=0; k<size ; k++, neight++ )
      {
        // if the triangle was processed( its index is less then current index), 
        // just next triangle.
        if( *neight <= t) continue;     
        pVertexIndex2 = (startTriangle + (*neight))->aVertex;
        for(j=0; j<3; j++)
        {
          if(pVertexIndex2[j] != sv2) continue;
          else 
          {   
            // find the edge;  if the mesh is a manifold, we don't search again.
            // but if the mesh isn't a manifold, we have to search other triangles.
            //k = size;              
            //if( bflag == false )
            {  
              // the mesh isn't a manifold. the edge have three connected triangles at less!
            }
          }

          dv2 = pVertexIndex2[(j+1)%3];
          if( dv2 == sv1)  
          {
            // if the mesh is manifold, each edge only have two adjacent triangles.
            // assert pEdge->aVertex[3]<0 && pEdge->aTriangle[1]<0;

            sv4 = pVertexIndex2[(j+2)%3];
            tright = *neight;
            (startTriangle + (*neight))->aEdge[j] = numberEdges;
          }
          else
          {
            // bad mesh. two neighbor triangle has different vertex order.
            // assert pVertexIndex2[(j+2)%3] == sv1
            
            // vtkstd::cout << "bad mesh! two neighbor triangles have a different vertex order\n";
            sv4 = dv2;
            tright = *neight; 
            (startTriangle + (*neight))->aEdge[(j+2)%3] = numberEdges;
          }
          // the edge isn't a boundary edge
          bflag = false;
          break;		
        }
      }
      pEdge->SetVertex(sv1,sv2,sv3,sv4);
      pEdge->SetTriangle(t,tright);
      pEdge->bBoundary = bflag;
      numberEdges++;
    }
  }
  edges.resize(numberEdges);

  vtkstd::vector<CVertex>::pointer      pVertex;
  for(pVertex=startVertex,i=0;i<numberVertexes;i++,pVertex++)
  {
    size = pVertex->degree;
    pVertex->TwoRingVertex.reserve(3*size);    
    pVertex->OneRingEdge.reserve(size);    
    pVertex->OneRingVertex.reserve(size);    
  }

  for(i=0,pEdge=&(edges[0]); i<numberEdges; pEdge++,i++)
  {
    sv1 = pEdge->aVertex[0];
    sv2 = pEdge->aVertex[1];

    pVertex1 = startVertex+sv1;   
    pVertex2 = startVertex+sv2;   

    pVertex1->OneRingEdge.push_back(i);
    pVertex2->OneRingEdge.push_back(i);

    pVertex1->OneRingVertex.push_back(sv2);
    pVertex2->OneRingVertex.push_back(sv1);			
  }

  vtkstd::vector<int>::const_pointer	  first,second;
  vtkstd::vector<int>::iterator         tworingstart,tworingend;

  for(i=0;i<numberVertexes;i++)
  {
    pVertex = startVertex+i;
    if (!pVertex->OneRingVertex.size())
      continue;

    first = &(pVertex->OneRingVertex[0]);
    size = pVertex->degree;

    for(j=0; j<size; j++, first++)
    {
      pVertex1 = startVertex + *first;
      second = first+1;
      for( k=j+1; k<size; k++, second++)
      {
        tworingend = pVertex1->TwoRingVertex.end();
        if( tworingend != vtkstd::find(pVertex1->TwoRingVertex.begin(),tworingend,*second) ) continue;

        pVertex1->TwoRingVertex.push_back(*second);
        pVertex2 = startVertex + *second;
        pVertex2->TwoRingVertex.push_back(*first);
      }
    }	
  }
}

// ------------------------------------------------------------------------------
vtkStandardNewMacro(MeshEmptyFilter);


// ----- implementation of internal structures

//----------------------------------------------------------------------------
void MeshEmptyFilter::CVertex::SetCoord(double *pCoord)
//----------------------------------------------------------------------------
{
  dCoord[0] = pCoord[0];
  dCoord[1] = pCoord[1];
  dCoord[2] = pCoord[2];
}

//----------------------------------------------------------------------------
MeshEmptyFilter::CVertex::CVertex()
//----------------------------------------------------------------------------
{
  degree = 0; 
  OneRingTriangle.reserve(8);
}

//----------------------------------------------------------------------------
MeshEmptyFilter::CVertex::~CVertex()
//----------------------------------------------------------------------------
{
  OneRingVertex.clear();
  OneRingTriangle.clear();
  OneRingEdge.clear();
  TwoRingVertex.clear();
}

//----------------------------------------------------------------------------
MeshEmptyFilter::CTriangle::CTriangle()
//----------------------------------------------------------------------------
{
  aVertex[0]=aVertex[1]=aVertex[2] = -1;
  aEdge[0]=aEdge[1]=aEdge[2] = -1;
}

//----------------------------------------------------------------------------
void MeshEmptyFilter::CTriangle::SetVertex(int *v)
//----------------------------------------------------------------------------
{
  aVertex[0]=v[0];
  aVertex[1]=v[1];
  aVertex[2]=v[2];
}

//----------------------------------------------------------------------------
void MeshEmptyFilter::CTriangle::SetVertex(int v0,int v1,int v2)
//----------------------------------------------------------------------------
{
  aVertex[0]=v0;
  aVertex[1]=v1;
  aVertex[2]=v2;
}

//----------------------------------------------------------------------------
MeshEmptyFilter::CEdge::CEdge()
//----------------------------------------------------------------------------
{
  aVertex[0]=aVertex[1]=aVertex[2]=aVertex[3]=-1;
  aTriangle[0]=aTriangle[1]=-1;
}

//----------------------------------------------------------------------------
void MeshEmptyFilter::CEdge::SetVertex(int v0,int v1,int v2,int v3)
//----------------------------------------------------------------------------
{
  aVertex[0] = v0;
  aVertex[1] = v1;
  aVertex[2] = v2;
  aVertex[3] = v3;
}

//----------------------------------------------------------------------------
void MeshEmptyFilter::CEdge::SetTriangle(int t0,int t1)
//----------------------------------------------------------------------------
{
  aTriangle[0]=t0;
  aTriangle[1]=t1;
}
