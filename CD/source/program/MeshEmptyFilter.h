///
/// @file MeshEmptyFilter.h
/// @author Michal Zak
/// @brief MeshEmptyFilter class, empty filter with internal structures
///

#pragma once
#include "Utils.h"


/// Empty filter - builds internal structures and writes them back.
/// Inherit this class to override structure creation / execution / write back methods.
class MeshEmptyFilter : public vtkPolyDataAlgorithm
{
public:
  //----------------------------------------------------------------------------
  /** Nested Vertex class 
  This is a list of the triangles, edges and vertices which are joined to this vertex.*/
  //----------------------------------------------------------------------------
  class CVertex
  {
  public:
    double  dCoord[3];
    int     degree;

    vtkstd::vector<int> OneRingTriangle;
    vtkstd::vector<int> TwoRingVertex;

    vtkstd::vector<int> OneRingEdge;
    vtkstd::vector<int> OneRingVertex;

    CVertex();
    ~CVertex();
    void SetCoord(double *pCoord);
  };

  //----------------------------------------------------------------------------
  /** Nested Triangle class 
  Each triangle has three edges and three vertices.*/
  //----------------------------------------------------------------------------
  class  CTriangle
  {
  public:
    int   aVertex[3];
    int   aEdge[3];
  public:
    CTriangle();
    void SetVertex(int* v);
    void SetVertex(int v0,int v1,int v2);
  };

  //----------------------------------------------------------------------------
  /** Nested Edge class 
  Normally, each edge has two neighbor triangles. Two vertices consist of an edge.*/
  //----------------------------------------------------------------------------
  class CEdge
  {
  public:
    int   aVertex[4];       //first second, left right
    int   aTriangle[2];     //left right
    bool  bBoundary;
    CEdge(); 
    void SetTriangle(int t0,int t1);
    void SetVertex(int v0,int v1,int v2,int v3);
  };
private:  
  /// Not implemented.
  MeshEmptyFilter(const MeshEmptyFilter&) : vtkPolyDataAlgorithm() { }
  /// Not implemented.
  void operator=(const MeshEmptyFilter&) { }

protected:

  int numberEdges;
  int numberVertexes;
  int numberTriangles;
  vtkstd::vector<CVertex>     vertexes;
  vtkstd::vector<CTriangle>   triangles;
  vtkstd::vector<CEdge>       edges;
  CBoundBox aabb;

  /// Copy data to internal structures.
  virtual void InitMesh(vtkPolyData* fromPolydata);
  /// Build connections in internal data structures.
  virtual void BuildConnection();
  /// Copy data from internal structures.
  virtual void DoneMesh(vtkPolyData* toPolydata);
  /// Execution method.
  virtual void Execute() { }

  /// Default constructor.
  MeshEmptyFilter(void);
  /// Default destructor.
  virtual ~MeshEmptyFilter(void);
  /// Overridden function, this method does the process.
  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
public:
  /// VTK macro.
  vtkTypeMacro(MeshEmptyFilter,vtkPolyDataAlgorithm);
  /// Print info about this object.
  void PrintSelf(ostream& os, vtkIndent indent);
  /// Create new instance.
  static MeshEmptyFilter *New();
};

