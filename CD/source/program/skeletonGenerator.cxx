///
/// @file skeletonGenerator.cxx
/// @author Michal Zak
/// @brief Main program, mostly writen in static functions.
///

#include "stdafx.h"
#include "MeshLaplaceFilter.h"
#include "EdgeCollapseFilter.h"
#include "Assignment.h"
#include "SkinnedMesh.h"
#include "Utils.h"

// ------------------------------------------------------------------------------------------

/// Default address of template skeleton.
const char* skeletonDefaultAddr = "../data/humanBones.txt";
// application settings (all values in one structure)
TAppSettings settings;

// default skeleton, that means "template skeleton"
CSkeleton skeletonDefault;
// skeleton of mesh
CSkeleton skeletonMesh;
// skeleton of mesh - rest pose
CSkeleton skeletonMeshRest;

// scene objects related to mesh
vtkSmartPointer<CSkinnedMesh> skinnedMeshSource;
vtkSmartPointer<vtkActor> skinnedMeshActor;
// scene objects related to skeleton
CSkeletonVisualization skeletonVisualization;

// global pointers: renderer, render window, interactor
vtkSmartPointer<vtkRenderer> renderer;
vtkSmartPointer<vtkRenderWindow> renderWindow;
vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor;

// algorithm pointers
vtkSmartPointer<MeshLaplaceFilter> laplaceTransform;
vtkSmartPointer<vtkActor> laplaceTransformActor;
vtkSmartPointer<EdgeCollapseFilter> edgeCollapse;

vtkSmartPointer<vtkSphereSource> sphereSource;

/// Structure containing pair "actor" : "node index".
struct TLeafNode
{
  vtkSmartPointer<vtkActor> actor;   //!< pointer to actor placed in scene
  int nodeIndex;   //!< index of node in skeleton
};

std::vector<TLeafNode> leafNodes;  //!< array of leaf nodes

// ------------------------------------------------------------------------------------------

/// Load default skeleton (skeleton template).
bool LoadDefaultSkeleton();
/// Load model from obj file and fill given data structures.
bool LoadModelAndComputeSkeleton(CSkinnedMesh* skinnedMesh, CSkeleton* computedSkeleton);
/// Parse command line and gain given arguments. Returns true on success.
bool ParseCommandLine(int argc, char *argv[]);
/// Prepare 3D scene and renderer (construct necessary objects).
bool PrepareScene();
/// Callback capturing key presses.
void KeypressCallbackFunction(vtkObject* caller, long unsigned int vtkNotUsed(eventId), void* clientData, void* vtkNotUsed(callData));
/// Process inverse kinematics.
void ProcessIK(vtkActor* updatedEffector, bool allowStretching);

// ------------------------------------------------------------------------------------------
class MouseInteractorPicker : public vtkInteractorStyleTrackballCamera
{
private:
  vtkActor* pickedActor = NULL;
  double surfaceCoeff[4];
  double pickedActorOrigColors[4];
public:
  static MouseInteractorPicker* New();
  vtkTypeMacro(MouseInteractorPicker, vtkInteractorStyleTrackballCamera);

  // ----------------------------------------------
  virtual void OnLeftButtonDown() override
  {
    // event location
    int* clickPos = this->GetInteractor()->GetEventPosition();
    // Pick from this location.
    vtkSmartPointer<vtkCellPicker> picker = vtkSmartPointer<vtkCellPicker>::New();
    if (picker->Pick(clickPos[0], clickPos[1], 0, this->GetDefaultRenderer()))
    {
      // something was picked!
      pickedActor = picker->GetActor();
      pickedActor->GetProperty()->GetColor(pickedActorOrigColors);
      double colorSelected[4] {1, 0.75, 0, 1 };
      pickedActor->GetProperty()->SetColor(colorSelected);

      double actorPos[3];
      memcpy(actorPos, pickedActor->GetPosition(), sizeof(double) * 3);

      vtkCamera *camera = renderer->GetActiveCamera();
      camera->GetViewPlaneNormal(surfaceCoeff);

      surfaceCoeff[3] = 0 - surfaceCoeff[0] * actorPos[0] - surfaceCoeff[1] * actorPos[1] - surfaceCoeff[2] * actorPos[2];
      renderer->GetRenderWindow()->Render();
    }
    else
    {
      // forward events
      vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
    }
  }

  // ----------------------------------------------
  virtual void OnMouseMove() override
  {
    // event location
    int* clickPos = this->GetInteractor()->GetEventPosition();
    // bring it to world coordinates...
    double worldClickPos[4];
    vtkInteractorObserver::ComputeDisplayToWorld(this->GetDefaultRenderer(), clickPos[0], clickPos[1], 0, worldClickPos);
    worldClickPos[0] /= worldClickPos[3];
    worldClickPos[1] /= worldClickPos[3];
    worldClickPos[2] /= worldClickPos[3];
    worldClickPos[3] = 1;

    if (pickedActor)
    {
      // move actor along plane parallel to camera projection plane
      double cameraFocus[3];
      this->GetDefaultRenderer()->GetActiveCamera()->GetPosition(cameraFocus);

      double vecFromCamera[3];
      vecFromCamera[0] = worldClickPos[0] - cameraFocus[0];
      vecFromCamera[1] = worldClickPos[1] - cameraFocus[1];
      vecFromCamera[2] = worldClickPos[2] - cameraFocus[2];
      
      double numerator = -surfaceCoeff[0] * cameraFocus[0] - surfaceCoeff[1] * cameraFocus[1] - surfaceCoeff[2] * cameraFocus[2] - surfaceCoeff[3];
      double denominator = surfaceCoeff[0] * vecFromCamera[0] + surfaceCoeff[1] * vecFromCamera[1] + surfaceCoeff[2] * vecFromCamera[2];
      double t = numerator / denominator;

      double wpos[3];
      wpos[0] = cameraFocus[0] + vecFromCamera[0] * t;
      wpos[1] = cameraFocus[1] + vecFromCamera[1] * t;
      wpos[2] = cameraFocus[2] + vecFromCamera[2] * t;

      pickedActor->SetPosition(wpos);

      ProcessIK(pickedActor, settings.ik_allowStretching);

      renderer->GetRenderWindow()->Render();
    }
    else
    {
      vtkInteractorStyleTrackballCamera::OnMouseMove();
    }
  }

  // ----------------------------------------------
  virtual void OnLeftButtonUp() override
  {
    if (pickedActor)
    {
      pickedActor->GetProperty()->SetColor(pickedActorOrigColors);

      ProcessIK(pickedActor, settings.ik_allowStretching);

      pickedActor = NULL;
      renderer->GetRenderWindow()->Render();
    }
    else
    {
      vtkInteractorStyleTrackballCamera::OnLeftButtonUp();
    }
  }
};
vtkStandardNewMacro(MouseInteractorPicker);
 
// ------------------------------------------------------------------------------------------

/// Entry point of the program.
int main(int argc, char *argv[])
{
  if (!ParseCommandLine(argc, argv))
  {
    printf("Invalid arguments.\n");
    return EXIT_FAILURE;
  }

  if (!LoadDefaultSkeleton())
  {
    printf("Error - cannot load default skeleton.\n");
    return EXIT_FAILURE;
  }

  skinnedMeshSource = vtkSmartPointer<CSkinnedMesh>::New();
  if (!LoadModelAndComputeSkeleton(skinnedMeshSource, &skeletonMesh))
  {
    printf("Error - cannot load model.\n");
    return EXIT_FAILURE;
  }
  skeletonMeshRest = skeletonMesh;

  if (!PrepareScene())
  {
    printf("Error - cannot prepare scene.\n");
    return EXIT_FAILURE;
  }

  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}

/// Parse command line and gain given arguments. Returns true on success.
bool ParseCommandLine(int argc, char *argv[])
{
  // help (no arguments given)
  if (argc < 2 || argc % 2 != 0)
  {
    std::cout << "Usage: " << argv[0] << "  Filename(.obj) [options]" << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << " itercount N       iteration limit" << std::endl;
    std::cout << " poscoeff D        initial coeff for positions " << std::endl;
    std::cout << " areacoeff D       initial coeff for areas " << std::endl;
    std::cout << " sl D              multiplying coeff for Laplacian" << std::endl;
    std::cout << " volumecoeff D     stop when volume is 1/Dx smaller than initial" << std::endl;
    std::cout << "Example:" << std::endl;
    std::cout << " " << argv[0] << " model.obj itercount 5 poscoeff 0.5 areacoeff 1 sl 2 volumecoeff 0.0001" << std::endl;

    std::cout << std::endl;
    std::cout << "Program will now proceed using default values." << std::endl;
  }

  // parsing command line
  if (argc >= 2)
    settings.inputFilename = argv[1];

  for (int i = 2; i < argc; i += 2)
  {
    if (!strcmp(argv[i], "itercount"))
      settings.maxitercount = strtol(argv[i + 1], NULL, 10);
    else if (!strcmp(argv[i], "poscoeff"))
      settings.poscoeff = strtod(argv[i + 1], NULL);
    else if (!strcmp(argv[i], "areacoeff"))
      settings.areacoef = strtod(argv[i + 1], NULL);
    else if (!strcmp(argv[i], "sl"))
      settings.sL = strtod(argv[i + 1], NULL);
    else if (!strcmp(argv[i], "volumecoeff"))
      settings.volumeCoef = strtod(argv[i + 1], NULL);
    else if (!strcmp(argv[i], "keepfaces"))
      settings.keep_faces_perces = strtod(argv[i + 1], NULL);
    else
    {
      std::cout << "Unknown option " << argv[i] << std::endl;
      return false;
    }
  }

  return true;
}

// --------------------------------------------------------------------
bool LoadModelAndComputeSkeleton(CSkinnedMesh* skinnedMesh, CSkeleton* computedSkeleton)
{
  std::chrono::high_resolution_clock::time_point timeStart = std::chrono::high_resolution_clock::now();

  vtkSmartPointer<vtkOBJReader> reader = vtkSmartPointer<vtkOBJReader>::New();
  reader->SetFileName(settings.inputFilename.c_str());

  // Filter #1: Laplacian transformation
  laplaceTransform = vtkSmartPointer<MeshLaplaceFilter>::New();
  laplaceTransform->SetConstants(settings.maxitercount, settings.sL, settings.areacoef, settings.volumeCoef, settings.poscoeff);
  laplaceTransform->SetInputConnection(reader->GetOutputPort());

  // Filter #2: Edge collapse
  edgeCollapse = vtkSmartPointer<EdgeCollapseFilter>::New();
  edgeCollapse->originalVertexes = &laplaceTransform->originalVertexes;
  edgeCollapse->SetInputConnection(laplaceTransform->GetOutputPort());

  edgeCollapse->Update();

  std::chrono::high_resolution_clock::time_point timeAfterCollapse = std::chrono::high_resolution_clock::now();

  double generatedSkeletonCenterX, generatedSkeletonCenterY, generatedSkeletonCenterZ;
  edgeCollapse->GetSkeleton().boundbox.GetCenter(generatedSkeletonCenterX, generatedSkeletonCenterY, generatedSkeletonCenterZ);
  skeletonDefault.Rescale(
    settings.templateSkeletonScalingFactor * edgeCollapse->GetSkeleton().longestPath / skeletonDefault.longestPath,
    generatedSkeletonCenterX, generatedSkeletonCenterY, generatedSkeletonCenterZ, false);
  *computedSkeleton = CSkeleton::Match(edgeCollapse->GetSkeleton(), skeletonDefault);
  computedSkeleton->PrepareBones();

  std::chrono::high_resolution_clock::time_point timeAfterSkeletonMatch = std::chrono::high_resolution_clock::now();

  skinnedMesh->SetSkeletonRestPose(*computedSkeleton);
  skinnedMesh->SetSkeletonCurrPose(*computedSkeleton);
  skinnedMesh->SetMeshData(reader->GetOutput());
  //skinnedMesh->SetMeshData(laplaceTransform->GetOutput());
  skinnedMesh->ComputeWeights(laplaceTransform->GetOutput());
  //skinnedMesh->ComputeWeights(reader->GetOutput());

  std::chrono::high_resolution_clock::time_point timeAfterComputeWeights = std::chrono::high_resolution_clock::now();

  std::chrono::duration<double> durationOfGeneration(timeAfterCollapse - timeStart);
  std::chrono::duration<double> durationOfMatching(timeAfterSkeletonMatch - timeAfterCollapse);
  std::chrono::duration<double> durationOfWeightAssignment(timeAfterComputeWeights - timeAfterSkeletonMatch);
  std::chrono::duration<double> durationTotal(timeAfterComputeWeights - timeStart);

  std::cout << "\n--------------------------------------------\nMesh attributes:\n";
  std::cout << "vertex count: " << skinnedMesh->GetVertexCount() << ", triangle count: " << skinnedMesh->GetTriangleCount() << "\n";
  std::cout << "--------------------------------------------\nComputation times:\n";
  std::cout << "Skeleton generation: " << 1000 * durationOfGeneration.count() << " ms\n";
  std::cout << "Matching template skeleton: " << 1000 * durationOfMatching.count() << " ms\n";
  std::cout << "Computation of skinning weights: " << 1000 * durationOfWeightAssignment.count() << " ms\n";
  std::cout << "--------------------------------------------\n";
  std::cout << "Total time: " << 1000 * durationTotal.count() << " ms\n";
  std::cout << "--------------------------------------------\n";

  return true;
}

/// Load default skeleton (skeleton template).
bool LoadDefaultSkeleton()
{
  if (!skeletonDefault.Load(skeletonDefaultAddr))
    return false;

  skeletonDefault.ComputeDistanceMatrix();
  skeletonDefault.FindHeadAndReorder();
  return true;
}

/// Prepare 3D scene (construct necessary objects).
bool PrepareScene()
{
  // actor - mesh
  vtkSmartPointer<vtkPolyDataMapper> mapperMesh = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperMesh->SetInputConnection(skinnedMeshSource->GetOutputPort());

  skinnedMeshActor = vtkSmartPointer<vtkActor>::New();
  skinnedMeshActor->SetMapper(mapperMesh);
  skinnedMeshActor->GetProperty()->SetOpacity(0.55);
  //skinnedMeshActor->GetProperty()->BackfaceCullingOn();
  //skinnedMeshActor->GetProperty()->LightingOff();
  skinnedMeshActor->PickableOff();

  // renderer, renderer window
  renderer = vtkSmartPointer<vtkRenderer>::New();
  renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);
  vtkSmartPointer<MouseInteractorPicker> style = vtkSmartPointer<MouseInteractorPicker>::New();
  style->SetDefaultRenderer(renderer);
  renderWindowInteractor->SetInteractorStyle(style);
  
  renderer->SetBackground(1, 1, 1);
  renderer->AddActor(skinnedMeshActor);

  // actor - skeleton
  skeletonVisualization.Refresh(renderer, skeletonMesh);

  sphereSource = vtkSphereSource::New();
  sphereSource->SetThetaResolution(16);
  sphereSource->SetPhiResolution(16);
  sphereSource->SetRadius(0.01 * skeletonDefault.longestPath);

  vtkSmartPointer<vtkPolyDataMapper> sphereMapper = vtkPolyDataMapper::New();
  sphereMapper->SetInputConnection(sphereSource->GetOutputPort());

  for (CSkeletonJoint& node : skeletonMesh.nodes)
  {
    if (node.links.size() == 1)
    {
      vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
      actor->SetMapper(sphereMapper);
      actor->SetPosition(node.x, node.y, node.z);
      actor->GetProperty()->BackfaceCullingOn();
      actor->GetProperty()->SetColor(0, 0, 1);
      TLeafNode leafNode;
      leafNode.actor = actor;
      leafNode.nodeIndex = node.index;
      leafNodes.push_back(leafNode);
      renderer->AddActor(actor);
    }
  }

  skinnedMeshSource->SetSkeletonRestPose(skeletonMeshRest);
  skinnedMeshSource->SetSkeletonCurrPose(skeletonMesh);
  skinnedMeshSource->ComputeWeights(laplaceTransform->GetOutput());

  vtkCallbackCommand *keypressCallback = vtkCallbackCommand::New();
  keypressCallback->SetCallback(&KeypressCallbackFunction);
  renderWindowInteractor->AddObserver(vtkCommand::KeyPressEvent, keypressCallback);
  
  renderWindow->SetSize(1024, 700);
  renderWindow->SetWindowName("Skeleton generator");
  renderWindow->Render();

  vtkSmartPointer<vtkPolyDataMapper> mapperLaplaceMesh = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperLaplaceMesh->SetInputConnection(laplaceTransform->GetOutputPort());
  laplaceTransformActor = vtkSmartPointer<vtkActor>::New();
  laplaceTransformActor->SetMapper(mapperLaplaceMesh);
  laplaceTransformActor->GetProperty()->SetOpacity(1);
  laplaceTransformActor->GetProperty()->LightingOff();
  laplaceTransformActor->GetProperty()->SetColor(0, 1, 0);
  laplaceTransformActor->SetVisibility(false);
  laplaceTransformActor->PickableOff();
  renderer->AddActor(laplaceTransformActor);

  // Setup the text and add it to the window
  vtkSmartPointer<vtkTextActor> textActor = vtkSmartPointer<vtkTextActor>::New();
  textActor->SetInput("Stisk klaves zobrazuje/skryva: (1) kostru, (2) lokalni trojhrany, (4) sit, (5) segmentaci site, (6) smrstenou sit, (7) povolit pruzne kosti");
  textActor->GetTextProperty()->SetFontSize(14);
  textActor->GetTextProperty()->SetColor(0.0, 0.0, 0.0);
  textActor->SetDisplayPosition(10, 10);
  renderer->AddActor2D(textActor);

  return true;
}

/// Callback capturing key presses.
void KeypressCallbackFunction(vtkObject* caller, long unsigned int vtkNotUsed(eventId), void* clientData, void* vtkNotUsed(callData))
{
  int keyPressed = ((vtkRenderWindowInteractor*)caller)->GetKeyCode();

  //osetreni klavesy
  switch (keyPressed)
  {
  case 27/*escape*/:
    ((vtkRenderWindowInteractor*)caller)->ExitCallback();
    return;
  case ' '/*space*/:
    skeletonMesh = skeletonMeshRest;

    for (auto& leafNode : leafNodes)
    {
      const auto& node = skeletonMesh.nodes[leafNode.nodeIndex];
      leafNode.actor->SetPosition(node.x, node.y, node.z);
    }

    skinnedMeshSource->SetSkeletonCurrPose(skeletonMesh);
    skinnedMeshSource->Modified();
    skinnedMeshSource->Update();

    skeletonVisualization.Refresh(renderer, skeletonMesh);
    renderWindow->Render();
    return;
  case '1':
    skeletonVisualization.skeletonVisible = !skeletonVisualization.skeletonVisible;
    skeletonVisualization.Refresh(renderer, skeletonMesh);
    renderWindow->Render();
    return;
  case '2':
    skeletonVisualization.axesVisible = !skeletonVisualization.axesVisible;
    skeletonVisualization.Refresh(renderer, skeletonMesh);
    renderWindow->Render();
    return;
  case '4':
    skinnedMeshActor->SetVisibility(!skinnedMeshActor->GetVisibility());
    renderWindow->Render();
    return;
  case '5':
    skinnedMeshSource->ColorizeSegmentation = !skinnedMeshSource->ColorizeSegmentation;
    skinnedMeshSource->Modified();
    skinnedMeshSource->Update();
    renderWindow->Render();
    return;
  case '6':
    laplaceTransformActor->SetVisibility(!laplaceTransformActor->GetVisibility());
    renderWindow->Render();
    return;
  case '7':
    skeletonMesh = skeletonMeshRest;
    settings.ik_allowStretching = !settings.ik_allowStretching;
    for (auto& leaf : leafNodes)
      ProcessIK(leaf.actor, settings.ik_allowStretching);
    renderWindow->Render();
    return;
  case '8':
    for (auto& leafNode : leafNodes)
    {
      const auto& node = skeletonMesh.nodes[leafNode.nodeIndex];
      leafNode.actor->SetVisibility(!leafNode.actor->GetVisibility());
      skinnedMeshActor->GetProperty()->SetOpacity(leafNode.actor->GetVisibility() ? 0.55 : 1.0);
    }
  default:;
  }

  renderWindow->Render();
}

/// Function that rotates bone around axis specified by index "axisIndex" to minimize distance of end effector from desired position.
bool FindAndApplyBestRotation(Eigen::Matrix4d& currentTransform, Eigen::Matrix4d& deltaTransformOut, int axisIndex,
  double constraintLow, double constraintHigh, Eigen::Vector4d& endEffector, const Eigen::Vector4d& desiredPos, double angleMultiplier,
  double& currentAxisAngle)
{
  const Eigen::Vector3d endEffector3(endEffector.x(), endEffector.y(), endEffector.z());
  const Eigen::Vector3d desiredPos3(desiredPos.x(), desiredPos.y(), desiredPos.z());

  // construct direction of rotation axis
  Eigen::Vector3d axis(currentTransform(0, axisIndex), currentTransform(1, axisIndex), currentTransform(2, axisIndex));
  axis.normalize();
  // find center of rotation
  Eigen::Vector3d center(currentTransform(0, 3), currentTransform(1, 3), currentTransform(2, 3));
  // construct rotational plane
  Eigen::Hyperplane<double, 3> rotPlane(axis, center);

  // project important points on rotational plane
  Eigen::Vector3d endEffectorProjected(rotPlane.projection(endEffector3));
  Eigen::Vector3d desiredPosProjected(rotPlane.projection(desiredPos3));

  // construct vectors from center
  Eigen::Vector3d A = endEffectorProjected - center;
  Eigen::Vector3d B = desiredPosProjected - center;
  double aNorm = A.norm();
  if (aNorm < DBL_EPSILON)
    return false;
  A /= aNorm;

  double bNorm = B.norm();
  if (bNorm < DBL_EPSILON)
    return false;
  B /= bNorm;
  
  // compute rotation angle
  double angle = acos(clamp(A.dot(B), -1.0, 1.0));
  if (rotPlane.signedDistance(center + A.cross(B)) < 0)
    angle = -angle;
  else if (rotPlane.signedDistance(center + A.cross(B)) > 0)
    angle = angle;

  angle *= angleMultiplier;

  // apply constraints
  if (currentAxisAngle + angle < constraintLow)
    //return false;
    angle = constraintLow - currentAxisAngle;
  if (currentAxisAngle + angle > constraintHigh)
    //return false;
    angle = constraintHigh - currentAxisAngle;

  // compute final matrix 
  Eigen::Matrix3d rotation3(Eigen::AngleAxisd(angle, axis).matrix());
  Eigen::Matrix4d rotation(Eigen::Matrix4d::Identity());
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      rotation(i, j) = rotation3(i, j);

  Eigen::Matrix4d translateToOrigin(Eigen::Matrix4d::Identity());
  translateToOrigin(0, 3) = -center.x();
  translateToOrigin(1, 3) = -center.y();
  translateToOrigin(2, 3) = -center.z();
  Eigen::Matrix4d translateBack(Eigen::Matrix4d::Identity());
  translateBack(0, 3) = center.x();
  translateBack(1, 3) = center.y();
  translateBack(2, 3) = center.z();

  Eigen::Matrix4d deltaTransform = translateBack * rotation * translateToOrigin;

  // apply!
  currentAxisAngle += angle;
  currentTransform = deltaTransform * currentTransform;
  endEffector = deltaTransform * endEffector;
  deltaTransformOut = deltaTransform * deltaTransformOut;
  return true;
}

/// Function that stretches bone around axis specified by index "axisIndex" to minimize distance of end effector from desired position.
bool FindAndApplyBestStretch(Eigen::Matrix4d& currentTransform, Eigen::Matrix4d& deltaTransformOut, Eigen::Vector4d& endEffector, const Eigen::Vector4d& desiredPos, 
  double stretchMultiplier, double& currentStrech)
{
  const Eigen::Vector3d endEffector3(endEffector.x(), endEffector.y(), endEffector.z());
  const Eigen::Vector3d desiredPos3(desiredPos.x(), desiredPos.y(), desiredPos.z());
  // describe current bone
  Eigen::Vector3d boneOrigin(currentTransform(0, 3), currentTransform(1, 3), currentTransform(2, 3));
  Eigen::Vector3d boneDir(currentTransform(0, 1), currentTransform(1, 1), currentTransform(2, 1));
  boneDir.normalize();
  double boneScale = currentStrech;

  // project important points on bone line segment
  double endEffectorProjectedDistFromOrigin = (endEffector3 - boneOrigin).dot(boneDir);
  double desiredPosProjectedDistFromOrigin = (desiredPos3 - boneOrigin).dot(boneDir);
  double translationChange = (desiredPosProjectedDistFromOrigin - endEffectorProjectedDistFromOrigin) * stretchMultiplier;

  double scaling = (translationChange + boneScale) / boneScale;
  if (scaling * boneScale < 1)
    return false;

  Eigen::Matrix4d deltaTranslation(Eigen::Matrix4d::Identity());
  deltaTranslation(0, 3) = translationChange * boneDir.x();
  deltaTranslation(1, 3) = translationChange * boneDir.y();
  deltaTranslation(2, 3) = translationChange * boneDir.z();
  currentTransform = deltaTranslation * currentTransform;
  deltaTransformOut = deltaTranslation * deltaTransformOut;

  currentStrech *= scaling;
  return true;
}

/// Process inverse kinematics.
void ProcessIK(vtkActor* updatedEffector, bool allowStretching)
{
#ifdef SG_QPC_PRESENT
  LARGE_INTEGER qpcFreq, qpcStart, qpcEnd;
  QueryPerformanceFrequency(&qpcFreq);
  QueryPerformanceCounter(&qpcStart);
#endif
  std::cout << "Inverse kinematics" << (allowStretching ? " with streching bones" : "") << ".\n";
  std::chrono::high_resolution_clock::time_point timeStartIK = std::chrono::high_resolution_clock::now();

  const TLeafNode* pLeafNode = NULL;
  for (const TLeafNode& leafNode : leafNodes)
  {
    if (leafNode.actor == updatedEffector)
    {
      pLeafNode = &leafNode;
      break;
    }
  }
  if (!pLeafNode)
    return;

  double reachPos[3];
  pLeafNode->actor->GetPosition(reachPos);

  CSkeletonJoint& endJoint = skeletonMesh.nodes[pLeafNode->nodeIndex];
  const Eigen::Vector4d desiredPosWorld(reachPos[0], reachPos[1], reachPos[2], 1);

  // find bone
  size_t boneIndexEndEffector = -1;
  for (size_t i = 0; i < skeletonMesh.bones.size(); i++)
  {
    if (skeletonMesh.bones[i].nodeTail == pLeafNode->nodeIndex)
    {
      boneIndexEndEffector = i;
      break;
    }
  }

  // collect bone chain (sorted from end effector to root bone)
  std::vector<size_t> boneChainIndices;
  size_t currBoneIndex = boneIndexEndEffector;
  while (currBoneIndex != -1)
  {
    int tailCheck = skeletonMesh.bones[currBoneIndex].nodeTail;
    if (skeletonMesh.nodes[tailCheck].links.size() > 2)
      break;
    boneChainIndices.push_back(currBoneIndex);
    currBoneIndex = skeletonMesh.bones[currBoneIndex].parentBone;
  }

  if (boneChainIndices.size() == 0)
  {
    std::cout << "Cannot control any bones..." << std::endl;
    return;
  }

  // position of end effector
  const int maxitercount = settings.ik_maxitercount;
  const double mindist = skeletonMesh.longestPath * 0.005;
  for (int i = 0; i < maxitercount; i++)
  {
    const Eigen::Vector4d endJointPosWorld(endJoint.x, endJoint.y, endJoint.z, 1);
    if ((endJointPosWorld - desiredPosWorld).squaredNorm() < mindist * mindist)
      break;

    double angleMultiplier = 0.5;
    double stretchMultiplier = (0.5 - (double)i / maxitercount) * (0.5 - (double)i / maxitercount); // = 0.25 * i / maxitercount;

    for (size_t boneDepthI = 0; boneDepthI < boneChainIndices.size() - allowStretching; boneDepthI++)
    {
      // shortcuts
      const int currentBoneIndex = boneChainIndices[boneDepthI];
      CSkeletonBone& currentBone = skeletonMesh.bones[currentBoneIndex];
      const CSkeletonJoint& currentBoneTailJoint = skeletonMesh.nodes[currentBone.nodeTail];
      // local variables
      Eigen::Vector4d endJointPos(endJoint.x, endJoint.y, endJoint.z, 1); // position of end effector - must be here again! we need to actualize position of end effector each loop
      Eigen::Matrix4d matrixBoneCurrent = currentBone.worldTransform; // current state of bone
      Eigen::Matrix4d deltaTransformationMatrix(Eigen::Matrix4d::Identity()); // change of current state
      // rotating bone
      if (!FindAndApplyBestRotation(matrixBoneCurrent, deltaTransformationMatrix, 2, currentBoneTailJoint.constraint_Parent.minz, currentBoneTailJoint.constraint_Parent.maxz,
        endJointPos, desiredPosWorld, angleMultiplier, currentBone.axisAngles.z()))
        continue;
      if (!FindAndApplyBestRotation(matrixBoneCurrent, deltaTransformationMatrix, 1, currentBoneTailJoint.constraint_Parent.miny, currentBoneTailJoint.constraint_Parent.maxy,
        endJointPos, desiredPosWorld, angleMultiplier, currentBone.axisAngles.y()))
        continue;
      if (!FindAndApplyBestRotation(matrixBoneCurrent, deltaTransformationMatrix, 0, currentBoneTailJoint.constraint_Parent.minx, currentBoneTailJoint.constraint_Parent.maxx,
        endJointPos, desiredPosWorld, angleMultiplier, currentBone.axisAngles.x()))
        continue;
      // stretching bone
      if (allowStretching)
      {
        FindAndApplyBestStretch(matrixBoneCurrent, deltaTransformationMatrix, endJointPos, desiredPosWorld, stretchMultiplier, currentBone.stretch);
      }
      // update bones
      for (int boneUpdateI = boneDepthI; boneUpdateI >= 0; boneUpdateI--)
      {
        CSkeletonBone& updatingBone = skeletonMesh.bones[boneChainIndices[boneUpdateI]];
        CSkeletonJoint& updatingJoint = skeletonMesh.nodes[updatingBone.nodeTail];

        updatingBone.worldTransform = deltaTransformationMatrix * updatingBone.worldTransform;

        updatingJoint.matrixWorld_Parent = updatingBone.worldTransform;
        Eigen::Vector4d temporary(updatingJoint.x, updatingJoint.y, updatingJoint.z, 1);
        temporary = (deltaTransformationMatrix)* temporary;
        updatingJoint.x = temporary.x();
        updatingJoint.y = temporary.y();
        updatingJoint.z = temporary.z();
      }
    } // for loop: bone traversal
  } // for loop: iterations

  std::chrono::high_resolution_clock::time_point timeFinishedIK = std::chrono::high_resolution_clock::now();

  std::chrono::duration<double> durationOfIK(timeFinishedIK - timeStartIK);

  std::cout << "IK finished after " << (1000 * durationOfIK.count()) << " ms.\n";
#ifdef SG_QPC_PRESENT
  QueryPerformanceCounter(&qpcEnd);
  std::cout << "...more precisely: " << 1000ULL * 1000ULL * (qpcEnd.QuadPart - qpcStart.QuadPart) / qpcFreq.QuadPart << "microsecs." << std::endl;
#endif

  skinnedMeshSource->SetSkeletonRestPose(skeletonMeshRest);
  skinnedMeshSource->SetSkeletonCurrPose(skeletonMesh);
  skinnedMeshSource->Modified();
  skinnedMeshSource->Update();

  skeletonVisualization.Refresh(renderer, skeletonMesh);
}