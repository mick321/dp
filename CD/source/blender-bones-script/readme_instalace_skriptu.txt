Instalace exportovaciho skriptu kosti:

1) Zkopirujte soubor io_export_bones.py do adresare <installpath>\Blender\2.72\scripts\addons,
   kde <installpath> predstavuje umisteni instalace aplikace Blender. Cislo verze (2.72) se lisi
   podle Vami instalovane verze, skript vsak byl pro verzi 2.72 vyvijen a nemusi na jine fungovat.

2) V nastaveni rozsireni Blenderu (User Preferences > Addons) povolte rozsireni "Import-Export: Export Bones To TXT".

3) Restartujte Blender.

4) Export probiha standardni cestou - File > Export > Export Bones To TXT.