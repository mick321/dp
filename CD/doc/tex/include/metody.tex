% ------------------
\chapter{Metody extrakce kostry}
\label{sec:met}
% ------------------

\vspace{-0.1cm}
K~automatické extrakci kostry vznikla řada metod, které jsou založeny na různých přístupech.
Práce se zabývá kostrami složenými ze křivek \textit{(curve skeletons)}, které lze definovat
jako jednorozměrnou strukturu zjednodušující původní objekt, přičemž zachovávají
topologickou a~geometrickou informaci.

\textit{Kostra} se skládá z~dílčích křivek nazývaných \textit{kosti}, zpravidla uspořádaných do hierarchie.
Některé metody produkují kostru složenou pouze z~úseček. Při cílovém užití vzniklé kostry
ji obvykle rozšíříme do podoby parametrizovaného Frenetova trojhranu (kvůli jednoznačnému určení
lokální soustavy souřadnic).

\textit{Objekt} bývá obvykle reprezentován povrchově pomocí trojúhelníkové sítě, které kromě pozic 
vrcholů a~jejich spojení v~trojúhelníky obsahují informaci o~sousednostech. V~tomto případě
nastupují geometrické a~topologické metody tvorby kostry, v~případě objemové (voxelové) reprezentace
metody pracující v~diskrétním prostoru.

\vspace{-0.2cm}
\section{Medial axis transform}
\vspace{-0.2cm}

Harry Blum v~roce 1967 \cite{Blum1967} prvně formalizoval střední osu objektu. Z~jeho definice
v~podstatě všechny novější postupy vycházejí, nicméně přidávají další kritéria a~upouštějí
od přesného vyjádření.

\vspace{-0.1cm}
\begin{figure}[!htp]
\centering
\includegraphics[width=6.8cm]{img/ma-example.pdf}
\caption{Ukázka střední osy dvourozměrného objektu (vyznačena přerušovaně).}
\label{fig:ma-example}
\end{figure}

Střední osa objektu \textit{(medial axis)} je definována jako množina bodů $Q$ majících stejnou 
vzdálenost k~alespoň dvěma bodům hranice objektu, aniž by se jiný bod náležící hranici nacházel
blíže k~$Q$. 

Formálně lze definici zapsat matematickou formulací:
\begin{align}
\label{eq:ma-formulace}
\forall X \in E^3: X \in \text{MA}(O) \; \iff \; \exists {A_1, A_2} &\in \text{surface}(O): \| A_1 X \| = \| A_2 X \|  \\ 
           \wedge \; \nexists B &\in \text{surface}(O): \| B X \| < \| A_1 X \|
\end{align}

\noindent
kde $O$ značí vstupní objekt, $\text{MA}(O)$ formálně střední osu objektu $O$ a~$\text{surface}(O)$ povrch objektu. 
Příklad střední osy objektu je ukázán na obrázku \ref{fig:ma-example}.

Blum ve svém článku použil definici pomocí propagace události v~čase: Na počátku vznikne na hranici
objektu událost, která se dál homogenně šíří prostorem. Pro příklad si můžeme objekt představit jako louku
a~událost jako šířící se požár založený na její hranici. Daným místem prostoru může událost projít pouze jednou,
při použití naší analogie lze říct, že požár nepokračuje zpět na spáleniště. Bod, ve~kterém se v~jednom čase 
setká více událostí pocházejících z~odlišných zdrojů (jiných bodů hranice), tvoří střední osu objektu.
Proces je znázorněn na obrázku \ref{fig:ma-casove}.

\begin{figure}[!htp]
\centering
\includegraphics[width=12cm]{img/ma-casove.pdf}
\caption{Medial axis transform, ilustrace propagace události v~čase. Vlevo se nachází původní objekt, vpravo objekt s~výslednou střední osou.
         V~místech nespojitosti vzniká střední osa objektu.}
\label{fig:ma-casove}
\end{figure}

V~souvislosti se střední osou objektu se hovoří o~tzv.~\textit{medial axis transform}. Jedná se o~transformaci, při které
povrchem reprezentovaný objekt $O$ nahradíme množinou maximálních prázdných hyperkoulí $S_i$ tak, že:
\begin{align}
\bigcup_{\forall i} S_i \; = \; \bar O
\end{align}

\newpage
Hyperkoule se mohou vzájemně protínat, povrch objektu $O$ však nesmí být obsažen uvnitř objemu libovolné hyperkoule,
pouze na jejím povrchu (pro středy hyperkoulí platí formulace \ref{eq:ma-formulace}). Tato transformace plně zachovává původní tvar objektu, 
spolu se střední osou musíme však uchovat i~poloměr vzniklých hyperkoulí.

Sluší se poznamenat, že ačkoli disponujeme elegantní definicí střední osy, využití její surové
podoby není příliš žádoucí. Zaprvé, hledání střední osy je výpočetně náročný úkon, zadruhé, výsledná křivka
je~obtížně parametrizovatelná. Jako poslední bod zmiňme, že tvorba střední osy je vysoce citlivá na šum na povrchu objektu
-- sebedrobnější nuance tvoří nové větve střední osy.

Střední osu lze aproximovat pomocí po částech lineárním skeletem tvořeného vnitřními póly Voroného diagramů.
Techniku detailně rozebírá Amenta et al. \cite{Amenta00} za účelem rekonstrukce povrchu z~navzorkovaných bodů.
Mezi další aplikace střední osy patří například strukturní popis objektů.

% -----------------------------------------------------------------------
% -----------------------------------------------------------------------

\section{Metody založené na geometrii a~topologii}

V~tomto případě nijak neměníme charakter vstupních dat, během výpočtu využíváme vstupní trojúhelníkovou síť.
Díky tomu můžeme ušetřit výpočetní čas, který bychom museli věnovat případné konverzi reprezentace dat.

% -----------------------------------------------------------------------

\subsection{Smrštění modelu s~následnou redukcí geometrie}
\label{sec:met-au}
\label{subsec:met-au}

Řešení problému, které navrhuje Au et al. \cite{Au2008}, se sestává z~několika fází.
Nejprve iterativně smršťujeme trojúhelníkovou síť, po dosažení určeného objemu přichází
na řadu redukce geometrie, přičemž vzniká jednorozměrná kostra objektu. Na závěr 
provedeme úpravu pozic vrcholů kostry.

\subsubsection{Smrštění sítě}

Během smršťování sítě dochází k~vyhlazování detailů a~posunu vrcholů proti směru vypočtené normály.
Řešíme přeurčenou soustavu rovnic \ref{eq:me-smrsteni} pro neznámý vektor pozic $V'$, po jeho nalezení
přiřadíme $V \leftarrow V'$ a~výpočet opakujeme, dokud není dosažena zastavující podmínka.

\begin{equation}
\label{eq:me-smrsteni}
\begin{bmatrix} \mathbf W_L \mathbf L \\ \mathbf W_H \end{bmatrix} \mathbf V' 
\;=\;
\begin{bmatrix} 0 \\ \mathbf W_H V \end{bmatrix}
\end{equation}

Popišme si význam jednotlivých členů soustavy. Vektor $\mathbf V$ značí všechny aktuální pozice vrcholů sítě,
$\mathbf V'$ pozice nové, hledané. Diagonální matice $\mathbf W_L$ (vynucující setrvání v~pozici) a~$\mathbf W_H$ 
(zesilující kontrakci) zanášejí do výpočtu potřebné váhy.
Matice $\mathbf L$ provádí diskrétní laplaceovské \uv{vyhlazování} proti směru normály a~je definována následovně:

\begin{equation}
\mathbf L_{ij} \;=\;
\left\{
\begin{array}{l l}
  \omega_{ij} = cotg \, \alpha_{ij}  + cotg \, \beta_{ij} & \quad \text{je-li } (i,j) \text{ hranou mezi vrcholy } v_i \text{ a } v_j \\
  \sum_{(i,k) \in E} -\omega_{ik} & \quad \text{je-li } i = j \\
  0 & \quad \text{v~jiném případě}
\end{array} 
\right.
\end{equation}

\noindent
přičemž úhly $\alpha_{ij}$ a~$\beta_{ij}$ jsou protilehlé úhly vůči hraně $(i,j)$. Jak již bylo řečeno, řešení soustavy 
musíme opakovat, jedna iterace k~řešení nestačí. Za zastavovací podmínku autoři článku volí dosažení $\epsilon$-násobku
původního objemu, případně překročení maximálního možného počtu kroků (řešení soustavy rovnic).

\begin{figure}[!htp]
\centering
\includegraphics[width=13.5cm]{img/me-smrsteni.png}
\caption{Smršťování modelu. Konektivita původní geometrie je zachována.}
\label{fig:me-smrsteni}
\end{figure}

\newpage
V~článku doporučují váhy na počátku nastavit na hodnoty $\mathbf W_H^{(0)} = 0$ a~$\mathbf W_L^{(0)} = 10^{-3} \sqrt{A}$.
Úprava vah probíhá po každém kroku:
\begin{equation}
\begin{array}{l}
\mathbf W_L^{(t+1)} \leftarrow s_L \mathbf W_L^{(t)} \text{, přičemž doporučená hodnota } s_L = 2 \\
\mathbf W_{H,i}^{(t+1)} \leftarrow W_{H,i}^{(t)} \sqrt{\dfrac{A_i^{(0)}}{A_i^{(t)}}} \text{, kde } A_i \text{ představuje plochu one-ring kolem vrcholu } i
\end{array}
\end{equation}

Průběh kontrakce sítě je znázorněn na obrázku \ref{fig:me-smrsteni}, pro názornost byly vynechány dílčí iterace.

\subsubsection{Redukce geometrie}

Smrštěná síť si zachovává svoji původní konektivitu, k~dosažení jednorozměrné kostry použijeme redukci 
geometrie pomocí \textit{half-edge collapse}. Tento postup lze popsat tak, že vybereme nejméně významnou
hranu $(i,j)$, kterou ze sítě odstraníme (viz obrázek \ref{fig:me-edgecollapse}). Vrchol $\mathbf v_i$ je poté ztotožněn s~vrcholem $\mathbf v_j$
a~dojde k~odstranění stěn incidujících s~hranou $(i,j)$. Hrany odebíráme opakovaně do dosažení zastavovací 
podmínky.

\begin{figure}[!htp]
\centering
\includegraphics[width=12cm]{img/me-edgecollapse.pdf}
\caption{Half-edge collapse. Vrchol $\mathbf v_i$ je ztotožněn s~$\mathbf v_j$, hrana $\mathbf e$ je odstraněna spolu s~incidujícími trojúhelníky.
 Slučování hran je popsáno v~obrázku pomocí číslování v~obrázku. }
\label{fig:me-edgecollapse}
\end{figure}

Ohodnocení hrany stanovíme jako vážený součet dílčích funkcí:
\begin{equation}
F(i,j) \; = \; w_a F_a(i,j) + w_b F_b(i,j) \quad \text{ doporučeno: } w_a = 1,\; w_b = 0.1
\end{equation}

Funkce $F_a(i,j)$ vyjadřuje míru chyby vzniklé odstraněním hrany $(i,j)$:

\begin{equation}
F_a(i,j) \; = \; F(\mathbf v_j, i) + F(\mathbf v_j, j)
\end{equation}

přičemž:
\begin{align}
F(\mathbf v_j, i) \; &= \; \mathbf v_{j}^T \sum_{(i,j) \in E} \; (\mathbf K_{ij}^T \mathbf K_{ij}) \mathbf v_j \\ 
\mathbf K_{ij}  \; &= \; 
\begin{bmatrix}
0 & -a_z & a_y & -b_x \\
a_z & 0 & -a_x & -b_y \\
-a_y & a_x & 0 & -b_z
\end{bmatrix}
\end{align}

\noindent
kde $\mathbf a$ je normalizovaným vektorem hrany $(i,j)$ a~$\mathbf b = \mathbf a \times \mathbf v_i$ a~$E$ představuje
množinu hran sítě (viz obrázek \ref{fig:me-vektorovysoucin}). Předpokládáme, že $\mathbf v_i$ neleží v~počátku soustavy souřadnic,
jinak vyjde vektor $\mathbf b$ nulový.

\begin{figure}[!htp]
\centering
\includegraphics[width=4.5cm]{img/me-vektorovysoucin.pdf}
\caption{Znázornění vektorového součinu $\mathbf b = \mathbf a \times \mathbf v_i$, bod~$\mathbf O$ představuje počátek soustavy souřadnic. Vektor $\mathbf b$ je kolmý k~$\mathbf a$~i~$\mathbf v_i$.}
\label{fig:me-vektorovysoucin}
\end{figure}

Funkce $F_b(i,j)$ pokutuje délku hrany $(i,j)$:

\begin{equation}
F_b(i,j) \; = \; \| \mathbf v_i -  \mathbf v_j \| \sum_{(i,k) \in E} \; \| \mathbf v_i -  \mathbf v_k \|
\end{equation}

Během redukce udržujeme vazbu redukované geometrie na původní objekt. Pro počáteční stav je triviální jej nastavit,
v~každém vrcholu $\mathbf v_i$ vytvoříme seznam obsahující právě tentýž vrchol ($\mathbf v_i$). Při redukci hrany $\mathbf v_i \mathbf v_j$
seznamy vrcholů sloučíme a~uložíme do $\mathbf v_j$. Takto postupně tvoříme mapování, které přiřazuje vrcholu kostry
množinu příslušných vrcholů původní geometrie.

\subsubsection{Závěrečná úprava pozic}
\vspace{-0.1cm}

Z~předchozího kroku jsme získali podmnožinu vrcholů reprezentující kostru objektu, jejíž pozice však musíme
dodatečně upravit. Vrcholy kostry se totiž nemusejí nutně nacházet uvnitř modelu. Proto dochází k~záverečné úpravě, jež
přesune vrcholy kostry směrem doprostřed objemu tvořeného sítí. Využijeme mapování kostry na síť, které vzniklo
v~předchozím kroku. 
\vspace{-0.1cm}

K~vrcholu kostry $\mathbf u$ s~pomocí mapování získáme množinu příslušných vrcholů $\mathbf v_i$, která je ohraničena $N$ hranicemi $\xi_k$. 
Korekce pozice vrcholu kostry $\mathbf u$ je dána jako:
\vspace{-0.1cm}
\begin{align}
\mathbf u \; =& \; \mathbf u - \sum_k^N \dfrac{\mathbf d_k}{N} \\
\mathbf d_k \; = & \; \dfrac{ \sum_{i \in \xi_k} l_{k,i} (\mathbf v_i' - \mathbf v_i) }{ \sum_{i \in \xi_k} l_{k,i} }
\end{align}

\vspace{-0.1cm}
\noindent 
kde $\mathbf d_k$ představuje dílčí příspěvek daný vrcholy hranice $\xi_k$, $\mathbf v_i'$ pozici vrcholu po smrštění sítě
a~$l_{k,i}$ součet délek hran incidujících s~vrcholem $\mathbf v_i$, které musí zároveň patřit do hranice $\xi_k$.

\vspace{-0.1cm}
Autoři přiznávají, že ne pro každou síť závěrečná úprava pozic skutečně přemístí vrcholy kostry dovnitř modelu.
V~článku nezmiňují, o~kolik je zvolený postup lepší než použití obyčejného centroidu.

\subsubsection{Klady a~zápory metody}
\vspace{-0.1cm}

Na metodě oceníme zejména robustnost vůči šumu, neboť během kontrakce sítě pomocí diskrétního laplaciánu dochází
k~jejímu vyhlazování. Tvorba kostry je netečná vůči rotaci objektu (jako jeden z~důvodů uveďme, že pracujeme s~původní
sítí, ne s~diskretizovanou reprezentací v~podobě voxelové mřížky).

Citelnou nevýhodou je výpočetní náročnost, ostatně při výpočtu opakovaně řešíme rozsáhlou přeurčenou soustavu rovnic.
Právě tento krok způsobuje, že výsledná výpočetní algoritmická složitost je $O(I_C \,N^3)$, kde $N$ značí počet vrcholů
a~$I_C$ počet iterací. Nastavitelné parametry se musí volit ručně dle sítě, která se nachází na vstupu -- článek nepopisuje vhodnou automatickou volbu,
doporučuje \uv{osvědčené} hodnoty.

% -----------------------------------------------------------------------

\subsection{Tvorba kostry spojováním významných bodů}
\label{subsec:met-ma14}

Článek od Jaehwan Ma a~Sunghee Choi \cite{Ma2014} se zaobírá tvorbou kostry přímo pro animační účely, při generování
vznikají záměrně dlouhé kosti, nikoliv sada drobných (na rozdíl od metody od Au~et~al. popsané v~kapitole~\label{subsec:met-au}), 
které bychom museli dodatečně na závěr spojovat. 

Shrňme si nejdříve princip v~několika větách. Nejprve jsou nalezeny význačné vrcholy sítě, říkejme jim kandidáti na listy kostry. 
Mezi nimi se vyskytují i~takové body, které vznikly kvůli šumu na povrchu sítě, proto dochází k~filtrování kandidátů.
Poté započne generování kostí směrem od listů k~prozatím neznámemu kořenu a~jejich spojování v~kostru. Následují závěrečné úpravy: přidání dodatečných vnitřních bodů kostry
a~umístění kořene kostry.

\subsubsection{Předzpracování dat}

Autoři použili svoji vlastní metodu hledání aproximace střední osy objektu (Ma et.~al, 2012 \cite{Ma2012}) k~nalezení
maximální tečných koulí \textit{(tangent balls)} vůči trojúhelníkové síti ve všech jeho vrcholech (viz obrázek \ref{fig:me-tangentballs}). Velikost tečné koule
příslušící vybranému vrcholu je použita ve výpočtu později.

\begin{figure}[!htp]
\centering
\includegraphics[width=10cm]{img/me-tangentballs.pdf}
\caption{Ukázka tečných koulí $B_i$ na části sítě (vrcholech $\mathbf v_i$), $m$ značí odhad střední osy. Střed koule leží na v~příslušném vrcholu střední osy 
(který odpovídá pólu Voronoi diagramu), poloměr je volen tak, aby vrchol $\mathbf v_i$ ležel na jejím povrchu. Pro názornost znázorněno ve dvourozměrné analogii. }
\label{fig:me-tangentballs}
\end{figure}

\subsubsection{Hledání význačných vrcholů}

Význačné vrcholy jsou chápány jako lokálně nejvzdálenější vrcholy vzhledem k~ostatním v~síti. Intiutivně bychom je mohli popsat jako
\uv{zakončení modelu}. Pohybovat se můžeme pouze po hranách sítě, tím pádem se jedná o~grafovou úlohu.

Nejprve zvolíme libovolný vrchol sítě $v_r$, ze kterého provedeme část známého Dijkstrova algoritmu pro hledání cesty 
(algoritmická složitost při použití binární haldy a~předpokladu rovinného grafu, ve kterém počet hran lineárně závisí na počtu vrcholů,
je $O(N log(N))$, kde $N$ značí počet vrcholů),
tj.~vzdálenostní ohodnocení vrcholů. Nejvýše ohodnocený $v_0$ se stává prvním kandidátem na~význačný vrchol. Jako další
krok opět spustíme Dijsktrův algoritmus, ale tentokrát začneme ve vrcholu $v_0$. Kandidáty se stávají všechny vrcholy, pro které platí, že jejich vzdálenostní ohodnocení je vyšší 
než ohodnocení jejich libovolného souseda.

Získané body se mohou nacházet mezi kandidáty například kvůli šumu na povrchu sítě. Vrchol odstraníme ze seznamu kandidátů
v~případě, že šumem způsobený výčnělek sítě, kterému vrchol náleží, je mělčí než zvolený práh. Pokud se v~jednom výčnělku nachází
více kandidátů, akceptujeme z~nich jen takový vrchol, kterému přísluší nejmenší tečná koule.

\subsubsection{Tvorba kostry}

Kostru tvoříme opakováním dvou kroků: odřezáváním a~slučováním větví vznikající kostry (viz níže). Dílčí kroky si ve stručnosti
představíme.

Od všech význačných vrcholů $v_i$ začneme pseudoparalelně tvořit větve grafu kostry. To provedeme tak, že z~každého vrcholu $v_j$ 
spustíme Dijkstrův algoritmus, čímž vypočteme ostatním vrcholům sítě vzdálenost od $v_j$. Poté uniformně navzorkujeme izočáry, 
které představují množinu bodů stejně vzdálených od $v_i$. V~metodě byl použit odhad, kdy místo přesných izočar byly použity
jejich odhady (vzorkování bylo provedeno na hranách sítě a~body stejné vzdálenosti byly propojeny do po částech lineární 
uzavřené křivky). Izočáry $c$ oindexujeme podle vzdálenosti od význačného vrcholu a~označíme je $c_j$. Pokud platí, že $|c_{j+1}| > |c_j|$, kde
$|c_j|$ značí počet samostatných izočar stejné úrovně, pak došlo k~topologickému rozdělení. V~těchto místech se navíc velmi rychle mění
charakteristika izočáry (střední průměr a~rozptyl).

Právě zde vytvoříme nový vrchol, který propojíme s~význačným. Příslušnou větev kostry tímto považujeme za dokončenou.
Může dojít ke sloučení s~jinou větví, je-li nalezena izočára s~podobnou charakteristikou (pro tento účel je~vytvořen nový vrchol).
V~opačném případě výpočet pokračuje v~generování izočár jiné větve modelu. Za zmínku stojí, že se v~průběhu algoritmu upřednostňují \uv{tenké} větve 
modelu (mající nejmenší střední průměr izočar) a~že velmi důležitou roli v~algoritmu hraje hustota vzorkování izočar
(příliš velká klade vyšší výpočetní nároky, příliš nízká znemožňuje správný chod algoritmu).

\vspace{-0.1cm}
\subsubsection{Závěrečné úpravy}
\vspace{-0.1cm}

Každá větev sítě obsahuje nyní pouze jednu kost, která se pne od význačného vrcholu ke spoji s~jinými kostmi (sourozenci a~nadřazený předek).
Do kostry vkládáme vrcholy střední osy objektu mající od ní největší odchylku. Tento proces opakujeme, dokud vkládání
způsobuje změny vyšší, než byl stanoven práh. 
\vspace{-0.1cm}

Posledním krokem je určení pozice kořene kostry. Veškeré dílčí větve jsou v~tento moment odřezány, zbyla nám množina
vrcholů (v~článku označené jako \textit{root region}), z~jejichž pozic vypočteme vážený centroid, přičemž jako váhy
použijeme poloměry příslušných tečných koulí.

\vspace{-0.1cm}
\subsubsection{Klady a~zápory metody}
\vspace{-0.1cm}

Algoritmus produkuje kostru, která je dostatečně jednoduchá, abychom ji mohli použít k~animaci objektu. Stejně jako
u~výše popsané metody od Au~et~al. (viz sekce \ref{sec:met-au}) je tvorba netečná vůči rotaci objektu či jeho póze.
Vzhledem k~tomu, jak je tvořena hierarchie kostí (od listů směrem ke kořeni), má výsledná kostra \uv{hvězdicovitou}
podobu, kosti se sbíhají do kořene. Sami autoři článku přiznávají, že to může bránit praktickému využití algoritmu 
pro generování koster humanoidů.

Podle autorů největší výpočetní zátěž tvoří právě stavba kostry (nikoliv prvotní hledání význačných bodů). Výpočetní 
složitost algoritmu závisí nejen na počtu vrcholů, ale i~na použité hustotě vzorkování a~topologické složitosti 
modelu (kde dochází k~více slučování dílčích větví). Vyčíslená složitost $O(q  \, c(i) \,N\, log E)$ odpovídá procházení 
rovinného grafu, kdy v~každém kroku $i$ srovnáváme měnící se počet izočar~$c$, $N$ značí počet vrcholů, $E$ počet hran,
$q$ hustotu vzorkování.

Za předpokladu, že délky hran jsou srovnatelné, můžeme zvolit parametr $q$ tak, abychom navzorkovali izočárou každou hranu
přibližně jednou. Potom můžeme výpočetní složitost vyjádřit jako $O(E \,N\, log E)$. Hrany povrchu sítě však tvoří
rovinný graf, jejich počet je tedy lineárně závislý na počtu vrcholů $N$, což nám umožní závěrečné zjednodušení složitosti:
$O(N^2\, log N)$. Nezapomeňme však, že k~tomuto předpisu jsme dospěli po zmíněném zjednodušujícím předpokladu.

% -----------------------------------------------------------------------
% -----------------------------------------------------------------------

\section{Volumetrické metody}

Před použitím volumetrických metod dochází k~diskretizaci objektu: nahradíme původní objem voxely (obecným 
rozšířením pixelu pro prostor), které představují plný prostor. Vzniklý diskretizovaný objekt ztenčujeme
odebíráním hraničních voxelů. Pořadí jejich odebírání určuje prioritní funkce, která se v~různých metodách liší.
Stejně tak se rozchází definice hraničních voxelů ku příkladu z~důvodu použití rozdílné sousednosti (6-okolí apod.).
Jako příklad ztenčování uveďme metodu od Ma et al. \cite{Ma2002}. Nevýhodou metod založených na ztenčování
je nemožnost práce se surovými vstupními daty, nutnost volby hustoty mřížky a~vysoká citlivost na šum, 
z~vytvořené kostry musíme odstraňovat bezvýznamné větve.

\begin{figure}[!htp]
\centering
\includegraphics[width=7cm]{img/me-thinning.png}
\caption{Ukázka iterací ztenčování modelu reprezentovaného voxely. Zdroj: {\small \texttt{http://csvision.swan.ac.uk/index.php?n=Site.BloodFlowModelling}}, cit.~6.~2015.}
\label{fig:me-smrsteni}
\end{figure}

Obdobně nerobustní chování (podle Au et al. \cite{Au2008}) mají i~metody založené na distančním poli (\textit{distance field methods}), 
u~kterých každému voxelu přiřadíme hodnotu odpovídající vzdálenosti od hranice objektu. Lokální maxima
reprezentují kandidáty, jejichž spojením získáme aproximaci střední osy objektu.

Při porovnání s~geometrickými metodami, které byly rozebrány výše, trpí volumetrické metody několika nepřehlédnutelnými problémy:
\begin{enumerate}
\item Geometrická data vyžadují převod do voxelové reprezentace, nejsou-li již na vstupu v~této podobě.
\item Při konverzi musíme vhodně zvolit hustotu voxelové mřížky.
\item Voxelová reprezentace je zpravidla paměťově náročnější oproti geometrické.
\end{enumerate}

\subsection{Generování kostry snímaného člověka v~reálném čase}
\label{sec:met-straka}
\label{subsec:met-straka}

Straka et.~al \cite{Straka2011} snímali člověka několika kamerami a~v reálném čase byli schopni vypočítat jeho kostru.
Snímáním získali přibližnou volumetrickou reprezentaci osoby, ze které metodou \textit{volume scooping}~\cite{Rodriguez2009} 
vypočítali přibližnou podobu střed\-ní osy objektu. Na první pohled se může zdát, že jsme se ocitli v~oblasti 
strojového vidění, principy extrakce kostry však zůstávájí stejné a~můžeme je využít pro data libovolného původu.

Střední osu není možné použít přímo jako kostru objektu, neboť je zatížena nepřesnostmi -- oproti požadovanému stavu obsahuje nadbytečné kosti.
Algoritmu proto dodáme tzv.~vzorovou kostru $S$, podle níž se dočasně vypočtená kostra upraví. Vzor velmi zjednodušeně reprezentuje člověka, 
například celá paže včetně předloktí je reprezentována pouhými dvěma kostmi se společným kloubem v~lokti. 

Vzorová kostra $S$ i~dočasně vypočítaná kostra $X$ jsou obě vlastně grafem, pro který určíme distanční matici, která poslouží v~následujících 
výpočtech jako metrika pro výpočet chyby přiřazení dvojic vrcholů z~$X$~a~$S$. Následně se zaměříme na koncové 
vrcholy $E(X)$~a~$E(S)$ (listy grafu), přičemž chceme nalézt přiřazení:

\begin{equation}
f(E(S)) \rightarrow f(E(X))
\end{equation}

\noindent 
takové, že výsledná chyba je minimální. Autoři jej hledají kombinací \textit{dynamic time warping} 
(\textit{DTW}, použita kvůli možnosti rozdílného počtu koncových vrcholů) a~\textit{maďarské metody} (přiřazení dvojic 
s~nejmenší globální chybou). Obdobným způsobem jsou spárovány vnitřní klouby. Díky tomu můžeme vzorovou kostru vložit 
do skenovaného modelu a~provést už jen závěrečné úpravy pozic (viz původní článek).

Nesporná výhoda, která se v~metodě ukrývá, je znemožnění tvorby nesmyslné topologie kostry (přídavné končetiny apod.).
Algoritmus může selhat a~přiřadit k~sobě špatné dvojice kloubů z~koster $S$~a~$X$, požadovaná topologie je však vždy zachována.
Ačkoliv autoři svoji metodu použili pro volumetrická vstupní data, nic nebrání nasazení vstupu v~podobě trojúhelníkové sítě,
tj.~nahradit krok tvorby dočasné kostry (odhadu střední osy).

% -----------------------------------------------------------------------
% -----------------------------------------------------------------------

\section{Příbuzné oblasti}

Metoda od Yoshizawa~et~al.~\cite{Yoshizawa07} se zabývá generováním kostry v~poněkud jiné podobě než doposud zmíněné přístupy.
Kostru představuje odhad střední osy sítě vypočítaný pomocí pólů Voroného diagramu, které jsou spojeny topologicky stejně
jako původní trojúhelníková síť~\cite{Amenta00}. Původní síť byla v~podstatě zdeformována do podoby střední osy, k~žádné
redukci na jednorozměrný graf zde nedochází. Pro potlačení šumu doporučují autoři vstupní model nejprve zjednodušit.

\begin{figure}[!htp]
\centering
\includegraphics[width=12cm]{img/me-yoshizawa.png}
\caption{Ukázka výsledků metody Yoshizawa~et~al. Uvnitř modelu se nachází vygenerovaný skelet, který přenáší deformaci ručně vytvořené jednorozměrné kostry na povrch tělesa.}
\label{fig:me-smrsteni}
\end{figure}

\newpage
Po aplikaci libovolných \textit{free-form} deformací na vzniklou síť střední osy
můžeme zrekonstruovat původní objekt se zachováním lokální tloušťky, což vyplývá z~vlastností \textit{medial axis transform}.
Byla-li původní síť před transformací zjednodušena, dochází k~opětovnému zesložitění s~užitím diskétních diferenciálních souřadnic.

Skelet, kterým se řídí tvarování objektu, nevyužijeme přímo, neboť je složen ze stejného počtu vrcholů, který měla původní síť, jen se nacházející
blízko střední osy. Nedosáhli bychom tak žádného zjednodušení. Autoři proto používají ručně vytvořenou (jednorozměrnou) kostru, kterou řídí deformaci generovaného skeletu.



