% -------------------------
\chapter{Podrobný rozbor metody}
% -------------------------

V~kapitole \ref{ch:navrh} byl navržen postup generování kostry a~následná demonstrace výsledku pomocí interaktivní vizualizace. Nyní si rozebereme
dílčí kroky podrobněji. Popíšeme si algoritmy, jejich parametry a~charakter dat, se kterými se na dané úrovni pracuje. 

% -------------------------------------------------------
\section{Vstupní data}

Model humanoida, který načteme na počátku celého procesu, je reprezentován povrchově pomocí trojúhelníkové sítě. Navržená metoda tvorby kostry
požaduje, aby síť byla manifoldní (blízké okolí libovolného bodu náležícího síti je homeomorfní vůči euklidovskému prostoru $E^2$). Díky
tomu například můžeme užít jednodušší datové struktury či korektně spočítat objem uzavíraný sítí. Předpokládáme, že humanoid ve scéně stojí,
je rozpažený a~dívá se ve směru osy $z$.

\begin{figure}[!htp]
\centering
\includegraphics[width=5.5cm]{img/ro-kloub-rotace.pdf}
\caption{Znázornění rotací kosti podle os $x$, $y$, $z$. Použitý lokální systém je pravotočivý, směr osy $y$ je ztotožněn se směrem kosti.}
\label{fig:ro-kloub-rotace}
\end{figure}

Referenční kostra (označme si ji $S_R$) popisuje pozice jednotlivých kloubů a~jejich spojení kostmi. Dá se na ni pohlížet jako na graf umístěný v~prostoru, 
ve kterém klouby představují uzly a~kosti hrany. Tohoto pozorování využijeme během generování animační kostry -- je možné použít grafové algoritmy.
Podle výše uvedených parametrů jsme schopni kosti chápat také jako úsečky v~prostoru. Tím je dána orientace kosti pouze v~jednom směru,
proto ke každé kosti připojíme lokální souřadnicový systém, směr osy $y$ ztotožníme se směrem kosti.
Mimo to známe omezení její rotace vůči klidové póze kolem os lokálního souřadnihového systému (viz obrázek \ref{fig:ro-kloub-rotace}),
což je ekvivalentní s~limitací pohybu v~nadřazeném kloubu.

Podoba referenční kostry je velmi zjednodušenou podobou kostry člověka (viz obrázek \ref{fig:ro-ref-kostra}. V~první řadě se oprošťuje od anatomie, 
neobsahuje např.~hrudní koš nebo zdvojené kosti v~končetinách -- u dolních končetin jsou kost lýtková a~holenní sloučeny do jedné, podobně u~horních
sloučíme kost loketní a~vřetenní. Tato podoba kostry pokazí vzhled modelu pouze při extrémních zkrutech či ohybech. V~druhé řadě nepostihujeme
drobné detaily, jimiž jsou jednotlivé prsty končetin a~čelisti, čímž odpadá možnost otevírat ústa.

\begin{figure}[!htp]
\centering
\includegraphics[width=8.5cm]{img/ro-ref-kostra.png}
\caption{Referenční kostra, pro ilustraci znázorněn i~obrys člověka.}
\label{fig:ro-ref-kostra}
\end{figure}


% -------------------------------------------------------
\section{Generování prozatímní kostry}
\label{sec:gen-proz-kostr}

Z~trojúhelníkové sítě vygenerujeme prozatímní kostru $S_G$ s~využitím metody autorů Au~et~al. podrobně popsané v~kapitole \ref{subsec:met-au},
tj. nejprve smršťujeme trojúhelníkovou síť laplaceovským vyhlazováním a~následně opakovaně odstraňujeme technikou edge-collapse nadbytečné hrany,
než se zbavíme všech trojúhelníků sítě. Vzniklé jednorozměrné struktuře upravíme pozicování v~prostoru.

% -------------------------------------------------------
\section{Vytvoření animační kostry}
\label{sec:vytv-anim-kostr}

Připravenou referenční kostru $S_R$ humanoida budeme nyní vkládat dovnitř modelu. Tato část metody vychází z~metody popsané v~kapitole \ref{subsec:met-straka}.
Hledáme prosté zobrazení $M: U(S_R) \;\rightarrow\; U(S_G)$,
které jednoznačně přiřadí uzlům kostry $S_R$ uzly z~prozatímní kostry $S_G$. Pro existenci takového zobrazení musí platit nerovnost:
\begin{align}
|U(S_R)| \;\leq\; |U(S_G)|\text{.}
\end{align}

Výsledná animační kostra je grafovým isomorfismem referenční kostry $S_R$, jejíž uzly $U$ jsou umístěny na pozice $M(U)$.

\subsection{Přípravné kroky a~přiřazení hlavy}

Předpokládáme, že koncový efektor představující hlavu se nachází nejvýše, tj.~jeho souřadnice $y$ je maximální (osa $y$ míří vzhůru). Tím určíme pozici hlavy
v~prozatímní kostře. 

V~obou grafech (referenční i~prozatímní kostře) spočteme vzdálenost mezi všemi dvojicemi vrcholů, což lze provést například oblíbeným 
Floyd-Warshallovým algoritmem, a~následně seřadíme vrcholy podle geodetické vzdálenosti od uzlu představujícího hlavu (např.~pomocí quick sort nebo merge sort).
Řazení je nezbytné pro správnou funkci dalších kroků.

Určíme nejdelší cestu v~obou grafech, její délka bude sloužit jako měřítko velikosti kostry. Poté provedeme přeškálování referenční kostry
tak, aby velikostně odpovídala prozatímní. Zvětšení $s$ odpovídá vztahu
\begin{align}
s \;=\; \dfrac{L_{temp}}{L_{ref}},
\end{align}

\noindent kde $L_{temp}$ představuje délku nejdelší cesty v~grafu prozatímní kostry a~$L_{ref}$ délku nejdelší cesty v~grafu kostry referenční.
Vypočtené délky cest mezi dvojicemi vrcholů přenásobíme stejným faktorem.

\newpage
Jako poslední krok je nutné vyrovnat odlišné umístění koster -- referenční kostra se může v~souřadnicovém prostoru nacházet na odlišné pozici než kostra
prozatímní. U~obou koster zjistíme rozměry axis-aligned bounding boxu $B_{temp}$ a~$B_{ref}$ (obalové těleso o~podobě kvádru orientované souhlasně s~osami).
Posun referenční kostry $\mathbf T(x,y,z)$ je pak určen jako:
\begin{align}
\mathbf T(x,y,z) \;=\; \begin{bmatrix} B_{temp}^{maxx} - B_{temp}^{minx} - B_{ref}^{maxx} + B_{ref}^{minx} \\[12pt] 
                               B_{temp}^{maxy} - B_{temp}^{miny} - B_{ref}^{maxy} + B_{ref}^{miny} \\[12pt]
                              B_{temp}^{maxz} - B_{temp}^{minz} - B_{ref}^{maxz} + B_{ref}^{minz} \\ \end{bmatrix}.
\end{align}

\noindent Proces zarovnání koster je znázorněn na obrázku \ref{fig:ppm-korekce-velikosti-pozice}.

Výpočetní složitost předzpracování kostry je dána jejím nejnáročnějším krokem, kterým je výpočet vzdálenosti mezi všemi dvojicemi
uzlů. Floyd-Warshallův algoritmus, který dokáže problém efektivně řešit, nabývá složitosti $O(N^3 + M^3)$, kde $N$ představuje počet
vrcholů referenční kostry a~$M$ počet vrcholů kostry prozatímní.

\begin{figure}[!htp]
\centering
\includegraphics[width=14.5cm]{img/ppm-korekce-velikosti-pozice.pdf}
\caption{Znázornění sjednocení velikosti a~pozice referenční a~prozatímní kostry. Referenční kostra má vyznačenou hlavu černě, prozatímní bíle. Tečkovaně jsou vyznačeny obalující boxy.}
\label{fig:ppm-korekce-velikosti-pozice}
\end{figure}

\subsection{Přiřazení listů}

V~této části se zabýváme přiřazením listových uzlů. Hlava je v~tuto chvíli už přiřazena, a~proto ji z~tohoto procesu vyjmeme.
Z~listových vrcholů utvoříme seznamy, formálně si je označíme: pro referenční kostru $L_{ref} = (r_1, r_2, \,\dots\, ,r_n)$,
pro prozatímní kostru pak $L_{temp} = (t_1, t_2, \,\dots\, ,t_m)$. Obecně nelze říci, že by byl počet listů stejný, naopak předpokládáme,
že $m \geq n$. Je nutné poznamenat, že respektujeme pořadí vrcholů vzniklé řazením podle vzdálenosti od hlavy.

Listy obou grafů k~sobě přiřadíme užitím maďarské metody \cite{HunWiki}, která minimalizuje globální cenu (penalizaci) přiřazení. Pro vyčíslení
ceny $E_{i,j}$ přiřazení dvojice $(t_i, r_j)$ použijeme vztah:
\begin{align}
E_{i,j} \;=\; c_{DWT} \cdot DWT(d_{t_i}, d_{r_j})^2 + c_{dist} \cdot dist(t_i, r_j)^2,
\end{align}

\noindent Předpis ceny je váženým součtem dvou členů, neprve si objasníme výraz funkce $dist$. Ta vrací euklidovskou vzdálenost
mezi předanými vrcholy, $c_{dist}$ představuje násobicí konstantu (váhu).
Funkce $DWT$ pak spočítá hodnotu \textit{dynamic time warping} dvou sekvencí $d_{t_i}$ a~$d_{r_j}$. V~nich jsou obsaženy geodetické
vzdálenosti listů $(t_1, t_2, \,\dots\,, t_m)$ a~$(r_1, r_2, \,\dots\,, r_n)$ od vrcholu $t_i$, respektive $r_j$ (vzdálenosti už byly vypočteny 
během přípravného kroku Floyd-Warshallovým algoritmem). Konstanta $c_{DWT}$ je váhou funkce $DWT$.

Výpočetní složitost přiřazení listů se skládá z~několika částí. Výpočet hodnoty DWT pro dvě sekvence, z~nichž má první délku $N$ a~druhá
$M$, je obecně složitosti $O(M \, N)$. Tento výpočet však provádíme pro všechny prvky matice velikosti $M \,\times\,N$, tudíž složitost
samotné přípravy matice pro maďarskou metodu bude $O(M^2 \, N^2)$. Následuje výpočet optimálního přiřazení (maďarskou metodou) o~složitosti $O(M \, N^2)$.

Jak již bylo řečeno výše, předpokládáme, že počet listů prozatímní kostry je větší nebo roven počtu listů referenční kostry, tj.~$M \geq N$.
Některé listy prozatímní kostry tak zůstanou nepřiřazeny (viz dále). Není-li dodržena podmínka $M \geq N$, algoritmus přiřazení listů selže, neboť
není možné nechat jediný list referenční kostry, kterou hodláme ovládat model, nepřiřazený.

Po dokončení této fáze se zaměříme na vnitřní uzly.

\subsection{Přiřazení vnitřních uzlů}

Před samotným přiřazováním vnitřních uzlů projdeme graf prozatímní kostry od každého přiřazeného listu směrem ke kořeni. Po průchodu mohou zbýt některé 
vrcholy nenavštívené, jedná se o~vrcholy spadající do větví nepřiřazených listů. Chápeme je jako nežádoucí a~z~grafu je odstraníme (viz obrázek \ref{fig:ppm-odstraneni-vetvi}).

\begin{figure}[!htp]
\centering
\includegraphics[width=6.5cm]{img/ppm-odstraneni-vetvi.pdf}
\caption{Prořezávání větví dočasné kostry. Nepřiřazené listy $L_2$, $L_3$, $L_4$ budou včetně celé větve z~dočasné kostry odstraněny.}
\label{fig:ppm-odstraneni-vetvi}
\end{figure}

Vnitřní uzly přiřazujeme pouze na základě optimalizace lokální chyby, tedy bez ohledu na penalizaci ostatních vrcholů -- pro definici 
chyby využijeme hodnoty, které jsou na přiřazení nezávislé (viz níže). Drobnou výjimkou je, že již přiřazený vrchol nesmí být použit opětovně 
(to lze provést například označením vrcholu). Vnitřní vrcholy náležící dočasné kostře 
označíme $u_i$, vnitřní vrcholy náležící referenční kostře pak $v_j$. Pro každý $u_i$ tedy procházíme všechny nepoužité uzly $v_j$,
přičemž hledáme takové přiřazení, které je nejméně penalizováno.

Pro každý vrchol lze vyjádřit tyto dílčí penalizace:
\begin{enumerate}
\item \textbf{Rozdíl vzdáleností k~listům.} Známe přiřazení listů, v~každém grafu taktéž geodetickou vzdálenost k~příslušnému listu.
    Tato vzdálenost se v~obou grafech bude nejspíše lišit. Sečtením kvadrátů všech rozdílů dostáváme chybu, kterou označíme $E_L$.
\item \textbf{Rozdíl stupně vrcholů.} Kvadrát rozdílu stupně vrcholů $u_i$ a~$v_j$, označíme jej $E_D$.
\item \textbf{Vzdálenost vrcholů.} Kvadrát euklidovské vzdálenosti vrcholů $u_i$ a~$v_j$, označíme jej $E_P$.
\end{enumerate}

Výsledná chyba odpovídá jejich váženému součtu:
\begin{align}
E \;=\; c_{E_L} \cdot E_L + c_{E_D} \cdot E_D + c_{E_P} \cdot E_P.
\end{align}

Tato hodnota není závislá na přiřazení jiných vnitřních vrcholů, proto můžeme výslednou konfiguraci hledat s~pomocí optimalizace lokální chyby.
Složitost pomocí naivní implementace je pak rovna $O(M\,N)$, $N$ znamená počet vnitřních vrcholů vzorové kostry, $M$ počet vrcholů prozatímní.

Momentálně jsme referenční kostru umístili do vstupní trojúhelníkové sítě, čímž jsme vytvořili animační kostru. 
Kosti jsou v~tuto chvíli reprezentovány pouze pomocí úseček (hrany grafu), pro interaktivní demo deformující síť
musíme určit orientaci lokálních souřadnicových systémů.

\subsection{Zkopírování lokálních souřadnicových systémů}

Pro lokální souřadnicový systém platí, že osa $y$ je ztotožněná s~orientací kosti $\overrightarrow{\mathbf B}$. Dále víme, že animační kostra
si je s~referenční velmi podobná (vzhledem k~charakteru vstupních dat). Označme si orientaci os lokálního systému animační
kostry $\mathbf A_x, \mathbf A_y, \mathbf A_z$ a~u~referenční kostry $\mathbf R_x, \mathbf R_y, \mathbf R_z$. Pak platí:
\begin{align}
\mathbf A_y \;&=\; \dfrac{\overrightarrow{\mathbf B}}{\|\overrightarrow{\mathbf B}\|} \\
\mathbf A_z \;&=\; \mathbf R_x \times \mathbf A_y \\
\mathbf A_x \;&=\; \mathbf A_y \times \mathbf A_z
\end{align}

Tento krok má viditelně lineární složitost $O(N)$. Příklad možného výsledku se nachází na obrázku \ref{fig:ppm-lokalni-system}.

\begin{figure}[!htp]
\centering
\includegraphics[width=10cm]{img/ppm-lokalni-system.PNG}
\caption{Znázornění lokálních souřadnicových systémů kostí.}
\label{fig:ppm-lokalni-system}
\end{figure}

\subsection{Zkopírování limitace pohybu v~kloubech}

Hodnoty omezení v~kloubech jsou do výsledného modelu jednoduše zkopírovány. Předpokládáme, že jsou oba modely v~klidové
póze a~je díky tomu možné použít stejné hodnoty.

% -------------------------------------------------------
\section{Interaktivní deformace}

Animační kostra nyní obsahuje veškeré informace nutné pro interaktivní manipulaci. V~místech koncových efektorů
se objeví prvky, které uživatel může přesouvat v~prostoru a~zadá tak novou pozici příslušného koncového 
efektoru, který se k~ní přiblíží s~využitím inverzní kinematiky.

% -----------------------------
\subsection{Inverzní kinematika}

Metody řešící úlohu inverzní kinematiky byly popsány v~kapitole \ref{ch:prima-a-inverzni-kinematika}.
Pro výpočet v~interaktivní ukázce byl zvolen přístup \textit{cyclic coordinate descent} (CCD),
která konverguje zpravidla během několika set iterací a~umožňuje řešit úlohu přímo 
v~globálním souřadnicovém systému.

Limity rotací v~kloubech jsou zadány jako maximální hodnoty rotací kolem lokální osy $x$, $y$ a~$z$.
Krok algoritmu, kdy optimálně natáčíme právě vybranou kost, aby vzdálenost koncového efektoru $X$ od
zadané pozice $Y$ byla minimální, rozložíme na tři individuální rotace podle lokálních os. Překročí-li
rotace zadané omezení, je její hodnota upravena oříznutím na povolený interval.

\begin{figure}[!htp]
\centering
\includegraphics[width=11.0cm]{img/ro-ccd-rotace.pdf}
\caption{Znázornění rotace kolem lokální osy $y$. Bod $X$ představuje pozici koncového efektoru, $A$ nově zadanou pozici,
$O$ počátek lokální soustavy souřadnic. Rovina $\sigma$ znázorňuje rotační rovinu, $\alpha$ hledaný úhel rotace.
$X'$ je průmětem koncového efektoru $X$ do roviny rotace, $Y'$ průmětem koncového efektoru po provedení rotace.}
\label{fig:ro-ccd-rotace}
\end{figure}

Rotace kolem jedné z~os je znázorněna na obrázku \ref{fig:ro-ccd-rotace}. Provedeme ortogonální projekci koncového efektoru $X$
do roviny rotace $\sigma$, kterou vyjádříme pomocí bodu $O$ a~normálového normalizovaného vektoru $\textbf n$ 
(souhlasně orientovaným se~zvolenou osou rotace):
\begin{align}
X' \;=\; X - (\overrightarrow{OX} \cdot \textbf n) \; \textbf n.
\end{align}

\vspace{0.1cm}
Obdobně promítneme do roviny $\sigma$ i~cílový bod $Y$. Takto jsme získali vektory $\overrightarrow{OX'}$ a~$\overrightarrow{OY'}$,
které svírají hledaný úhel ideální rotace $\alpha$, jehož velikost vypočteme podle předpisu:
\begin{align}
\alpha \;=\; arccos\left( \frac{\overrightarrow{OX'}}{\|\overrightarrow{OX'}\|} \cdot \frac{\overrightarrow{OY'}}{\|\overrightarrow{OY'}\|} \right).
\end{align}

\vspace{0.1cm}
\noindent Výsledná hodnota se nachází v~intervalu $\langle 0, \pi \rangle$, neboť se jedná pouze o~absolutní velikost.
Naštěstí znaménko můžeme snadno určit užitím vektorového součinu, výsledný vztah potom nabyde tvaru:

\begin{align}
\alpha \;=\; arccos\left( \frac{\overrightarrow{OX'}}{\|\overrightarrow{OX'}\|} \cdot \frac{\overrightarrow{OY'}}{\|\overrightarrow{OY'}\|} \right) \cdot sgn\left((\overrightarrow{OX'} \times \overrightarrow{OY'}) \cdot \textbf n \right),
\end{align}

\noindent kde $sgn$ představuje matematickou funkci $signum$, která pro záporné číslo vrací hodnotu $-1$, pro kladné $+1$
a~pro nulu $0$.

\begin{figure}[!htp]
\centering
\includegraphics[width=8cm]{img/ro-ccd-prodlouzeni.pdf}
\caption{Znázornění prodloužení kosti podél lokální osy $y$. Bod $X$ představuje pozici koncového efektoru, $A$ nově zadanou pozici,
$O$ počátek lokální soustavy souřadnic. $X'$ je průmětem koncového efektoru $X$ na lokální osu $y$, $A'$ průmětem zadané pozice.}
\label{fig:ro-ccd-prodlouzeni}
\end{figure}

\subsubsection*{Inverzní kinematika s~pružnými kostmi}

V~případě, že kosti lze natahovat, přidáme kromě tří rotačních kroků ještě změnu délky kosti. Tento krok bude proveden pouze podél
osy $y$, s~níž je kost souhlasně orientovaná. Situace je zachycena na obrázku \ref{fig:ro-ccd-prodlouzeni}, na němž se používá stejné značení jako u~rotace.
Hledaná změna délky kosti odpovídá orientované úsečce $\overrightarrow{X'A'}$, body $X'$ a~$A'$ určíme pomocí průmětu na lokální osu $y$:
\begin{align}
X' \;&=\; O \,+\, (\overrightarrow{OX} \cdot \textbf b) \; \textbf b, \\
A' \;&=\; O \,+\, (\overrightarrow{OA} \cdot \textbf b) \; \textbf b.
\end{align}

\noindent kde $\textbf b$ je normalizovaný vektor souhlasně orientovaný s~lokální osou $y$. Abychom zabránili singularitám, 
nedovolíme zkrácení kosti pod určitý práh (například nedovolíme, aby kost byla kratší než v~klidovém stavu).

\subsubsection*{Výpočetní složitost}

Složitost výpočtu úlohy inverzní kinematiky metodou CCD záleží na počtu kostí $P$, ze kterých se skládá segmentová struktura.
Dále se mění počet nutných iterací $I_{CCD}$ v~závislosti na počáteční a~cílové konfiguraci struktury. Výslednou výpočetní
složitost lze formálně vyjádřit předpisem $O(I_{CCD} \, P)$.

% -----------------------------
\subsection{Mesh-skinning}

Změna animační kostry vyžaduje patřičnou úpravu trojúhelníkové sítě. Proto použijeme
tzv.~mesh-skinning, techniku deformace sítě, kdy se trojúhelníková síť přizpůsobuje kostře, která je jí přiřazena.
Pro demonstrační aplikaci byla naimplementována varianta nazývaná linear blend skinning (LBS).

\begin{figure}[!htp]
\centering
\includegraphics[width=9.5cm]{img/skin-boneweight.png}
\caption{Vizualizace vah kostí (ovlivnění vrcholu danou kostí). Světlá barva znamená velké ovlivnění, černá žádné. V~místě kloubu mají vliv obě kosti.}
\label{fig:boneweight}
\end{figure}

Linear blend skinning (LBS) deformuje síť pomocí lineární kombinace transformač\-ních matic definovaných v~kostech. Jedná se o~nejzákladnější
přístup k~problematice, a~přesto často používaný například v~počítačových hrách zejména kvůli jednoduché implementaci a~nízkým nárokům na 
cílový hardware.

Na vstupu máme trojúhelníkovou síť s~$m$ vrcholy $\mathbf{v}_i$ a~kostru tvořenou uspořádanou množinou $n$ kostí $\mathbf{b}_j$.
Pro každou kost $\mathbf{b}_j$ je definována matice afinní transformace $\mathbf{M}_j$. Každý vrchol $\mathbf{v}_i$
obsahuje $n$ definovaných vah příspěvku kosti $w_{ij}$ (viz obrázek~\ref{fig:boneweight}). Výstupem je deformovaná množina vrcholů, přičemž 
původní topologie sítě zůstává zachována. Deformace jednoho vrcholu se provádí podle vztahu \ref{eq:impl-skinning-lbs}.
Algoritmus je vysoce paralelizovatelný, vrcholy lze zpracovat naprosto nezávisle.
\begin{equation}
\label{eq:impl-skinning-lbs}
\mathbf{v'}\!_i = \sum_{j=0}^n \; w_{ij} \, (\mathbf{M}_j \, \mathbf{v}_i) = \left(\sum_{j=0}^n \; w_{ij} \, \mathbf{M}_j\right) \, \mathbf{v}_i
\end{equation}

Jednoduchost LBS se podepisuje na viditelných artefaktech, mezi něž patří:
\begin{itemize}
\item ztráta objemu (zborcení sítě) zejména v~místě velkého ohybu či zkrutu,
\item sebeprotínání sítě v místech velkého ohybu či zkrutu.
\end{itemize}

\subsubsection{Výpočet vah mesh-skinningu}

Spolu s~automaticky vygenerovanou kostrou bohužel nedostáváme rovnou váhy určené pro mesh-skinning. Tyto hodnoty zpravidla tvoří
výtvarník (animátor) ve vhod\-ném nástroji, lze je však pro naše účely poměrně dobře odhadnout.

K~výpočtu použijeme síť ve stavu, v~jakém se nacházela při generování kostry po poslední iteraci laplaceovského smršťování (tj.~před zahájením
decimace sítě). Od vstupní sítě předané aplikaci ke zpracování se liší pouze pozicemi vrcholů, které se ale vyskytují vhodně blízko
vygenerované kostry. 

Na základě tohoto pozorování určíme váhu kosti~$\mathbf{b}_i$ pro vrchol $\mathbf{v}_j$ podle vztahu:
\begin{equation}
w_{ij} \; = \; \dfrac{1}{\varepsilon + \mathit{dist}(\mathbf{b}_i, \mathbf{v}_j)^2}
\end{equation}

\noindent
kde $\varepsilon$ značí vhodné malé číslo (v~aplikaci zvoleno $\varepsilon\,=\,10^{-5}$, čím vyšší, tím plynulejší bude změna vlivu kostí v~okolí kloubů),
funkce $\mathit{dist}$ vzdálenost vrcholu $\mathbf{v}_j$ od kosti $\mathbf{b}_i$, kterou při výpočtu vah chápeme jako úsečku. Získané váhy posléze musíme normalizovat:
\begin{equation}
\hat{w}_{ij} \; = \dfrac{w_{ij}}{ \sum_i w_{ij} }
\end{equation}

Z~výkonnostních důvodů se obvykle používá stanovený počet vah, neboť valná většina je téměř rovna nule a~kost má tak na deformovaný vrchol sotva zanedbatelný vliv.
Demonstrační aplikace provádí mesh-skinning s~užitím čtyř nejvýznamějších vah na jeden vrchol.
