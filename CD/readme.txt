 Z�pado�esk� univerzita v Plzni

 Fakulta aplikovan�ch v�d

 Katedra informatiky a v�po�etn� techniky



 Diplomov� pr�ce

 Generov�n� kostry objekt� reprezentovan�ch troj�heln�kov�mi s�t�mi

 Michal ��k

 Plze� 2015



Obsah CD:


* binary                    spustiteln� soubory pr�ce a pot�ebn� data

   * build                  EXE a DLL soubory

   * data                   modely ve form�tu Wavefront OBJ

   * run                    p�ipraven� spou�t�c� skripty

* doc                       diplomov� pr�ce ve form�tu PDF

   * tex                    zdrojov� soubory bakal��sk� pr�ce (LaTeX)

* source                    ve�ker� zdrojov� soubory aplikace

   * blender-bones-script   exportovac� skript kostry pro program Blender

   * program                zdrojov� soubory samotn� aplikace