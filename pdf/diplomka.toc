\select@language {czech}
\contentsline {chapter}{\numberline {1}\IeC {\'U}vod}{1}
\contentsline {chapter}{\numberline {2}Metody extrakce kostry}{3}
\contentsline {section}{\numberline {2.1}Medial axis transform}{3}
\contentsline {section}{\numberline {2.2}Metody zalo\IeC {\v z}en\IeC {\'e} na geometrii a~topologii}{5}
\contentsline {subsection}{\numberline {2.2.1}Smr\IeC {\v s}t\IeC {\v e}n\IeC {\'\i } modelu s~n\IeC {\'a}slednou redukc\IeC {\'\i } geometrie}{5}
\contentsline {subsubsection}{Smr\IeC {\v s}t\IeC {\v e}n\IeC {\'\i } s\IeC {\'\i }t\IeC {\v e}}{5}
\contentsline {subsubsection}{Redukce geometrie}{7}
\contentsline {subsubsection}{Z\IeC {\'a}v\IeC {\v e}re\IeC {\v c}n\IeC {\'a} \IeC {\'u}prava pozic}{9}
\contentsline {subsubsection}{Klady a~z\IeC {\'a}pory metody}{9}
\contentsline {subsection}{\numberline {2.2.2}Tvorba kostry spojov\IeC {\'a}n\IeC {\'\i }m v\IeC {\'y}znamn\IeC {\'y}ch bod\IeC {\r u}}{10}
\contentsline {subsubsection}{P\IeC {\v r}edzpracov\IeC {\'a}n\IeC {\'\i } dat}{10}
\contentsline {subsubsection}{Hled\IeC {\'a}n\IeC {\'\i } v\IeC {\'y}zna\IeC {\v c}n\IeC {\'y}ch vrchol\IeC {\r u}}{11}
\contentsline {subsubsection}{Tvorba kostry}{11}
\contentsline {subsubsection}{Z\IeC {\'a}v\IeC {\v e}re\IeC {\v c}n\IeC {\'e} \IeC {\'u}pravy}{12}
\contentsline {subsubsection}{Klady a~z\IeC {\'a}pory metody}{12}
\contentsline {section}{\numberline {2.3}Volumetrick\IeC {\'e} metody}{13}
\contentsline {subsection}{\numberline {2.3.1}Generov\IeC {\'a}n\IeC {\'\i } kostry sn\IeC {\'\i }man\IeC {\'e}ho \IeC {\v c}lov\IeC {\v e}ka v~re\IeC {\'a}ln\IeC {\'e}m \IeC {\v c}ase}{14}
\contentsline {section}{\numberline {2.4}P\IeC {\v r}\IeC {\'\i }buzn\IeC {\'e} oblasti}{15}
\contentsline {chapter}{\numberline {3}P\IeC {\v r}\IeC {\'\i }m\IeC {\'a} a~inverzn\IeC {\'\i } kinematika}{17}
\contentsline {section}{\numberline {3.1}P\IeC {\v r}\IeC {\'\i }m\IeC {\'a} kinematika}{18}
\contentsline {section}{\numberline {3.2}Inverzn\IeC {\'\i } kinematika}{18}
\contentsline {subsection}{\numberline {3.2.1}Linearizace pomoc\IeC {\'\i } jakobi\IeC {\'a}nu}{19}
\contentsline {subsection}{\numberline {3.2.2}Cyclic coordinate descent}{20}
\contentsline {subsection}{\numberline {3.2.3}Obohacen\IeC {\'\i } o~posuvn\IeC {\'a} spojen\IeC {\'\i }}{21}
\contentsline {chapter}{\numberline {4}N\IeC {\'a}vrh vlastn\IeC {\'\i } metody}{22}
\contentsline {section}{\numberline {4.1}Generov\IeC {\'a}n\IeC {\'\i } kostry}{22}
\contentsline {section}{\numberline {4.2}Interaktivn\IeC {\'\i } uk\IeC {\'a}zka}{23}
\contentsline {chapter}{\numberline {5}Podrobn\IeC {\'y} rozbor metody}{24}
\contentsline {section}{\numberline {5.1}Vstupn\IeC {\'\i } data}{24}
\contentsline {section}{\numberline {5.2}Generov\IeC {\'a}n\IeC {\'\i } prozat\IeC {\'\i }mn\IeC {\'\i } kostry}{25}
\contentsline {section}{\numberline {5.3}Vytvo\IeC {\v r}en\IeC {\'\i } anima\IeC {\v c}n\IeC {\'\i } kostry}{26}
\contentsline {subsection}{\numberline {5.3.1}P\IeC {\v r}\IeC {\'\i }pravn\IeC {\'e} kroky a~p\IeC {\v r}i\IeC {\v r}azen\IeC {\'\i } hlavy}{26}
\contentsline {subsection}{\numberline {5.3.2}P\IeC {\v r}i\IeC {\v r}azen\IeC {\'\i } list\IeC {\r u}}{27}
\contentsline {subsection}{\numberline {5.3.3}P\IeC {\v r}i\IeC {\v r}azen\IeC {\'\i } vnit\IeC {\v r}n\IeC {\'\i }ch uzl\IeC {\r u}}{28}
\contentsline {subsection}{\numberline {5.3.4}Zkop\IeC {\'\i }rov\IeC {\'a}n\IeC {\'\i } lok\IeC {\'a}ln\IeC {\'\i }ch sou\IeC {\v r}adnicov\IeC {\'y}ch syst\IeC {\'e}m\IeC {\r u}}{30}
\contentsline {subsection}{\numberline {5.3.5}Zkop\IeC {\'\i }rov\IeC {\'a}n\IeC {\'\i } limitace pohybu v~kloubech}{30}
\contentsline {section}{\numberline {5.4}Interaktivn\IeC {\'\i } deformace}{31}
\contentsline {subsection}{\numberline {5.4.1}Inverzn\IeC {\'\i } kinematika}{31}
\contentsline {subsection}{\numberline {5.4.2}Mesh-skinning}{34}
\contentsline {subsubsection}{V\IeC {\'y}po\IeC {\v c}et vah mesh-skinningu}{35}
\contentsline {chapter}{\numberline {6}Implementace demonstra\IeC {\v c}n\IeC {\'\i } aplikace}{37}
\contentsline {section}{\numberline {6.1}Pou\IeC {\v z}it\IeC {\'e} knihovny a~jazyk}{37}
\contentsline {section}{\numberline {6.2}Form\IeC {\'a}t vstupn\IeC {\'\i }ch dat}{38}
\contentsline {subsection}{\numberline {6.2.1}Vzorov\IeC {\'a} kostra}{38}
\contentsline {subsection}{\numberline {6.2.2}Troj\IeC {\'u}heln\IeC {\'\i }kov\IeC {\'a} s\IeC {\'\i }\IeC {\v t}}{39}
\contentsline {section}{\numberline {6.3}Generov\IeC {\'a}n\IeC {\'\i } kostry}{39}
\contentsline {section}{\numberline {6.4}Uk\IeC {\'a}zkov\IeC {\'a} aplikace}{40}
\contentsline {subsection}{\numberline {6.4.1}Inverzn\IeC {\'\i } kinematika}{41}
\contentsline {subsection}{\numberline {6.4.2}Mesh-skinning}{41}
\contentsline {subsection}{\numberline {6.4.3}Nastaven\IeC {\'\i }}{41}
\contentsline {chapter}{\numberline {7}V\IeC {\'y}sledky}{42}
\contentsline {section}{\numberline {7.1}Testovac\IeC {\'\i } modely}{42}
\contentsline {section}{\numberline {7.2}Nastaven\IeC {\'\i } konstant generov\IeC {\'a}n\IeC {\'\i } kostry}{44}
\contentsline {section}{\numberline {7.3}V\IeC {\'y}sledn\IeC {\'a} podoba kostry}{45}
\contentsline {section}{\numberline {7.4}Doba generov\IeC {\'a}n\IeC {\'\i } kostry}{47}
\contentsline {section}{\numberline {7.5}Pam\IeC {\v e}\IeC {\v t}ov\IeC {\'a} slo\IeC {\v z}itost generov\IeC {\'a}n\IeC {\'\i } kostry}{49}
\contentsline {section}{\numberline {7.6}Doba v\IeC {\'y}po\IeC {\v c}tu inverzn\IeC {\'\i } kinematiky}{49}
\contentsline {section}{\numberline {7.7}Srovn\IeC {\'a}n\IeC {\'\i } s~existuj\IeC {\'\i }c\IeC {\'\i }mi metodami}{50}
\contentsline {chapter}{\numberline {8}Z\IeC {\'a}v\IeC {\v e}r}{52}
\contentsline {chapter}{\numberline {A}P\IeC {\v r}\IeC {\'\i }loha: Obsah CD}{55}
\contentsline {chapter}{\numberline {B}P\IeC {\v r}\IeC {\'\i }loha: U\IeC {\v z}ivatelsk\IeC {\'y} manu\IeC {\'a}l}{56}
\contentsline {section}{\numberline {B.1}P\IeC {\v r}eklad}{56}
\contentsline {section}{\numberline {B.2}Parametry p\IeC {\v r}\IeC {\'\i }kazov\IeC {\'e} \IeC {\v r}\IeC {\'a}dky}{56}
\contentsline {section}{\numberline {B.3}Ovl\IeC {\'a}d\IeC {\'a}n\IeC {\'\i } aplikace}{57}
