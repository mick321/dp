# EXPORT BONES TO TXT
# VERSION FROM:
#    Year: 2015 
#   Month: 03
#     Day: 14

import bpy

from bpy.props import *
import mathutils, math, struct
import os
from os import remove
from os import path
import time
import bpy_extras
from bpy_extras.io_utils import ExportHelper
from bpy.types import AddonPreferences
import time
import shutil
import bpy
import mathutils
from mathutils import Matrix
import struct
from pprint import pprint

bl_info = {
    "name": "Export Bones To TXT",
    "description": "Script exporting bones to text file.",
    "author": "Michal Zak",
    "version": (1, 0),
    "blender": (2, 65, 0),
    "location": "File > Export",
    "warning": "", # used for warning icon and text in addons panel
    "wiki_url": "",
    "category": "Import-Export"}
        
# ------------------------------------------------------------
class BonesExporter:

    # --------------------------------------------------------
    # prepare internal data structures
    def prepare(self, context, filepath, options):
    
        self.context = context;
        self.filepath = filepath;
        self.options = options;
        
        print("[INFO] Preparing data to be exported to file %s." % self.filepath)
        
        objmesh = []
        
        sel_obj = []
        all_objects = []
        armatures = []
        
        if options['selected']:
            all_objects = list(context.selected_objects)
        else:
            all_objects = context.scene.objects
            
        for obj in all_objects:
            if obj.type == 'MESH':
                sel_obj.append(obj)
            elif obj.type == 'ARMATURE':
                armatures.append(obj)
                
        self.armatures = armatures
                
        self.current_scene = context.scene
        apply_modifiers = False
        
        return True
        
    # --------------------------------------------------------  
    # save prepared data to file
    def save(self):
    
        print("[INFO] Exporting to file %s." % self.filepath)
        
        f = None
        try:
            f = open(self.filepath, 'w')
            # save bones
            self.saveBones(f)
    
            f.close()
        except IOError:
            if f is not None:
                f.close()
            print("[ERROR] Cannot save to file %s." % self.filepath)
            raise Exception("[ERROR] Cannot save to file %s." % self.filepath)
            return False
        else:
            if f is not None:
                f.close()
            pass
            
        print("[INFO] Exporting complete.")
            
        for scene in bpy.data.scenes:
            scene.update()
        return True
        
    # --------------------------------------------------------
    # save animations to text file
    def saveBones(self, f):
        armatures = self.armatures
        print('  bones...')
        
        bones_i = {}
        index = 0
        # export armatures in default position
        for armature in armatures:
            for bone in armature.data.bones:
                bones_i[bone] = index
                index += 1
                
        bone_count = index
        f.write("%d\n" % bone_count)
        matrix_correction = Matrix.Rotation(-math.pi/2.0, 4, 'X')
        
        for armature in armatures:
            armmatrix = armature.matrix_world
            for posebone in armature.pose.bones:
                
                bone = posebone.bone
                if bone.parent is None:
                    parentIndex = -1
                else:
                    parentIndex = bones_i[bone.parent]
                
                boneStart = matrix_correction* (armmatrix * bone.head_local)
                boneEnd = matrix_correction* (armmatrix * bone.tail_local)
                
                f.write("%d %f %f %f %f %f %f\n" % (parentIndex, boneStart.x, boneStart.y, boneStart.z, boneEnd.x, boneEnd.y, boneEnd.z))
                
                rotminx = -math.pi;
                rotmaxx = math.pi;
                rotminy = -math.pi;
                rotmaxy = math.pi;
                rotminz = -math.pi;
                rotmaxz = math.pi;
                
                for constr in posebone.constraints:
                    if constr.type == 'LIMIT_ROTATION':
                        rotminx = constr.min_x;
                        rotmaxx = constr.max_x;
                        rotminy = constr.min_y;
                        rotmaxy = constr.max_y;
                        rotminz = constr.min_z;
                        rotmaxz = constr.max_z;
                        break
                        
                f.write("%f %f %f %f %f %f\n\n" % (rotminx, rotmaxx, rotminy, rotmaxy, rotminz, rotmaxz))
                
                bm =  matrix_correction* (armmatrix * bone.matrix_local)
                for mati in range(4):
                    for matj in range(4):
                        f.write("%f " % bm[mati][matj])
                        
                    f.write("\n")
                    
                f.write("\n\n")
               
        
# ------------------------------------------------------------
        
# ExportHelper is a helper class, defines filename and
# invoke() function which calls the file selector.
from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty
from bpy.types import Operator

# description of dialog shown on export
class ExportBonesDialog(Operator, ExportHelper):
    """Exports bones in text format"""
    bl_idname = "export_scene.bones"  # important since its how bpy.ops.export_script.bones is constructed
    bl_label = "Export Bones"

    # ExportHelper mixin class uses this
    filename_ext = ".txt"

    filter_glob = StringProperty(
            default="*.txt",
            options={'HIDDEN'},
            )

    # List of operator properties, the attributes will be assigned
    # to the class instance from the operator settings before calling.
    selected_only = BoolProperty(
            name="Selected objects only",
            default=False,
            )
            
    def execute(self, context):
        print("")
        print(" ----- Bones export started. ------")
        options = {}
        if bpy.context.user_preferences.system.author != "":
            options["author"] = bpy.context.user_preferences.system.author
        else:
            options["author"] = "Anonymous"
            
        options["name"] = os.path.basename(self.filepath)
        options["path"] = os.path.dirname(self.filepath)
        options["selected"] = self.selected_only
        
        exporter = BonesExporter()
        if exporter.prepare(context, self.filepath, options) == False:
            return {'CANCELLED'}
        if exporter.save() == False:
            return {'CANCELLED'}
        
        print(" ----- Bones export finished. -----")
        print("")
        return {'FINISHED'}

# Only needed if you want to add into a dynamic menu
def menu_func_export(self, context):
    self.layout.operator(ExportBonesDialog.bl_idname, text="Export bones to text file (.txt)")


def register():
    bpy.utils.register_class(ExportBonesDialog)
    bpy.types.INFO_MT_file_export.append(menu_func_export)


def unregister():
    bpy.utils.unregister_class(ExportBonesDialog)
    bpy.types.INFO_MT_file_export.remove(menu_func_export)

if __name__ == "__main__":  
    register()