% ------------------
\chapter{Přímá a~inverzní kinematika}
% ------------------
\label{ch:prima-a-inverzni-kinematika}

V~předchozí kapitole jsme popsali metody, které slouží ke generování kostry. Obohatíme-li vzniklou 
kostru o~vhodná omezení pohybu v~kloubech (viz níže), můžeme ji rozhýbat a~animovat původní model.
Jeden ze způsobů animace je využití tzv.~inverzní kinematiky.

Kostra objektů představuje otevřenou hierarchickou segmentovou strukturu, ve které bod položený 
nejvýše v~hierarchii bývá pevně ukotven. Volné listy v~hierarchii nazveme koncové efektory.

Polohu volného tělesa v~prostoru lze určit pomocí zvoleného souřadnicového systému a~šesti čísel, takzvanými stupni volnosti.
Uvažujeme-li systém těles, počet stupňů volnosti roste s~jejich přidaným počtem. Takový systém může ovšem obsahovat
různá omezení pohybu (vazby mezi objekty, klouby), která naopak sníží výsledný počet stupňů volnosti. Na obrázku \ref{fig:ik-stupnevolnosti}
je znázorněn příklad systému se dvěma stupni volnosti, jehož celkový stav lze popsat vektorem $(\alpha,\, \beta)$.
Délka vektoru odpovídá počtu stupňů volnosti soustavy, přičemž poloha koncového efektoru je určena stavovým vektorem
jednoznačně.

Inverzní kinematika se zabývá určením stavového vektoru na základě zadané pozice koncového efektoru a~sady omezení pohybu.
O~základech této problematiky pojednává např.~Žára et al. \cite{Zara04}, podrobněji Goliáš ve své diplomové práci \cite{Golias1999}.


\begin{figure}[!htp]
\centering
\includegraphics[width=6.5cm]{img/ik-stupnevolnosti.pdf}
\caption{Příklad segmentové struktury se dvěma stupni volnosti. Koncový efektor je označen písmenem~X.}
\label{fig:ik-stupnevolnosti}
\end{figure}

\section{Přímá kinematika}

Přímá kinematika slouží k~výpočtu pozice koncového efektoru na základě zadaného vstupního stavového vektoru $\theta$.
Vztah lze vyjádřit rovností:

\begin{equation}
X \;=\;f(\theta)
\end{equation}

kde $X$ značí pozici koncového efektoru a~$f$ použité zobrazení. Přímá kinematika se nehodí k~interaktivním animacím,
nasazuje se v~případech, kdy známe časový průběh stavového vektoru $\theta$. Jako příklady využití přímé kinematiky, které souvisejí s~animační kostrou, uveďme:

\begin{itemize}
\item \textit{Motion capture}, tj. zaznamenání pohybu reálného tělesa v~místech blízkých jednotlivým kostem a~následné přepočítání stavového vektoru~$\theta$.
\item \textit{Mesh-skinning}, tj. potažení kostry trojúhelníkovou sítí, kdy na základě stavového vektoru $\theta$ určujeme výsledné pozice povrchu.
\end{itemize}

\section{Inverzní kinematika}

S~pomocí inverzní kinematiky, jak již bylo výše řečeno, se snažíme o~určení stavového vektoru $\theta$ na základě zadané pozice $X$ koncového efektoru.
Formálně bychom tuto představu mohli zapsat vztahem:

\begin{equation}
\theta \;=\;f^{-1}(X)
\end{equation}

Na rozdíl od přímé kinematiky však nemusí existovat jednoznačné řešení, v~některých případech dokonce žádné řešení neexistuje (viz obrázek \ref{fig:ik-moznareseni}).
Právě z~tohoto důvodu se zavádí omezení volnosti pohybu v~kloubech, abychom náležitě zjednodušili podobu stavového prostoru. I~tak se nevyhneme singularitám, navíc
zobrazení $f$ není lineární a~analytické řešení rovnosti prakticky nelze realizovat.

\subsection{Linearizace pomocí jakobiánu}

Problém tedy řešíme v~linearizované podobě za pomoci jakobiánu. V~takovém případě můžeme předpokládat, že v~dostatečně malém okolí bodu $X$ platí:

\begin{equation}
\Delta X \;=\; J_{f} (\theta) \Delta \theta \;=\; 
 \begin{bmatrix}
  \frac{\partial f_1}{\partial \theta_1}  & \frac{\partial f_1}{\partial \theta_2} & \cdots & \frac{\partial f_1}{\partial \theta_n} \\[6pt]
  \frac{\partial f_2}{\partial \theta_1}  & \frac{\partial f_2}{\partial \theta_2} & \cdots & \frac{\partial f_2}{\partial \theta_n} \\[6pt]
  \vdots  & \vdots  & \ddots & \vdots  \\[6pt]
  \frac{\partial f_m}{\partial \theta_1}  & \frac{\partial f_m}{\partial \theta_2} & \cdots & \frac{\partial f_m}{\partial \theta_n} \\[6pt]
 \end{bmatrix} \;
 \begin{bmatrix}
  \Delta \theta_1 \\[6pt]
  \Delta \theta_2 \\[6pt]
  \vdots \\[6pt]
  \Delta \theta_n \\[6pt]
 \end{bmatrix}
\end{equation}

\begin{figure}[!htp]
\centering
\includegraphics[width=12cm]{img/ik-moznareseni.pdf}
\caption{Singulární případy. Vlevo dvě řešení, vpravo úloha neřešitelná.}
\label{fig:ik-moznareseni}
\end{figure}

Vektor $\Delta X$ je nám známý -- jedná se o~rozdíl požadované a~aktuální pozice. Výpočet změny $\Delta \theta$ lze formálně zapsat:

\begin{equation}
\Delta \theta \;=\; J_{f}(\theta)^{-1} \Delta X
\end{equation}

Matice $J_{f}$ zpravidla nebude čtvercová, klasická inverze tudíž ani nemůže být použita -- přikročíme k~tzv.~pseudoinverzi
obdélníkové matice, kterou můžeme realizovat například pomocí metody SVD (singular value decomposition) nebo vyjádřením (podle Mereditha a~Maddocka \cite{Meredith2004})

\begin{equation}
J_{f}(\theta)^{-1} = J_{f}(\theta)^T (J_{f}(\theta) J_{f}(\theta)^T)^{-1}.
\end{equation}

Místo pseudoinverze lze užít také transpozici jakobiánu, v~takovém případě realizujeme vlastně numerické
řešení vztahu metodou největšího spádu. Transpozice klade nižší nároky na výkon v~krocích iterace,
na druhou stranu výpočet ztrácí na numerické stabilitě.

Výsledný algoritmus tak či onak pracuje iterativně, dokud není dosaženo kýžené pozice koncového efektoru:
\begin{enumerate}
\item Vypočti jakobián $J_{f}(\theta)$.
\item Vypočti pseudoinverzi jakobiánu $J_{f}(\theta)^{-1}$.
\item Vypočti nové $\Delta X$ jako rozdíl cílové a~aktuální pozice koncového efektoru.
\item Vypočti změnu vnitřního stavu $\Delta \theta \;=\; J_{f}(\theta)^{-1} \Delta X$.
\item Přiřaď pro novou iteraci vnitřní stav $\theta_{+1} \;=\; \theta + \alpha \, \Delta \theta $, kde $\alpha$ představuje 
      řídicí konstantu rychlosti konvergence.
\end{enumerate}

\subsection{Cyclic coordinate descent}

Minimalizaci chyby, tj.~vzdálenosti koncového efektoru od jeho požadované pozice, lze realizovat také metodou cyclic coordinate descent (CCD).
Tento způsob řešení úlohy inverzní kinematiky navrhli Wang a~Chen \cite{Wang91}. 

Algoritmus stejně jako předchozí metoda pracuje iterativně, v~každé iteraci prochází hierarchii segmentů od koncového efektoru směrem ke kořenu. Každému segmentu přísluší kloub,
který ji propojuje s~nadřazeným segmentem. U~kloubu provedeme změnu hodnot popisující jeho konfiguraci, která minimalizuje vzdálenost koncového efektoru od zadané
pozice (viz obrázek \ref{fig:ik-ccd}). Lokální úprava poté končí a~přesuneme se k~nadřazenému segmentu. V~aplikacích dochází k~ukončení výpočtu po dosažení prahové hodnoty chyby
nebo po stanoveném počtu iterací, řešení totiž nemusí existovat a~uživatel by marně čekal na odezvu.

Metoda se vyznačuje poměrně rychlou konvergencí v~řádu stovek iterací, avšak hrozí uváznutí v~lokálním optimu. I~přes zavedení omezení pohybu v~kloubech může metoda vygenerovat
na rozdíl od výpočtu pomocí jakobiánu velmi nepřirozený pohyb. Toto chování je možné redukovat, pokud nedovolíme změnu hodnot představující konfiguraci kloubu větší než stanovený 
práh (v~takovém případě upravujeme konfiguraci o~prahovou hodnotu), 
což na druhou stranu může zpomalit konvergenci algoritmu. Oproti výpočtu pomocí jakobiánu se CCD vyznačuje jednodušší implementací.

\begin{figure}[!htp]
\centering
\includegraphics[width=14cm]{img/ik-ccd.pdf}
\caption{Ukázka jedné iterace metody cyclic coordinate descent.}
\label{fig:ik-ccd}
\end{figure}

\subsection{Obohacení o~posuvná spojení}

Posuvná spojení (také posuvné klouby, anglicky nazývané \textit{prismatic joints}) mimo jiné umožní realizovat
\textit{pružné segmenty} kostí. Tímto obohatíme segmentovou strukturu o~další stupeň volnosti.
Uvažme však, že po formální stránce zůstává celý výpočet stejný, můžeme použít výpočty popsané výše,
pokud zaneseme posuvná spojení do stavového vektoru $\theta$ náležícího segmentové struktuře.
To je možné například použitím DH-notace (Denavit–Hartenbergovy parametry \cite{Denavit1955}, popsáno např. v~\cite{Zara04}, podkapitola 18.2.2).