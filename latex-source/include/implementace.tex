% ------------------
\chapter{Implementace demonstrační aplikace}
% ------------------

Kapitola se zabývá implementací metody generování kostry a~následné interaktivní deformace modelu. Popisuje rozhodnutí, k~nimž došlo
během vývoje, specifikuje přesný formát vstupních dat a~zabývá se architekturou vzniklé aplikace.

Demonstrační aplikace je spouštěna jako konzolová aplikace, která posléze vytvoří okno pro vykreslování scény. Uživatel tak zadává vstup
skrze parametry programu a~následně (po dokončení preprocessingu) ovládá scénu interaktivně myší a~klávesnicí skrze vykreslovací okno.

% -------------------------------------------------------
\section{Použité knihovny a~jazyk}
\vspace{-0.5em}

Demonstrační aplikace byla naprogramována v~jazyce \texttt{C++} verze 2011 (\texttt{C++11}) a~závisí na frameworku \textit{VTK (Visualization Toolkit)} 
a~knihovnách \textit{libhungarian} a~\textit{Eigen}. Mimo tyto knihovny třetích stran je pro překlad vyžadována standardní knihovna \texttt{C++} opět verze 2011.

\textit{VTK} je open source projektem společnosti Kitware, která jej aktivně vyvíjí a~poskytuje k~němu podporu. Tento rozsáhlý framework
mimo jiné nabízí třídy umožňující vizualizace, zpracování obrazu, ale i~abstrakci cílové platformy. V~demonstrační aplikaci má na starosti
veškeré vykreslování obsahu, zpracování vstupů od uživatele a~pohyb ve scéně. Kitware poskytuje \textit{VTK} v~době psaní práce (2015) pod benevolentní 
BSD licencí.\footnote{Oficiální licenční informace lze nalézt na adrese \texttt{http://www.vtk.org/licensing/}.}

Knihovna \textit{libhungarian} poskytuje implementaci maďarské metody v~jazyce~\texttt{C}, řeší tedy již zmiňované optimální párování s~nejmenší ztrátou.
Jejím autorem je Cyrill Stachniss.

\textit{Eigen} je knihovna pro výpočty z~oblasti lineární algebry. Kromě operací s~maticemi a~vektory disponuje například řešením
soustav lineárních rovnic. Knihovna je zaštítěna licencí MPL2 \footnote{Lze nalézt na adrese \texttt{http://eigen.tuxfamily.org/index.php?title=Main\_Page\#License}.}.


% -------------------------------------------------------
\section{Formát vstupních dat}
\vspace{-0.5em}

Abychom ukázkovou aplikaci nezatěžovali dalšími pomocnými knihovnami, bylo nutné zvolit takový vstupní formát dat, jehož načítání je
podporováno již v~rámci frameworku \textit{VTK} nebo jeho vlastní implementace není obtížná. Druhým požadavkem je, abychom do použitého formátu
snadlo exportovali existující data, a~to opět existujícím řešením nebo vlastní implementací.

\subsection{Vzorová kostra}

Pro uchování vzorové kostry, která musí navíc uchovat omezení pohybu kloubech a~lokální trojhrany, bohužel nebyl nalezen vhodný 
a~zároveň jednoduchý formát. Kostra byla připravena v~modelovacím a~animačním programu Blender, který je naštěstí rozšiřitelný
uživatelskými skripty v~jazyce Python, pro něž poskytuje API v~podstatě k~veškeré své funkcionalitě 
\footnote{Viz Blender/Python Documentation:\\ \texttt{http://www.blender.org/api/blender\_python\_api\_2\_74\_release/}}. Díky
tomu mohl vzniknout exportovací skript do vlastního textového formátu, který obsahuje pouze námi vyžadované informace.

Soubor je uvozen nejprve číslem určujícím uložený počet kostí, které po něm následují. Každé kosti je dle pořadí přiřazen index,
přičemž začínáme číslem nula. Zapsané souřadnice a~matice jsou spadají do světových souřadnic, nejsou podřízeny jiné transformaci.
Hodnoty v~souboru jsou odděleny libovolným množstvím mezer, tabulátorů nebo odřádkování. Následuje strukturovaný popis formátu s~vysvětlivkami.

\begin{center}
    \begin{tabular}{l p{8.5cm}}
    %\hline
    \textbf{Hlavička} & \\[0.5em]
    \texttt{[počet kostí]} & Soubor je uvozen nejprve číslem určujícím uložený počet kostí, které po něm následují. \\[1.0em]
    %\hline
    \textbf{Pro každou kost} & \\[0.5em]
    \texttt{[index nadřazené kosti]} & Index kosti nadřazené v~hierarchii, $-1$ značí, že nadřazená vůči aktuální neexistuje. \\[0.5em]
    \end{tabular}
\end{center}
\begin{center}
    \begin{tabular}{l p{8.5cm}}
    \texttt{[headx] [heady] [headz]} & Souřadnice počátku kosti zapsané v~desetinných číslech. \\[0.5em]
    \texttt{[tailx] [taily] [tailz]} & Souřadnice konce kosti zapsané v~desetinných číslech. \\[0.5em]
    \texttt{[rotx-min] [rotx-max]} & Limity rotace kosti kolem osy~x v~radiánech. \\[0.5em]
    \texttt{[roty-min] [roty-max]} & Limity rotace kosti kolem osy~y v~radiánech. \\[0.5em]
    \texttt{[rotz-min] [rotz-max]} & Limity rotace kosti kolem osy~z v~radiánech. \\[0.5em]
    \texttt{[m11] [m12] [m13] [m14]} & Matice definující lokální prostor kosti. \\
    \texttt{[m21] [m22] [m23] [m24]} & \\
    \texttt{[m31] [m32] [m33] [m34]} & \\
    \texttt{[m41] [m42] [m43] [m44]} & \\[0.5em]
    %\hline
    \end{tabular}
\end{center}
\vspace{-0.2cm}

Limitace rotace kosti, která vlastně představuje omezení pohybu v~nadřazeném kloubu, určuje maximální rotaci kolem dané osy (procházející nadřazeným kloubem)
z~klidové pózy.

\vspace{-0.12cm}
\subsection{Trojúhelníková síť}
\vspace{-0.12cm}

Pro vstupní trojúhelníhovou síť reprezentující humanoida jsem zvolil formát \textit{Wavefront OBJ}. Mimo trojúhelníkové
sítě poskytuje uchovávání Bézierových plátů, přiřazení materiálů plochám atd. Aplikace nicméně předpokládá, že vstupní soubor obsahuje 
pouze trojúhelníky. Shrnující informace o~formátu lze nalézt například v~\cite{wav15}.

\vspace{-0.1cm}
Formát \textit{Wavefront OBJ} je podporován drtivou většinou programů pracujících s~trojrozměrnými modely, které jej dokáží importovat
i~exportovat. V~naší aplikaci jej snadno načteme užitím třídy \texttt{vtkOBJReader}, kterou poskytuje \textit{VTK}.

% -------------------------------------------------------
\section{Generování kostry}
\vspace{-0.2cm}

Kostra je v~programu reprezentována třídou \texttt{CSkeleton}, která dokáže vykonat nad kostrou potřebné úkony,
mimo jiné načíst ji ze souboru (využito u~referenční kostry) nebo přiřadit odpovídající vrcholy
prozatímní a~referenční kostry a~tak vytvořit výslednou animační kostru.

Tvorba prozatímní kostry (viz sekce \ref{sec:gen-proz-kostr}) byla naprogramována děděním abstraktní třídy \texttt{vtkPolyDataAlgorithm}, která poskytuje
možnost zpracování libovolných primitiv (přesněji vrcholů a~jejich spojení v~grafická primitiva). Dílčí
části procesu byly rozděleny do dvou tříd: \texttt{MeshLaplaceFilter} zajišťuje smršťování modelu užitím laplaceovského
vyhlazování, \texttt{EdgeCollapseFilter} provádí redukci geometrie a~závěrečnou úpravu pozic. 

Výsledná animační kostra, jak již bylo zmíněno, je vytvořena jednou z~metod třídy \texttt{CSkeleton} -- statickou metodou
\texttt{CSkeleton::Match}, která přebírá přes argumenty referenční a~prozatímní kostru. Mechanismus tvorby byl popsán
v~sekci \ref{sec:vytv-anim-kostr}.

% -------------------------------------------------------
\section{Ukázková aplikace}

V~rámci diplomové práce byla kromě generování animační kostry naimplementována také demostrační aplikace, která dovoluje vzniklou
kostru rozhýbat uživatelem, deformovat tak vstupní síť s~použitím mesh-skinningu a~výsledek zobrazovat. Načtení sítě ve formátu OBJ,
rendering a~zpracování vstupu byly realizovány pomocí tříd nabízených frameworkem VTK (\texttt{vtkPolyData}, \texttt{vtkPolyDataMapper}, \texttt{vtkActor} aj.). 
Znázornění pipeline se nachází na obrázku \ref{fig:impl-pipeline}.

\begin{figure}[!htp]
\centering
\includegraphics[width=\textwidth]{img/impl-pipeline.png}
\caption{Znázornění pipeline demonstrační aplikace. Šedě jsou vyznačeny části implementované VTK, bíle vlastní implementace.}
\label{fig:impl-pipeline}
\end{figure}

% -------------------------------------------------------
\subsection{Inverzní kinematika}
\vspace{-0.1cm}

Přikročíme k~interaktivnímu ovlivňování scény. Na pozice koncových efektorů umístíme pomocné prvky, jejichž přetažením dojde přepočítání konfigurace
kostry -- řešení úlohy inverzní kinematiky. Pomocné prvky jsou vybírány myší, pro výběr objektu ve scéně byla využita již naimplementovaná třída 
\texttt{vtkCellPicker} z~frameworku VTK.

Inverzní kinematika realizovaná pomocí CCD byla naimplementována v~pouhých několika funkcích, neboť se jedná o~demonstrační ovlivnění scény nevelkého rozsahu.
Funkce \texttt{ProcessIK}, po jejímž zavolání dojde k~výpočtu, má dva parametry: první určuje, který koncový efektor byl přetažen na jiné místo,
druhý je příznak, jestli jsou kosti pevné či pružné.

% -------------------------------------------------------
\vspace{-0.1cm}
\subsection{Mesh-skinning}
\vspace{-0.1cm}

V~demonstrační aplikaci je skinning realizován jako třída \texttt{CSkinnedMesh}, která si udržuje stav kostry v~klidové a~současné póze 
a~trojúhelníkovou síť v~klidové póze. Tato data jsou uchována v~podobě hluboké kopie, což umožňuje připravovat další snímek paralelně.
Třída dědí od \texttt{vtkPolyDataAlgorithm}, jedná se o~modul poskytovaný frameworkem VTK, který disponuje vstupními a~výstupními datovými porty. 
Díky tomu dokáže předat výslednou síť standardní cestou k~dalšímu zpracování nebo vizualizaci. Při změně konfigurace kostry dochází
pouze ke změně pozic vrcholů, indexace sítě zůstává zachována.

% -------------------------------------------------------

\subsection{Nastavení}
\vspace{-0.1cm}

Nastavení kostant a~názvů souborů používaných aplikací je shomážděno centrálně ve třídě \texttt{TAppSettings} včetně přednastavených
hodnot, které byly určeny empiricky na základě výsledků testování. \texttt{TAppSettings} je naimplementována jako struktura,
což způsobuje, že všechny její atributy jsou defaultně veřejné.

Po spuštění aplikace nic nebrání dodatečné úpravě hodnot například parametry předanými skrze příkazovou řádku. Přesně tak lze měnit 
vybrané konstanty v~demonstrační aplikaci.
