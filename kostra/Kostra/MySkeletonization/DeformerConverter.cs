using System;
using System.ComponentModel;
using System.Globalization;
namespace IsolineEditing
{
	public class DeformerConverter : ExpandableObjectConverter
	{
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(Deformer) || base.CanConvertTo(context, destinationType);
		}
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof(string) && value is Deformer)
			{
				Deformer deformer = (Deformer)value;
				return deformer.ToString();
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}
	}
}
