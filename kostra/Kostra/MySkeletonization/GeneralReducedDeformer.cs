using CsGL.OpenGL;
using MyGeometry;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
namespace IsolineEditing
{
	public class GeneralReducedDeformer : Deformer, IDisposable
	{
		public enum EnumDeformMethod
		{
			Standard,
			Reduced
		}
		public class Option
		{
			private double handleConstraintScale = 100.0;
			private double transformationConstraintScale = 100.0;
			private double transformationConstraintScale2 = 0.01;
			private double segmentLengthConstraintScale = 0.1;
			private bool createStandardSolver;
			public double HandleConstraintScale
			{
				get
				{
					return this.handleConstraintScale;
				}
				set
				{
					this.handleConstraintScale = value;
				}
			}
			public double TransformationConstraintScale
			{
				get
				{
					return this.transformationConstraintScale;
				}
				set
				{
					this.transformationConstraintScale = value;
				}
			}
			public double TransformationConstraintScale2
			{
				get
				{
					return this.transformationConstraintScale2;
				}
				set
				{
					this.transformationConstraintScale2 = value;
				}
			}
			public double SegmentLengthConstraintScale
			{
				get
				{
					return this.segmentLengthConstraintScale;
				}
				set
				{
					this.segmentLengthConstraintScale = value;
				}
			}
			public bool CreateStandardSolver
			{
				get
				{
					return this.createStandardSolver;
				}
				set
				{
					this.createStandardSolver = value;
				}
			}
		}
		private Mesh mesh;
		private MeshSegmentation seg;
		private GeneralReducedDeformer.Option opt;
		private int nConstrainedVertices;
		private double[] old_pos;
		private double[] old_normal;
		private double[] x;
		private double[][] b;
		private double[][] old_b;
		private double[] h;
		private double[] ATb;
		private SparseMatrix A;
		private CCSMatrix ccsA;
		private CCSMatrix ccsATA;
		private unsafe void* solver = null;
		private GeneralReducedDeformer.EnumDeformMethod deformMethod = GeneralReducedDeformer.EnumDeformMethod.Reduced;
		public bool displaySegmentation;
		public bool displaySkeleton;
		private SparseMatrix R_pos;
		private CCSMatrix ccsR_pos;
		private double[][] tran_pos;
		private SparseMatrix R_lap;
		private CCSMatrix ccsR_lap;
		private double[][] tran_lap;
		private SparseMatrix AR_pos;
		private CCSMatrix ccsAR_pos;
		private unsafe void* reducedSolver = null;
		private double[] RATb;
		private double[] oldSegLength;
		private bool[] constraintedNode;
		private Vector3d[] nodePosition;
		private int[] segmentPair;
		public GeneralReducedDeformer.EnumDeformMethod DeformMethod
		{
			get
			{
				return this.deformMethod;
			}
			set
			{
				this.deformMethod = value;
			}
		}
		public bool DisplaySegmentation
		{
			get
			{
				return this.displaySegmentation;
			}
			set
			{
				this.displaySegmentation = value;
			}
		}
		public bool DisplaySkeleton
		{
			get
			{
				return this.displaySkeleton;
			}
			set
			{
				this.displaySkeleton = value;
			}
		}
		[DllImport("taucs.dll")]
		private unsafe static extern void* CreaterCholeskySolver(int n, int nnz, int* rowIndex, int* colIndex, double* value);
		[DllImport("taucs.dll")]
		private unsafe static extern int Solve(void* solver, double* x, double* b);
		[DllImport("taucs.dll")]
		private unsafe static extern double SolveEx(void* solver, double* x, int xIndex, double* b, int bIndex);
		[DllImport("taucs.dll")]
		private unsafe static extern int FreeSolver(void* sp);
		public unsafe GeneralReducedDeformer(Mesh mesh, MeshSegmentation seg, GeneralReducedDeformer.Option opt)
		{
			Program.PrintText("Create general reduced deformer");
			int vn = mesh.VertexCount;
			int fn = mesh.FaceCount;
			int sn = seg.Segments.Length;
			Program.PrintText("...Allocate memeory");
			this.mesh = mesh;
			this.seg = seg;
			this.opt = opt;
			this.old_pos = (mesh.VertexPos.Clone() as double[]);
			this.old_normal = (mesh.VertexNormal.Clone() as double[]);
			this.x = new double[vn];
			this.b = new double[3][];
			this.old_b = new double[3][];
			this.h = new double[fn];
			this.ATb = new double[vn];
			this.nodePosition = new Vector3d[sn];
			this.nConstrainedVertices = 0;
			for (int i = 0; i < sn; i++)
			{
				this.nodePosition[i] = seg.NodePosition[i];
			}
			for (int j = 0; j < vn; j++)
			{
				if (mesh.Flag[j] != 0)
				{
					this.nConstrainedVertices++;
				}
			}
			Program.PrintText("...Build matrix L");
			this.A = this.BuildMatrixDL();
			this.AddHandleConstraint(this.A);
			this.ccsA = new CCSMatrix(this.A);
			Program.PrintText("...Build standard solver");
			if (opt.CreateStandardSolver)
			{
				this.ccsATA = this.MultiplyATA(this.ccsA);
				this.solver = this.Factorization(this.ccsATA);
				if (this.solver == null)
				{
					throw new Exception();
				}
			}
			Program.PrintText("...Init Laplacian coordinates");
			for (int k = 0; k < 3; k++)
			{
				this.old_b[k] = new double[fn];
				this.b[k] = new double[this.ccsA.RowSize + seg.Segments.Length * 3];
				for (int l = 0; l < this.b[k].Length; l++)
				{
					this.b[k][l] = 0.0;
				}
				int m = 0;
				int n = 0;
				while (m < vn)
				{
					this.x[m] = mesh.VertexPos[n + k];
					m++;
					n += 3;
				}
				this.ccsA.Multiply(this.x, this.b[k]);
				for (int j2 = 0; j2 < fn; j2++)
				{
					this.old_b[k][j2] = this.b[k][j2];
				}
			}
			Program.PrintText("...Build matrix R_pos");
			this.R_pos = this.BuildMatrixR_pos();
			this.ccsR_pos = new CCSMatrix(this.R_pos);
			this.tran_pos = new double[3][];
			for (int i2 = 0; i2 < 3; i2++)
			{
				this.tran_pos[i2] = new double[sn * 4];
			}
			Program.PrintText("...Build matrix R_lap");
			this.R_lap = this.BuildMatrixR_lap();
			this.ccsR_lap = new CCSMatrix(this.R_lap);
			this.tran_lap = new double[3][];
			for (int i3 = 0; i3 < 3; i3++)
			{
				this.tran_lap[i3] = new double[sn * 3];
				int j3 = 0;
				int k2 = i3;
				while (j3 < sn)
				{
					this.tran_lap[i3][k2] = 1.0;
					j3++;
					k2 += 3;
				}
			}
			Program.PrintText("...Build matrix AR_pos");
			this.AR_pos = this.BuildMatrixAR_pos(this.A, this.R_pos);
			this.segmentPair = this.CreateSegmentPair();
			this.AddSegmentPairConstraint(this.AR_pos, this.segmentPair);
			this.constraintedNode = this.AddTransformationConstraint(this.AR_pos);
			Program.PrintText("...Build reduced solver");
			this.ccsAR_pos = new CCSMatrix(this.AR_pos);
			this.RATb = new double[this.AR_pos.ColumnSize];
			this.reducedSolver = this.Factorization(this.MultiplyATA(this.ccsAR_pos));
			if (this.reducedSolver == null)
			{
				throw new Exception();
			}
			Program.PrintText("...Init b");
			for (int i4 = 0; i4 < 3; i4++)
			{
				this.b[i4] = new double[this.AR_pos.RowSize];
				for (int j4 = 0; j4 < this.b[i4].Length; j4++)
				{
					this.b[i4][j4] = 0.0;
				}
				for (int j5 = 0; j5 < fn; j5++)
				{
					this.old_b[i4][j5] = this.b[i4][j5];
				}
				int j6 = 0;
				int k3 = fn + this.nConstrainedVertices;
				while (j6 < this.segmentPair.Length)
				{
					Vector3d u = seg.NodePosition[this.segmentPair[j6]];
					Vector3d u2 = seg.NodePosition[this.segmentPair[j6 + 1]];
					this.b[i4][k3++] = (u[i4] - u2[i4]) * opt.SegmentLengthConstraintScale;
					j6 += 2;
				}
			}
			this.Move();
			List<double> len = new List<double>();
			for (int i5 = 0; i5 < this.segmentPair.Length; i5 += 2)
			{
				Vector3d u3 = seg.NodePosition[this.segmentPair[i5]];
				Vector3d u4 = seg.NodePosition[this.segmentPair[i5 + 1]];
				len.Add((u3 - u4).Length());
			}
			this.oldSegLength = len.ToArray();
			Program.PrintText("...Done");
		}
		~GeneralReducedDeformer()
		{
			this.Dispose();
		}
		private SparseMatrix BuildMatrixDL()
		{
			int vn = this.mesh.VertexCount;
			int fn = this.mesh.FaceCount;
			SparseMatrix L = new SparseMatrix(fn, vn);
			for (int i = 0; i < fn; i++)
			{
				int f = this.mesh.AdjFF[i][0];
				int f2 = this.mesh.AdjFF[i][1];
				int f3 = this.mesh.AdjFF[i][2];
				Vector3d dv = this.mesh.ComputeDualPosition(i);
				Vector3d dv2 = this.mesh.ComputeDualPosition(f);
				Vector3d dv3 = this.mesh.ComputeDualPosition(f2);
				Vector3d dv4 = this.mesh.ComputeDualPosition(f3);
				Vector3d u = dv - dv4;
				Vector3d v = dv2 - dv4;
				Vector3d v2 = dv3 - dv4;
				Vector3d normal = v.Cross(v2).Normalize();
				Matrix3d M = new Matrix3d(v, v2, normal);
				Vector3d coord = M.Inverse() * u;
				double alpha = 0.33333333333333331;
				int j = 0;
				int k = i * 3;
				while (j < 3)
				{
					L.AddValueTo(i, this.mesh.FaceIndex[k++], alpha);
					j++;
				}
				alpha = coord[0] / 3.0;
				int l = 0;
				int m = f * 3;
				while (l < 3)
				{
					L.AddValueTo(i, this.mesh.FaceIndex[m++], -alpha);
					l++;
				}
				alpha = coord[1] / 3.0;
				int n = 0;
				int k2 = f2 * 3;
				while (n < 3)
				{
					L.AddValueTo(i, this.mesh.FaceIndex[k2++], -alpha);
					n++;
				}
				alpha = (1.0 - coord[0] - coord[1]) / 3.0;
				int j2 = 0;
				int k3 = f3 * 3;
				while (j2 < 3)
				{
					L.AddValueTo(i, this.mesh.FaceIndex[k3++], -alpha);
					j2++;
				}
				this.h[i] = coord.z;
			}
			L.SortElement();
			return L;
		}
		private void AddHandleConstraint(SparseMatrix A)
		{
			for (int i = 0; i < this.mesh.VertexCount; i++)
			{
				if (this.mesh.Flag[i] != 0)
				{
					A.AddRow();
					A.AddElement(A.RowSize - 1, i, this.opt.HandleConstraintScale);
				}
			}
			A.SortElement();
		}
		private CCSMatrix MultiplyATA(CCSMatrix A)
		{
			int[] last = new int[A.RowSize];
			int[] next = new int[A.NumNonZero];
			int[] colIndex = new int[A.NumNonZero];
			for (int i = 0; i < last.Length; i++)
			{
				last[i] = -1;
			}
			for (int j = 0; j < next.Length; j++)
			{
				next[j] = -1;
			}
			for (int k = 0; k < A.ColumnSize; k++)
			{
				for (int l = A.ColIndex[k]; l < A.ColIndex[k + 1]; l++)
				{
					int m = A.RowIndex[l];
					if (last[m] != -1)
					{
						next[last[m]] = l;
					}
					last[m] = l;
					colIndex[l] = k;
				}
			}
			CCSMatrix ATA = new CCSMatrix(A.ColumnSize, A.ColumnSize);
			Set<int> set = new Set<int>();
			double[] tmp = new double[A.ColumnSize];
			List<int> ATA_RowIndex = new List<int>();
			List<double> ATA_Value = new List<double>();
			for (int n = 0; n < A.ColumnSize; n++)
			{
				tmp[n] = 0.0;
			}
			for (int j2 = 0; j2 < A.ColumnSize; j2++)
			{
				for (int col = A.ColIndex[j2]; col < A.ColIndex[j2 + 1]; col++)
				{
					int arg_122_0 = A.RowIndex[col];
					double val = A.Values[col];
					int curr = col;
					while (true)
					{
						int i2 = colIndex[curr];
						set.Add(i2);
						tmp[i2] += val * A.Values[curr];
						if (next[curr] == -1)
						{
							break;
						}
						curr = next[curr];
					}
				}
				int[] s = set.ToArray();
				Array.Sort<int>(s);
				int count = 0;
				int[] array = s;
				for (int num = 0; num < array.Length; num++)
				{
					int k2 = array[num];
					if (tmp[k2] != 0.0)
					{
						ATA_RowIndex.Add(k2);
						ATA_Value.Add(tmp[k2]);
						tmp[k2] = 0.0;
						count++;
					}
				}
				ATA.ColIndex[j2 + 1] = ATA.ColIndex[j2] + count;
				set.Clear();
			}
			ATA.RowIndex = ATA_RowIndex.ToArray();
			ATA.Values = ATA_Value.ToArray();
			return ATA;
		}
		private unsafe void* Factorization(CCSMatrix C)
		{
			fixed (int* ri = C.RowIndex)
			{
				fixed (int* ci = C.ColIndex)
				{
					fixed (double* val = C.Values)
					{
						return GeneralReducedDeformer.CreaterCholeskySolver(C.ColumnSize, C.NumNonZero, ri, ci, val);
					}
				}
			}
		}
		private SparseMatrix BuildMatrixR_pos()
		{
			int vn = this.mesh.VertexCount;
			int arg_17_0 = this.mesh.FaceCount;
			int sn = this.seg.Segments.Length * 4;
			SparseMatrix R = new SparseMatrix(vn, sn);
			int i = 0;
			int j = 0;
			while (i < vn)
			{
				int segIndex = this.seg.SegmentIndex[i] - 1;
				int baseIndex = segIndex * 4;
				Vector3d s = this.seg.NodePosition[segIndex];
				Vector3d v = new Vector3d(this.mesh.VertexPos, j);
				double minDis = 1.7976931348623157E+308;
				double minRatio = 0.0;
				int minIndex = -1;
				foreach (int adj in this.seg.AdjIndex[segIndex])
				{
					int segIndex2 = adj - 1;
					Vector3d s2 = this.seg.NodePosition[segIndex2];
					Vector3d s3 = s2 - s;
					double a = (v - s).Dot(s3) / s3.Dot(s3);
					if (a >= 0.0 && a <= 1.0)
					{
						double dis = (v - (s * (1.0 - a) + s2 * a)).Length();
						if (dis < minDis)
						{
							minDis = dis;
							minRatio = 1.0 - a;
							minIndex = segIndex2;
						}
					}
				}
				if (minIndex != -1)
				{
					int baseIndex2 = minIndex * 4;
					double r = minRatio;
					double r2 = 1.0 - r;
					R.AddValueTo(i, baseIndex, v.x * r);
					R.AddValueTo(i, baseIndex + 1, v.y * r);
					R.AddValueTo(i, baseIndex + 2, v.z * r);
					R.AddValueTo(i, baseIndex + 3, r);
					R.AddValueTo(i, baseIndex2, v.x * r2);
					R.AddValueTo(i, baseIndex2 + 1, v.y * r2);
					R.AddValueTo(i, baseIndex2 + 2, v.z * r2);
					R.AddValueTo(i, baseIndex2 + 3, r2);
				}
				else
				{
					R.AddElement(i, baseIndex, v.x);
					R.AddElement(i, baseIndex + 1, v.y);
					R.AddElement(i, baseIndex + 2, v.z);
					R.AddElement(i, baseIndex + 3, 1.0);
				}
				i++;
				j += 3;
			}
			R.SortElement();
			return R;
		}
		private SparseMatrix BuildMatrixR_pos2()
		{
			int vn = this.mesh.VertexCount;
			int arg_17_0 = this.mesh.FaceCount;
			int tn = this.seg.Segments.Length * 4;
			SparseMatrix R = new SparseMatrix(vn, tn);
			int i = 0;
			int j = 0;
			while (i < vn)
			{
				int segIndex = this.seg.SegmentIndex[i] - 1;
				int baseIndex = segIndex * 4;
				Vector3d v = new Vector3d(this.mesh.VertexPos, j);
				R.AddElement(i, baseIndex, v.x);
				R.AddElement(i, baseIndex + 1, v.y);
				R.AddElement(i, baseIndex + 2, v.z);
				R.AddElement(i, baseIndex + 3, 1.0);
				i++;
				j += 3;
			}
			R.SortElement();
			return R;
		}
		private SparseMatrix BuildMatrixR_lap()
		{
			int arg_0B_0 = this.mesh.VertexCount;
			int fn = this.mesh.FaceCount;
			int tn = this.seg.Segments.Length * 3;
			SparseMatrix R = new SparseMatrix(fn, tn);
			int i = 0;
			int j = 0;
			while (i < fn)
			{
				int c = this.mesh.FaceIndex[j];
				int c2 = this.mesh.FaceIndex[j + 1];
				int c3 = this.mesh.FaceIndex[j + 2];
				int s = (this.seg.SegmentIndex[c] - 1) * 3;
				int s2 = (this.seg.SegmentIndex[c2] - 1) * 3;
				int s3 = (this.seg.SegmentIndex[c3] - 1) * 3;
				double lap_x = this.old_b[0][i] / 3.0;
				double lap_y = this.old_b[1][i] / 3.0;
				double lap_z = this.old_b[2][i] / 3.0;
				R.AddElement(i, s, lap_x);
				R.AddElement(i, s + 1, lap_y);
				R.AddElement(i, s + 2, lap_z);
				R.AddValueTo(i, s2, lap_x);
				R.AddValueTo(i, s2 + 1, lap_y);
				R.AddValueTo(i, s2 + 2, lap_z);
				R.AddValueTo(i, s3, lap_x);
				R.AddValueTo(i, s3 + 1, lap_y);
				R.AddValueTo(i, s3 + 2, lap_z);
				i++;
				j += 3;
			}
			R.SortElement();
			return R;
		}
		private SparseMatrix BuildMatrixAR_pos(SparseMatrix A, SparseMatrix R_pos)
		{
			SparseMatrix M = new SparseMatrix(A.RowSize, R_pos.ColumnSize);
			double[] tmp = new double[R_pos.ColumnSize];
			Set<int> marked = new Set<int>();
			for (int i = 0; i < A.RowSize; i++)
			{
				marked.Clear();
				for (int j = 0; j < tmp.Length; j++)
				{
					tmp[j] = 0.0;
				}
				List<SparseMatrix.Element> rr = A.Rows[i];
				foreach (SparseMatrix.Element e in rr)
				{
					List<SparseMatrix.Element> rr2 = R_pos.Rows[e.j];
					foreach (SparseMatrix.Element e2 in rr2)
					{
						tmp[e2.j] += e.value * e2.value;
						marked.Add(e2.j);
					}
				}
				foreach (int index in marked)
				{
					if (tmp[index] != 0.0)
					{
						M.AddElement(i, index, tmp[index]);
					}
				}
			}
			M.SortElement();
			return M;
		}
		private SparseMatrix Multiply(SparseMatrix A1, SparseMatrix A2)
		{
			SparseMatrix M = new SparseMatrix(A1.RowSize, A2.ColumnSize);
			double[] tmp = new double[A2.ColumnSize];
			Set<int> marked = new Set<int>();
			for (int i = 0; i < tmp.Length; i++)
			{
				tmp[i] = 0.0;
			}
			for (int j = 0; j < A1.RowSize; j++)
			{
				marked.Clear();
				List<SparseMatrix.Element> rr = A1.Rows[j];
				foreach (SparseMatrix.Element e in rr)
				{
					List<SparseMatrix.Element> rr2 = A2.Rows[e.j];
					foreach (SparseMatrix.Element e2 in rr2)
					{
						tmp[e2.j] += e.value * e2.value;
						marked.Add(e2.j);
					}
				}
				foreach (int index in marked)
				{
					if (tmp[index] != 0.0)
					{
						M.AddElement(j, index, tmp[index]);
						tmp[index] = 0.0;
					}
				}
			}
			M.SortElement();
			return M;
		}
		private bool[] AddTransformationConstraint(SparseMatrix A)
		{
			int vn = this.mesh.VertexCount;
			int sn = this.seg.Segments.Length;
			bool[] containHandle = new bool[sn];
			bool[] constraintedNode = new bool[sn];
			for (int i = 0; i < sn; i++)
			{
				containHandle[i] = (constraintedNode[i] = false);
			}
			for (int j = 0; j < vn; j++)
			{
				if (this.mesh.Flag[j] != 0)
				{
					containHandle[this.seg.SegmentIndex[j] - 1] = true;
				}
			}
			bool needContinue = false;
			do
			{
				needContinue = false;
				for (int k = 0; k < sn; k++)
				{
					if (!containHandle[k] && !constraintedNode[k])
					{
						int nConstrained = 0;
						foreach (int adj in this.seg.AdjIndex[k])
						{
							if (constraintedNode[adj - 1])
							{
								nConstrained++;
							}
						}
						if (this.seg.AdjIndex[k].Count - nConstrained == 1)
						{
							Set<int> markedSegments = new Set<int>();
							int currSegment = k;
							int currAdjCount = this.seg.AdjIndex[k].Count - nConstrained;
							bool found = false;
							do
							{
								found = false;
								markedSegments.Add(currSegment);
								constraintedNode[currSegment] = true;
								foreach (int adj2 in this.seg.AdjIndex[currSegment])
								{
									if (!containHandle[adj2 - 1] && !constraintedNode[adj2 - 1])
									{
										nConstrained = 0;
										foreach (int adj3 in this.seg.AdjIndex[adj2 - 1])
										{
											if (constraintedNode[adj3 - 1])
											{
												nConstrained++;
											}
										}
										currAdjCount = this.seg.AdjIndex[adj2 - 1].Count - nConstrained;
										currSegment = adj2 - 1;
										found = true;
									}
								}
							}
							while (found && currAdjCount == 1);
							if (markedSegments.Count > 0)
							{
								needContinue = true;
								foreach (int s in markedSegments)
								{
									constraintedNode[s] = true;
									int baseIndex = s * 4;
									int baseIndex2 = currSegment * 4;
									A.AddRow();
									A.AddValueTo(A.RowSize - 1, baseIndex, this.opt.TransformationConstraintScale);
									A.AddValueTo(A.RowSize - 1, baseIndex2, -this.opt.TransformationConstraintScale);
									A.AddRow();
									A.AddValueTo(A.RowSize - 1, baseIndex + 1, this.opt.TransformationConstraintScale);
									A.AddValueTo(A.RowSize - 1, baseIndex2 + 1, -this.opt.TransformationConstraintScale);
									A.AddRow();
									A.AddValueTo(A.RowSize - 1, baseIndex + 2, this.opt.TransformationConstraintScale);
									A.AddValueTo(A.RowSize - 1, baseIndex2 + 2, -this.opt.TransformationConstraintScale);
									A.AddRow();
									A.AddValueTo(A.RowSize - 1, baseIndex + 3, this.opt.TransformationConstraintScale);
									A.AddValueTo(A.RowSize - 1, baseIndex2 + 3, -this.opt.TransformationConstraintScale);
								}
							}
						}
					}
				}
			}
			while (needContinue);
			for (int l = 0; l < sn; l++)
			{
				foreach (int adj4 in this.seg.AdjIndex[l])
				{
					int baseIndex3 = l * 4;
					int baseIndex4 = (adj4 - 1) * 4;
					A.AddRow();
					A.AddValueTo(A.RowSize - 1, baseIndex3, this.opt.TransformationConstraintScale2);
					A.AddValueTo(A.RowSize - 1, baseIndex4, -this.opt.TransformationConstraintScale2);
					A.AddRow();
					A.AddValueTo(A.RowSize - 1, baseIndex3 + 1, this.opt.TransformationConstraintScale2);
					A.AddValueTo(A.RowSize - 1, baseIndex4 + 1, -this.opt.TransformationConstraintScale2);
					A.AddRow();
					A.AddValueTo(A.RowSize - 1, baseIndex3 + 2, this.opt.TransformationConstraintScale2);
					A.AddValueTo(A.RowSize - 1, baseIndex4 + 2, -this.opt.TransformationConstraintScale2);
				}
			}
			return constraintedNode;
		}
		private void AddSegmentLengthConstraint(SparseMatrix A)
		{
			int arg_0B_0 = this.mesh.VertexCount;
			int sn = this.seg.Segments.Length;
			for (int i = 0; i < sn; i++)
			{
				foreach (int adj in this.seg.AdjIndex[i])
				{
					int s = i * 4;
					int s2 = (adj - 1) * 4;
					int r = A.RowSize;
					Vector3d u = this.seg.NodePosition[i];
					Vector3d u2 = this.seg.NodePosition[adj - 1];
					A.AddRow();
					A.AddElement(r, s, u.x * this.opt.SegmentLengthConstraintScale);
					A.AddElement(r, s + 1, u.y * this.opt.SegmentLengthConstraintScale);
					A.AddElement(r, s + 2, u.z * this.opt.SegmentLengthConstraintScale);
					A.AddElement(r, s + 3, this.opt.SegmentLengthConstraintScale);
					A.AddElement(r, s2, -u2.x * this.opt.SegmentLengthConstraintScale);
					A.AddElement(r, s2 + 1, -u2.y * this.opt.SegmentLengthConstraintScale);
					A.AddElement(r, s2 + 2, -u2.z * this.opt.SegmentLengthConstraintScale);
					A.AddElement(r, s2 + 3, -this.opt.SegmentLengthConstraintScale);
				}
			}
		}
		private void AddSegmentPairConstraint(SparseMatrix A, int[] pairs)
		{
			int arg_0B_0 = this.mesh.VertexCount;
			int arg_19_0 = this.seg.Segments.Length;
			for (int i = 0; i < pairs.Length; i += 2)
			{
				int s = pairs[i] * 4;
				int s2 = pairs[i + 1] * 4;
				int r = A.RowSize;
				Vector3d u = this.seg.NodePosition[pairs[i]];
				Vector3d u2 = this.seg.NodePosition[pairs[i + 1]];
				A.AddRow();
				A.AddElement(r, s, u.x * this.opt.SegmentLengthConstraintScale);
				A.AddElement(r, s + 1, u.y * this.opt.SegmentLengthConstraintScale);
				A.AddElement(r, s + 2, u.z * this.opt.SegmentLengthConstraintScale);
				A.AddElement(r, s + 3, this.opt.SegmentLengthConstraintScale);
				A.AddElement(r, s2, -u2.x * this.opt.SegmentLengthConstraintScale);
				A.AddElement(r, s2 + 1, -u2.y * this.opt.SegmentLengthConstraintScale);
				A.AddElement(r, s2 + 2, -u2.z * this.opt.SegmentLengthConstraintScale);
				A.AddElement(r, s2 + 3, -this.opt.SegmentLengthConstraintScale);
			}
		}
		private int[] CreateSegmentPair()
		{
			int sn = this.seg.Segments.Length;
			List<int> pairs = new List<int>();
			for (int i = 0; i < sn; i++)
			{
				int[] adjIndex = this.seg.AdjIndex[i].ToArray();
				Array.Sort<int>(adjIndex);
				for (int j = 0; j < adjIndex.Length; j++)
				{
					for (int k = j + 1; k < adjIndex.Length; k++)
					{
						pairs.Add(adjIndex[j] - 1);
						pairs.Add(adjIndex[k] - 1);
					}
				}
				foreach (int adj in this.seg.AdjIndex[i])
				{
					if (i < adj - 1)
					{
						pairs.Add(i);
						pairs.Add(adj - 1);
					}
				}
			}
			return pairs.ToArray();
		}
		private void Update_Standard()
		{
			int fn = this.mesh.FaceCount;
			for (int i = 0; i < fn; i++)
			{
				int f = this.mesh.AdjFF[i][0];
				int f2 = this.mesh.AdjFF[i][1];
				int f3 = this.mesh.AdjFF[i][2];
				Vector3d p = new Vector3d(this.mesh.DualVertexPos, f * 3);
				Vector3d p2 = new Vector3d(this.mesh.DualVertexPos, f2 * 3);
				Vector3d p3 = new Vector3d(this.mesh.DualVertexPos, f3 * 3);
				Vector3d normal = (p - p3).Cross(p2 - p3).Normalize() * this.h[i];
				this.b[0][i] = normal.x;
				this.b[1][i] = normal.y;
				this.b[2][i] = normal.z;
			}
		}
		private void Update_Recuded()
		{
			int i = 0;
			int j = 0;
			while (i < this.seg.Segments.Length)
			{
				List<int> s = this.seg.Segments[i];
				Matrix3d U = new Matrix3d();
				Matrix3d V = new Matrix3d();
				foreach (int index in s)
				{
					int k = index * 3;
					Vector3d u = new Vector3d(this.old_normal, k);
					Vector3d v = new Vector3d(this.mesh.VertexNormal, k);
					U += u.OuterCross(u);
					V += v.OuterCross(u);
				}
				Matrix3d T = V * U.Inverse();
				T = T.OrthogonalFactor(1E-06);
				for (int l = 0; l < 3; l++)
				{
					this.tran_lap[l][j] = T[l, 0];
					this.tran_lap[l][j + 1] = T[l, 1];
					this.tran_lap[l][j + 2] = T[l, 2];
				}
				i++;
				j += 3;
			}
		}
		private unsafe void Deform_Standard()
		{
			int vn = this.mesh.VertexCount;
			for (int i = 0; i < 3; i++)
			{
				this.ccsA.PreMultiply(this.b[i], this.ATb);
				fixed (double* _x = this.x, _ATb = this.ATb)
				{
					GeneralReducedDeformer.Solve(this.solver, _x, _ATb);
				}
				int j = 0;
				int j2 = i;
				while (j < vn)
				{
					if (this.mesh.Flag[j] == 0)
					{
						this.mesh.VertexPos[j2] = this.x[j];
					}
					j++;
					j2 += 3;
				}
			}
			this.mesh.ComputeDualPosition();
			this.mesh.ComputeFaceNormal();
			this.mesh.ComputeVertexNormal();
		}
		private unsafe void Deform_Recuded()
		{
			int vn = this.mesh.VertexCount;
			int fn = this.mesh.FaceCount;
			int sn = this.seg.Segments.Length;
			int arg_33_0 = this.seg.Segments.Length;
			List<Vector3d> newSegDiff = new List<Vector3d>();
			int i = 0;
			int j = 0;
			while (i < this.segmentPair.Length)
			{
				Vector3d u = this.nodePosition[this.segmentPair[i]];
				Vector3d u2 = this.nodePosition[this.segmentPair[i + 1]];
				newSegDiff.Add((u - u2).Normalize() * this.oldSegLength[j]);
				i += 2;
				j++;
			}
			for (int k = 0; k < 3; k++)
			{
				this.ccsR_lap.Multiply(this.tran_lap[k], this.b[k]);
				int l = 0;
				int m = fn + this.nConstrainedVertices;
				while (l < this.segmentPair.Length / 2)
				{
					this.b[k][m++] = newSegDiff[l][k] * this.opt.SegmentLengthConstraintScale;
					l++;
				}
				this.ccsAR_pos.PreMultiply(this.b[k], this.RATb);
				fixed (double* _x = this.tran_pos[k], _RATb = this.RATb)
				{
					GeneralReducedDeformer.Solve(this.reducedSolver, _x, _RATb);
				}
			}
			for (int n = 0; n < 3; n++)
			{
				this.ccsR_pos.Multiply(this.tran_pos[n], this.x);
				int j2 = 0;
				int k2 = 0;
				while (j2 < sn)
				{
					this.nodePosition[j2][n] = this.tran_pos[n][k2] * this.seg.NodePosition[j2].x + this.tran_pos[n][k2 + 1] * this.seg.NodePosition[j2].y + this.tran_pos[n][k2 + 2] * this.seg.NodePosition[j2].z + this.tran_pos[n][k2 + 3];
					j2++;
					k2 += 4;
				}
				int j3 = 0;
				int j4 = n;
				while (j3 < vn)
				{
					if (this.mesh.Flag[j3] == 0)
					{
						this.mesh.VertexPos[j4] = this.x[j3];
					}
					j3++;
					j4 += 3;
				}
			}
			this.mesh.ComputeDualPosition();
			this.mesh.ComputeFaceNormal();
			this.mesh.ComputeVertexNormal();
		}
		private unsafe void DrawSegmentation()
		{
			OpenGL.glPointSize(4f);
			OpenGL.glEnableClientState(32884u);
			fixed (double* vp = this.mesh.VertexPos)
			{
				OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
				OpenGL.glBegin(0u);
				int count = 0;
				List<int>[] segments = this.seg.Segments;
				for (int j = 0; j < segments.Length; j++)
				{
					List<int> s = segments[j];
					switch (count % 6)
					{
					case 0:
						OpenGL.glColor3f(1f, 1f, 0f);
						break;
					case 1:
						OpenGL.glColor3f(1f, 0f, 0f);
						break;
					case 2:
						OpenGL.glColor3f(0f, 1f, 0f);
						break;
					case 3:
						OpenGL.glColor3f(0f, 0f, 1f);
						break;
					case 4:
						OpenGL.glColor3f(0f, 1f, 1f);
						break;
					case 5:
						OpenGL.glColor3f(1f, 0f, 1f);
						break;
					}
					count++;
					foreach (int i in s)
					{
						OpenGL.glArrayElement(i);
					}
				}
				OpenGL.glEnd();
			}
			OpenGL.glDisableClientState(32884u);
		}
		private void DrawSkeleton()
		{
			int sn = this.seg.Segments.Length;
			OpenGL.glPointSize(8f);
			OpenGL.glBegin(0u);
			for (int i = 0; i < sn; i++)
			{
				if (this.constraintedNode[i])
				{
					OpenGL.glColor3f(0f, 1f, 0f);
				}
				else
				{
					OpenGL.glColor3f(1f, 0f, 0f);
				}
				OpenGL.glVertex3d(this.nodePosition[i].x, this.nodePosition[i].y, this.nodePosition[i].z);
			}
			OpenGL.glEnd();
			OpenGL.glColor3f(0f, 0f, 1f);
			OpenGL.glLineWidth(2f);
			OpenGL.glBegin(1u);
			for (int j = 0; j < sn; j++)
			{
				foreach (int adj in this.seg.AdjIndex[j])
				{
					int k = adj - 1;
					OpenGL.glVertex3d(this.nodePosition[j].x, this.nodePosition[j].y, this.nodePosition[j].z);
					OpenGL.glVertex3d(this.nodePosition[k].x, this.nodePosition[k].y, this.nodePosition[k].z);
				}
			}
			OpenGL.glEnd();
		}
		public void Deform()
		{
			switch (this.deformMethod)
			{
			case GeneralReducedDeformer.EnumDeformMethod.Standard:
				if (this.opt.CreateStandardSolver)
				{
					this.Deform_Standard();
					return;
				}
				break;
			case GeneralReducedDeformer.EnumDeformMethod.Reduced:
				this.Deform_Recuded();
				break;
			default:
				return;
			}
		}
		public void Update()
		{
			switch (this.deformMethod)
			{
			case GeneralReducedDeformer.EnumDeformMethod.Standard:
				if (this.opt.CreateStandardSolver)
				{
					this.Update_Standard();
					return;
				}
				break;
			case GeneralReducedDeformer.EnumDeformMethod.Reduced:
				this.Update_Recuded();
				break;
			default:
				return;
			}
		}
		public void Display()
		{
			if (this.displaySegmentation)
			{
				this.DrawSegmentation();
			}
			if (this.displaySkeleton)
			{
				this.DrawSkeleton();
			}
		}
		public void Move()
		{
			int i = this.mesh.FaceCount;
			int j = 0;
			int k = 0;
			while (j < this.mesh.VertexCount)
			{
				if (this.mesh.Flag[j] != 0)
				{
					this.b[0][i] = this.mesh.VertexPos[k] * this.opt.HandleConstraintScale;
					this.b[1][i] = this.mesh.VertexPos[k + 1] * this.opt.HandleConstraintScale;
					this.b[2][i++] = this.mesh.VertexPos[k + 2] * this.opt.HandleConstraintScale;
				}
				j++;
				k += 3;
			}
		}
		public void MouseDown()
		{
		}
		public void MouseUp()
		{
		}
		public unsafe void Dispose()
		{
			if (this.solver != null)
			{
				GeneralReducedDeformer.FreeSolver(this.solver);
				this.solver = null;
			}
		}
	}
}
