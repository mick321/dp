using MyGeometry;
using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
namespace IsolineEditing
{
	internal static class Program
	{
		public enum EnumOperationMode
		{
			Viewing,
			Selection,
			Moving
		}
		public static Program.EnumOperationMode currentMode = Program.EnumOperationMode.Viewing;
		public static DisplayProperty displayProperty = new DisplayProperty();
		public static ToolsProperty toolsProperty = new ToolsProperty();
		[STAThread]
		private static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Thread.CurrentThread.Priority = ThreadPriority.BelowNormal;
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
			Application.Run(new FormMain());
		}
		public static void PrintText(string s)
		{
			FormMain f = Form.ActiveForm as FormMain;
			if (f != null)
			{
				f.PrintText(s);
			}
		}
		public static void Print3DText(Vector3d pos, string s)
		{
			FormMain f = Form.ActiveForm as FormMain;
			if (f != null)
			{
				f.Print3DText(pos, s);
			}
		}
		public static void RefreshAllForms()
		{
			foreach (Form f in Application.OpenForms)
			{
                if (!f.InvokeRequired)
                    f.Refresh();
                else 
                    f.BeginInvoke(new Action<Form>(
                           (sender) => sender.Refresh()
                        ), f 
                    );
			}
		}
	}
}
