using MyGeometry;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
namespace IsolineEditing
{
	public class Skeletonizer2
	{
		private class vertexDistanceRecord : PriorityQueueElement, IComparable
		{
			public int pqIndex;
			public int vIndex;
			public bool visited;
			public double minDistance = 1.7976931348623157E+308;
			public int PQIndex
			{
				get
				{
					return this.pqIndex;
				}
				set
				{
					this.pqIndex = value;
				}
			}
			public vertexDistanceRecord(int vIndex)
			{
				this.vIndex = vIndex;
			}
			public int CompareTo(object obj)
			{
				Skeletonizer2.vertexDistanceRecord rec = obj as Skeletonizer2.vertexDistanceRecord;
				if (this.minDistance < rec.minDistance)
				{
					return -1;
				}
				if (this.minDistance > rec.minDistance)
				{
					return 1;
				}
				return 0;
			}
		}
		private Mesh mesh;
		private double harmonicConstraintScale = 100.0;
		[DllImport("taucs.dll")]
		private unsafe static extern void* CreaterCholeskySolver(int n, int nnz, int* rowIndex, int* colIndex, double* value);
		[DllImport("taucs.dll")]
		private unsafe static extern int Solve(void* solver, double* x, double* b);
		[DllImport("taucs.dll")]
		private unsafe static extern double SolveEx(void* solver, double* x, int xIndex, double* b, int bIndex);
		[DllImport("taucs.dll")]
		private unsafe static extern int FreeSolver(void* sp);
		[DllImport("taucs.dll")]
		private unsafe static extern void* CreaterSymbolicSolver(int n, int nnz, int* rowIndex, int* colIndex, double* value);
		[DllImport("taucs.dll")]
		private unsafe static extern void FreeSymbolicSolver(void* sp);
		[DllImport("taucs.dll")]
		private unsafe static extern int NumericFactor(void* sp, int n, int nnz, int* rowIndex, int* colIndex, double* value);
		[DllImport("taucs.dll")]
		private unsafe static extern void FreeNumericFactor(void* sp);
		[DllImport("taucs.dll")]
		private unsafe static extern int NumericSolve(void* sp, double* x, double* b);
		public Skeletonizer2(Mesh mesh)
		{
			this.mesh = mesh;
			this.FindFeatureVertices();
		}
		private unsafe int[] FindFeatureVertices()
		{
			int vn = this.mesh.VertexCount;
			List<int> featurePoints = new List<int>();
			int p = this.FindFarthestVertex(0);
			int p2 = this.FindFarthestVertex(p);
			int p3 = this.FindFarthestVertex(p2);
			this.mesh.Flag[p2] = 2;
			this.mesh.Flag[p3] = 1;
			featurePoints.Add(p2);
			featurePoints.Add(p3);
			SparseMatrix L = this.BuildMatrixL();
			L.AddRow();
			L.AddElement(vn, p2, this.harmonicConstraintScale);
			L.AddRow();
			L.AddElement(vn + 1, p3, this.harmonicConstraintScale);
			for (int iter = 0; iter < 10; iter++)
			{
				CCSMatrix ccsL = new CCSMatrix(L);
				CCSMatrix ccsLTL = this.MultiplyATA(ccsL);
				void* solver = this.Factorization(ccsLTL);
				if (solver == null)
				{
					throw new Exception();
				}
				double[] b = new double[L.RowSize];
				double[] ATb = new double[vn];
				double[] x = new double[vn];
				double[] dis = this.FindDistance(featurePoints[0]);
				for (int i = 0; i < vn; i++)
				{
					b[i] = 0.0;
				}
				for (int j = 0; j < featurePoints.Count; j++)
				{
					b[j + vn] = dis[featurePoints[j]] * this.harmonicConstraintScale;
				}
				ccsL.PreMultiply(b, ATb);
				fixed (double* _x = x, _ATb = ATb)
				{
					Skeletonizer2.Solve(solver, _x, _ATb);
				}
				Skeletonizer2.FreeSolver(solver);
				int newFeatureIndex = this.FindLargestDistortionVertex(x);
				this.mesh.Flag[newFeatureIndex] = 1;
				L.AddRow();
				L.AddElement(vn + featurePoints.Count, newFeatureIndex, this.harmonicConstraintScale);
				featurePoints.Add(newFeatureIndex);
			}
			return featurePoints.ToArray();
		}
		private int FindFarthestVertex(int source)
		{
			int vn = this.mesh.VertexCount;
			Skeletonizer2.vertexDistanceRecord[] records = new Skeletonizer2.vertexDistanceRecord[vn];
			PriorityQueue queue = new PriorityQueue(vn);
			for (int i = 0; i < vn; i++)
			{
				records[i] = new Skeletonizer2.vertexDistanceRecord(i);
			}
			records[source].minDistance = 0.0;
			records[source].visited = true;
			queue.Insert(records[source]);
			while (!queue.IsEmpty())
			{
				Skeletonizer2.vertexDistanceRecord rec = queue.DeleteMin() as Skeletonizer2.vertexDistanceRecord;
				Vector3d v = new Vector3d(this.mesh.VertexPos, rec.vIndex * 3);
				int[] array = this.mesh.AdjVV[rec.vIndex];
				for (int k = 0; k < array.Length; k++)
				{
					int j = array[k];
					Skeletonizer2.vertexDistanceRecord rec2 = records[j];
					Vector3d v2 = new Vector3d(this.mesh.VertexPos, rec2.vIndex * 3);
					double dis = (v2 - v).Length() + rec.minDistance;
					if (!rec2.visited)
					{
						rec2.visited = true;
						queue.Insert(rec2);
					}
					if (dis < rec2.minDistance)
					{
						rec2.minDistance = dis;
						queue.Update(rec2);
					}
				}
				if (queue.IsEmpty() && rec.vIndex != source)
				{
					return rec.vIndex;
				}
			}
			return -1;
		}
		private double[] FindDistance(int source)
		{
			int vn = this.mesh.VertexCount;
			Skeletonizer2.vertexDistanceRecord[] records = new Skeletonizer2.vertexDistanceRecord[vn];
			PriorityQueue queue = new PriorityQueue(vn);
			for (int i = 0; i < vn; i++)
			{
				records[i] = new Skeletonizer2.vertexDistanceRecord(i);
			}
			records[source].minDistance = 0.0;
			records[source].visited = true;
			queue.Insert(records[source]);
			while (!queue.IsEmpty())
			{
				Skeletonizer2.vertexDistanceRecord rec = queue.DeleteMin() as Skeletonizer2.vertexDistanceRecord;
				Vector3d v = new Vector3d(this.mesh.VertexPos, rec.vIndex * 3);
				int[] array = this.mesh.AdjVV[rec.vIndex];
				for (int l = 0; l < array.Length; l++)
				{
					int j = array[l];
					Skeletonizer2.vertexDistanceRecord rec2 = records[j];
					Vector3d v2 = new Vector3d(this.mesh.VertexPos, rec2.vIndex * 3);
					double dis = (v2 - v).Length() + rec.minDistance;
					if (!rec2.visited)
					{
						rec2.visited = true;
						queue.Insert(rec2);
					}
					if (dis < rec2.minDistance)
					{
						rec2.minDistance = dis;
						queue.Update(rec2);
					}
				}
			}
			double[] distance = new double[vn];
			for (int k = 0; k < vn; k++)
			{
				distance[k] = records[k].minDistance;
			}
			return distance;
		}
		private int FindLargestDistortionVertex(double[] func)
		{
			int arg_0B_0 = this.mesh.VertexCount;
			int fn = this.mesh.FaceCount;
			double minRatio = 1.7976931348623157E+308;
			int minIndex = -1;
			int i = 0;
			int j = 0;
			while (i < fn)
			{
				int c = this.mesh.FaceIndex[j];
				int c2 = this.mesh.FaceIndex[j + 1];
				int c3 = this.mesh.FaceIndex[j + 2];
				if (this.mesh.Flag[c] == 0 && this.mesh.Flag[c2] == 0 && this.mesh.Flag[c3] == 0)
				{
					Vector3d v = new Vector3d(this.mesh.VertexPos, c * 3);
					Vector3d v2 = new Vector3d(this.mesh.VertexPos, c2 * 3);
					Vector3d v3 = new Vector3d(this.mesh.VertexPos, c3 * 3);
					Vector3d v4 = v - v3;
					Vector3d v5 = v2 - v3;
					double h = func[c];
					double h2 = func[c2];
					double h3 = func[c3];
					double g = (h - h3) / v4.Length();
					double proj = v5.Dot(v4) / v4.Length();
					double ortho = Math.Sqrt(v5.Dot(v5) - proj * proj);
					double hx = h3 + g * proj;
					double g2 = (h2 - hx) / ortho;
					double gradSq = g * g + g2 * g2;
					if (gradSq < minRatio)
					{
						minRatio = gradSq;
						minIndex = i;
					}
				}
				i++;
				j += 3;
			}
			return this.mesh.FaceIndex[minIndex * 3];
		}
		private SparseMatrix BuildMatrixL()
		{
			int vn = this.mesh.VertexCount;
			int fn = this.mesh.FaceCount;
			SparseMatrix A = new SparseMatrix(vn, vn);
			int i = 0;
			int j = 0;
			while (i < fn)
			{
				int c = this.mesh.FaceIndex[j];
				int c2 = this.mesh.FaceIndex[j + 1];
				int c3 = this.mesh.FaceIndex[j + 2];
				Vector3d v = new Vector3d(this.mesh.VertexPos, c * 3);
				Vector3d v2 = new Vector3d(this.mesh.VertexPos, c2 * 3);
				Vector3d v3 = new Vector3d(this.mesh.VertexPos, c3 * 3);
				double cot = (v2 - v).Dot(v3 - v) / (v2 - v).Cross(v3 - v).Length();
				double cot2 = (v3 - v2).Dot(v - v2) / (v3 - v2).Cross(v - v2).Length();
				double cot3 = (v - v3).Dot(v2 - v3) / (v - v3).Cross(v2 - v3).Length();
				if (double.IsNaN(cot))
				{
					throw new Exception();
				}
				if (double.IsNaN(cot2))
				{
					throw new Exception();
				}
				if (double.IsNaN(cot3))
				{
					throw new Exception();
				}
				A.AddValueTo(c2, c2, -cot);
				A.AddValueTo(c2, c3, cot);
				A.AddValueTo(c3, c3, -cot);
				A.AddValueTo(c3, c2, cot);
				A.AddValueTo(c3, c3, -cot2);
				A.AddValueTo(c3, c, cot2);
				A.AddValueTo(c, c, -cot2);
				A.AddValueTo(c, c3, cot2);
				A.AddValueTo(c, c, -cot3);
				A.AddValueTo(c, c2, cot3);
				A.AddValueTo(c2, c2, -cot3);
				A.AddValueTo(c2, c, cot3);
				i++;
				j += 3;
			}
			A.SortElement();
			return A;
		}
		private CCSMatrix MultiplyATA(CCSMatrix A)
		{
			int[] last = new int[A.RowSize];
			int[] next = new int[A.NumNonZero];
			int[] colIndex = new int[A.NumNonZero];
			for (int i = 0; i < last.Length; i++)
			{
				last[i] = -1;
			}
			for (int j = 0; j < next.Length; j++)
			{
				next[j] = -1;
			}
			for (int k = 0; k < A.ColumnSize; k++)
			{
				for (int l = A.ColIndex[k]; l < A.ColIndex[k + 1]; l++)
				{
					int m = A.RowIndex[l];
					if (last[m] != -1)
					{
						next[last[m]] = l;
					}
					last[m] = l;
					colIndex[l] = k;
				}
			}
			CCSMatrix ATA = new CCSMatrix(A.ColumnSize, A.ColumnSize);
			Set<int> set = new Set<int>();
			double[] tmp = new double[A.ColumnSize];
			List<int> ATA_RowIndex = new List<int>();
			List<double> ATA_Value = new List<double>();
			for (int n = 0; n < A.ColumnSize; n++)
			{
				tmp[n] = 0.0;
			}
			for (int j2 = 0; j2 < A.ColumnSize; j2++)
			{
				for (int col = A.ColIndex[j2]; col < A.ColIndex[j2 + 1]; col++)
				{
					int arg_122_0 = A.RowIndex[col];
					double val = A.Values[col];
					int curr = col;
					while (true)
					{
						int i2 = colIndex[curr];
						set.Add(i2);
						tmp[i2] += val * A.Values[curr];
						if (next[curr] == -1)
						{
							break;
						}
						curr = next[curr];
					}
				}
				int[] s = set.ToArray();
				Array.Sort<int>(s);
				int count = 0;
				int[] array = s;
				for (int num = 0; num < array.Length; num++)
				{
					int k2 = array[num];
					if (tmp[k2] != 0.0)
					{
						ATA_RowIndex.Add(k2);
						ATA_Value.Add(tmp[k2]);
						tmp[k2] = 0.0;
						count++;
					}
				}
				ATA.ColIndex[j2 + 1] = ATA.ColIndex[j2] + count;
				set.Clear();
			}
			ATA.RowIndex = ATA_RowIndex.ToArray();
			ATA.Values = ATA_Value.ToArray();
			return ATA;
		}
		private unsafe void* Factorization(CCSMatrix C)
		{
			fixed (int* ri = C.RowIndex)
			{
				fixed (int* ci = C.ColIndex)
				{
					fixed (double* val = C.Values)
					{
						return Skeletonizer2.CreaterCholeskySolver(C.ColumnSize, C.NumNonZero, ri, ci, val);
					}
				}
			}
		}
	}
}
