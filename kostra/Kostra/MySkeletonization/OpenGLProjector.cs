using CsGL.OpenGL;
using MyGeometry;
using System;
namespace IsolineEditing
{
	public class OpenGLProjector
	{
		private double[] modelView = new double[16];
		private double[] projection = new double[16];
		private int[] viewport = new int[4];
		private float[] depthBuffer;
		public double[] ModelViewMatrix
		{
			get
			{
				return this.modelView;
			}
		}
		public double[] ProjectionMatrix
		{
			get
			{
				return this.projection;
			}
		}
		public int[] Viewport
		{
			get
			{
				return this.viewport;
			}
		}
		public float[] DepthBuffer
		{
			get
			{
				return this.depthBuffer;
			}
		}
		public unsafe OpenGLProjector()
		{
			GL.glGetDoublev(2982u, this.modelView);
			GL.glGetDoublev(2983u, this.projection);
			GL.glGetIntegerv(2978u, this.viewport);
			this.depthBuffer = new float[this.viewport[2] * this.viewport[3]];
			fixed (float* pt = this.depthBuffer)
			{
				OpenGL.glReadPixels(this.viewport[0], this.viewport[1], this.viewport[2], this.viewport[3], 6402u, 5126u, (void*)pt);
			}
		}
		public Vector3d UnProject(double inX, double inY, double inZ)
		{
			double x;
			double y;
			double z;
			GL.gluUnProject(inX, inY, inZ, this.modelView, this.projection, this.viewport, out x, out y, out z);
			return new Vector3d(x, y, z);
		}
		public Vector3d UnProject(Vector3d p)
		{
			double x;
			double y;
			double z;
			GL.gluUnProject(p.x, p.y, p.z, this.modelView, this.projection, this.viewport, out x, out y, out z);
			return new Vector3d(x, y, z);
		}
		public Vector3d UnProject(double[] arr, int index)
		{
			double x;
			double y;
			double z;
			GL.gluUnProject(arr[index], arr[index + 1], arr[index + 2], this.modelView, this.projection, this.viewport, out x, out y, out z);
			return new Vector3d(x, y, z);
		}
		public Vector3d Project(double inX, double inY, double inZ)
		{
			double x;
			double y;
			double z;
			GL.gluProject(inX, inY, inZ, this.modelView, this.projection, this.viewport, out x, out y, out z);
			return new Vector3d(x, y, z);
		}
		public Vector3d Project(Vector3d p)
		{
			double x;
			double y;
			double z;
			GL.gluProject(p.x, p.y, p.z, this.modelView, this.projection, this.viewport, out x, out y, out z);
			return new Vector3d(x, y, z);
		}
		public Vector3d Project(double[] arr, int index)
		{
			double x;
			double y;
			double z;
			GL.gluProject(arr[index], arr[index + 1], arr[index + 2], this.modelView, this.projection, this.viewport, out x, out y, out z);
			return new Vector3d(x, y, z);
		}
		public double GetDepthValue(int x, int y)
		{
			return (double)this.depthBuffer[(y - this.viewport[1]) * this.viewport[2] + (x - this.viewport[0])];
		}
	}
}
