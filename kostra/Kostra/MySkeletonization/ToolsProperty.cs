using System;
using System.ComponentModel;
namespace IsolineEditing
{
	public class ToolsProperty
	{
		public enum EnumSelectingMethod
		{
			Rectangle,
			Point
		}
		private bool selectionLaser = true;
		private double depthTolerance = -0.0001;
		private ToolsProperty.EnumSelectingMethod selectionMethod;
		[Category("Selection Tool")]
		public bool Laser
		{
			get
			{
				return this.selectionLaser;
			}
			set
			{
				this.selectionLaser = value;
			}
		}
		[Category("Selection Tool")]
		public double DepthTolerance
		{
			get
			{
				return this.depthTolerance;
			}
			set
			{
				this.depthTolerance = value;
			}
		}
		[Category("Selection Tool")]
		public ToolsProperty.EnumSelectingMethod SelectionMethod
		{
			get
			{
				return this.selectionMethod;
			}
			set
			{
				this.selectionMethod = value;
			}
		}
	}
}
