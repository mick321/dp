using MyGeometry;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
namespace IsolineEditing
{
	public class MultigridHarmonicFieldSolver
	{
		private class EdgeCollapseRecord : PriorityQueueElement, IComparable
		{
			public int pqIndex = -1;
			public int minIndex = -1;
			public int vIndex = -1;
			public double minError = 1.7976931348623157E+308;
			public double area;
			public Set<int> adjV = new Set<int>(6);
			public int PQIndex
			{
				get
				{
					return this.pqIndex;
				}
				set
				{
					this.pqIndex = value;
				}
			}
			public EdgeCollapseRecord(int vIndex)
			{
				this.vIndex = vIndex;
			}
			public int CompareTo(object obj)
			{
				MultigridHarmonicFieldSolver.EdgeCollapseRecord rec = obj as MultigridHarmonicFieldSolver.EdgeCollapseRecord;
				if (this.minError < rec.minError)
				{
					return -1;
				}
				if (this.minError > rec.minError)
				{
					return 1;
				}
				return 0;
			}
		}
		private class ColMatrixCreator
		{
			public int m;
			public int n;
			public List<int>[] rowIndex;
			public List<double>[] values;
			public int RowSize
			{
				get
				{
					return this.m;
				}
			}
			public int ColumnSize
			{
				get
				{
					return this.n;
				}
			}
			public ColMatrixCreator(int m, int n)
			{
				this.m = m;
				this.n = n;
				this.rowIndex = new List<int>[n];
				this.values = new List<double>[n];
				for (int i = 0; i < n; i++)
				{
					this.rowIndex[i] = new List<int>();
					this.values[i] = new List<double>();
				}
			}
			public void AddValueTo(int i, int j, double value)
			{
				List<int> r = this.rowIndex[j];
				if (i >= this.m)
				{
					this.m = i + 1;
				}
				int ri = -1;
				for (int k = 0; k < r.Count; k++)
				{
					if (r[k] == i)
					{
						ri = k;
						break;
					}
				}
				if (ri == -1)
				{
					r.Add(i);
					this.values[j].Add(value);
					return;
				}
				List<double> list;
				int index;
				(list = this.values[j])[index = ri] = list[index] + value;
			}
			public MultigridHarmonicFieldSolver.ColMatrix ToColMatrix()
			{
				MultigridHarmonicFieldSolver.ColMatrix C = new MultigridHarmonicFieldSolver.ColMatrix(this.m, this.n);
				for (int i = 0; i < this.n; i++)
				{
					try
					{
						C.rowIndex[i] = this.rowIndex[i].ToArray();
					}
					catch (OutOfMemoryException)
					{
						GC.Collect();
						C.rowIndex[i] = this.rowIndex[i].ToArray();
					}
					this.rowIndex[i] = null;
					try
					{
						C.values[i] = this.values[i].ToArray();
					}
					catch (OutOfMemoryException)
					{
						GC.Collect();
						C.values[i] = this.values[i].ToArray();
					}
					this.values[i] = null;
				}
				return C;
			}
		}
		private class ColMatrix
		{
			public int m;
			public int n;
			public int[][] rowIndex;
			public double[][] values;
			public int RowSize
			{
				get
				{
					return this.m;
				}
			}
			public int ColumnSize
			{
				get
				{
					return this.n;
				}
			}
			public ColMatrix(int m, int n)
			{
				this.m = m;
				this.n = n;
				this.rowIndex = new int[n][];
				this.values = new double[n][];
				for (int i = 0; i < n; i++)
				{
					this.rowIndex[i] = null;
					this.values[i] = null;
				}
			}
			public int NumOfElements()
			{
				int c = 0;
				int[][] array = this.rowIndex;
				for (int i = 0; i < array.Length; i++)
				{
					int[] r = array[i];
					c += r.Length;
				}
				return c;
			}
			public void Multiply(double[] xIn, double[] xOut)
			{
				if (xIn.Length < this.n || xOut.Length < this.m)
				{
					throw new ArgumentException();
				}
				for (int i = 0; i < this.m; i++)
				{
					xOut[i] = 0.0;
				}
				for (int j = 0; j < this.n; j++)
				{
					int[] r = this.rowIndex[j];
					double[] v = this.values[j];
					for (int k = 0; k < r.Length; k++)
					{
						int ri = r[k];
						xOut[ri] += v[k] * xIn[j];
					}
				}
			}
			public void PreMultiply(double[] xIn, double[] xOut)
			{
				if (xIn.Length < this.m || xOut.Length < this.n)
				{
					throw new ArgumentException();
				}
				for (int i = 0; i < this.n; i++)
				{
					xOut[i] = 0.0;
				}
				for (int j = 0; j < this.n; j++)
				{
					double sum = 0.0;
					int[] r = this.rowIndex[j];
					double[] v = this.values[j];
					for (int k = 0; k < r.Length; k++)
					{
						int ri = r[k];
						sum += v[k] * xIn[ri];
					}
					xOut[j] = sum;
				}
			}
			public void PreMultiply(double[] xIn, double[] xOut, int[] index)
			{
				if (xIn.Length < this.m || xOut.Length < this.n)
				{
					throw new ArgumentException();
				}
				for (int i = 0; i < this.n; i++)
				{
					xOut[i] = 0.0;
				}
				for (int l = 0; l < index.Length; l++)
				{
					int j = index[l];
					double sum = 0.0;
					int[] r = this.rowIndex[j];
					double[] v = this.values[j];
					for (int k = 0; k < r.Length; k++)
					{
						int ri = r[k];
						sum += v[k] * xIn[ri];
					}
					xOut[j] = sum;
				}
			}
			public void PreMultiplyOffset(double[] xIn, double[] xOut, int startOut, int offsetOut)
			{
				for (int i = startOut; i < this.n + offsetOut; i += offsetOut)
				{
					xOut[i] = 0.0;
				}
				int j = 0;
				int k = startOut;
				while (j < this.n)
				{
					double sum = 0.0;
					int[] r = this.rowIndex[j];
					double[] v = this.values[j];
					for (int l = 0; l < r.Length; l++)
					{
						int ri = r[l];
						sum += v[l] * xIn[ri];
					}
					xOut[k] = sum;
					j++;
					k += offsetOut;
				}
			}
			public MultigridHarmonicFieldSolver.ColMatrix Transpose()
			{
				MultigridHarmonicFieldSolver.ColMatrixCreator C = new MultigridHarmonicFieldSolver.ColMatrixCreator(this.n, this.m);
				for (int i = 0; i < this.n; i++)
				{
					int[] r = this.rowIndex[i];
					double[] v = this.values[i];
					for (int j = 0; j < r.Length; j++)
					{
						C.rowIndex[r[j]].Add(i);
						C.values[r[j]].Add(v[j]);
					}
				}
				return C;
			}
			public static implicit operator MultigridHarmonicFieldSolver.ColMatrix(MultigridHarmonicFieldSolver.ColMatrixCreator CC)
			{
				return CC.ToColMatrix();
			}
			public void ATACG(double[] x, double[] b, double eps, int maxIter)
			{
				double[] inv = new double[this.n];
				double[] r = new double[this.n];
				double[] d = new double[this.n];
				double[] q = new double[this.n];
				double[] s = new double[this.n];
				double errNew = 0.0;
				double[] tmp = new double[this.m];
				for (int i = 0; i < this.n; i++)
				{
					inv[i] = 0.0;
				}
				for (int j = 0; j < this.n; j++)
				{
					for (int k = 0; k < this.rowIndex[j].Length; k++)
					{
						double val = this.values[j][k];
						inv[j] += val * val;
					}
				}
				for (int l = 0; l < this.n; l++)
				{
					inv[l] = 1.0 / inv[l];
				}
				int iter = 0;
				this.Multiply(x, tmp);
				this.PreMultiply(tmp, r);
				for (int m = 0; m < this.n; m++)
				{
					r[m] = b[m] - r[m];
				}
				for (int n = 0; n < this.n; n++)
				{
					d[n] = inv[n] * r[n];
				}
				for (int i2 = 0; i2 < this.n; i2++)
				{
					errNew += r[i2] * d[i2];
				}
				double err = errNew;
				while (iter < maxIter && errNew > eps * err)
				{
					this.Multiply(d, tmp);
					this.PreMultiply(tmp, q);
					double alpha = 0.0;
					for (int i3 = 0; i3 < this.n; i3++)
					{
						alpha += d[i3] * q[i3];
					}
					alpha = errNew / alpha;
					for (int i4 = 0; i4 < this.n; i4++)
					{
						x[i4] += alpha * d[i4];
					}
					if (iter % 500 == 0)
					{
						this.Multiply(x, tmp);
						this.PreMultiply(tmp, r);
						for (int i5 = 0; i5 < this.n; i5++)
						{
							r[i5] = b[i5] - r[i5];
						}
					}
					else
					{
						for (int i6 = 0; i6 < this.n; i6++)
						{
							r[i6] -= alpha * q[i6];
						}
					}
					for (int i7 = 0; i7 < this.n; i7++)
					{
						s[i7] = inv[i7] * r[i7];
					}
					double errOld = errNew;
					errNew = 0.0;
					for (int i8 = 0; i8 < this.n; i8++)
					{
						errNew += r[i8] * s[i8];
					}
					double beta = errNew / errOld;
					for (int i9 = 0; i9 < this.n; i9++)
					{
						d[i9] = s[i9] + beta * d[i9];
					}
					iter++;
				}
				Program.PrintText("iter: " + iter.ToString() + "\n");
			}
		}
		private Mesh mesh;
		private double reduceRatio;
		private int minLevelSize;
		private int[] levelSize;
		private int nHandles;
		private int regionNum;
		private double handleConstraintScale = 100.0;
		private MultigridHarmonicFieldSolver.EdgeCollapseRecord[] orderedEdgeRecord;
		private double convergeRatio = 1E-05;
		public int[] FaceIndex;
		public double[][] HarmonicFields;
		[DllImport("taucs.dll")]
		private unsafe static extern void* CreaterCholeskySolver(int n, int nnz, int* rowIndex, int* colIndex, double* value);
		[DllImport("taucs.dll")]
		private unsafe static extern int Solve(void* solver, double* x, double* b);
		[DllImport("taucs.dll")]
		private unsafe static extern double SolveEx(void* solver, double* x, int xIndex, double* b, int bIndex);
		[DllImport("taucs.dll")]
		private unsafe static extern int FreeSolver(void* sp);
		public MultigridHarmonicFieldSolver(Mesh mesh, double reduceRatio, int minLevelSize, int regionNum)
		{
			int vn = mesh.VertexCount;
			this.mesh = mesh;
			this.reduceRatio = reduceRatio;
			this.minLevelSize = minLevelSize;
			this.regionNum = regionNum;
			int i = 0;
			for (int j = 0; j < mesh.VertexCount; j++)
			{
				if ((int)mesh.Flag[j] > i)
				{
					i = (int)mesh.Flag[j];
				}
			}
			this.nHandles = i;
			int targetSize = mesh.VertexCount;
			List<int> sizes = new List<int>();
			sizes.Add(targetSize);
			Program.PrintText(targetSize.ToString() + "\n");
			while ((double)targetSize * reduceRatio > (double)minLevelSize)
			{
				targetSize = (int)((double)targetSize * reduceRatio);
				sizes.Add(targetSize);
				Program.PrintText(targetSize.ToString() + "\n");
			}
			sizes.Reverse();
			this.levelSize = sizes.ToArray();
			double[][] h = new double[this.nHandles][];
			for (int k = 0; k < this.nHandles; k++)
			{
				h[k] = new double[vn];
			}
			MultigridHarmonicFieldSolver.EdgeCollapseRecord[] collapseRec = this.SimplifyModel(targetSize);
			int[] collapseIndex = this.BuildCollapseIndex(collapseRec, mesh.VertexCount - this.levelSize[0]);
			int[] array = this.FaceIndex = this.BuildSimplifiedFaceIndex(collapseIndex);
			int[] forwardMap = this.BuildForwardMap(collapseIndex);
			int[] inverseMap = this.BuildInverseMap(collapseIndex);
			double[][] base_h = this.SolveHarmonicValues(this.FaceIndex, forwardMap, inverseMap);
			for (int l = 0; l < base_h.Length; l++)
			{
				for (int m = 0; m < base_h[l].Length; m++)
				{
					h[l][inverseMap[m]] = base_h[l][m];
				}
			}
			GC.Collect();
			for (int iter = 1; iter < this.levelSize.Length; iter++)
			{
				this.InterpolateSolution(h, collapseRec, mesh.VertexCount - this.levelSize[iter - 1], mesh.VertexCount - this.levelSize[iter]);
				collapseIndex = this.BuildCollapseIndex(collapseRec, mesh.VertexCount - this.levelSize[iter]);
				int[] faceIndex = this.BuildSimplifiedFaceIndex(collapseIndex);
				forwardMap = this.BuildForwardMap(collapseIndex);
				inverseMap = this.BuildInverseMap(collapseIndex);
				base_h = this.SolveHarmonicValuesCG(faceIndex, forwardMap, inverseMap, h);
				GC.Collect();
				for (int n = 0; n < base_h.Length; n++)
				{
					for (int j2 = 0; j2 < base_h[n].Length; j2++)
					{
						h[n][inverseMap[j2]] = base_h[n][j2];
					}
				}
			}
			this.HarmonicFields = h;
		}
		private MultigridHarmonicFieldSolver.EdgeCollapseRecord[] SimplifyModel(int target)
		{
			int vn = this.mesh.VertexCount;
			MultigridHarmonicFieldSolver.EdgeCollapseRecord[] edgeRec = this.CreateEdgeCollapseRecords();
			List<MultigridHarmonicFieldSolver.EdgeCollapseRecord> collapseRec = new List<MultigridHarmonicFieldSolver.EdgeCollapseRecord>();
			PriorityQueue queue = new PriorityQueue(vn);
			this.orderedEdgeRecord = edgeRec;
			for (int i = 0; i < vn; i++)
			{
				queue.Insert(edgeRec[i]);
			}
			for (int j = 0; j < vn - target; j++)
			{
				MultigridHarmonicFieldSolver.EdgeCollapseRecord rec = (MultigridHarmonicFieldSolver.EdgeCollapseRecord)queue.DeleteMin();
				MultigridHarmonicFieldSolver.EdgeCollapseRecord rec2 = edgeRec[rec.minIndex];
				rec2.area += rec.area;
				collapseRec.Add(rec);
				foreach (int k in rec.adjV)
				{
					edgeRec[k].adjV.Remove(rec.vIndex);
					if (k != rec2.vIndex)
					{
						edgeRec[k].adjV.Add(rec2.vIndex);
						edgeRec[rec2.vIndex].adjV.Add(k);
					}
				}
				foreach (int l in rec2.adjV)
				{
					this.UpdateEdgeCollapseRecords(queue, edgeRec, l);
				}
				this.UpdateEdgeCollapseRecords(queue, edgeRec, rec2.vIndex);
			}
			return collapseRec.ToArray();
		}
		private MultigridHarmonicFieldSolver.EdgeCollapseRecord[] CreateEdgeCollapseRecords()
		{
			int vn = this.mesh.VertexCount;
			int fn = this.mesh.FaceCount;
			MultigridHarmonicFieldSolver.EdgeCollapseRecord[] rec = new MultigridHarmonicFieldSolver.EdgeCollapseRecord[vn];
			for (int i = 0; i < vn; i++)
			{
				rec[i] = new MultigridHarmonicFieldSolver.EdgeCollapseRecord(i);
			}
			int j = 0;
			int k = 0;
			while (j < fn)
			{
				int c = this.mesh.FaceIndex[k];
				int c2 = this.mesh.FaceIndex[k + 1];
				int c3 = this.mesh.FaceIndex[k + 2];
				Vector3d v = new Vector3d(this.mesh.VertexPos, c * 3);
				Vector3d v2 = new Vector3d(this.mesh.VertexPos, c2 * 3);
				Vector3d v3 = new Vector3d(this.mesh.VertexPos, c3 * 3);
				double faceArea = (v2 - v).Cross(v3 - v).Length() / 2.0;
				rec[c].area += faceArea;
				rec[c2].area += faceArea;
				rec[c3].area += faceArea;
				j++;
				k += 3;
			}
			int l = 0;
			int m = 0;
			while (l < vn)
			{
				Vector3d u = new Vector3d(this.mesh.VertexPos, m);
				MultigridHarmonicFieldSolver.EdgeCollapseRecord uRec = rec[l];
				int[] array = this.mesh.AdjVV[l];
				for (int num = 0; num < array.Length; num++)
				{
					int n = array[num];
					Vector3d uv = u - new Vector3d(this.mesh.VertexPos, n * 3);
					double err = uRec.area * uv.Dot(uv);
					if (err < uRec.minError)
					{
						uRec.minError = err;
						uRec.minIndex = n;
					}
					uRec.adjV.Add(n);
				}
				l++;
				m += 3;
			}
			return rec;
		}
		private void UpdateEdgeCollapseRecords(PriorityQueue queue, MultigridHarmonicFieldSolver.EdgeCollapseRecord[] edgeRec, int index)
		{
			Vector3d u = new Vector3d(this.mesh.VertexPos, index * 3);
			MultigridHarmonicFieldSolver.EdgeCollapseRecord uRec = edgeRec[index];
			uRec.minError = 1.7976931348623157E+308;
			foreach (int i in uRec.adjV)
			{
				Vector3d uv = u - new Vector3d(this.mesh.VertexPos, i * 3);
				double err = uRec.area * uv.Dot(uv);
				if (err < uRec.minError)
				{
					uRec.minError = err;
					uRec.minIndex = i;
				}
			}
			queue.Update(uRec);
		}
		private int[] BuildCollapseIndex(MultigridHarmonicFieldSolver.EdgeCollapseRecord[] collapseRec, int cut)
		{
			int vn = this.mesh.VertexCount;
			int[] collapseIndex = new int[vn];
			for (int i = 0; i < vn; i++)
			{
				collapseIndex[i] = -1;
			}
			for (int j = cut - 1; j >= 0; j--)
			{
				MultigridHarmonicFieldSolver.EdgeCollapseRecord rec = collapseRec[j];
				int s = rec.vIndex;
				int t = rec.minIndex;
				if (collapseIndex[t] == -1)
				{
					collapseIndex[s] = t;
				}
				else
				{
					collapseIndex[s] = collapseIndex[t];
				}
			}
			return collapseIndex;
		}
		private int[] BuildSimplifiedFaceIndex(int[] collapseIndex)
		{
			int fn = this.mesh.FaceCount;
			List<int> faceIndex = new List<int>();
			int i = 0;
			int j = 0;
			while (i < fn)
			{
				int f = this.mesh.FaceIndex[j];
				int f2 = this.mesh.FaceIndex[j + 1];
				int f3 = this.mesh.FaceIndex[j + 2];
				if (collapseIndex[f] != -1)
				{
					f = collapseIndex[f];
				}
				if (collapseIndex[f2] != -1)
				{
					f2 = collapseIndex[f2];
				}
				if (collapseIndex[f3] != -1)
				{
					f3 = collapseIndex[f3];
				}
				if (f != f2 && f2 != f3 && f3 != f)
				{
					faceIndex.Add(f);
					faceIndex.Add(f2);
					faceIndex.Add(f3);
				}
				i++;
				j += 3;
			}
			return faceIndex.ToArray();
		}
		private int[] BuildForwardMap(int[] collapseIndex)
		{
			int vn = this.mesh.VertexCount;
			int[] map = new int[vn];
			int count = 0;
			for (int i = 0; i < vn; i++)
			{
				if (collapseIndex[i] == -1)
				{
					map[i] = count;
					count++;
				}
			}
			return map;
		}
		private int[] BuildInverseMap(int[] collapseIndex)
		{
			int vn = this.mesh.VertexCount;
			List<int> map = new List<int>();
			for (int i = 0; i < vn; i++)
			{
				if (collapseIndex[i] == -1)
				{
					map.Add(i);
				}
			}
			return map.ToArray();
		}
		private unsafe double[][] SolveHarmonicValues(int[] faceIndex, int[] forwardMap, int[] inverseMap)
		{
			int i = inverseMap.Length;
			MultigridHarmonicFieldSolver.ColMatrix colA = this.BuildMatrixH(faceIndex, forwardMap, inverseMap);
			CCSMatrix ATA = this.MultiplyATA(colA);
			void* solver = this.Factorization(ATA);
			if (solver == null)
			{
				Program.PrintText("!!!");
				throw new Exception();
			}
			double[][] h = new double[this.nHandles][];
			for (int j = 0; j < this.nHandles; j++)
			{
				h[j] = new double[i];
			}
			double[] b = new double[colA.RowSize];
			double[] ATb = new double[colA.ColumnSize];
			double initValue = (double)this.regionNum / 2.0;
			double initValue2 = this.handleConstraintScale * (double)this.regionNum;
			for (int k = 0; k < b.Length; k++)
			{
				b[k] = 0.0;
			}
			for (int l = 0; l < this.nHandles; l++)
			{
				for (int m = 0; m < h[l].Length; m++)
				{
					h[l][m] = initValue;
				}
				int n = 0;
				int k2 = i;
				while (n < i)
				{
					byte flag = this.mesh.Flag[inverseMap[n]];
					if (flag != 0)
					{
						h[l][n] = (((int)flag == l + 1) ? ((double)this.regionNum) : 0.0);
						b[k2++] = (((int)flag == l + 1) ? initValue2 : 0.0);
					}
					n++;
				}
				colA.PreMultiply(b, ATb);
				fixed (double* _x = h[l], _ATb = ATb)
				{
					MultigridHarmonicFieldSolver.Solve(solver, _x, _ATb);
				}
			}
			MultigridHarmonicFieldSolver.FreeSolver(solver);
			return h;
		}
		private double[][] SolveHarmonicValuesCG(int[] faceIndex, int[] forwardMap, int[] inverseMap, double[][] init)
		{
			int i = inverseMap.Length;
			MultigridHarmonicFieldSolver.ColMatrix colA = this.BuildMatrixH(faceIndex, forwardMap, inverseMap);
			GC.Collect();
			double[][] h = new double[this.nHandles][];
			for (int j = 0; j < this.nHandles; j++)
			{
				h[j] = new double[i];
			}
			double[] b = new double[colA.RowSize];
			double[] ATb = new double[colA.ColumnSize];
			double initValue2 = this.handleConstraintScale * (double)this.regionNum;
			for (int k = 0; k < b.Length; k++)
			{
				b[k] = 0.0;
			}
			for (int l = 0; l < this.nHandles; l++)
			{
				for (int m = 0; m < h[l].Length; m++)
				{
					h[l][m] = init[l][inverseMap[m]];
				}
				int n = 0;
				int k2 = i;
				while (n < i)
				{
					byte flag = this.mesh.Flag[inverseMap[n]];
					if (flag != 0)
					{
						h[l][n] = (((int)flag == l + 1) ? ((double)this.regionNum) : 0.0);
						b[k2++] = (((int)flag == l + 1) ? initValue2 : 0.0);
					}
					n++;
				}
				colA.PreMultiply(b, ATb);
				colA.ATACG(h[l], ATb, this.convergeRatio, i);
				for (int j2 = 0; j2 < i; j2++)
				{
					byte flag2 = this.mesh.Flag[inverseMap[j2]];
					if (flag2 != 0)
					{
						h[l][j2] = (((int)flag2 == l + 1) ? ((double)this.regionNum) : 0.0);
					}
				}
			}
			return h;
		}
		private MultigridHarmonicFieldSolver.ColMatrix BuildMatrixH(int[] faceIndex, int[] forwardMap, int[] inverseMap)
		{
			int i = inverseMap.Length;
			MultigridHarmonicFieldSolver.ColMatrixCreator L = new MultigridHarmonicFieldSolver.ColMatrixCreator(i, i);
			int j = 0;
			int k = 0;
			while (j < faceIndex.Length / 3)
			{
				int c = faceIndex[k];
				int c2 = faceIndex[k + 1];
				int c3 = faceIndex[k + 2];
				Vector3d v = new Vector3d(this.mesh.VertexPos, c * 3);
				Vector3d v2 = new Vector3d(this.mesh.VertexPos, c2 * 3);
				Vector3d v3 = new Vector3d(this.mesh.VertexPos, c3 * 3);
				double cot = (v2 - v).Dot(v3 - v) / (v2 - v).Cross(v3 - v).Length();
				double cot2 = (v3 - v2).Dot(v - v2) / (v3 - v2).Cross(v - v2).Length();
				double cot3 = (v - v3).Dot(v2 - v3) / (v - v3).Cross(v2 - v3).Length();
				if (double.IsNaN(cot) || double.IsNaN(cot2) || double.IsNaN(cot3))
				{
					Program.PrintText(string.Concat(new object[]
					{
						"bad: ",
						c,
						" ",
						c2,
						" ",
						c3
					}));
					Program.PrintText((v2 - v).Cross(v3 - v).ToString());
					Program.PrintText((v3 - v2).Cross(v - v2).ToString());
					Program.PrintText((v - v3).Cross(v2 - v3).ToString());
				}
				int cc = forwardMap[c];
				int cc2 = forwardMap[c2];
				int cc3 = forwardMap[c3];
				if (this.mesh.Flag[c] == 0)
				{
					L.AddValueTo(cc, cc2, cot3);
				}
				if (this.mesh.Flag[c2] == 0)
				{
					L.AddValueTo(cc2, cc, cot3);
				}
				if (this.mesh.Flag[c2] == 0)
				{
					L.AddValueTo(cc2, cc3, cot);
				}
				if (this.mesh.Flag[c3] == 0)
				{
					L.AddValueTo(cc3, cc2, cot);
				}
				if (this.mesh.Flag[c3] == 0)
				{
					L.AddValueTo(cc3, cc, cot2);
				}
				if (this.mesh.Flag[c] == 0)
				{
					L.AddValueTo(cc, cc3, cot2);
				}
				j++;
				k += 3;
			}
			double[] sum = new double[i];
			int[] count = new int[i];
			for (int l = 0; l < i; l++)
			{
				sum[l] = 0.0;
				count[l] = 0;
			}
			for (int m = 0; m < i; m++)
			{
				List<int> r = L.rowIndex[m];
				List<double> v4 = L.values[m];
				for (int n = 0; n < r.Count; n++)
				{
					sum[r[n]] += v4[n];
					count[r[n]]++;
				}
			}
			for (int i2 = 0; i2 < i; i2++)
			{
				if (this.mesh.Flag[inverseMap[i2]] == 0)
				{
					L.AddValueTo(i2, i2, -sum[i2]);
					if (count[i2] == 0)
					{
						Set<int> s = this.orderedEdgeRecord[inverseMap[i2]].adjV;
						double w = 1.0 / (double)s.Count;
						foreach (int j2 in s)
						{
							L.AddValueTo(i2, forwardMap[j2], w);
						}
						L.AddValueTo(i2, i2, -1.0);
					}
				}
			}
			Set<int> handle = new Set<int>();
			int ri = i;
			for (int i3 = 0; i3 < i; i3++)
			{
				if (this.mesh.Flag[inverseMap[i3]] != 0)
				{
					handle.Add((int)this.mesh.Flag[inverseMap[i3]]);
					L.AddValueTo(ri, i3, this.handleConstraintScale);
					ri++;
				}
			}
			return L;
		}
		private void InterpolateSolution(double[][] h, MultigridHarmonicFieldSolver.EdgeCollapseRecord[] collapseRec, int cut1, int cut2)
		{
			double[] newValues = new double[h.Length];
			for (int i = cut1 - 1; i >= cut2; i--)
			{
				MultigridHarmonicFieldSolver.EdgeCollapseRecord rec = collapseRec[i];
				for (int j = 0; j < h.Length; j++)
				{
					newValues[j] = 0.0;
				}
				foreach (int k in rec.adjV)
				{
					for (int l = 0; l < h.Length; l++)
					{
						newValues[l] += h[l][k];
					}
				}
				for (int m = 0; m < h.Length; m++)
				{
					h[m][rec.vIndex] = newValues[m] / (double)rec.adjV.Count;
				}
			}
		}
		private CCSMatrix MultiplyATA(MultigridHarmonicFieldSolver.ColMatrix A)
		{
			int[] count = new int[A.RowSize];
			for (int i = 0; i < count.Length; i++)
			{
				count[i] = 0;
			}
			int[][] rowIndex = A.rowIndex;
			for (int num = 0; num < rowIndex.Length; num++)
			{
				int[] r = rowIndex[num];
				int[] array = r;
				for (int num2 = 0; num2 < array.Length; num2++)
				{
					int ri = array[num2];
					count[ri]++;
				}
			}
			int[][] colIndex = new int[A.RowSize][];
			int[][] listIndex = new int[A.RowSize][];
			for (int j = 0; j < A.RowSize; j++)
			{
				colIndex[j] = new int[count[j]];
				listIndex[j] = new int[count[j]];
			}
			for (int k = 0; k < count.Length; k++)
			{
				count[k] = 0;
			}
			for (int l = 0; l < A.values.Length; l++)
			{
				int[] row = A.rowIndex[l];
				for (int m = 0; m < row.Length; m++)
				{
					int r2 = row[m];
					int c = count[r2];
					colIndex[r2][c] = l;
					listIndex[r2][c] = m;
					count[r2]++;
				}
			}
			CCSMatrix ATA = new CCSMatrix(A.ColumnSize, A.ColumnSize);
			Set<int> set = new Set<int>();
			double[] tmp = new double[A.ColumnSize];
			List<int> ATA_RowIndex = new List<int>();
			List<double> ATA_Value = new List<double>();
			for (int n = 0; n < A.ColumnSize; n++)
			{
				tmp[n] = 0.0;
			}
			for (int j2 = 0; j2 < A.ColumnSize; j2++)
			{
				for (int ri2 = 0; ri2 < A.rowIndex[j2].Length; ri2++)
				{
					int k2 = A.rowIndex[j2][ri2];
					double val = A.values[j2][ri2];
					for (int k3 = 0; k3 < colIndex[k2].Length; k3++)
					{
						int i2 = colIndex[k2][k3];
						if (i2 >= j2)
						{
							set.Add(i2);
							tmp[i2] += val * A.values[i2][listIndex[k2][k3]];
						}
					}
				}
				int[] s = set.ToArray();
				Array.Sort<int>(s);
				int cc = 0;
				int[] array2 = s;
				for (int num3 = 0; num3 < array2.Length; num3++)
				{
					int k4 = array2[num3];
					if (tmp[k4] != 0.0)
					{
						ATA_RowIndex.Add(k4);
						ATA_Value.Add(tmp[k4]);
						tmp[k4] = 0.0;
						cc++;
					}
				}
				ATA.ColIndex[j2 + 1] = ATA.ColIndex[j2] + cc;
				set.Clear();
			}
			ATA.RowIndex = ATA_RowIndex.ToArray();
			ATA.Values = ATA_Value.ToArray();
			return ATA;
		}
		private CCSMatrix MultiplyATA_Full(MultigridHarmonicFieldSolver.ColMatrix A)
		{
			int[] count = new int[A.RowSize];
			for (int i = 0; i < count.Length; i++)
			{
				count[i] = 0;
			}
			int[][] rowIndex = A.rowIndex;
			for (int num = 0; num < rowIndex.Length; num++)
			{
				int[] r = rowIndex[num];
				int[] array = r;
				for (int num2 = 0; num2 < array.Length; num2++)
				{
					int ri = array[num2];
					count[ri]++;
				}
			}
			int[][] colIndex = new int[A.RowSize][];
			int[][] listIndex = new int[A.RowSize][];
			for (int j = 0; j < A.RowSize; j++)
			{
				colIndex[j] = new int[count[j]];
				listIndex[j] = new int[count[j]];
			}
			for (int k = 0; k < count.Length; k++)
			{
				count[k] = 0;
			}
			for (int l = 0; l < A.values.Length; l++)
			{
				int[] row = A.rowIndex[l];
				for (int m = 0; m < row.Length; m++)
				{
					int r2 = row[m];
					int c = count[r2];
					colIndex[r2][c] = l;
					listIndex[r2][c] = m;
					count[r2]++;
				}
			}
			CCSMatrix ATA = new CCSMatrix(A.ColumnSize, A.ColumnSize);
			Set<int> set = new Set<int>();
			double[] tmp = new double[A.ColumnSize];
			List<int> ATA_RowIndex = new List<int>();
			List<double> ATA_Value = new List<double>();
			for (int n = 0; n < A.ColumnSize; n++)
			{
				tmp[n] = 0.0;
			}
			for (int j2 = 0; j2 < A.ColumnSize; j2++)
			{
				for (int ri2 = 0; ri2 < A.rowIndex[j2].Length; ri2++)
				{
					int k2 = A.rowIndex[j2][ri2];
					double val = A.values[j2][ri2];
					for (int k3 = 0; k3 < colIndex[k2].Length; k3++)
					{
						int i2 = colIndex[k2][k3];
						set.Add(i2);
						tmp[i2] += val * A.values[i2][listIndex[k2][k3]];
					}
				}
				int[] s = set.ToArray();
				Array.Sort<int>(s);
				int cc = 0;
				int[] array2 = s;
				for (int num3 = 0; num3 < array2.Length; num3++)
				{
					int k4 = array2[num3];
					if (tmp[k4] != 0.0)
					{
						ATA_RowIndex.Add(k4);
						ATA_Value.Add(tmp[k4]);
						tmp[k4] = 0.0;
						cc++;
					}
				}
				ATA.ColIndex[j2 + 1] = ATA.ColIndex[j2] + cc;
				set.Clear();
			}
			ATA.RowIndex = ATA_RowIndex.ToArray();
			ATA.Values = ATA_Value.ToArray();
			return ATA;
		}
		private unsafe void* Factorization(CCSMatrix C)
		{
			fixed (int* ri = C.RowIndex)
			{
				fixed (int* ci = C.ColIndex)
				{
					fixed (double* val = C.Values)
					{
						return MultigridHarmonicFieldSolver.CreaterCholeskySolver(C.ColumnSize, C.NumNonZero, ri, ci, val);
					}
				}
			}
		}
	}
}
