using System;
using System.ComponentModel;
using System.Drawing;
namespace IsolineEditing
{
	public class DisplayProperty
	{
		public enum EnumMeshDisplayMode
		{
			None,
			Points,
			Wireframe,
			FlatShaded,
			SmoothShaded,
			FlatShadedHiddenLine,
			SmoothShadedHiddenLine,
			TransparentSmoothShaded,
			TransparentFlatShaded
		}
		private DisplayProperty.EnumMeshDisplayMode meshDisplayMode = DisplayProperty.EnumMeshDisplayMode.SmoothShaded;
		private Color meshColor = Color.CornflowerBlue;
		private Color pointColor = Color.Blue;
		private Color lineColor = Color.Black;
		private float pointSize = 2f;
		private float lineWidth = 1f;
		private bool displaySelectedVertices = true;
		public DisplayProperty.EnumMeshDisplayMode MeshDisplayMode
		{
			get
			{
				return this.meshDisplayMode;
			}
			set
			{
				this.meshDisplayMode = value;
			}
		}
		[Category("Color")]
		public Color MeshColor
		{
			get
			{
				return this.meshColor;
			}
			set
			{
				this.meshColor = value;
			}
		}
		[Category("Color")]
		public Color PointColor
		{
			get
			{
				return this.pointColor;
			}
			set
			{
				this.pointColor = value;
			}
		}
		[Category("Color")]
		public Color LineColor
		{
			get
			{
				return this.lineColor;
			}
			set
			{
				this.lineColor = value;
			}
		}
		[Category("Element size")]
		public float PointSize
		{
			get
			{
				return this.pointSize;
			}
			set
			{
				this.pointSize = value;
				if (this.pointSize <= 0f)
				{
					this.pointSize = 0.1f;
				}
			}
		}
		[Category("Element size")]
		public float LineWidth
		{
			get
			{
				return this.lineWidth;
			}
			set
			{
				this.lineWidth = value;
			}
		}
		public bool DisplaySelectedVertices
		{
			get
			{
				return this.displaySelectedVertices;
			}
			set
			{
				this.displaySelectedVertices = value;
			}
		}
	}
}
