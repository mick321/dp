using System;
using System.ComponentModel;
namespace IsolineEditing
{
	[TypeConverter(typeof(DeformerConverter))]
	public interface Deformer
	{
		void Deform();
		void Update();
		void Display();
		void Move();
		void MouseDown();
		void MouseUp();
	}
}
