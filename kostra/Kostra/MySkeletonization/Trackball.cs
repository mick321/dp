using System;
namespace MyGeometry
{
	public class Trackball
	{
		public enum MotionType
		{
			None,
			Rotation,
			Pan,
			Scale
		}
		private Trackball.MotionType type;
		private Vector2d stPt;
		private Vector2d edPt;
		private Vector3d stVec;
		private Vector3d edVec;
		private Vector4d quat;
		private double w;
		private double h;
		private double adjustWidth;
		private double adjustHeight;
		public Trackball(double w, double h)
		{
			this.SetBounds(w, h);
		}
		public void SetBounds(double w, double h)
		{
			double b = (w < h) ? w : h;
			this.w = w / 2.0;
			this.h = h / 2.0;
			this.adjustWidth = 1.0 / ((b - 1.0) * 0.5);
			this.adjustHeight = 1.0 / ((b - 1.0) * 0.5);
		}
		public void Click(Vector2d pt, Trackball.MotionType type)
		{
			this.stPt = pt;
			this.stVec = this.MapToSphere(pt);
			this.type = type;
		}
		public void Drag(Vector2d pt)
		{
			this.edPt = pt;
			this.edVec = this.MapToSphere(pt);
			double epsilon = 1E-05;
			Vector3d prep = this.stVec.Cross(this.edVec);
			if (prep.Length() > epsilon)
			{
				this.quat = new Vector4d(prep, this.stVec.Dot(this.edVec));
				return;
			}
			this.quat = default(Vector4d);
		}
		public void End()
		{
			this.quat = default(Vector4d);
			this.type = Trackball.MotionType.None;
		}
		public Matrix4d GetMatrix()
		{
			if (this.type == Trackball.MotionType.Rotation)
			{
				return this.QuatToMatrix4d(this.quat);
			}
			if (this.type == Trackball.MotionType.Scale)
			{
				Matrix4d i = Matrix4d.IdentityMatrix();
				i[0, 0] = (i[1, 1] = (i[2, 2] = 1.0 + (this.edPt.x - this.stPt.x) * this.adjustWidth));
				return i;
			}
			if (this.type == Trackball.MotionType.Pan)
			{
				Matrix4d j = Matrix4d.IdentityMatrix();
				j[0, 3] = this.edPt.x - this.stPt.x;
				j[1, 3] = this.edPt.y - this.stPt.y;
				return j;
			}
			return Matrix4d.IdentityMatrix();
		}
		public double GetScale()
		{
			if (this.type == Trackball.MotionType.Scale)
			{
				return 1.0 + (this.edPt.x - this.stPt.x) * this.adjustWidth;
			}
			return 1.0;
		}
		private Vector3d MapToSphere(Vector2d pt)
		{
			Vector2d v = default(Vector2d);
			v.x = (this.w - pt.x) * this.adjustWidth;
			v.y = (this.h - pt.y) * this.adjustHeight;
			double lenSq = v.Dot(v);
			if (lenSq > 1.0)
			{
				double norm = 1.0 / Math.Sqrt(lenSq);
				return new Vector3d(v.x * norm, v.y * norm, 0.0);
			}
			return new Vector3d(v.x, v.y, Math.Sqrt(1.0 - lenSq));
		}
		private Matrix3d QuatToMatrix3d(Vector4d q)
		{
			double i = q.Dot(q);
			double s = (i > 0.0) ? (2.0 / i) : 0.0;
			double xs = q.x * s;
			double ys = q.y * s;
			double zs = q.z * s;
			double wx = q.w * xs;
			double wy = q.w * ys;
			double wz = q.w * zs;
			double xx = q.x * xs;
			double xy = q.x * ys;
			double xz = q.x * zs;
			double yy = q.y * ys;
			double yz = q.y * zs;
			double zz = q.z * zs;
			Matrix3d j = new Matrix3d();
			j[0, 0] = 1.0 - (yy + zz);
			j[1, 0] = xy - wz;
			j[2, 0] = xz + wy;
			j[0, 1] = xy + wz;
			j[1, 1] = 1.0 - (xx + zz);
			j[2, 1] = yz - wx;
			j[0, 2] = xz - wy;
			j[1, 2] = yz + wx;
			j[2, 2] = 1.0 - (xx + yy);
			return j;
		}
		private Matrix4d QuatToMatrix4d(Vector4d q)
		{
			double i = q.Dot(q);
			double s = (i > 0.0) ? (2.0 / i) : 0.0;
			double xs = q.x * s;
			double ys = q.y * s;
			double zs = q.z * s;
			double wx = q.w * xs;
			double wy = q.w * ys;
			double wz = q.w * zs;
			double xx = q.x * xs;
			double xy = q.x * ys;
			double xz = q.x * zs;
			double yy = q.y * ys;
			double yz = q.y * zs;
			double zz = q.z * zs;
			Matrix4d j = new Matrix4d();
			j[0, 0] = 1.0 - (yy + zz);
			j[1, 0] = xy - wz;
			j[2, 0] = xz + wy;
			j[0, 1] = xy + wz;
			j[1, 1] = 1.0 - (xx + zz);
			j[2, 1] = yz - wx;
			j[0, 2] = xz - wy;
			j[1, 2] = yz + wx;
			j[2, 2] = 1.0 - (xx + yy);
			j[3, 3] = 1.0;
			return j;
		}
	}
}
