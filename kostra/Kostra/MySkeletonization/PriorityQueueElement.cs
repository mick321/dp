using System;
namespace System.Collections.Generic
{
	public interface PriorityQueueElement : IComparable
	{
		int PQIndex
		{
			get;
			set;
		}
	}
}
