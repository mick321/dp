using System;
namespace MyGeometry
{
	public struct Vector2d
	{
		public static Vector2d MinValue = new Vector2d(-1.7976931348623157E+308, -1.7976931348623157E+308);
		public static Vector2d MaxValue = new Vector2d(1.7976931348623157E+308, 1.7976931348623157E+308);
		public double x;
		public double y;
		public double this[int index]
		{
			get
			{
				if (index == 0)
				{
					return this.x;
				}
				if (index == 1)
				{
					return this.y;
				}
				throw new ArgumentException();
			}
			set
			{
				if (index == 0)
				{
					this.x = value;
				}
				if (index == 1)
				{
					this.y = value;
				}
			}
		}
		public Vector2d(double x, double y)
		{
			this.x = x;
			this.y = y;
		}
		public Vector2d(double[] arr, int index)
		{
			this.x = arr[index];
			this.y = arr[index + 1];
		}
		public double Dot(Vector2d v)
		{
			return this.x * v.x + this.y * v.y;
		}
		public double Length()
		{
			return Math.Sqrt(this.x * this.x + this.y * this.y);
		}
		public Vector2d Normalize()
		{
			return this / this.Length();
		}
		public override string ToString()
		{
			return this.x.ToString() + " " + this.y.ToString();
		}
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			Vector2d v = (Vector2d)obj;
			return this.x == v.x && this.y == v.y;
		}
		public override int GetHashCode()
		{
			return (this.x + this.y).GetHashCode();
		}
		public static Vector2d Max(Vector2d v1, Vector2d v2)
		{
			return new Vector2d((v1.x > v2.x) ? v1.x : v2.x, (v1.y > v2.y) ? v1.y : v2.y);
		}
		public static Vector2d Min(Vector2d v1, Vector2d v2)
		{
			return new Vector2d((v1.x < v2.x) ? v1.x : v2.x, (v1.y < v2.y) ? v1.y : v2.y);
		}
		public static Vector2d operator +(Vector2d v1, Vector2d v2)
		{
			return new Vector2d(v1.x + v2.x, v1.y + v2.y);
		}
		public static Vector2d operator -(Vector2d v1, Vector2d v2)
		{
			return new Vector2d(v1.x - v2.x, v1.y - v2.y);
		}
		public static Vector2d operator *(Vector2d v, double s)
		{
			return new Vector2d(v.x * s, v.y * s);
		}
		public static Vector2d operator *(double s, Vector2d v)
		{
			return new Vector2d(v.x * s, v.y * s);
		}
		public static Vector2d operator /(Vector2d v, double s)
		{
			return new Vector2d(v.x / s, v.y / s);
		}
		public static bool operator ==(Vector2d v1, Vector2d v2)
		{
			return v1.x == v2.x && v1.y == v2.y;
		}
		public static bool operator !=(Vector2d v1, Vector2d v2)
		{
			return !(v1 == v2);
		}
	}
}
