using MyGeometry;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
namespace IsolineEditing
{
	public class MultigridContractionSolver
	{
		private class ColMatrixCreator
		{
			public int m;
			public int n;
			public List<int>[] rowIndex;
			public List<double>[] values;
			public int RowSize
			{
				get
				{
					return this.m;
				}
			}
			public int ColumnSize
			{
				get
				{
					return this.n;
				}
			}
			public ColMatrixCreator(int m, int n)
			{
				this.m = m;
				this.n = n;
				this.rowIndex = new List<int>[n];
				this.values = new List<double>[n];
				for (int i = 0; i < n; i++)
				{
					this.rowIndex[i] = new List<int>();
					this.values[i] = new List<double>();
				}
			}
			public void AddValueTo(int i, int j, double value)
			{
				List<int> r = this.rowIndex[j];
				if (i >= this.m)
				{
					this.m = i + 1;
				}
				int ri = -1;
				for (int k = 0; k < r.Count; k++)
				{
					if (r[k] == i)
					{
						ri = k;
						break;
					}
				}
				if (ri == -1)
				{
					r.Add(i);
					this.values[j].Add(value);
					return;
				}
				List<double> list;
				int index;
				(list = this.values[j])[index = ri] = list[index] + value;
			}
			public MultigridContractionSolver.ColMatrix ToColMatrix()
			{
				MultigridContractionSolver.ColMatrix C = new MultigridContractionSolver.ColMatrix(this.m, this.n);
				for (int i = 0; i < this.n; i++)
				{
					try
					{
						C.rowIndex[i] = this.rowIndex[i].ToArray();
					}
					catch (OutOfMemoryException)
					{
						GC.Collect();
						C.rowIndex[i] = this.rowIndex[i].ToArray();
					}
					this.rowIndex[i] = null;
					try
					{
						C.values[i] = this.values[i].ToArray();
					}
					catch (OutOfMemoryException)
					{
						GC.Collect();
						C.values[i] = this.values[i].ToArray();
					}
					this.values[i] = null;
				}
				return C;
			}
		}
		private class ColMatrix
		{
			public int m;
			public int n;
			public int[][] rowIndex;
			public double[][] values;
			public int RowSize
			{
				get
				{
					return this.m;
				}
			}
			public int ColumnSize
			{
				get
				{
					return this.n;
				}
			}
			public ColMatrix(int m, int n)
			{
				this.m = m;
				this.n = n;
				this.rowIndex = new int[n][];
				this.values = new double[n][];
				for (int i = 0; i < n; i++)
				{
					this.rowIndex[i] = null;
					this.values[i] = null;
				}
			}
			public int NumOfElements()
			{
				int c = 0;
				int[][] array = this.rowIndex;
				for (int i = 0; i < array.Length; i++)
				{
					int[] r = array[i];
					c += r.Length;
				}
				return c;
			}
			public void Multiply(double[] xIn, double[] xOut)
			{
				if (xIn.Length < this.n || xOut.Length < this.m)
				{
					throw new ArgumentException();
				}
				for (int i = 0; i < this.m; i++)
				{
					xOut[i] = 0.0;
				}
				for (int j = 0; j < this.n; j++)
				{
					int[] r = this.rowIndex[j];
					double[] v = this.values[j];
					for (int k = 0; k < r.Length; k++)
					{
						int ri = r[k];
						xOut[ri] += v[k] * xIn[j];
					}
				}
			}
			public void PreMultiply(double[] xIn, double[] xOut)
			{
				if (xIn.Length < this.m || xOut.Length < this.n)
				{
					throw new ArgumentException();
				}
				for (int i = 0; i < this.n; i++)
				{
					xOut[i] = 0.0;
				}
				for (int j = 0; j < this.n; j++)
				{
					double sum = 0.0;
					int[] r = this.rowIndex[j];
					double[] v = this.values[j];
					for (int k = 0; k < r.Length; k++)
					{
						int ri = r[k];
						sum += v[k] * xIn[ri];
					}
					xOut[j] = sum;
				}
			}
			public void PreMultiply(double[] xIn, double[] xOut, int[] index)
			{
				if (xIn.Length < this.m || xOut.Length < this.n)
				{
					throw new ArgumentException();
				}
				for (int i = 0; i < this.n; i++)
				{
					xOut[i] = 0.0;
				}
				for (int l = 0; l < index.Length; l++)
				{
					int j = index[l];
					double sum = 0.0;
					int[] r = this.rowIndex[j];
					double[] v = this.values[j];
					for (int k = 0; k < r.Length; k++)
					{
						int ri = r[k];
						sum += v[k] * xIn[ri];
					}
					xOut[j] = sum;
				}
			}
			public void PreMultiplyOffset(double[] xIn, double[] xOut, int startOut, int offsetOut)
			{
				for (int i = startOut; i < this.n + offsetOut; i += offsetOut)
				{
					xOut[i] = 0.0;
				}
				int j = 0;
				int k = startOut;
				while (j < this.n)
				{
					double sum = 0.0;
					int[] r = this.rowIndex[j];
					double[] v = this.values[j];
					for (int l = 0; l < r.Length; l++)
					{
						int ri = r[l];
						sum += v[l] * xIn[ri];
					}
					xOut[k] = sum;
					j++;
					k += offsetOut;
				}
			}
			public MultigridContractionSolver.ColMatrix Transpose()
			{
				MultigridContractionSolver.ColMatrixCreator C = new MultigridContractionSolver.ColMatrixCreator(this.n, this.m);
				for (int i = 0; i < this.n; i++)
				{
					int[] r = this.rowIndex[i];
					double[] v = this.values[i];
					for (int j = 0; j < r.Length; j++)
					{
						C.rowIndex[r[j]].Add(i);
						C.values[r[j]].Add(v[j]);
					}
				}
				return C;
			}
			public static implicit operator MultigridContractionSolver.ColMatrix(MultigridContractionSolver.ColMatrixCreator CC)
			{
				return CC.ToColMatrix();
			}
			public int ATACG(double[] x, double[] b, double eps, int maxIter)
			{
				double[] inv = new double[this.n];
				double[] r = new double[this.n];
				double[] d = new double[this.n];
				double[] q = new double[this.n];
				double[] s = new double[this.n];
				double errNew = 0.0;
				double[] tmp = new double[this.m];
				for (int i = 0; i < this.n; i++)
				{
					inv[i] = 0.0;
				}
				for (int j = 0; j < this.n; j++)
				{
					for (int k = 0; k < this.rowIndex[j].Length; k++)
					{
						double val = this.values[j][k];
						inv[j] += val * val;
					}
				}
				for (int l = 0; l < this.n; l++)
				{
					inv[l] = 1.0 / inv[l];
				}
				int iter = 0;
				this.Multiply(x, tmp);
				this.PreMultiply(tmp, r);
				for (int m = 0; m < this.n; m++)
				{
					r[m] = b[m] - r[m];
				}
				for (int n = 0; n < this.n; n++)
				{
					d[n] = inv[n] * r[n];
				}
				for (int i2 = 0; i2 < this.n; i2++)
				{
					errNew += r[i2] * d[i2];
				}
				double err = errNew;
				Program.PrintText("err: " + err.ToString());
				while (iter < maxIter && errNew > eps * err)
				{
					this.Multiply(d, tmp);
					this.PreMultiply(tmp, q);
					double alpha = 0.0;
					for (int i3 = 0; i3 < this.n; i3++)
					{
						alpha += d[i3] * q[i3];
					}
					alpha = errNew / alpha;
					for (int i4 = 0; i4 < this.n; i4++)
					{
						x[i4] += alpha * d[i4];
					}
					if (iter % 50 == 0)
					{
						this.Multiply(x, tmp);
						this.PreMultiply(tmp, r);
						for (int i5 = 0; i5 < this.n; i5++)
						{
							r[i5] = b[i5] - r[i5];
						}
					}
					else
					{
						for (int i6 = 0; i6 < this.n; i6++)
						{
							r[i6] -= alpha * q[i6];
						}
					}
					for (int i7 = 0; i7 < this.n; i7++)
					{
						s[i7] = inv[i7] * r[i7];
					}
					double errOld = errNew;
					errNew = 0.0;
					for (int i8 = 0; i8 < this.n; i8++)
					{
						errNew += r[i8] * s[i8];
					}
					double beta = errNew / errOld;
					for (int i9 = 0; i9 < this.n; i9++)
					{
						d[i9] = s[i9] + beta * d[i9];
					}
					iter++;
				}
				return iter;
			}
		}
		public class Resolution
		{
			public int[] vertexList;
			public int[] faceList;
			public Set<int>[] adjVertex;
			public Set<int>[] adjFace;
			public int[] collapsedIndex;
			public Dictionary<int, double>[] weight;
			public Resolution(Mesh mesh)
			{
				int vn = mesh.VertexCount;
				this.vertexList = new int[vn];
				this.faceList = (int[])mesh.FaceIndex.Clone();
				this.adjVertex = new Set<int>[vn];
				this.adjFace = new Set<int>[vn];
				for (int i = 0; i < vn; i++)
				{
					this.vertexList[i] = i;
					this.adjVertex[i] = new Set<int>(8);
					this.adjFace[i] = new Set<int>(8);
					int[] array = mesh.AdjVV[i];
					for (int j = 0; j < array.Length; j++)
					{
						int adj = array[j];
						this.adjVertex[i].Add(adj);
					}
					int[] array2 = mesh.AdjVF[i];
					for (int k = 0; k < array2.Length; k++)
					{
						int adj2 = array2[k];
						this.adjFace[i].Add(adj2);
					}
				}
			}
			public Resolution()
			{
			}
		}
		private Mesh mesh;
		private int targetVertexCount = 2000;
		private List<MultigridContractionSolver.Resolution> resolutions = new List<MultigridContractionSolver.Resolution>();
		private double convergeRatio = 0.001;
		public int Levels
		{
			get
			{
				return this.resolutions.Count;
			}
		}
		public List<MultigridContractionSolver.Resolution> Resolutions
		{
			get
			{
				return this.resolutions;
			}
		}
		[DllImport("taucs.dll")]
		private unsafe static extern void* CreaterCholeskySolver(int n, int nnz, int* rowIndex, int* colIndex, double* value);
		[DllImport("taucs.dll")]
		private unsafe static extern int Solve(void* solver, double* x, double* b);
		[DllImport("taucs.dll")]
		private unsafe static extern double SolveEx(void* solver, double* x, int xIndex, double* b, int bIndex);
		[DllImport("taucs.dll")]
		private unsafe static extern int FreeSolver(void* sp);
		public MultigridContractionSolver(Mesh mesh)
		{
			Program.PrintText("[Create Multigrid Solver]");
			this.mesh = mesh;
			this.resolutions.Add(new MultigridContractionSolver.Resolution(mesh));
			while (this.resolutions[this.resolutions.Count - 1].vertexList.Length > this.targetVertexCount)
			{
				MultigridContractionSolver.Resolution r = this.resolutions[this.resolutions.Count - 1];
				Program.PrintText(r.vertexList.Length.ToString());
				this.resolutions.Add(this.GetNextLevel(r));
			}
			Program.PrintText(this.resolutions[this.resolutions.Count - 1].vertexList.Length.ToString());
			this.resolutions.Reverse();
		}
		private MultigridContractionSolver.Resolution GetNextLevel(MultigridContractionSolver.Resolution r)
		{
			int vn = this.mesh.VertexCount;
			int[] oldMap = new int[vn];
			for (int i = 0; i < vn; i++)
			{
				oldMap[i] = -1;
			}
			for (int j = 0; j < r.vertexList.Length; j++)
			{
				oldMap[r.vertexList[j]] = j;
			}
			bool[] marked = new bool[vn];
			for (int k = 0; k < vn; k++)
			{
				marked[k] = false;
			}
			List<int> collapsedIndex = new List<int>();
			List<int> remainingIndex = new List<int>();
			for (int l = 0; l < r.vertexList.Length; l++)
			{
				int index = r.vertexList[l];
				bool found = false;
				foreach (int adj in r.adjVertex[l])
				{
					int count = 0;
					foreach (int adj2 in r.adjVertex[oldMap[adj]])
					{
						if (r.adjVertex[oldMap[adj2]].Contains(index))
						{
							count++;
						}
					}
					if (count == 2)
					{
						found = true;
						break;
					}
				}
				if (!found)
				{
					marked[index] = true;
				}
				if (marked[index])
				{
					remainingIndex.Add(index);
				}
				else
				{
					collapsedIndex.Add(index);
					foreach (int adj3 in r.adjVertex[l])
					{
						marked[adj3] = true;
					}
				}
			}
			int[] collapseTo = new int[vn];
			for (int m = 0; m < vn; m++)
			{
				collapseTo[m] = m;
			}
			for (int n = 0; n < collapsedIndex.Count; n++)
			{
				int index2 = collapsedIndex[n];
				Vector3d p = new Vector3d(this.mesh.VertexPos, index2 * 3);
				double minLen = 1.7976931348623157E+308;
				int minAdj = -1;
				foreach (int adj4 in r.adjVertex[oldMap[index2]])
				{
					int count2 = 0;
					foreach (int adj5 in r.adjVertex[oldMap[adj4]])
					{
						if (r.adjVertex[oldMap[adj5]].Contains(index2))
						{
							count2++;
						}
					}
					if (count2 == 2)
					{
						if (adj4 == index2)
						{
							Program.PrintText("?");
						}
						else
						{
							Vector3d q = new Vector3d(this.mesh.VertexPos, adj4 * 3);
							double len = (p - q).Length();
							if (len < minLen)
							{
								minLen = len;
								minAdj = adj4;
							}
							if (adj4 == index2)
							{
								throw new Exception();
							}
						}
					}
				}
				if (minAdj == -1)
				{
					throw new Exception();
				}
				collapseTo[index2] = minAdj;
			}
			Set<int>[] tmpAdjList = new Set<int>[r.vertexList.Length];
			for (int i2 = 0; i2 < r.vertexList.Length; i2++)
			{
				Set<int> s = new Set<int>(6);
				foreach (int adj6 in r.adjVertex[i2])
				{
					s.Add(adj6);
				}
				tmpAdjList[i2] = s;
			}
			foreach (int index3 in collapsedIndex)
			{
				int i3 = oldMap[index3];
				int to = collapseTo[index3];
				int k2 = oldMap[to];
				foreach (int adj7 in r.adjVertex[i3])
				{
					int j2 = oldMap[adj7];
					tmpAdjList[j2].Remove(index3);
					if (adj7 != to && j2 != -1)
					{
						tmpAdjList[j2].Add(to);
						tmpAdjList[k2].Add(adj7);
					}
				}
			}
			List<Set<int>> newAdjVertexIndex = new List<Set<int>>();
			for (int i4 = 0; i4 < r.vertexList.Length; i4++)
			{
				int index4 = r.vertexList[i4];
				if (marked[index4])
				{
					newAdjVertexIndex.Add(tmpAdjList[i4]);
				}
			}
			List<int> newFaceIndex = new List<int>();
			for (int i5 = 0; i5 < r.faceList.Length; i5 += 3)
			{
				int c = r.faceList[i5];
				int c2 = r.faceList[i5 + 1];
				int c3 = r.faceList[i5 + 2];
				c = collapseTo[c];
				c2 = collapseTo[c2];
				c3 = collapseTo[c3];
				if (c != c2 && c2 != c3 && c3 != c)
				{
					newFaceIndex.Add(c);
					newFaceIndex.Add(c2);
					newFaceIndex.Add(c3);
				}
			}
			int[] map = new int[vn];
			for (int i6 = 0; i6 < vn; i6++)
			{
				map[i6] = -1;
			}
			Set<int>[] newAdjFaceIndex = new Set<int>[remainingIndex.Count];
			for (int i7 = 0; i7 < remainingIndex.Count; i7++)
			{
				map[remainingIndex[i7]] = i7;
				newAdjFaceIndex[i7] = new Set<int>();
			}
			int i8 = 0;
			int j3 = 0;
			while (i8 < newFaceIndex.Count)
			{
				int c4 = map[newFaceIndex[i8]];
				int c5 = map[newFaceIndex[i8 + 1]];
				int c6 = map[newFaceIndex[i8 + 2]];
				if (c4 == -1 || c5 == -1 || c6 == -1)
				{
					throw new Exception();
				}
				newAdjFaceIndex[c4].Add(j3);
				newAdjFaceIndex[c5].Add(j3);
				newAdjFaceIndex[c6].Add(j3);
				i8 += 3;
				j3++;
			}
			Dictionary<int, double>[] interpolateWeight = new Dictionary<int, double>[collapsedIndex.Count];
			for (int i9 = 0; i9 < collapsedIndex.Count; i9++)
			{
				Dictionary<int, double> dict = new Dictionary<int, double>(6);
				interpolateWeight[i9] = dict;
				int index5 = collapsedIndex[i9];
				int j4 = oldMap[index5];
				foreach (int adj8 in r.adjFace[j4])
				{
					int k3 = adj8 * 3;
					int c7 = r.faceList[k3];
					int c8 = r.faceList[k3 + 1];
					int c9 = r.faceList[k3 + 2];
					if (c8 == index5)
					{
						c8 = c9;
						c9 = c7;
						c7 = index5;
					}
					if (c9 == index5)
					{
						c9 = c8;
						c8 = c7;
						c7 = index5;
					}
					Vector3d v = new Vector3d(this.mesh.VertexPos, c7 * 3);
					Vector3d v2 = new Vector3d(this.mesh.VertexPos, c8 * 3);
					Vector3d v3 = new Vector3d(this.mesh.VertexPos, c9 * 3);
					double cot = (v2 - v).Dot(v3 - v) / (v2 - v).Cross(v3 - v).Length();
					double cot2 = (v3 - v2).Dot(v - v2) / (v3 - v2).Cross(v - v2).Length();
					double cot3 = (v - v3).Dot(v2 - v3) / (v - v3).Cross(v2 - v3).Length();
					if (double.IsNaN(cot))
					{
						throw new Exception();
					}
					if (double.IsNaN(cot2))
					{
						throw new Exception();
					}
					if (double.IsNaN(cot3))
					{
						throw new Exception();
					}
					if (dict.ContainsKey(c9))
					{
						Dictionary<int, double> dictionary;
						int key;
						(dictionary = dict)[key = c9] = dictionary[key] + cot2;
					}
					else
					{
						dict.Add(c9, cot2);
					}
					if (dict.ContainsKey(c8))
					{
						Dictionary<int, double> dictionary;
						int key;
						(dictionary = dict)[key = c8] = dictionary[key] + cot3;
					}
					else
					{
						dict.Add(c8, cot3);
					}
				}
			}
			return new MultigridContractionSolver.Resolution
			{
				vertexList = remainingIndex.ToArray(),
				faceList = newFaceIndex.ToArray(),
				adjVertex = newAdjVertexIndex.ToArray(),
				adjFace = newAdjFaceIndex,
				collapsedIndex = collapsedIndex.ToArray(),
				weight = interpolateWeight
			};
		}
		public unsafe double[][] SolveSystem(double[] lapWeight, double[] posWeight)
		{
			int vn = this.mesh.VertexCount;
			int arg_17_0 = this.mesh.FaceCount;
			double[][] pos = new double[][]
			{
				new double[vn],
				new double[vn],
				new double[vn]
			};
			int i = 0;
			int j = 0;
			while (i < vn)
			{
				pos[0][i] = this.mesh.VertexPos[j];
				pos[1][i] = this.mesh.VertexPos[j + 1];
				pos[2][i] = this.mesh.VertexPos[j + 2];
				i++;
				j += 3;
			}
			for (int level = 0; level < this.resolutions.Count; level++)
			{
				MultigridContractionSolver.Resolution r = this.resolutions[level];
				MultigridContractionSolver.ColMatrix colA = this.BuildMatrixA(r, lapWeight, posWeight);
				CCSMatrix ccsATA = this.MultiplyATA(colA);
				string s = "#: " + r.vertexList.Length + " iter: ";
				int k = r.vertexList.Length;
				double[] x = new double[k];
				double[] b = new double[k * 2];
				double[] ATb = new double[k];
				void* solver = null;
				if (level == 0)
				{
					solver = this.Factorization(ccsATA);
				}
				for (int l = 0; l < 3; l++)
				{
					for (int m = 0; m < k; m++)
					{
						b[m] = 0.0;
						int n = r.vertexList[m];
						b[m + k] = this.mesh.VertexPos[n * 3 + l] * posWeight[n];
						x[m] = pos[l][n];
					}
					colA.PreMultiply(b, ATb);
					if (level == 0)
					{
						fixed (double* _x = x, _ATb = ATb)
						{
							MultigridContractionSolver.Solve(solver, _x, _ATb);
						}
					}
					else
					{
						int iter = colA.ATACG(x, ATb, this.convergeRatio, k);
						s = s + " " + iter;
					}
					for (int j2 = 0; j2 < k; j2++)
					{
						pos[l][r.vertexList[j2]] = x[j2];
					}
				}
				Program.PrintText(s);
				if (level < this.resolutions.Count - 1)
				{
					for (int i2 = 0; i2 < r.collapsedIndex.Length; i2++)
					{
						int index = r.collapsedIndex[i2];
						Vector3d p = default(Vector3d);
						double totWeight = 0.0;
						foreach (int adj in r.weight[i2].Keys)
						{
							double w = r.weight[i2][adj];
							p.x += pos[0][adj] * w;
							p.y += pos[1][adj] * w;
							p.z += pos[2][adj] * w;
							totWeight += w;
						}
						pos[0][index] = (p.x /= totWeight);
						pos[1][index] = (p.y /= totWeight);
						pos[2][index] = (p.z /= totWeight);
					}
				}
			}
			return pos;
		}
		private MultigridContractionSolver.ColMatrix BuildMatrixA(MultigridContractionSolver.Resolution r, double[] lapWeight, double[] posWeight)
		{
			int[] map = new int[this.mesh.VertexCount];
			for (int i = 0; i < r.vertexList.Length; i++)
			{
				map[r.vertexList[i]] = i;
			}
			int j = r.vertexList.Length;
			int fn = r.faceList.Length / 3;
			SparseMatrix A = new SparseMatrix(2 * j, j);
			int k = 0;
			int l = 0;
			while (k < fn)
			{
				int c = r.faceList[l];
				int c2 = r.faceList[l + 1];
				int c3 = r.faceList[l + 2];
				Vector3d v = new Vector3d(this.mesh.VertexPos, c * 3);
				Vector3d v2 = new Vector3d(this.mesh.VertexPos, c2 * 3);
				Vector3d v3 = new Vector3d(this.mesh.VertexPos, c3 * 3);
				double cot = (v2 - v).Dot(v3 - v) / (v2 - v).Cross(v3 - v).Length();
				double cot2 = (v3 - v2).Dot(v - v2) / (v3 - v2).Cross(v - v2).Length();
				double cot3 = (v - v3).Dot(v2 - v3) / (v - v3).Cross(v2 - v3).Length();
				if (double.IsNaN(cot))
				{
					throw new Exception();
				}
				if (double.IsNaN(cot2))
				{
					throw new Exception();
				}
				if (double.IsNaN(cot3))
				{
					throw new Exception();
				}
				c = map[c];
				c2 = map[c2];
				c3 = map[c3];
				A.AddValueTo(c2, c2, -cot);
				A.AddValueTo(c2, c3, cot);
				A.AddValueTo(c3, c3, -cot);
				A.AddValueTo(c3, c2, cot);
				A.AddValueTo(c3, c3, -cot2);
				A.AddValueTo(c3, c, cot2);
				A.AddValueTo(c, c, -cot2);
				A.AddValueTo(c, c3, cot2);
				A.AddValueTo(c, c, -cot3);
				A.AddValueTo(c, c2, cot3);
				A.AddValueTo(c2, c2, -cot3);
				A.AddValueTo(c2, c, cot3);
				k++;
				l += 3;
			}
			for (int m = 0; m < j; m++)
			{
				double tot = 0.0;
				foreach (SparseMatrix.Element e in A.Rows[m])
				{
					if (e.i != e.j)
					{
						tot += e.value;
					}
				}
				if (tot > 10000.0)
				{
					foreach (SparseMatrix.Element e2 in A.Rows[m])
					{
						e2.value /= tot / 10000.0;
					}
				}
				foreach (SparseMatrix.Element e3 in A.Rows[m])
				{
					e3.value *= lapWeight[r.vertexList[m]];
				}
			}
			for (int n = 0; n < j; n++)
			{
				A.AddElement(n + j, n, posWeight[r.vertexList[n]]);
			}
			A.SortElement();
			MultigridContractionSolver.ColMatrixCreator cmA = new MultigridContractionSolver.ColMatrixCreator(2 * j, j);
			foreach (List<SparseMatrix.Element> row in A.Rows)
			{
				foreach (SparseMatrix.Element e4 in row)
				{
					cmA.AddValueTo(e4.i, e4.j, e4.value);
				}
			}
			return cmA;
		}
		private CCSMatrix MultiplyATA(MultigridContractionSolver.ColMatrix A)
		{
			int[] count = new int[A.RowSize];
			for (int i = 0; i < count.Length; i++)
			{
				count[i] = 0;
			}
			int[][] rowIndex = A.rowIndex;
			for (int num = 0; num < rowIndex.Length; num++)
			{
				int[] r = rowIndex[num];
				int[] array = r;
				for (int num2 = 0; num2 < array.Length; num2++)
				{
					int ri = array[num2];
					count[ri]++;
				}
			}
			int[][] colIndex = new int[A.RowSize][];
			int[][] listIndex = new int[A.RowSize][];
			for (int j = 0; j < A.RowSize; j++)
			{
				colIndex[j] = new int[count[j]];
				listIndex[j] = new int[count[j]];
			}
			for (int k = 0; k < count.Length; k++)
			{
				count[k] = 0;
			}
			for (int l = 0; l < A.values.Length; l++)
			{
				int[] row = A.rowIndex[l];
				for (int m = 0; m < row.Length; m++)
				{
					int r2 = row[m];
					int c = count[r2];
					colIndex[r2][c] = l;
					listIndex[r2][c] = m;
					count[r2]++;
				}
			}
			CCSMatrix ATA = new CCSMatrix(A.ColumnSize, A.ColumnSize);
			Set<int> set = new Set<int>();
			double[] tmp = new double[A.ColumnSize];
			List<int> ATA_RowIndex = new List<int>();
			List<double> ATA_Value = new List<double>();
			for (int n = 0; n < A.ColumnSize; n++)
			{
				tmp[n] = 0.0;
			}
			for (int j2 = 0; j2 < A.ColumnSize; j2++)
			{
				for (int ri2 = 0; ri2 < A.rowIndex[j2].Length; ri2++)
				{
					int k2 = A.rowIndex[j2][ri2];
					double val = A.values[j2][ri2];
					for (int k3 = 0; k3 < colIndex[k2].Length; k3++)
					{
						int i2 = colIndex[k2][k3];
						if (i2 >= j2)
						{
							set.Add(i2);
							tmp[i2] += val * A.values[i2][listIndex[k2][k3]];
						}
					}
				}
				int[] s = set.ToArray();
				Array.Sort<int>(s);
				int cc = 0;
				int[] array2 = s;
				for (int num3 = 0; num3 < array2.Length; num3++)
				{
					int k4 = array2[num3];
					if (tmp[k4] != 0.0)
					{
						ATA_RowIndex.Add(k4);
						ATA_Value.Add(tmp[k4]);
						tmp[k4] = 0.0;
						cc++;
					}
				}
				ATA.ColIndex[j2 + 1] = ATA.ColIndex[j2] + cc;
				set.Clear();
			}
			ATA.RowIndex = ATA_RowIndex.ToArray();
			ATA.Values = ATA_Value.ToArray();
			return ATA;
		}
		private unsafe void* Factorization(CCSMatrix C)
		{
			fixed (int* ri = C.RowIndex)
			{
				fixed (int* ci = C.ColIndex)
				{
					fixed (double* val = C.Values)
					{
						return MultigridContractionSolver.CreaterCholeskySolver(C.ColumnSize, C.NumNonZero, ri, ci, val);
					}
				}
			}
		}
	}
}
