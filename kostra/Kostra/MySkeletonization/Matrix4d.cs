using NumericalRecipes;
using System;
using System.IO;
using System.Xml.Serialization;
namespace MyGeometry
{
	[XmlRoot(IsNullable = false)]
	public class Matrix4d
	{
		private const int len = 16;
		private const int row_size = 4;
		private double[] e = new double[16];
		public double this[int index]
		{
			get
			{
				return this.e[index];
			}
			set
			{
				this.e[index] = value;
			}
		}
		public double this[int row, int column]
		{
			get
			{
				return this.e[row * 4 + column];
			}
			set
			{
				this.e[row * 4 + column] = value;
			}
		}
		public double[] Element
		{
			get
			{
				return this.e;
			}
			set
			{
				if (value.Length < 16)
				{
					throw new Exception();
				}
				this.e = value;
			}
		}
		public Matrix4d()
		{
		}
		public Matrix4d(double[] arr)
		{
			for (int i = 0; i < 16; i++)
			{
				this.e[i] = arr[i];
			}
		}
		public Matrix4d(double[,] arr)
		{
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					this[i, j] = arr[i, j];
				}
			}
		}
		public Matrix4d(Matrix4d m) : this(m.e)
		{
		}
		public Matrix4d(Matrix3d m)
		{
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					this[i, j] = m[i, j];
				}
			}
		}
		public Matrix4d(StreamReader sr)
		{
			int c = 0;
			char[] delimiters = new char[]
			{
				' ',
				'\t'
			};
			while (sr.Peek() > -1)
			{
				string s = sr.ReadLine();
				string[] tokens = s.Split(delimiters);
				for (int i = 0; i < tokens.Length; i++)
				{
					this.e[c++] = double.Parse(tokens[i]);
					if (c >= 16)
					{
						return;
					}
				}
			}
		}
		public double[] ToArray()
		{
			return (double[])this.e.Clone();
		}
		public Matrix3d ToMatrix3d()
		{
			Matrix3d ret = new Matrix3d();
			ret[0] = this.e[0];
			ret[1] = this.e[1];
			ret[2] = this.e[2];
			ret[3] = this.e[4];
			ret[4] = this.e[5];
			ret[5] = this.e[6];
			ret[6] = this.e[8];
			ret[7] = this.e[9];
			ret[8] = this.e[10];
			return ret;
		}
		public double Trace()
		{
			return this.e[0] + this.e[5] + this.e[10] + this.e[15];
		}
		public Matrix4d Inverse()
		{
			SVD svd = new SVD(this.e, 4, 4);
			if (!svd.State)
			{
				throw new ArithmeticException();
			}
			return new Matrix4d(svd.Inverse);
		}
		public Matrix4d Transpose()
		{
			Matrix4d i = new Matrix4d();
			for (int j = 0; j < 4; j++)
			{
				for (int k = 0; k < 4; k++)
				{
					i[k, j] = this[j, k];
				}
			}
			return i;
		}
		public static Matrix4d IdentityMatrix()
		{
			Matrix4d i = new Matrix4d();
			i[0] = (i[5] = (i[10] = (i[15] = 1.0)));
			return i;
		}
		public static Vector4d operator *(Vector4d v, Matrix4d m)
		{
			return m.Transpose() * v;
		}
		public static Matrix4d operator *(Matrix4d m1, Matrix4d m2)
		{
			Matrix4d ret = new Matrix4d();
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					ret[i, j] = 0.0;
					for (int k = 0; k < 4; k++)
					{
						Matrix4d matrix4d;
						int row;
						int column;
						(matrix4d = ret)[row = i, column = j] = matrix4d[row, column] + m1[i, k] * m2[k, j];
					}
				}
			}
			return ret;
		}
		public static Vector4d operator *(Matrix4d m, Vector4d v)
		{
			return new Vector4d
			{
				x = m[0] * v.x + m[1] * v.y + m[2] * v.z + m[3] * v.w,
				y = m[4] * v.x + m[5] * v.y + m[6] * v.z + m[7] * v.w,
				z = m[8] * v.x + m[9] * v.y + m[10] * v.z + m[11] * v.w,
				w = m[12] * v.x + m[13] * v.y + m[14] * v.z + m[15] * v.w
			};
		}
		public static Matrix4d operator +(Matrix4d m1, Matrix4d m2)
		{
			Matrix4d ret = new Matrix4d();
			for (int i = 0; i < 16; i++)
			{
				ret[i] = m1[i] + m2[i];
			}
			return ret;
		}
		public static Matrix4d operator -(Matrix4d m1, Matrix4d m2)
		{
			Matrix4d ret = new Matrix4d();
			for (int i = 0; i < 16; i++)
			{
				ret[i] = m1[i] - m2[i];
			}
			return ret;
		}
		public static Matrix4d operator *(Matrix4d m, double d)
		{
			Matrix4d ret = new Matrix4d();
			for (int i = 0; i < 16; i++)
			{
				ret[i] = m[i] * d;
			}
			return ret;
		}
		public override string ToString()
		{
			string s = "";
			double[] array = this.e;
			for (int i = 0; i < array.Length; i++)
			{
				s = s + array[i].ToString() + "\n";
			}
			return s;
		}
	}
}
