using MyGeometry;
using System;
using System.ComponentModel;
using System.IO;
namespace IsolineEditing
{
	public class MeshRecord
	{
		private string filename;
		private Mesh mesh;
		private Matrix4d modelViewMatrix = Matrix4d.IdentityMatrix();
		private Deformer deformer;
		private Skeletonizer skeletonizer;
		public string Filename
		{
			get
			{
				return this.filename;
			}
		}
		public int VertexCount
		{
			get
			{
				return this.mesh.VertexCount;
			}
		}
		public int FaceCount
		{
			get
			{
				return this.mesh.FaceCount;
			}
		}
		[Browsable(false)]
		public Mesh Mesh
		{
			get
			{
				return this.mesh;
			}
		}
		[Browsable(false)]
		public Matrix4d ModelViewMatrix
		{
			get
			{
				return this.modelViewMatrix;
			}
			set
			{
				this.modelViewMatrix = value;
			}
		}
		public Deformer Deformer
		{
			get
			{
				return this.deformer;
			}
			set
			{
				this.deformer = value;
			}
		}
		public Skeletonizer Skeletonizer
		{
			get
			{
				return this.skeletonizer;
			}
			set
			{
				this.skeletonizer = value;
			}
		}
		public MeshRecord(string filename, Mesh mesh)
		{
			this.filename = filename;
			this.mesh = mesh;
		}
		public override string ToString()
		{
			return Path.GetFileName(this.filename);
		}
	}
}
