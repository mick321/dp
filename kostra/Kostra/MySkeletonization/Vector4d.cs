using System;
namespace MyGeometry
{
	public struct Vector4d
	{
		public double x;
		public double y;
		public double z;
		public double w;
		public Vector4d(double x, double y, double z, double w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}
		public Vector4d(Vector2d v)
		{
			this.x = v.x;
			this.y = v.y;
			this.z = 0.0;
			this.w = 0.0;
		}
		public Vector4d(Vector2d v, double z, double w)
		{
			this.x = v.x;
			this.y = v.y;
			this.z = z;
			this.w = w;
		}
		public Vector4d(Vector3d v)
		{
			this.x = v.x;
			this.y = v.y;
			this.z = v.z;
			this.w = 0.0;
		}
		public Vector4d(Vector3d v, double w)
		{
			this.x = v.x;
			this.y = v.y;
			this.z = v.z;
			this.w = w;
		}
		public Vector4d(double[] arr, int index)
		{
			this.x = arr[index];
			this.y = arr[index + 1];
			this.z = arr[index + 2];
			this.w = arr[index + 3];
		}
		public double Dot(Vector4d v)
		{
			return this.x * v.x + this.y * v.y + this.z * v.z + this.w * v.w;
		}
		public double Length()
		{
			return Math.Sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
		}
		public Vector4d Normalize()
		{
			return this / this.Length();
		}
		public Matrix4d OuterCross(Vector4d v)
		{
			Matrix4d i = new Matrix4d();
			i[0, 0] = this.x * v.x;
			i[0, 1] = this.x * v.y;
			i[0, 2] = this.x * v.z;
			i[0, 3] = this.x * v.w;
			i[1, 0] = this.y * v.x;
			i[1, 1] = this.y * v.y;
			i[1, 2] = this.y * v.z;
			i[1, 3] = this.y * v.w;
			i[2, 0] = this.z * v.x;
			i[2, 1] = this.z * v.y;
			i[2, 2] = this.z * v.z;
			i[2, 3] = this.z * v.w;
			i[3, 0] = this.w * v.x;
			i[3, 1] = this.w * v.y;
			i[3, 2] = this.w * v.z;
			i[3, 3] = this.w * v.w;
			return i;
		}
		public override string ToString()
		{
			return string.Concat(new string[]
			{
				this.x.ToString(),
				" ",
				this.y.ToString(),
				" ",
				this.z.ToString(),
				" ",
				this.w.ToString()
			});
		}
		public static Vector4d Max(Vector4d v1, Vector4d v2)
		{
			return new Vector4d((v1.x > v2.x) ? v1.x : v2.x, (v1.y > v2.y) ? v1.y : v2.y, (v1.z > v2.z) ? v1.z : v2.z, (v1.w > v2.w) ? v1.w : v2.w);
		}
		public static Vector4d Min(Vector4d v1, Vector4d v2)
		{
			return new Vector4d((v1.x < v2.x) ? v1.x : v2.x, (v1.y < v2.y) ? v1.y : v2.y, (v1.z < v2.z) ? v1.z : v2.z, (v1.w < v2.w) ? v1.w : v2.w);
		}
		public static Vector4d operator +(Vector4d v1, Vector4d v2)
		{
			return new Vector4d(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w);
		}
		public static Vector4d operator -(Vector4d v1, Vector4d v2)
		{
			return new Vector4d(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w);
		}
		public static Vector4d operator *(Vector4d v, double s)
		{
			return new Vector4d(v.x * s, v.y * s, v.z * s, v.w * s);
		}
		public static Vector4d operator *(double s, Vector4d v)
		{
			return new Vector4d(v.x * s, v.y * s, v.z * s, v.w * s);
		}
		public static Vector4d operator /(Vector4d v, double s)
		{
			return new Vector4d(v.x / s, v.y / s, v.z / s, v.w / s);
		}
	}
}
