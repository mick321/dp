using MyGeometry;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
namespace IsolineEditing
{
	public class DualLaplacianDeformer : Deformer, IDisposable
	{
		private Mesh mesh;
		private double[] x;
		private double[][] b;
		private double[] h;
		private double[] ATb;
		private SparseMatrix A;
		private CCSMatrix ccsA;
		private CCSMatrix ccsATA;
		private unsafe void* solver = null;
		private double handleConstraintScale = 100.0;
		[DllImport("taucs.dll")]
		private unsafe static extern void* CreaterCholeskySolver(int n, int nnz, int* rowIndex, int* colIndex, double* value);
		[DllImport("taucs.dll")]
		private unsafe static extern int Solve(void* solver, double* x, double* b);
		[DllImport("taucs.dll")]
		private unsafe static extern double SolveEx(void* solver, double* x, int xIndex, double* b, int bIndex);
		[DllImport("taucs.dll")]
		private unsafe static extern int FreeSolver(void* sp);
		
        public unsafe DualLaplacianDeformer(Mesh mesh)
		{
			Program.PrintText("Create dual Laplacian deformer");
			int vn = mesh.VertexCount;
			int fn = mesh.FaceCount;
			this.mesh = mesh;
			this.x = new double[vn];
			this.b = new double[3][];
			this.h = new double[fn];
			this.ATb = new double[vn];
			this.A = this.BuildMatrixDL();
			this.AddHandleConstraint(this.A);
			this.ccsA = new CCSMatrix(this.A);
			this.ccsATA = this.MultiplyATA(this.ccsA);
			this.solver = this.Factorization(this.ccsATA);
			if (this.solver == null)
			{
				throw new Exception();
			}
			for (int i = 0; i < 3; i++)
			{
				int j = 0;
				int k = 0;
				while (j < vn)
				{
					this.x[j] = mesh.VertexPos[k + i];
					j++;
					k += 3;
				}
				this.b[i] = new double[this.ccsA.RowSize];
				for (int l = 0; l < this.ccsA.RowSize; l++)
				{
					this.b[i][l] = 0.0;
				}
				this.ccsA.Multiply(this.x, this.b[i]);
			}
			Program.PrintText("...Done");
		}
		~DualLaplacianDeformer()
		{
			this.Dispose();
		}
		private SparseMatrix BuildMatrixDL()
		{
			int vn = this.mesh.VertexCount;
			int fn = this.mesh.FaceCount;
			SparseMatrix L = new SparseMatrix(fn, vn);
			for (int i = 0; i < fn; i++)
			{
				int f = this.mesh.AdjFF[i][0];
				int f2 = this.mesh.AdjFF[i][1];
				int f3 = this.mesh.AdjFF[i][2];
				Vector3d dv = this.mesh.ComputeDualPosition(i);
				Vector3d dv2 = this.mesh.ComputeDualPosition(f);
				Vector3d dv3 = this.mesh.ComputeDualPosition(f2);
				Vector3d dv4 = this.mesh.ComputeDualPosition(f3);
				Vector3d u = dv - dv4;
				Vector3d v = dv2 - dv4;
				Vector3d v2 = dv3 - dv4;
				Vector3d normal = v.Cross(v2).Normalize();
				Matrix3d M = new Matrix3d(v, v2, normal);
				Vector3d coord = M.Inverse() * u;
				double alpha = 0.33333333333333331;
				int j = 0;
				int k = i * 3;
				while (j < 3)
				{
					L.AddValueTo(i, this.mesh.FaceIndex[k++], alpha);
					j++;
				}
				alpha = coord[0] / 3.0;
				int l = 0;
				int m = f * 3;
				while (l < 3)
				{
					L.AddValueTo(i, this.mesh.FaceIndex[m++], -alpha);
					l++;
				}
				alpha = coord[1] / 3.0;
				int n = 0;
				int k2 = f2 * 3;
				while (n < 3)
				{
					L.AddValueTo(i, this.mesh.FaceIndex[k2++], -alpha);
					n++;
				}
				alpha = (1.0 - coord[0] - coord[1]) / 3.0;
				int j2 = 0;
				int k3 = f3 * 3;
				while (j2 < 3)
				{
					L.AddValueTo(i, this.mesh.FaceIndex[k3++], -alpha);
					j2++;
				}
				this.h[i] = coord.z;
			}
			L.SortElement();
			return L;
		}
		private void AddHandleConstraint(SparseMatrix A)
		{
			for (int i = 0; i < this.mesh.VertexCount; i++)
			{
				if (this.mesh.Flag[i] != 0)
				{
					A.AddRow();
					A.AddElement(A.RowSize - 1, i, this.handleConstraintScale);
				}
			}
			A.SortElement();
		}
		private CCSMatrix MultiplyATA(CCSMatrix A)
		{
			int[] last = new int[A.RowSize];
			int[] next = new int[A.NumNonZero];
			int[] colIndex = new int[A.NumNonZero];
			for (int i = 0; i < last.Length; i++)
			{
				last[i] = -1;
			}
			for (int j = 0; j < next.Length; j++)
			{
				next[j] = -1;
			}
			for (int k = 0; k < A.ColumnSize; k++)
			{
				for (int l = A.ColIndex[k]; l < A.ColIndex[k + 1]; l++)
				{
					int m = A.RowIndex[l];
					if (last[m] != -1)
					{
						next[last[m]] = l;
					}
					last[m] = l;
					colIndex[l] = k;
				}
			}
			CCSMatrix ATA = new CCSMatrix(A.ColumnSize, A.ColumnSize);
			Set<int> set = new Set<int>();
			double[] tmp = new double[A.ColumnSize];
			List<int> ATA_RowIndex = new List<int>();
			List<double> ATA_Value = new List<double>();
			for (int n = 0; n < A.ColumnSize; n++)
			{
				tmp[n] = 0.0;
			}
			for (int j2 = 0; j2 < A.ColumnSize; j2++)
			{
				for (int col = A.ColIndex[j2]; col < A.ColIndex[j2 + 1]; col++)
				{
					int arg_122_0 = A.RowIndex[col];
					double val = A.Values[col];
					int curr = col;
					while (true)
					{
						int i2 = colIndex[curr];
						set.Add(i2);
						tmp[i2] += val * A.Values[curr];
						if (next[curr] == -1)
						{
							break;
						}
						curr = next[curr];
					}
				}
				int[] s = set.ToArray();
				Array.Sort<int>(s);
				int count = 0;
				int[] array = s;
				for (int num = 0; num < array.Length; num++)
				{
					int k2 = array[num];
					if (tmp[k2] != 0.0)
					{
						ATA_RowIndex.Add(k2);
						ATA_Value.Add(tmp[k2]);
						tmp[k2] = 0.0;
						count++;
					}
				}
				ATA.ColIndex[j2 + 1] = ATA.ColIndex[j2] + count;
				set.Clear();
			}
			ATA.RowIndex = ATA_RowIndex.ToArray();
			ATA.Values = ATA_Value.ToArray();
			return ATA;
		}
		private unsafe void* Factorization(CCSMatrix C)
		{
			fixed (int* ri = C.RowIndex)
			{
				fixed (int* ci = C.ColIndex)
				{
					fixed (double* val = C.Values)
					{
						return DualLaplacianDeformer.CreaterCholeskySolver(C.ColumnSize, C.NumNonZero, ri, ci, val);
					}
				}
			}
		}
		public unsafe void Deform()
		{
			int vn = this.mesh.VertexCount;
			for (int i = 0; i < 3; i++)
			{
				this.ccsA.PreMultiply(this.b[i], this.ATb);
				fixed (double* _x = this.x, _ATb = this.ATb)
				{
					DualLaplacianDeformer.Solve(this.solver, _x, _ATb);
				}
				int j = 0;
				int j2 = i;
				while (j < vn)
				{
					if (this.mesh.Flag[j] == 0)
					{
						this.mesh.VertexPos[j2] = this.x[j];
					}
					j++;
					j2 += 3;
				}
			}
			this.mesh.ComputeDualPosition();
		}
		public void Update()
		{
			int fn = this.mesh.FaceCount;
			for (int i = 0; i < fn; i++)
			{
				int f = this.mesh.AdjFF[i][0];
				int f2 = this.mesh.AdjFF[i][1];
				int f3 = this.mesh.AdjFF[i][2];
				Vector3d p = new Vector3d(this.mesh.DualVertexPos, f * 3);
				Vector3d p2 = new Vector3d(this.mesh.DualVertexPos, f2 * 3);
				Vector3d p3 = new Vector3d(this.mesh.DualVertexPos, f3 * 3);
				Vector3d normal = (p - p3).Cross(p2 - p3).Normalize() * this.h[i];
				this.b[0][i] = normal.x;
				this.b[1][i] = normal.y;
				this.b[2][i] = normal.z;
			}
		}
		public void Display()
		{
		}
		public void Move()
		{
			int i = this.mesh.FaceCount;
			int j = 0;
			int k = 0;
			while (j < this.mesh.VertexCount)
			{
				if (this.mesh.Flag[j] != 0)
				{
					this.b[0][i] = this.mesh.VertexPos[k] * this.handleConstraintScale;
					this.b[1][i] = this.mesh.VertexPos[k + 1] * this.handleConstraintScale;
					this.b[2][i++] = this.mesh.VertexPos[k + 2] * this.handleConstraintScale;
				}
				j++;
				k += 3;
			}
		}
		public void MouseDown()
		{
		}
		public void MouseUp()
		{
		}
		public unsafe void Dispose()
		{
			if (this.solver != null)
			{
				DualLaplacianDeformer.FreeSolver(this.solver);
				this.solver = null;
			}
		}
	}
}
