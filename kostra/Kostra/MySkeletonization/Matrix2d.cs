using System;
namespace MyGeometry
{
	public struct Matrix2d
	{
		private const int len = 4;
		private const int row_size = 2;
		private double a;
		private double b;
		private double c;
		private double d;
		public double A
		{
			get
			{
				return this.a;
			}
			set
			{
				this.a = value;
			}
		}
		public double B
		{
			get
			{
				return this.b;
			}
			set
			{
				this.b = value;
			}
		}
		public double C
		{
			get
			{
				return this.c;
			}
			set
			{
				this.c = value;
			}
		}
		public double D
		{
			get
			{
				return this.d;
			}
			set
			{
				this.d = value;
			}
		}
		public Matrix2d(double[] arr)
		{
			this.a = arr[0];
			this.b = arr[1];
			this.c = arr[2];
			this.d = arr[3];
		}
		public Matrix2d(double[,] arr)
		{
			this.a = arr[0, 0];
			this.b = arr[0, 1];
			this.c = arr[1, 0];
			this.d = arr[1, 1];
		}
		public Matrix2d(Vector2d v1, Vector2d v2)
		{
			this.a = v1.x;
			this.b = v2.x;
			this.c = v1.y;
			this.d = v2.y;
		}
		public Matrix2d(double a, double b, double c, double d)
		{
			this.a = a;
			this.b = b;
			this.c = c;
			this.d = d;
		}
		public static Matrix2d operator *(Matrix2d m1, Matrix2d m2)
		{
			Matrix2d ret = new Matrix2d(m1.a * m2.a + m1.b * m2.c, m1.a * m2.b + m1.b * m2.d, m1.c * m2.a + m1.d * m2.c, m1.c * m2.b + m1.d * m2.d);
			return ret;
		}
		public static Vector2d operator *(Matrix2d m, Vector2d v)
		{
			return new Vector2d(m.A * v.x + m.B * v.y, m.C * v.x + m.D * v.y);
		}
		public Matrix2d Inverse()
		{
			double det = this.a * this.d - this.b * this.c;
			if (double.IsNaN(det))
			{
				throw new ArithmeticException();
			}
			return new Matrix2d(this.d / det, -this.b / det, -this.c / det, this.a / det);
		}
		public Matrix2d Transpose()
		{
			return new Matrix2d(this.a, this.c, this.b, this.d);
		}
		public double Trace()
		{
			return this.a + this.d;
		}
		public override string ToString()
		{
			return string.Concat(new string[]
			{
				this.a.ToString("F5"),
				" ",
				this.b.ToString("F5"),
				" ",
				this.c.ToString("F5"),
				" ",
				this.d.ToString("F5")
			});
		}
	}
}
