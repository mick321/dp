using System;
using System.Collections.Generic;
namespace MyGeometry
{
	internal class RowBasedMatrix
	{
		public struct Element : IComparable<RowBasedMatrix.Element>
		{
			public int colIndex;
			public double value;
			public Element(int c)
			{
				this.colIndex = c;
				this.value = 0.0;
			}
			public Element(int c, double value)
			{
				this.colIndex = c;
				this.value = value;
			}
			public int CompareTo(RowBasedMatrix.Element other)
			{
				return this.colIndex - other.colIndex;
			}
		}
		private List<RowBasedMatrix.Element>[] rows;
		public List<RowBasedMatrix.Element>[] Rows
		{
			get
			{
				return this.rows;
			}
		}
		public int RowSize
		{
			get
			{
				return this.rows.Length;
			}
		}
		public RowBasedMatrix(int rowSize)
		{
			this.rows = new List<RowBasedMatrix.Element>[rowSize];
			for (int i = 0; i < rowSize; i++)
			{
				this.rows[i] = new List<RowBasedMatrix.Element>();
			}
		}
		public RowBasedMatrix(int rowSize, int capacity)
		{
			this.rows = new List<RowBasedMatrix.Element>[rowSize];
			for (int i = 0; i < rowSize; i++)
			{
				this.rows[i] = new List<RowBasedMatrix.Element>(capacity);
			}
		}
		public RowBasedMatrix(SparseMatrix M) : this(M.RowSize)
		{
			foreach (List<SparseMatrix.Element> row in M.Rows)
			{
				foreach (SparseMatrix.Element e in row)
				{
					this.AddElement(e.i, e.j, e.value);
				}
			}
		}
		public void AddElement(int r, int c, double value)
		{
			this.rows[r].Add(new RowBasedMatrix.Element(c, value));
		}
		public int FindElement(int r, int c)
		{
			for (int i = 0; i < this.rows[r].Count; i++)
			{
				if (this.rows[r][i].colIndex == c)
				{
					return i;
				}
			}
			return -1;
		}
		public int BinarySearchElement(int r, int c)
		{
			return this.rows[r].BinarySearch(new RowBasedMatrix.Element(c));
		}
		public void Sort()
		{
			List<RowBasedMatrix.Element>[] array = this.rows;
			for (int i = 0; i < array.Length; i++)
			{
				List<RowBasedMatrix.Element> row = array[i];
				row.Sort();
			}
		}
		public void Multiply(double[] xIn, double[] xOut)
		{
			for (int i = 0; i < this.RowSize; i++)
			{
				double sum = 0.0;
				foreach (RowBasedMatrix.Element e in this.rows[i])
				{
					sum += e.value * xIn[e.colIndex];
				}
				xOut[i] = sum;
			}
		}
		public void PreMultiply(double[] xIn, double[] xOut)
		{
			for (int i = 0; i < xOut.Length; i++)
			{
				xOut[i] = 0.0;
			}
			for (int j = 0; j < this.RowSize; j++)
			{
				foreach (RowBasedMatrix.Element e in this.rows[j])
				{
					xOut[e.colIndex] += e.value * xIn[j];
				}
			}
		}
		public RowBasedMatrix IncompleteCholesky()
		{
			int i = this.RowSize;
			RowBasedMatrix L = new RowBasedMatrix(i);
			for (int j = 0; j < i; j++)
			{
				List<RowBasedMatrix.Element> r = this.rows[j];
				List<RowBasedMatrix.Element> rr = L.rows[j];
				for (int k = 0; k < r.Count; k++)
				{
					RowBasedMatrix.Element e = r[k];
					if (e.colIndex == j)
					{
						double sum = e.value;
						int c = 0;
						while (c < rr.Count && rr[c].colIndex < j)
						{
							sum -= rr[c].value * rr[c].value;
							c++;
						}
						if (sum > 0.0)
						{
							rr.Add(new RowBasedMatrix.Element(j, Math.Sqrt(sum)));
						}
						else
						{
							rr.Add(new RowBasedMatrix.Element(j, 1E-11));
						}
					}
					else
					{
						if (e.colIndex < j)
						{
							List<RowBasedMatrix.Element> rr2 = L.rows[e.colIndex];
							double sum2 = e.value;
							int c2 = 0;
							int c3 = 0;
							while (c2 < rr.Count)
							{
								if (c3 >= rr2.Count)
								{
									break;
								}
								RowBasedMatrix.Element e2 = rr[c2];
								RowBasedMatrix.Element e3 = rr2[c3];
								if (e2.colIndex >= e.colIndex || e3.colIndex >= e.colIndex)
								{
									break;
								}
								if (e2.colIndex > e3.colIndex)
								{
									c3++;
								}
								else
								{
									if (e2.colIndex < e3.colIndex)
									{
										c2++;
									}
									else
									{
										sum2 -= e2.value * e3.value;
										c2++;
										c3++;
									}
								}
							}
							while (rr2[c3].colIndex < e.colIndex)
							{
								c3++;
							}
							rr.Add(new RowBasedMatrix.Element(e.colIndex, sum2 / rr2[c3].value));
						}
					}
				}
			}
			return L;
		}
		public RowBasedMatrix IncompleteCholesky(double droptol)
		{
			int i = this.RowSize;
			int nAdd = 0;
			RowBasedMatrix L = new RowBasedMatrix(i);
			for (int j = 0; j < i; j++)
			{
				List<RowBasedMatrix.Element> r = this.rows[j];
				List<RowBasedMatrix.Element> rr = L.rows[j];
				double drop = 0.0;
				double norm = 0.0;
				foreach (RowBasedMatrix.Element e in r)
				{
					norm += e.value * e.value;
				}
				norm = Math.Sqrt(norm) * droptol;
				int k = 0;
				for (int l = 0; l < j; l++)
				{
					double sum = 0.0;
					if (k < r.Count && r[k].colIndex == l)
					{
						sum = r[k++].value;
					}
					int c = 0;
					int c2 = 0;
					List<RowBasedMatrix.Element> rr2 = L.rows[l];
					while (c < rr.Count)
					{
						if (c2 >= rr2.Count)
						{
							break;
						}
						RowBasedMatrix.Element e2 = rr[c];
						RowBasedMatrix.Element e3 = rr2[c2];
						if (e2.colIndex >= l || e3.colIndex >= l)
						{
							break;
						}
						if (e2.colIndex > e3.colIndex)
						{
							c2++;
						}
						else
						{
							if (e2.colIndex < e3.colIndex)
							{
								c++;
							}
							else
							{
								sum -= e2.value * e3.value;
								c++;
								c2++;
							}
						}
					}
					while (rr2[c2].colIndex < l)
					{
						c2++;
					}
					sum /= rr2[c2].value;
					if (Math.Abs(sum) > norm)
					{
						rr.Add(new RowBasedMatrix.Element(l, sum));
						nAdd++;
					}
					else
					{
						drop += sum;
					}
				}
				if (r[k].colIndex != j)
				{
					throw new Exception();
				}
				double diag = r[k].value;
				int c3 = 0;
				while (c3 < rr.Count && rr[c3].colIndex < j)
				{
					diag -= rr[c3].value * rr[c3].value;
					c3++;
				}
				if (diag <= 0.0)
				{
					throw new Exception();
				}
				rr.Add(new RowBasedMatrix.Element(j, Math.Sqrt(diag)));
			}
			return L;
		}
		public void Cholesky_Solve(double[] x, double[] b)
		{
			int i = this.RowSize;
			for (int j = 0; j < i; j++)
			{
				List<RowBasedMatrix.Element> r = this.rows[j];
				double sum = b[j];
				int k = 0;
				while (k < r.Count && r[k].colIndex < j)
				{
					sum -= r[k].value * x[r[k].colIndex];
					k++;
				}
				x[j] = sum / r[k].value;
			}
			for (int l = i - 1; l >= 0; l--)
			{
				List<RowBasedMatrix.Element> r2 = this.rows[l];
				x[l] /= r2[r2.Count - 1].value;
				int m = 0;
				while (m < r2.Count && r2[m].colIndex < l)
				{
					x[r2[m].colIndex] -= r2[m].value * x[l];
					m++;
				}
			}
		}
		public void CG_Solve(double[] x, double[] b, int maxIter, double eps, int recompute)
		{
			int i = this.RowSize;
			int iter = 0;
			double[] r = new double[i];
			double[] d = new double[i];
			double[] q = new double[i];
			double errNew = 0.0;
			for (int j = 0; j < i; j++)
			{
				double Ax = 0.0;
				foreach (RowBasedMatrix.Element e in this.rows[j])
				{
					Ax += e.value * x[e.colIndex];
				}
				d[j] = (r[j] = b[j] - Ax);
				errNew += r[j] * r[j];
			}
			double tolerance = eps * errNew;
			while (iter < maxIter && errNew > tolerance)
			{
				iter++;
				double dAd = 0.0;
				for (int k = 0; k < i; k++)
				{
					double Ad = 0.0;
					foreach (RowBasedMatrix.Element e2 in this.rows[k])
					{
						Ad += e2.value * d[e2.colIndex];
					}
					q[k] = Ad;
					dAd += d[k] * Ad;
				}
				double alpha = errNew / dAd;
				for (int l = 0; l < i; l++)
				{
					x[l] += alpha * d[l];
				}
				if (iter % recompute == 0)
				{
					for (int m = 0; m < i; m++)
					{
						double Ax2 = 0.0;
						foreach (RowBasedMatrix.Element e3 in this.rows[m])
						{
							Ax2 += e3.value * x[e3.colIndex];
						}
						r[m] = b[m] - Ax2;
					}
				}
				else
				{
					for (int n = 0; n < i; n++)
					{
						r[n] -= alpha * q[n];
					}
				}
				double errold = errNew;
				errNew = 0.0;
				for (int i2 = 0; i2 < i; i2++)
				{
					errNew += r[i2] * r[i2];
				}
				double beta = errNew / errold;
				for (int i3 = 0; i3 < i; i3++)
				{
					d[i3] = r[i3] + beta * d[i3];
				}
			}
		}
		public void CG_Solve(double[] x, double[] b, int maxIter, double eps, int recompute, double[] inv)
		{
			int i = this.RowSize;
			int iter = 0;
			double[] r = new double[i];
			double[] d = new double[i];
			double[] q = new double[i];
			double[] s = new double[i];
			double errNew = 0.0;
			for (int j = 0; j < i; j++)
			{
				double Ax = 0.0;
				foreach (RowBasedMatrix.Element e in this.rows[j])
				{
					Ax += e.value * x[e.colIndex];
				}
				r[j] = b[j] - Ax;
				d[j] = inv[j] * r[j];
				errNew += r[j] * d[j];
			}
			double tolerance = eps * errNew;
			while (iter < maxIter && errNew > tolerance)
			{
				iter++;
				double dAd = 0.0;
				for (int k = 0; k < i; k++)
				{
					double Ad = 0.0;
					foreach (RowBasedMatrix.Element e2 in this.rows[k])
					{
						Ad += e2.value * d[e2.colIndex];
					}
					q[k] = Ad;
					dAd += d[k] * Ad;
				}
				double alpha = errNew / dAd;
				for (int l = 0; l < i; l++)
				{
					x[l] += alpha * d[l];
				}
				if (iter % recompute == 0)
				{
					for (int m = 0; m < i; m++)
					{
						double Ax2 = 0.0;
						foreach (RowBasedMatrix.Element e3 in this.rows[m])
						{
							Ax2 += e3.value * x[e3.colIndex];
						}
						r[m] = b[m] - Ax2;
					}
				}
				else
				{
					for (int n = 0; n < i; n++)
					{
						r[n] -= alpha * q[n];
					}
				}
				for (int i2 = 0; i2 < i; i2++)
				{
					s[i2] = inv[i2] * r[i2];
				}
				double errold = errNew;
				errNew = 0.0;
				for (int i3 = 0; i3 < i; i3++)
				{
					errNew += r[i3] * s[i3];
				}
				double beta = errNew / errold;
				for (int i4 = 0; i4 < i; i4++)
				{
					d[i4] = s[i4] + beta * d[i4];
				}
			}
		}
		public void CG_SolveATA(double[] x, double[] b, int maxIter, double eps, int recompute)
		{
			int i = x.Length;
			int iter = 0;
			double[] r = new double[i];
			double[] d = new double[i];
			double[] q = new double[i];
			double[] s = new double[i];
			int arg_2A_0 = this.RowSize;
			double errNew = 0.0;
			double[] inv = new double[i];
			for (int j = 0; j < i; j++)
			{
				inv[j] = 1.0;
			}
			for (int k = 0; k < i; k++)
			{
				r[k] = b[k];
			}
			for (int l = 0; l < this.RowSize; l++)
			{
				double sum = 0.0;
				foreach (RowBasedMatrix.Element e in this.rows[l])
				{
					sum += e.value * x[e.colIndex];
				}
				foreach (RowBasedMatrix.Element e2 in this.rows[l])
				{
					r[e2.colIndex] -= e2.value * sum;
				}
			}
			for (int m = 0; m < i; m++)
			{
				d[m] = inv[m] * r[m];
				errNew += r[m] * d[m];
			}
			double tolerance = eps * errNew;
			while (iter < maxIter && errNew > tolerance)
			{
				iter++;
				double dAd = 0.0;
				for (int n = 0; n < i; n++)
				{
					q[n] = 0.0;
				}
				for (int i2 = 0; i2 < this.RowSize; i2++)
				{
					double sum2 = 0.0;
					foreach (RowBasedMatrix.Element e3 in this.rows[i2])
					{
						sum2 += e3.value * d[e3.colIndex];
					}
					foreach (RowBasedMatrix.Element e4 in this.rows[i2])
					{
						q[e4.colIndex] += e4.value * sum2;
					}
				}
				for (int i3 = 0; i3 < i; i3++)
				{
					dAd += d[i3] * q[i3];
				}
				double alpha = errNew / dAd;
				for (int i4 = 0; i4 < i; i4++)
				{
					x[i4] += alpha * d[i4];
				}
				if (iter % recompute == 0)
				{
					for (int i5 = 0; i5 < i; i5++)
					{
						r[i5] = b[i5];
					}
					for (int i6 = 0; i6 < this.RowSize; i6++)
					{
						double sum3 = 0.0;
						foreach (RowBasedMatrix.Element e5 in this.rows[i6])
						{
							sum3 += e5.value * x[e5.colIndex];
						}
						foreach (RowBasedMatrix.Element e6 in this.rows[i6])
						{
							r[e6.colIndex] -= e6.value * sum3;
						}
					}
				}
				else
				{
					for (int i7 = 0; i7 < i; i7++)
					{
						r[i7] -= alpha * q[i7];
					}
				}
				for (int i8 = 0; i8 < i; i8++)
				{
					s[i8] = inv[i8] * r[i8];
				}
				double errold = errNew;
				errNew = 0.0;
				for (int i9 = 0; i9 < i; i9++)
				{
					errNew += r[i9] * s[i9];
				}
				double beta = errNew / errold;
				for (int i10 = 0; i10 < i; i10++)
				{
					d[i10] = s[i10] + beta * d[i10];
				}
			}
			double err = 0.0;
			for (int i11 = 0; i11 < i; i11++)
			{
				err += r[i11] * r[i11];
			}
		}
		public void CG_SolveATA(double[] x, double[] b, int maxIter, double eps, int recompute, double[] inv)
		{
			int i = x.Length;
			int iter = 0;
			double[] r = new double[i];
			double[] d = new double[i];
			double[] q = new double[i];
			double[] s = new double[i];
			int arg_2A_0 = this.RowSize;
			double errNew = 0.0;
			if (inv == null)
			{
				inv = new double[i];
				for (int j = 0; j < i; j++)
				{
					inv[j] = 1.0;
				}
			}
			for (int k = 0; k < i; k++)
			{
				r[k] = b[k];
			}
			for (int l = 0; l < this.RowSize; l++)
			{
				double sum = 0.0;
				foreach (RowBasedMatrix.Element e in this.rows[l])
				{
					sum += e.value * x[e.colIndex];
				}
				foreach (RowBasedMatrix.Element e2 in this.rows[l])
				{
					r[e2.colIndex] -= e2.value * sum;
				}
			}
			for (int m = 0; m < i; m++)
			{
				d[m] = inv[m] * r[m];
				errNew += r[m] * d[m];
			}
			double tolerance = eps * errNew;
			while (iter < maxIter && errNew > tolerance)
			{
				iter++;
				double dAd = 0.0;
				for (int n = 0; n < i; n++)
				{
					q[n] = 0.0;
				}
				for (int i2 = 0; i2 < this.RowSize; i2++)
				{
					double sum2 = 0.0;
					foreach (RowBasedMatrix.Element e3 in this.rows[i2])
					{
						sum2 += e3.value * d[e3.colIndex];
					}
					foreach (RowBasedMatrix.Element e4 in this.rows[i2])
					{
						q[e4.colIndex] += e4.value * sum2;
					}
				}
				for (int i3 = 0; i3 < i; i3++)
				{
					dAd += d[i3] * q[i3];
				}
				double alpha = errNew / dAd;
				for (int i4 = 0; i4 < i; i4++)
				{
					x[i4] += alpha * d[i4];
				}
				if (iter % recompute == 0)
				{
					for (int i5 = 0; i5 < i; i5++)
					{
						r[i5] = b[i5];
					}
					for (int i6 = 0; i6 < this.RowSize; i6++)
					{
						double sum3 = 0.0;
						foreach (RowBasedMatrix.Element e5 in this.rows[i6])
						{
							sum3 += e5.value * x[e5.colIndex];
						}
						foreach (RowBasedMatrix.Element e6 in this.rows[i6])
						{
							r[e6.colIndex] -= e6.value * sum3;
						}
					}
				}
				else
				{
					for (int i7 = 0; i7 < i; i7++)
					{
						r[i7] -= alpha * q[i7];
					}
				}
				for (int i8 = 0; i8 < i; i8++)
				{
					s[i8] = inv[i8] * r[i8];
				}
				double errold = errNew;
				errNew = 0.0;
				for (int i9 = 0; i9 < i; i9++)
				{
					errNew += r[i9] * s[i9];
				}
				double beta = errNew / errold;
				for (int i10 = 0; i10 < i; i10++)
				{
					d[i10] = s[i10] + beta * d[i10];
				}
			}
			double err = 0.0;
			for (int i11 = 0; i11 < i; i11++)
			{
				err += r[i11] * r[i11];
			}
		}
		public void CG_SolveATA(double[] x, double[] b, int maxIter, double eps, int recompute, RowBasedMatrix inv)
		{
			int i = x.Length;
			int iter = 0;
			double[] r = new double[i];
			double[] d = new double[i];
			double[] q = new double[i];
			double[] s = new double[i];
			double errNew = 0.0;
			for (int j = 0; j < i; j++)
			{
				r[j] = b[j];
			}
			for (int k = 0; k < this.RowSize; k++)
			{
				double sum = 0.0;
				foreach (RowBasedMatrix.Element e in this.rows[k])
				{
					sum += e.value * x[e.colIndex];
				}
				foreach (RowBasedMatrix.Element e2 in this.rows[k])
				{
					r[e2.colIndex] -= e2.value * sum;
				}
			}
			inv.Cholesky_Solve(d, r);
			for (int l = 0; l < i; l++)
			{
				errNew += r[l] * d[l];
			}
			double tolerance = eps * errNew;
			while (iter < maxIter && errNew > tolerance)
			{
				iter++;
				double dAd = 0.0;
				for (int m = 0; m < i; m++)
				{
					q[m] = 0.0;
				}
				for (int n = 0; n < this.RowSize; n++)
				{
					double sum2 = 0.0;
					foreach (RowBasedMatrix.Element e3 in this.rows[n])
					{
						sum2 += e3.value * d[e3.colIndex];
					}
					foreach (RowBasedMatrix.Element e4 in this.rows[n])
					{
						q[e4.colIndex] += e4.value * sum2;
					}
				}
				for (int i2 = 0; i2 < i; i2++)
				{
					dAd += d[i2] * q[i2];
				}
				double alpha = errNew / dAd;
				for (int i3 = 0; i3 < i; i3++)
				{
					x[i3] += alpha * d[i3];
				}
				if (iter % recompute == 0)
				{
					for (int i4 = 0; i4 < i; i4++)
					{
						r[i4] = b[i4];
					}
					for (int i5 = 0; i5 < this.RowSize; i5++)
					{
						double sum3 = 0.0;
						foreach (RowBasedMatrix.Element e5 in this.rows[i5])
						{
							sum3 += e5.value * x[e5.colIndex];
						}
						foreach (RowBasedMatrix.Element e6 in this.rows[i5])
						{
							r[e6.colIndex] -= e6.value * sum3;
						}
					}
				}
				else
				{
					for (int i6 = 0; i6 < i; i6++)
					{
						r[i6] -= alpha * q[i6];
					}
				}
				inv.Cholesky_Solve(s, r);
				double errold = errNew;
				errNew = 0.0;
				for (int i7 = 0; i7 < i; i7++)
				{
					errNew += r[i7] * s[i7];
				}
				double beta = errNew / errold;
				for (int i8 = 0; i8 < i; i8++)
				{
					d[i8] = s[i8] + beta * d[i8];
				}
			}
			double err = 0.0;
			for (int i9 = 0; i9 < i; i9++)
			{
				err += r[i9] * r[i9];
			}
		}
	}
}
