using System;
using System.Collections.Generic;
namespace MyGeometry
{
	public class SparseMatrix
	{
		public class Element
		{
			public int i;
			public int j;
			public double value;
			public Element(int i, int j, double value)
			{
				this.i = i;
				this.j = j;
				this.value = value;
			}
		}
		private class RowComparer : IComparer<SparseMatrix.Element>
		{
			public int Compare(SparseMatrix.Element e1, SparseMatrix.Element e2)
			{
				return e1.j - e2.j;
			}
		}
		private class ColumnComparer : IComparer<SparseMatrix.Element>
		{
			public int Compare(SparseMatrix.Element e1, SparseMatrix.Element e2)
			{
				return e1.i - e2.i;
			}
		}
		private int m;
		private int n;
		private List<List<SparseMatrix.Element>> rows;
		private List<List<SparseMatrix.Element>> columns;
		public int RowSize
		{
			get
			{
				return this.m;
			}
		}
		public int ColumnSize
		{
			get
			{
				return this.n;
			}
		}
		public List<List<SparseMatrix.Element>> Rows
		{
			get
			{
				return this.rows;
			}
		}
		public List<List<SparseMatrix.Element>> Columns
		{
			get
			{
				return this.columns;
			}
		}
		public int NumOfElements()
		{
			int count = 0;
			if (this.m < this.n)
			{
				using (List<List<SparseMatrix.Element>>.Enumerator enumerator = this.rows.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						List<SparseMatrix.Element> r = enumerator.Current;
						count += r.Count;
					}
					return count;
				}
			}
			foreach (List<SparseMatrix.Element> c in this.columns)
			{
				count += c.Count;
			}
			return count;
		}
		public List<SparseMatrix.Element> GetRow(int index)
		{
			return this.rows[index];
		}
		public List<SparseMatrix.Element> GetColumn(int index)
		{
			return this.columns[index];
		}
		public SparseMatrix(int m, int n)
		{
			this.m = m;
			this.n = n;
			this.rows = new List<List<SparseMatrix.Element>>(m);
			this.columns = new List<List<SparseMatrix.Element>>(n);
			for (int i = 0; i < m; i++)
			{
				this.rows.Add(new List<SparseMatrix.Element>());
			}
			for (int j = 0; j < n; j++)
			{
				this.columns.Add(new List<SparseMatrix.Element>());
			}
		}
		public SparseMatrix(int m, int n, int nElements)
		{
			this.m = m;
			this.n = n;
			this.rows = new List<List<SparseMatrix.Element>>(m);
			this.columns = new List<List<SparseMatrix.Element>>(n);
			for (int i = 0; i < m; i++)
			{
				this.rows.Add(new List<SparseMatrix.Element>(nElements));
			}
			for (int j = 0; j < n; j++)
			{
				this.columns.Add(new List<SparseMatrix.Element>(nElements));
			}
		}
		public SparseMatrix(SparseMatrix right)
		{
			this.m = right.m;
			this.n = right.n;
			this.rows = new List<List<SparseMatrix.Element>>(this.m);
			this.columns = new List<List<SparseMatrix.Element>>(this.n);
			for (int i = 0; i < this.m; i++)
			{
				this.rows.Add(new List<SparseMatrix.Element>());
			}
			for (int j = 0; j < this.n; j++)
			{
				this.columns.Add(new List<SparseMatrix.Element>());
			}
			foreach (List<SparseMatrix.Element> list in right.Rows)
			{
				foreach (SparseMatrix.Element e in list)
				{
					this.AddElement(e.i, e.j, e.value);
				}
			}
		}
		public override bool Equals(object obj)
		{
			SparseMatrix right = obj as SparseMatrix;
			if (obj == null)
			{
				return false;
			}
			if (right.m != this.m)
			{
				return false;
			}
			if (right.n != this.n)
			{
				return false;
			}
			for (int i = 0; i < this.n; i++)
			{
				List<SparseMatrix.Element> c = this.columns[i];
				List<SparseMatrix.Element> c2 = right.columns[i];
				if (c.Count != c2.Count)
				{
					return false;
				}
				for (int j = 0; j < c.Count; j++)
				{
					SparseMatrix.Element e = c[j];
					SparseMatrix.Element e2 = c2[j];
					if (e.j != e2.j)
					{
						return false;
					}
					if (e.value != e2.value)
					{
						return false;
					}
				}
			}
			return true;
		}
		public override int GetHashCode()
		{
			return base.GetHashCode() + this.NumOfElements();
		}
		public bool CheckElements()
		{
			foreach (List<SparseMatrix.Element> r in this.rows)
			{
				foreach (SparseMatrix.Element e in r)
				{
					if (double.IsInfinity(e.value) || double.IsNaN(e.value) || e.value == 0.0)
					{
						return false;
					}
				}
			}
			return true;
		}
		public bool IsSymmetric()
		{
			if (this.m != this.n)
			{
				return false;
			}
			for (int i = 0; i < this.m; i++)
			{
				List<SparseMatrix.Element> row = this.GetRow(i);
				List<SparseMatrix.Element> col = this.GetColumn(i);
				if (row.Count != col.Count)
				{
					return false;
				}
				for (int j = 0; j < row.Count; j++)
				{
					SparseMatrix.Element e = row[j];
					SparseMatrix.Element e2 = col[j];
					if (e.i != e2.j)
					{
						return false;
					}
					if (e.j != e2.i)
					{
						return false;
					}
				}
			}
			return true;
		}
		public SparseMatrix.Element AddElement(int i, int j, double value)
		{
			List<SparseMatrix.Element> r = this.rows[i];
			List<SparseMatrix.Element> c = this.columns[j];
			SparseMatrix.Element e = new SparseMatrix.Element(i, j, value);
			r.Add(e);
			c.Add(e);
			return e;
		}
		public SparseMatrix.Element AddElement(SparseMatrix.Element e)
		{
			List<SparseMatrix.Element> r = this.rows[e.i];
			List<SparseMatrix.Element> c = this.columns[e.j];
			r.Add(e);
			c.Add(e);
			return e;
		}
		public SparseMatrix.Element FindElement(int i, int j)
		{
			List<SparseMatrix.Element> rr = this.rows[i];
			foreach (SparseMatrix.Element e in rr)
			{
				if (e.j == j)
				{
					return e;
				}
			}
			return null;
		}
		public SparseMatrix.Element AddElementIfNotExist(int i, int j, double value)
		{
			if (this.FindElement(i, j) == null)
			{
				return this.AddElement(i, j, value);
			}
			return null;
		}
		public SparseMatrix.Element AddValueTo(int i, int j, double value)
		{
			SparseMatrix.Element e = this.FindElement(i, j);
			if (e == null)
			{
				e = new SparseMatrix.Element(i, j, 0.0);
				this.AddElement(e);
			}
			e.value += value;
			return e;
		}
		public void SortElement()
		{
			SparseMatrix.RowComparer rComparer = new SparseMatrix.RowComparer();
			SparseMatrix.ColumnComparer cComparer = new SparseMatrix.ColumnComparer();
			foreach (List<SparseMatrix.Element> r in this.rows)
			{
				r.Sort(rComparer);
			}
			foreach (List<SparseMatrix.Element> c in this.columns)
			{
				c.Sort(cComparer);
			}
		}
		public void AddRow()
		{
			this.rows.Add(new List<SparseMatrix.Element>());
			this.m++;
		}
		public void AddColumn()
		{
			this.columns.Add(new List<SparseMatrix.Element>());
			this.n++;
		}
		public void Multiply(double[] xIn, double[] xOut)
		{
			if (xIn.Length < this.n || xOut.Length < this.m)
			{
				throw new ArgumentException();
			}
			for (int i = 0; i < this.m; i++)
			{
				List<SparseMatrix.Element> r = this.rows[i];
				double sum = 0.0;
				foreach (SparseMatrix.Element e in r)
				{
					sum += e.value * xIn[e.j];
				}
				xOut[i] = sum;
			}
		}
		public void Multiply(double[] xIn, int indexIn, double[] xOut, int indexOut)
		{
			if (xIn.Length - indexIn < this.n || xOut.Length - indexOut < this.m)
			{
				throw new ArgumentException();
			}
			for (int i = 0; i < this.m; i++)
			{
				List<SparseMatrix.Element> r = this.rows[i];
				double sum = 0.0;
				foreach (SparseMatrix.Element e in r)
				{
					sum += e.value * xIn[e.j + indexIn];
				}
				xOut[i + indexOut] = sum;
			}
		}
		public void PreMultiply(double[] xIn, double[] xOut)
		{
			if (xIn.Length < this.m || xOut.Length < this.n)
			{
				throw new ArgumentException();
			}
			for (int i = 0; i < this.n; i++)
			{
				List<SparseMatrix.Element> c = this.columns[i];
				double sum = 0.0;
				foreach (SparseMatrix.Element e in c)
				{
					sum += e.value * xIn[e.i];
				}
				xOut[i] = sum;
			}
		}
		public void Scale(double s)
		{
			foreach (List<SparseMatrix.Element> list in this.rows)
			{
				foreach (SparseMatrix.Element e in list)
				{
					e.value *= s;
				}
			}
		}
		public SparseMatrix Multiply(SparseMatrix right)
		{
			if (this.n != right.m)
			{
				throw new ArgumentException();
			}
			SparseMatrix ret = new SparseMatrix(this.m, right.n);
			for (int i = 0; i < this.Rows.Count; i++)
			{
				List<SparseMatrix.Element> rr = this.Rows[i];
				for (int j = 0; j < right.Columns.Count; j++)
				{
					List<SparseMatrix.Element> cc = right.Columns[j];
					int c = 0;
					int c2 = 0;
					double sum = 0.0;
					bool used = false;
					while (c < rr.Count && c2 < cc.Count)
					{
						SparseMatrix.Element e = rr[c];
						SparseMatrix.Element e2 = cc[c2];
						if (e.j < e2.i)
						{
							c++;
						}
						else
						{
							if (e.j > e2.i)
							{
								c2++;
							}
							else
							{
								sum += e.value * e2.value;
								c++;
								c2++;
								used = true;
							}
						}
					}
					if (used)
					{
						ret.AddElement(i, j, sum);
					}
				}
			}
			return ret;
		}
		public SparseMatrix Add(SparseMatrix right)
		{
			if (this.m != right.m || this.n != right.n)
			{
				throw new ArgumentException();
			}
			SparseMatrix ret = new SparseMatrix(this.m, this.m);
			for (int i = 0; i < this.m; i++)
			{
				List<SparseMatrix.Element> r = this.Rows[i];
				List<SparseMatrix.Element> r2 = right.Rows[i];
				int c = 0;
				int c2 = 0;
				while (c < r.Count)
				{
					if (c2 >= r2.Count)
					{
						break;
					}
					SparseMatrix.Element e = r[c];
					SparseMatrix.Element e2 = r2[c2];
					if (e.j < e2.j)
					{
						c++;
						ret.AddElement(i, e.j, e.value);
					}
					else
					{
						if (e.j > e2.j)
						{
							c2++;
							ret.AddElement(i, e2.j, e2.value);
						}
						else
						{
							ret.AddElement(i, e.j, e.value + e2.value);
							c++;
							c2++;
						}
					}
				}
				while (c < r.Count)
				{
					SparseMatrix.Element e3 = r[c];
					ret.AddElement(e3.i, e3.j, e3.value);
					c++;
				}
				while (c2 < r2.Count)
				{
					SparseMatrix.Element e4 = r2[c2];
					ret.AddElement(e4.i, e4.j, e4.value);
					c2++;
				}
			}
			return ret;
		}
		public SparseMatrix Transpose()
		{
			SparseMatrix ret = new SparseMatrix(this);
			int t = ret.m;
			ret.m = ret.n;
			ret.n = t;
			List<List<SparseMatrix.Element>> tmp = ret.rows;
			ret.rows = ret.columns;
			ret.columns = tmp;
			foreach (List<SparseMatrix.Element> r in ret.rows)
			{
				foreach (SparseMatrix.Element e in r)
				{
					t = e.i;
					e.i = e.j;
					e.j = t;
				}
			}
			return ret;
		}
		public double[] GetDiagonalPreconditionor()
		{
			if (this.m != this.n)
			{
				return null;
			}
			double[] ret = new double[this.n];
			for (int i = 0; i < this.n; i++)
			{
				SparseMatrix.Element d = this.FindElement(i, i);
				if (d == null)
				{
					ret[i] = 1.0;
				}
				else
				{
					if (d.value == 0.0)
					{
						ret[i] = 1.0;
					}
					else
					{
						ret[i] = 1.0 / d.value;
					}
				}
			}
			return ret;
		}
		public SparseMatrix ConcatRows(SparseMatrix right)
		{
			if (this.ColumnSize != right.ColumnSize)
			{
				throw new ArgumentException();
			}
			SparseMatrix i = new SparseMatrix(this.RowSize + right.RowSize, this.ColumnSize);
			foreach (List<SparseMatrix.Element> r in this.rows)
			{
				foreach (SparseMatrix.Element e in r)
				{
					i.AddElement(e.i, e.j, e.value);
				}
			}
			int r_base = this.RowSize;
			foreach (List<SparseMatrix.Element> r2 in right.rows)
			{
				foreach (SparseMatrix.Element e2 in r2)
				{
					i.AddElement(r_base + e2.i, e2.j, e2.value);
				}
			}
			return i;
		}
		public int[][] GetRowIndex()
		{
			int[][] arr = new int[this.m][];
			for (int i = 0; i < this.m; i++)
			{
				arr[i] = new int[this.rows[i].Count];
				int j = 0;
				foreach (SparseMatrix.Element e in this.rows[i])
				{
					arr[i][j++] = e.j;
				}
			}
			return arr;
		}
		public int[][] GetColumnIndex()
		{
			int[][] arr = new int[this.n][];
			for (int i = 0; i < this.n; i++)
			{
				arr[i] = new int[this.columns[i].Count];
				int j = 0;
				foreach (SparseMatrix.Element e in this.columns[i])
				{
					arr[i][j++] = e.i;
				}
			}
			return arr;
		}
		public static void ConjugateGradientsMethod(SparseMatrix A, double[] b, double[] x, int iter, double tolerance)
		{
			int i = A.ColumnSize;
			int rn = (int)Math.Sqrt((double)i);
			if (A.RowSize != A.ColumnSize)
			{
				throw new ArgumentException();
			}
			if (b.Length != i || x.Length != i)
			{
				throw new ArgumentException();
			}
			double[] r = new double[i];
			double[] d = new double[i];
			double[] q = new double[i];
			double[] t = new double[i];
			int j = 0;
			A.Multiply(x, r);
			SparseMatrix.Subtract(r, b, r);
			SparseMatrix.Assign(d, r);
			double newError = SparseMatrix.Dot(r, r);
			while (j < iter && newError > tolerance)
			{
				A.Multiply(d, q);
				double alpha = newError / SparseMatrix.Dot(d, q);
				SparseMatrix.Scale(t, d, alpha);
				SparseMatrix.Add(x, x, t);
				if (j % rn == 0)
				{
					A.Multiply(x, r);
					SparseMatrix.Subtract(r, b, r);
				}
				else
				{
					SparseMatrix.Scale(t, q, alpha);
					SparseMatrix.Subtract(r, r, t);
				}
				double oldError = newError;
				newError = SparseMatrix.Dot(r, r);
				if (newError < tolerance)
				{
					A.Multiply(x, r);
					SparseMatrix.Subtract(r, b, r);
					newError = SparseMatrix.Dot(r, r);
				}
				double beta = newError / oldError;
				SparseMatrix.Scale(d, d, beta);
				SparseMatrix.Add(d, d, r);
				j++;
			}
		}
		public static void ConjugateGradientsMethod2(SparseMatrix A, double[] b, double[] x, int iter, double tolerance)
		{
			int i = A.ColumnSize;
			int rn = (int)Math.Sqrt((double)i);
			if (A.RowSize != A.ColumnSize)
			{
				throw new ArgumentException();
			}
			if (b.Length != i || x.Length != i)
			{
				throw new ArgumentException();
			}
			double[] r = new double[i];
			double[] r2 = new double[i];
			double[] d = new double[i];
			double[] d2 = new double[i];
			double[] q = new double[i];
			double[] q2 = new double[i];
			double[] t = new double[i];
			int j = 0;
			A.Multiply(x, r);
			SparseMatrix.Subtract(r, b, r);
			SparseMatrix.Assign(r2, r);
			SparseMatrix.Assign(d, r);
			SparseMatrix.Assign(d2, r2);
			double newError = SparseMatrix.Dot(r2, r);
			while (j < iter && newError > tolerance)
			{
				A.Multiply(d, q);
				A.PreMultiply(d2, q2);
				double alpha = newError / SparseMatrix.Dot(d2, q);
				SparseMatrix.Scale(t, d, alpha);
				SparseMatrix.Add(x, x, t);
				if (j % rn == 0)
				{
					A.Multiply(x, r);
					SparseMatrix.Subtract(r, b, r);
					SparseMatrix.Assign(r2, r);
				}
				else
				{
					SparseMatrix.Scale(t, q, alpha);
					SparseMatrix.Subtract(r, r, t);
					SparseMatrix.Scale(t, q2, alpha);
					SparseMatrix.Subtract(r2, r2, t);
				}
				double oldError = newError;
				newError = SparseMatrix.Dot(r2, r);
				if (newError < tolerance)
				{
					A.Multiply(x, r);
					SparseMatrix.Subtract(r, b, r);
					SparseMatrix.Assign(r2, r);
					newError = SparseMatrix.Dot(r2, r);
				}
				double beta = newError / oldError;
				SparseMatrix.Scale(d, d, beta);
				SparseMatrix.Add(d, d, r);
				SparseMatrix.Scale(d2, d2, beta);
				SparseMatrix.Add(d2, d2, r2);
				j++;
			}
		}
		public static void ConjugateGradientsMethod3(SparseMatrix A, double[] b, double[] x, bool[] boundary, int iter, double tolerance)
		{
			int i = A.ColumnSize;
			int rn = (int)Math.Sqrt((double)i);
			if (A.RowSize != A.ColumnSize)
			{
				throw new ArgumentException();
			}
			if (b.Length != i || x.Length != i)
			{
				throw new ArgumentException();
			}
			double[] r = new double[i];
			double[] d = new double[i];
			double[] q = new double[i];
			double[] t = new double[i];
			int j = 0;
			A.Multiply(x, r);
			SparseMatrix.Subtract(r, b, r);
			SparseMatrix.Assign(d, r);
			double newError = SparseMatrix.Dot(r, r);
			while (j < iter && newError > tolerance)
			{
				A.Multiply(d, q);
				double alpha = newError / SparseMatrix.Dot(d, q);
				SparseMatrix.Scale(t, d, alpha);
				SparseMatrix.Add(x, x, t);
				if (j % rn == 0)
				{
					A.Multiply(x, t);
					SparseMatrix.Subtract(r, b, t);
				}
				else
				{
					SparseMatrix.Scale(t, q, alpha);
					SparseMatrix.Subtract(r, r, t);
				}
				double oldError = newError;
				newError = SparseMatrix.Dot(r, r);
				if (newError < tolerance)
				{
					A.Multiply(x, r);
					SparseMatrix.Subtract(r, b, r);
					newError = SparseMatrix.Dot(r, r);
				}
				double beta = newError / oldError;
				SparseMatrix.Scale(d, d, beta);
				SparseMatrix.Add(d, d, r);
				j++;
			}
		}
		public static void JacobiMethod(SparseMatrix A, double[] b, double[] x, int iter, double tolerance)
		{
			int i = A.ColumnSize;
			if (A.ColumnSize != A.RowSize)
			{
				throw new ArgumentException();
			}
			if (b.Length != i || x.Length != i)
			{
				throw new ArgumentException();
			}
			double[] r = new double[i];
			double[] d = new double[i];
			double[] t = new double[i];
			for (int j = 0; j < i; j++)
			{
				SparseMatrix.Element e = A.FindElement(j, j);
				if (e != null)
				{
					d[j] = 0.75 / e.value;
				}
				else
				{
					d[j] = 0.0;
				}
			}
			A.Multiply(x, t);
			SparseMatrix.Subtract(r, b, t);
			double error = SparseMatrix.Dot(r, r);
			int count = 0;
			while (count < iter && error > tolerance)
			{
				for (int k = 0; k < i; k++)
				{
					t[k] = r[k] * d[k];
				}
				SparseMatrix.Add(x, x, t);
				A.Multiply(x, t);
				SparseMatrix.Subtract(r, b, t);
				error = SparseMatrix.Dot(r, r);
				count++;
			}
		}
		private static void Assign(double[] u, double[] v)
		{
			if (u.Length != v.Length)
			{
				throw new ArgumentException();
			}
			for (int i = 0; i < u.Length; i++)
			{
				u[i] = v[i];
			}
		}
		private static void Subtract(double[] w, double[] u, double[] v)
		{
			if (u.Length != v.Length || v.Length != w.Length)
			{
				throw new ArgumentException();
			}
			for (int i = 0; i < u.Length; i++)
			{
				w[i] = u[i] - v[i];
			}
		}
		private static void Add(double[] w, double[] u, double[] v)
		{
			if (u.Length != v.Length || v.Length != w.Length)
			{
				throw new ArgumentException();
			}
			for (int i = 0; i < u.Length; i++)
			{
				w[i] = u[i] + v[i];
			}
		}
		private static void Scale(double[] w, double[] u, double s)
		{
			if (u.Length != w.Length)
			{
				throw new ArgumentException();
			}
			for (int i = 0; i < u.Length; i++)
			{
				w[i] = u[i] * s;
			}
		}
		private static double Dot(double[] u, double[] v)
		{
			if (u.Length != v.Length)
			{
				throw new ArgumentException();
			}
			double sum = 0.0;
			for (int i = 0; i < u.Length; i++)
			{
				sum += u[i] * v[i];
			}
			return sum;
		}
	}
}
