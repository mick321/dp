using CsGL.OpenGL;
using MyGeometry;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using Win32;
namespace IsolineEditing
{
	[TypeConverter(typeof(DeformerConverter))]
	public class Skeletonizer : IDisposable
	{
		public class Options
		{
			private bool addNoise;
			private double noiseRatio = 0.02;
			private bool twoDimensionModel;
			private bool applyRootFinding;
			private int maxIterations = 30;
			private double laplacianConstraintWeight = 1.0;
			private double positionalConstraintWeight = 1.0;
			private double originalPositionalConstraintWeight;
			private double laplacianConstraintScale = 2.0;
			private double positionalConstraintScale = 1.5;
			private double areaRatioThreshold = 0.001;
			private bool useSymbolicSolver;
			private bool useIterativeSolver;
			private double volumneRatioThreashold = 1E-05;
			private bool applySimplification = true;
			private bool displayIntermediateMesh;
			private bool useShapeEnergy = true;
			private bool useSamplingEnergy = true;
			private double shapeEnergyWeight = 0.1;
			private int targetVertexCount = 10;
			private bool applyEmbedding = true;
			private int numOfImprovement = 100;
			private bool postSimplify = true;
			private double postSimplifyErrorRatio = 0.9;
			private bool useBoundaryVerticesOnly = true;
			[Browsable(false), Category("0.General Options")]
			public bool AddNoise
			{
				get
				{
					return this.addNoise;
				}
				set
				{
					this.addNoise = value;
				}
			}
			[Browsable(false), Category("0.General Options")]
			public double NoiseRatio
			{
				get
				{
					return this.noiseRatio;
				}
				set
				{
					this.noiseRatio = value;
				}
			}
			[Browsable(false), Category("0.General Options")]
			public bool TwoDimensionModel
			{
				get
				{
					return this.twoDimensionModel;
				}
				set
				{
					this.twoDimensionModel = value;
				}
			}
			[Browsable(false), Category("0.General Options")]
			public bool ApplyRootFinding
			{
				get
				{
					return this.applyRootFinding;
				}
				set
				{
					this.applyRootFinding = value;
				}
			}
			[Browsable(false), Category("1.Geometry Contraction")]
			public int MaxIterations
			{
				get
				{
					return this.maxIterations;
				}
				set
				{
					this.maxIterations = value;
				}
			}
			[Category("1.Geometry Contraction")]
			public double LaplacianConstraintWeight
			{
				get
				{
					return this.laplacianConstraintWeight;
				}
				set
				{
					this.laplacianConstraintWeight = value;
				}
			}
			[Category("1.Geometry Contraction")]
			public double PositionalConstraintWeight
			{
				get
				{
					return this.positionalConstraintWeight;
				}
				set
				{
					this.positionalConstraintWeight = value;
				}
			}
			[Browsable(false), Category("1.Geometry Contraction")]
			public double OriginalPositionalConstraintWeight
			{
				get
				{
					return this.originalPositionalConstraintWeight;
				}
				set
				{
					this.originalPositionalConstraintWeight = value;
				}
			}
			[Category("1.Geometry Contraction")]
			public double LaplacianConstraintScale
			{
				get
				{
					return this.laplacianConstraintScale;
				}
				set
				{
					this.laplacianConstraintScale = value;
				}
			}
			[Browsable(false), Category("1.Geometry Contraction")]
			public double PositionalConstraintScale
			{
				get
				{
					return this.positionalConstraintScale;
				}
				set
				{
					this.positionalConstraintScale = value;
				}
			}
			[Browsable(false), Category("1.Geometry Contraction")]
			public double AreaRatioThreshold
			{
				get
				{
					return this.areaRatioThreshold;
				}
				set
				{
					this.areaRatioThreshold = value;
				}
			}
			[Browsable(false), Category("1.Geometry Contraction")]
			public bool UseSymbolicSolver
			{
				get
				{
					return this.useSymbolicSolver;
				}
				set
				{
					this.useSymbolicSolver = value;
					if (this.useSymbolicSolver)
					{
						this.useIterativeSolver = false;
					}
				}
			}
			[Browsable(false), Category("1.Geometry Contraction")]
			public bool UseIterativeSolver
			{
				get
				{
					return this.useIterativeSolver;
				}
				set
				{
					this.useIterativeSolver = value;
					if (this.useIterativeSolver)
					{
						this.useSymbolicSolver = false;
					}
				}
			}
			[Browsable(false), Category("1.Geometry Contraction")]
			public double VolumneRatioThreashold
			{
				get
				{
					return this.volumneRatioThreashold;
				}
				set
				{
					this.volumneRatioThreashold = value;
				}
			}
			[Category("2.Connectivity Surgery")]
			public bool ApplyConnectivitySurgery
			{
				get
				{
					return this.applySimplification;
				}
				set
				{
					this.applySimplification = value;
					if (!this.applySimplification)
					{
						this.applyEmbedding = false;
					}
				}
			}
			[Browsable(false), Category("2.Connectivity Surgery")]
			public bool DisplayIntermediateMesh
			{
				get
				{
					return this.displayIntermediateMesh;
				}
				set
				{
					this.displayIntermediateMesh = value;
				}
			}
			[Browsable(false), Category("2.Connectivity Surgery")]
			public bool UseShapeEnergy
			{
				get
				{
					return this.useShapeEnergy;
				}
				set
				{
					this.useShapeEnergy = value;
				}
			}
			[Browsable(false), Category("2.Connectivity Surgery")]
			public bool UseSamplingEnergy
			{
				get
				{
					return this.useSamplingEnergy;
				}
				set
				{
					this.useSamplingEnergy = value;
				}
			}
			[Browsable(false), Category("2.Connectivity Surgery")]
			public double ShapeEnergyWeight
			{
				get
				{
					return this.shapeEnergyWeight;
				}
				set
				{
					this.shapeEnergyWeight = value;
				}
			}
			[Browsable(false), Category("2.Connectivity Surgery")]
			public int TargetVertexCount
			{
				get
				{
					return this.targetVertexCount;
				}
				set
				{
					this.targetVertexCount = value;
				}
			}
			[Category("3.Embedding Refinement")]
			public bool ApplyEmbeddingRefinement
			{
				get
				{
					return this.applyEmbedding;
				}
				set
				{
					this.applyEmbedding = value;
					if (this.applyEmbedding)
					{
						this.applySimplification = true;
					}
				}
			}
			[Browsable(false), Category("3.Embedding Refinement")]
			public int NumOfImprovement
			{
				get
				{
					return this.numOfImprovement;
				}
				set
				{
					this.numOfImprovement = value;
				}
			}
			[Browsable(false), Category("3.Embedding Refinement")]
			public bool PostSimplify
			{
				get
				{
					return this.postSimplify;
				}
				set
				{
					this.postSimplify = value;
				}
			}
			[Browsable(false), Category("3.Embedding Refinement")]
			public double PostSimplifyErrorRatio
			{
				get
				{
					return this.postSimplifyErrorRatio;
				}
				set
				{
					this.postSimplifyErrorRatio = value;
				}
			}
			[Browsable(false), Category("3.Embedding Refinement")]
			public bool UseBoundaryVerticesOnly
			{
				get
				{
					return this.useBoundaryVerticesOnly;
				}
				set
				{
					this.useBoundaryVerticesOnly = value;
				}
			}
			public override string ToString()
			{
				return "Skeleton Extraction Options";
			}
		}
		public class SegmentationOpt
		{
			private int segmentCount = 10;
			public int SegmentCount
			{
				get
				{
					return this.segmentCount;
				}
				set
				{
					this.segmentCount = value;
					if (this.segmentCount <= 0)
					{
						this.segmentCount = 1;
					}
				}
			}
		}
		private class VertexRecord : PriorityQueueElement, IComparable
		{
			public Vector3d pos;
			public double radius;
			public Set<int> adjV = new Set<int>(8);
			public Set<int> adjF = new Set<int>(8);
			public Set<int> collapseFrom = new Set<int>(2);
			public Dictionary<int, double> boundaryLength;
			public int vIndex = -1;
			public int minIndex = -1;
			public int pqIndex = -1;
			public Matrix4d matrix = new Matrix4d();
			public double minError = 1.7976931348623157E+308;
			public int colorIndex = -1;
			public bool center;
			public double err;
			public double nodeSize = 1.0;
			public int PQIndex
			{
				get
				{
					return this.pqIndex;
				}
				set
				{
					this.pqIndex = value;
				}
			}
			public VertexRecord(Mesh mesh, int i)
			{
				this.vIndex = i;
				this.pos = new Vector3d(mesh.VertexPos, i * 3);
				int[] array = mesh.AdjVV[i];
				for (int j = 0; j < array.Length; j++)
				{
					int index = array[j];
					this.adjV.Add(index);
				}
				int[] array2 = mesh.AdjVF[i];
				for (int k = 0; k < array2.Length; k++)
				{
					int index2 = array2[k];
					this.adjF.Add(index2);
				}
			}
			public int CompareTo(object obj)
			{
				Skeletonizer.VertexRecord rec = obj as Skeletonizer.VertexRecord;
				if (this.minError < rec.minError)
				{
					return -1;
				}
				if (this.minError > rec.minError)
				{
					return 1;
				}
				return 0;
			}
		}
		private class CutRecord : IComparable
		{
			public int from = -1;
			public int to = -1;
			public double cost;
			public CutRecord(int from, int to, double cost)
			{
				this.from = from;
				this.to = to;
				this.cost = cost;
			}
			public int CompareTo(object obj)
			{
				Skeletonizer.CutRecord rec = obj as Skeletonizer.CutRecord;
				if (this.cost < rec.cost)
				{
					return -1;
				}
				if (this.cost > rec.cost)
				{
					return 1;
				}
				return 0;
			}
		}
		private Mesh mesh;
		private Skeletonizer.Options opt;
		private CCSMatrix ccsA;
		private CCSMatrix ccsATA;
		private double[][] lap = new double[3][];
		private double[] originalVertexPos;
		private double[] collapsedVertexPos;
		private double[] lapWeight;
		private double[] posWeight;
		private double[] originalFaceArea;
		private double[] collapsedLength;
		private double[] oldAreaRatio;
		private Skeletonizer.VertexRecord[] vRec;
		private List<Skeletonizer.VertexRecord> simplifiedVertexRec;
		private unsafe void* solver = null;
		private unsafe void* symbolicSolver = null;
		private int iter;
		private int[] faceIndex;
		private int[] adjSegmentVertex;
		private Skeletonizer.VertexRecord rootNode;
		private MultigridContractionSolver multigridSolver;
		private bool displaySkeleton = true;
		private bool displayOriginalMesh;
		private bool displayNodeSphere;
		private bool displaySimplifiedMesh;
		private int displaySimplifiedMeshIndex;
		private float skeletonNodeSize = 6f;
		private object myDisplayLock = new object();
		private bool displayIntermediateMesh;
		private int remainingVertexCount;
		public bool DisplaySkeleton
		{
			get
			{
				return this.displaySkeleton;
			}
			set
			{
				this.displaySkeleton = value;
			}
		}
		public bool DisplayOriginalMesh
		{
			get
			{
				return this.displayOriginalMesh;
			}
			set
			{
				this.displayOriginalMesh = value;
				if (this.displayOriginalMesh)
				{
					this.mesh.VertexPos = this.originalVertexPos;
					this.mesh.ComputeFaceNormal();
					this.mesh.ComputeVertexNormal();
					return;
				}
				this.mesh.VertexPos = this.collapsedVertexPos;
				this.mesh.ComputeFaceNormal();
				this.mesh.ComputeVertexNormal();
			}
		}
		[Browsable(false)]
		public bool DisplayNodeSphere
		{
			get
			{
				return this.displayNodeSphere;
			}
			set
			{
				this.displayNodeSphere = value;
			}
		}
		[Browsable(false)]
		public bool DisplaySimplifiedMesh
		{
			get
			{
				return this.displaySimplifiedMesh;
			}
			set
			{
				this.displaySimplifiedMesh = value;
			}
		}
		public float SkeletonNodeSize
		{
			get
			{
				return this.skeletonNodeSize;
			}
			set
			{
				this.skeletonNodeSize = value;
			}
		}
		[Browsable(false)]
		public int DisplaySimplifiedMeshIndex
		{
			get
			{
				return this.displaySimplifiedMeshIndex;
			}
			set
			{
				if (this.multigridSolver != null)
				{
					if (value < 0)
					{
						value = 0;
					}
					if (value >= this.multigridSolver.Levels)
					{
						value = this.multigridSolver.Levels - 1;
					}
				}
				else
				{
					value = -1;
				}
				this.displaySimplifiedMeshIndex = value;
			}
		}
		public int NodeCount
		{
			get
			{
				return this.remainingVertexCount;
			}
		}
        [DllImport("taucs.dll", CallingConvention = CallingConvention.Cdecl)]
		private unsafe static extern void* CreaterCholeskySolver(int n, int nnz, int* rowIndex, int* colIndex, double* value);
        [DllImport("taucs.dll", CallingConvention=CallingConvention.Cdecl)]
		private unsafe static extern int Solve(void* solver, double* x, double* b);
        [DllImport("taucs.dll", CallingConvention = CallingConvention.Cdecl)]
		private unsafe static extern double SolveEx(void* solver, double* x, int xIndex, double* b, int bIndex);
        [DllImport("taucs.dll", CallingConvention = CallingConvention.Cdecl)]
		private unsafe static extern int FreeSolver(void* sp);
        [DllImport("taucs.dll", CallingConvention = CallingConvention.Cdecl)]
		private unsafe static extern void* CreaterSymbolicSolver(int n, int nnz, int* rowIndex, int* colIndex, double* value);
        [DllImport("taucs.dll", CallingConvention = CallingConvention.Cdecl)]
		private unsafe static extern void FreeSymbolicSolver(void* sp);
        [DllImport("taucs.dll", CallingConvention = CallingConvention.Cdecl)]
		private unsafe static extern int NumericFactor(void* sp, int n, int nnz, int* rowIndex, int* colIndex, double* value);
        [DllImport("taucs.dll", CallingConvention = CallingConvention.Cdecl)]
		private unsafe static extern void FreeNumericFactor(void* sp);
        [DllImport("taucs.dll", CallingConvention = CallingConvention.Cdecl)]
		private unsafe static extern int NumericSolve(void* sp, double* x, double* b);
		public Skeletonizer(Mesh mesh, Skeletonizer.Options opt)
		{
			int i = mesh.VertexCount;
			int fn = mesh.FaceCount;
			this.mesh = mesh;
			this.opt = opt;
			if (opt.AddNoise)
			{
				this.AddNoise();
			}
			this.lapWeight = new double[i];
			this.posWeight = new double[i];
			this.collapsedLength = new double[i];
			this.originalVertexPos = (double[])mesh.VertexPos.Clone();
			this.originalFaceArea = new double[fn];
			for (int j = 0; j < fn; j++)
			{
				this.originalFaceArea[j] = Math.Abs(mesh.ComputeFaceArea(j));
			}
			for (int k = 0; k < i; k++)
			{
				this.lapWeight[k] = opt.LaplacianConstraintWeight;
				this.posWeight[k] = opt.PositionalConstraintWeight;
				this.collapsedLength[k] = 0.0;
			}
			if (opt.UseIterativeSolver)
			{
				this.multigridSolver = new MultigridContractionSolver(mesh);
			}
		}
		~Skeletonizer()
		{
			this.Dispose();
		}
		public void Display()
		{
			if (this.displaySkeleton && this.simplifiedVertexRec != null)
			{
				this.DrawSimplifiedVertices();
			}
			if (this.displayNodeSphere && this.simplifiedVertexRec != null)
			{
				this.DrawNodeSphere();
			}
			if (this.displaySkeleton && this.rootNode != null)
			{
				this.DrawRootNode();
			}
			if (this.displaySimplifiedMesh && this.multigridSolver != null)
			{
				OpenGL.glColor3d(0.0, 0.9, 0.0);
				this.DrawSimplifiedMesh_by_Vertices();
			}
			if (this.displayIntermediateMesh && this.vRec != null)
			{
				this.DrawIntermediateMesh();
			}
		}

		public void WriteSegmentation(StreamWriter sw)
		{
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");

			int counter = 0;
			foreach (Skeletonizer.VertexRecord rec in this.simplifiedVertexRec)
				rec.minIndex = counter++;

            sw.WriteLine("# vtk DataFile Version 3.0");
            sw.WriteLine("vtk output");
            sw.WriteLine("ASCII");
            sw.WriteLine("DATASET POLYDATA");
            sw.WriteLine("POINTS " + simplifiedVertexRec.Count + " float");
            int a = 0;
			foreach (Skeletonizer.VertexRecord rec2 in this.simplifiedVertexRec)
			{
                sw.Write((float)rec2.pos.x + " " + (float)rec2.pos.y + " " + (float)rec2.pos.z + " ");

                if (++a % 3 == 0)
				    sw.WriteLine();
			}

            int hran = 0;
            foreach (Skeletonizer.VertexRecord rec2 in this.simplifiedVertexRec)
            {
                foreach (int k in rec2.adjV)
                    vRec[k].adjV.Remove(rec2.vIndex);
                hran += rec2.adjV.Count;
            }
            sw.WriteLine();
            sw.WriteLine("LINES " + hran + " " + 3 * hran);
            foreach (Skeletonizer.VertexRecord rec2 in this.simplifiedVertexRec)
                foreach (int k in rec2.adjV)
                    sw.WriteLine(2 + " " + this.vRec[k].minIndex + " " + rec2.minIndex + " ");
		}

		public void Start()
		{
            Stopwatch stopky = new Stopwatch();
            stopky.Start();
			Program.displayProperty.MeshDisplayMode = DisplayProperty.EnumMeshDisplayMode.SmoothShaded;
			Program.displayProperty.DisplaySelectedVertices = true;
			this.GeometryCollapse(this.opt.MaxIterations);
			if (this.opt.DisplayIntermediateMesh)
			{
				Program.displayProperty.MeshDisplayMode = DisplayProperty.EnumMeshDisplayMode.None;
				Program.displayProperty.DisplaySelectedVertices = false;
			}
			if (this.opt.ApplyConnectivitySurgery)
			{
				this.Simplification();
			}
			if (this.opt.ApplyEmbeddingRefinement)
			{
				this.EmbeddingImproving();
			}
			if (this.opt.ApplyRootFinding)
			{
				this.FindRootNode();
			}
			if (this.opt.ApplyConnectivitySurgery)
			{
				this.DisplayOriginalMesh = true;
				Program.displayProperty.MeshDisplayMode = DisplayProperty.EnumMeshDisplayMode.TransparentSmoothShaded;
				Program.displayProperty.DisplaySelectedVertices = false;
			}
			else
			{
				this.DisplayOriginalMesh = false;
				Program.displayProperty.MeshDisplayMode = DisplayProperty.EnumMeshDisplayMode.Wireframe;
				Program.displayProperty.DisplaySelectedVertices = false;
			}
			Program.PrintText("[Ready]");
			Program.RefreshAllForms();

            stopky.Stop();

            Console.WriteLine("cas " + stopky.ElapsedMilliseconds);

            StreamWriter sw = new StreamWriter("vystup");
            WriteSegmentation(sw);
            sw.Close();
		}
		private unsafe void DrawSimplifiedMesh_by_Faces()
		{
			int[] faceIndex = this.multigridSolver.Resolutions[this.displaySimplifiedMeshIndex].faceList;
			int fn = faceIndex.Length / 3;
			OpenGL.glPolygonMode(1032u, 6913u);
			OpenGL.glDisable(2884u);
			Color c = Program.displayProperty.LineColor;
			OpenGL.glColor3ub(c.R, c.G, c.B);
			OpenGL.glLineWidth(Program.displayProperty.LineWidth);
			OpenGL.glEnableClientState(32884u);
			fixed (double* vp = this.originalVertexPos)
			{
				fixed (int* index = faceIndex)
				{
					OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
					OpenGL.glDrawElements(4u, fn * 3, 5125u, (void*)index);
				}
			}
			OpenGL.glDisableClientState(32884u);
			OpenGL.glEnable(2884u);
		}
		private unsafe void DrawSimplifiedMesh_by_Edges()
		{
			int[] faceIndex = this.multigridSolver.Resolutions[this.displaySimplifiedMeshIndex].faceList;
			int arg_21_0 = faceIndex.Length / 3;
			OpenGL.glPolygonMode(1032u, 6913u);
			OpenGL.glDisable(2884u);
			Color c = Program.displayProperty.LineColor;
			OpenGL.glColor3ub(c.R, c.G, c.B);
			OpenGL.glLineWidth(Program.displayProperty.LineWidth);
			OpenGL.glEnableClientState(32884u);
			fixed (double* vp = this.originalVertexPos)
			{
				OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
				OpenGL.glBegin(1u);
				MultigridContractionSolver.Resolution r = this.multigridSolver.Resolutions[this.displaySimplifiedMeshIndex];
				int[] v = r.vertexList;
				Set<int>[] s = r.adjVertex;
				for (int i = 0; i < v.Length; i++)
				{
					foreach (int adj in s[i])
					{
						OpenGL.glArrayElement(v[i]);
						OpenGL.glArrayElement(adj);
					}
				}
				OpenGL.glEnd();
			}
			OpenGL.glDisableClientState(32884u);
			OpenGL.glEnable(2884u);
		}
		private unsafe void DrawSimplifiedMesh_by_Vertices()
		{
			OpenGL.glLineWidth(Program.displayProperty.LineWidth);
			OpenGL.glEnableClientState(32884u);
			fixed (double* vp = this.originalVertexPos)
			{
				OpenGL.glPointSize(4f);
				OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
				OpenGL.glBegin(0u);
				MultigridContractionSolver.Resolution r = this.multigridSolver.Resolutions[this.displaySimplifiedMeshIndex];
				int[] v = r.vertexList;
				Set<int>[] arg_7A_0 = r.adjVertex;
				for (int i = 0; i < v.Length; i++)
				{
					OpenGL.glArrayElement(v[i]);
				}
				OpenGL.glEnd();
			}
			OpenGL.glDisableClientState(32884u);
			OpenGL.glEnable(2884u);
		}
        private unsafe void GeometryCollapse(int maxIter)
		{
			Program.PrintText("[Geometry collaping]");
			double originalVolume = this.mesh.Volume();
			double originalArea = 0.0;
			for (int i = 0; i < this.mesh.FaceCount; i++)
			{
				originalArea += this.mesh.ComputeFaceArea(i);
			}
			if (maxIter == 0)
			{
				return;
			}
			while (true)
			{
				HiPerfTimer timer = new HiPerfTimer();
				timer.Start();
				if (this.opt.UseIterativeSolver)
				{
					double[][] pos = this.multigridSolver.SolveSystem(this.lapWeight, this.posWeight);
					int j = 0;
					int k = 0;
					while (j < this.mesh.VertexCount)
					{
						this.mesh.VertexPos[k] = pos[0][j];
						this.mesh.VertexPos[k + 1] = pos[1][j];
						this.mesh.VertexPos[k + 2] = pos[2][j];
						j++;
						k += 3;
					}
					this.BuildMatrixA();
					this.mesh.ComputeFaceNormal();
					this.mesh.ComputeVertexNormal();
					this.iter++;
				}
				else
				{
					SparseMatrix A = this.BuildMatrixA();
					this.ccsA = new CCSMatrix(A);
					this.ccsATA = this.MultiplyATA(this.ccsA);
					GC.Collect();
					if (this.opt.UseSymbolicSolver)
					{
						this.symbolicSolver = this.SymbolicFactorization(this.ccsATA);
						Program.PrintText("Symbolic solver: " + (this.symbolicSolver != null).ToString());
						int ret = this.NumericFactorization(this.symbolicSolver, this.ccsATA);
						Program.PrintText("Numeric solver: " + (ret == 0).ToString());
					}
					else
					{
						if (this.solver != null)
						{
							Skeletonizer.FreeSolver(this.solver);
						}
						this.solver = this.Factorization(this.ccsATA);
						if (this.solver == null)
						{
							break;
						}
						GC.Collect();
					}
					this.ImplicitSmooth();
				}
				timer.Stop();
				double currentVolume = this.mesh.Volume();
				double currentArea = 0.0;
				for (int l = 0; l < this.mesh.FaceCount; l++)
				{
					currentArea += this.mesh.ComputeFaceArea(l);
				}
				Program.PrintText(string.Concat(new object[]
				{
					this.iter,
					" Area: ",
					currentArea / originalArea,
					" Vol: ",
					currentVolume / originalVolume,
					" CPU Time: ",
					timer.Duration
				}));
				Thread.Sleep(0);
				if (currentVolume / originalVolume <= this.opt.VolumneRatioThreashold || this.iter >= maxIter)
				{
					goto IL_2E7;
				}
			}
			throw new Exception();
			IL_2E7:
			this.Dispose();
			this.collapsedVertexPos = (this.mesh.VertexPos.Clone() as double[]);
		}
        private unsafe void GeometryCollapse2(int maxIter)
		{
			Program.PrintText("[Geometry Contraction]");
			double originalVolume = this.mesh.Volume();
			double originalArea = 0.0;
			for (int i = 0; i < this.mesh.FaceCount; i++)
			{
				originalArea += this.mesh.ComputeFaceArea(i);
			}
			if (maxIter == 0)
			{
				return;
			}
			while (true)
			{
				HiPerfTimer timer = new HiPerfTimer();
				timer.Start();
				if (this.opt.UseIterativeSolver)
				{
					double[][] pos = this.multigridSolver.SolveSystem(this.lapWeight, this.posWeight);
					int j = 0;
					int k = 0;
					while (j < this.mesh.VertexCount)
					{
						this.mesh.VertexPos[k] = pos[0][j];
						this.mesh.VertexPos[k + 1] = pos[1][j];
						this.mesh.VertexPos[k + 2] = pos[2][j];
						j++;
						k += 3;
					}
					this.BuildMatrixA();
					this.mesh.ComputeFaceNormal();
					this.mesh.ComputeVertexNormal();
					this.iter++;
				}
				else
				{
					SparseMatrix A = this.BuildMatrixA();
					this.ccsA = new CCSMatrix(A);
					this.ccsATA = this.MultiplyATA(this.ccsA);
					GC.Collect();
					if (this.opt.UseSymbolicSolver)
					{
						this.symbolicSolver = this.SymbolicFactorization(this.ccsATA);
						this.NumericFactorization(this.symbolicSolver, this.ccsATA);
					}
					else
					{
						if (this.solver != null)
						{
							Skeletonizer.FreeSolver(this.solver);
						}
						this.solver = this.Factorization(this.ccsATA);
						if (this.solver == null)
						{
							break;
						}
						GC.Collect();
					}
					this.ImplicitSmooth();
				}
				timer.Stop();
				double currentVolume = this.mesh.Volume();
				double currentArea = 0.0;
				for (int l = 0; l < this.mesh.FaceCount; l++)
				{
					currentArea += this.mesh.ComputeFaceArea(l);
				}
				Program.PrintText(string.Concat(new object[]
				{
					"[Geometry Contraction] - iteration",
					this.iter,
					" CPU Time: ",
					timer.Duration
				}));
				Thread.Sleep(0);
				Program.RefreshAllForms();
				if (currentArea / originalArea <= 0.001 || currentVolume / originalVolume <= 1E-05 || this.iter >= maxIter)
				{
					goto IL_294;
				}
			}
			throw new Exception();
			IL_294:
			this.Dispose();
			this.collapsedVertexPos = (this.mesh.VertexPos.Clone() as double[]);
		}
		private unsafe void ImplicitSmooth()
		{
			int i = this.mesh.VertexCount;
			double[] x = new double[i];
			double[] b = new double[i * 3];
			double[] ATb = new double[i];
			double[] oldPos = (double[])this.mesh.VertexPos.Clone();
			for (int j = 0; j < 3; j++)
			{
				int k = 0;
				int l = 0;
				while (k < i)
				{
					b[k] = 0.0;
					b[k + i] = this.mesh.VertexPos[l + j] * this.posWeight[k];
					b[k + i + i] = 0.0;
					k++;
					l += 3;
				}
				this.ccsA.PreMultiply(b, ATb);
				if (this.opt.UseSymbolicSolver)
				{
					fixed (double* _x = x, _ATb = ATb)
					{
						Skeletonizer.NumericSolve(this.symbolicSolver, _x, _ATb);
					}
				}
				else
				{
					fixed (double* _x2 = x, _ATb2 = ATb)
					{
						Skeletonizer.Solve(this.solver, _x2, _ATb2);
					}
				}
				double[] vertexPos;
				Monitor.Enter(vertexPos = this.mesh.VertexPos);
				try
				{
					int m = 0;
					int n = 0;
					while (m < i)
					{
						this.mesh.VertexPos[n + j] = x[m];
						m++;
						n += 3;
					}
				}
				finally
				{
					Monitor.Exit(vertexPos);
				}
			}
			this.iter++;
			this.mesh.ComputeFaceNormal();
			this.mesh.ComputeVertexNormal();
			int i2 = 0;
			int j2 = 0;
			while (i2 < i)
			{
				double d = this.mesh.VertexPos[j2] - oldPos[j2];
				double d2 = this.mesh.VertexPos[j2 + 1] - oldPos[j2 + 1];
				double d3 = this.mesh.VertexPos[j2 + 2] - oldPos[j2 + 2];
				this.collapsedLength[i2] += Math.Sqrt(d * d + d2 * d2 + d3 * d3);
				i2++;
				j2 += 3;
			}
		}
		private SparseMatrix BuildMatrixA()
		{
			int i = this.mesh.VertexCount;
			int fn = this.mesh.FaceCount;
			SparseMatrix A = new SparseMatrix(i, i);
			double[] areaRatio = new double[fn];
			bool[] collapsed = new bool[i];
			if (this.oldAreaRatio == null)
			{
				this.oldAreaRatio = new double[fn];
				for (int j = 0; j < fn; j++)
				{
					this.oldAreaRatio[j] = 0.4;
				}
			}
			int k = 0;
			int l = 0;
			while (k < fn)
			{
				int c = this.mesh.FaceIndex[l];
				int c2 = this.mesh.FaceIndex[l + 1];
				int c3 = this.mesh.FaceIndex[l + 2];
				Vector3d v = new Vector3d(this.mesh.VertexPos, c * 3);
				Vector3d v2 = new Vector3d(this.mesh.VertexPos, c2 * 3);
				Vector3d v3 = new Vector3d(this.mesh.VertexPos, c3 * 3);
				areaRatio[k] = Math.Abs(this.mesh.ComputeFaceArea(k)) / this.originalFaceArea[k];
				double arg_116_0 = areaRatio[k];
				double arg_115_0 = this.opt.AreaRatioThreshold;
				double cot = (v2 - v).Dot(v3 - v) / (v2 - v).Cross(v3 - v).Length();
				double cot2 = (v3 - v2).Dot(v - v2) / (v3 - v2).Cross(v - v2).Length();
				double cot3 = (v - v3).Dot(v2 - v3) / (v - v3).Cross(v2 - v3).Length();
				try
				{
					if (double.IsNaN(cot))
					{
						throw new Exception();
					}
					if (double.IsNaN(cot2))
					{
						throw new Exception();
					}
					if (double.IsNaN(cot3))
					{
						throw new Exception();
					}
				}
				catch (Exception e)
				{
					Program.PrintText(e.Message);
					Program.PrintText(string.Concat(new object[]
					{
						"!!! ",
						cot,
						" ",
						cot2,
						" ",
						cot3
					}));
					cot2 = (cot = (cot3 = 0.0));
				}
				A.AddValueTo(c2, c2, -cot);
				A.AddValueTo(c2, c3, cot);
				A.AddValueTo(c3, c3, -cot);
				A.AddValueTo(c3, c2, cot);
				A.AddValueTo(c3, c3, -cot2);
				A.AddValueTo(c3, c, cot2);
				A.AddValueTo(c, c, -cot2);
				A.AddValueTo(c, c3, cot2);
				A.AddValueTo(c, c, -cot3);
				A.AddValueTo(c, c2, cot3);
				A.AddValueTo(c2, c2, -cot3);
				A.AddValueTo(c2, c, cot3);
				k++;
				l += 3;
			}
			double count = 0.0;
			for (int m = 0; m < i; m++)
			{
				double totRatio = 0.0;
				double oldTotRatio = 0.0;
				int[] array = this.mesh.AdjVF[m];
				for (int num = 0; num < array.Length; num++)
				{
					int n = array[num];
					totRatio += areaRatio[n];
					oldTotRatio += this.oldAreaRatio[n];
				}
				totRatio /= (double)this.mesh.AdjVF[m].Length;
				oldTotRatio /= (double)this.mesh.AdjVF[m].Length;
				double tot = 0.0;
				tot = 0.0;
				foreach (SparseMatrix.Element e2 in A.Rows[m])
				{
					if (e2.i != e2.j)
					{
						tot += e2.value;
					}
				}
				if (tot > 10000.0)
				{
					collapsed[m] = true;
					this.mesh.Flag[m] = 1;
					foreach (SparseMatrix.Element e3 in A.Rows[m])
					{
						e3.value /= tot / 10000.0;
					}
				}
				foreach (SparseMatrix.Element e4 in A.Rows[m])
				{
					e4.value *= this.lapWeight[m];
				}
				this.lapWeight[m] *= this.opt.LaplacianConstraintScale;
				if (this.lapWeight[m] > 2048.0)
				{
					this.lapWeight[m] = 2048.0;
				}
				double d = 1.0 / Math.Sqrt(totRatio) * this.opt.PositionalConstraintWeight;
				if (!double.IsNaN(d))
				{
					this.posWeight[m] = d;
				}
				if (this.posWeight[m] > 10000.0)
				{
					this.posWeight[m] = 10000.0;
				}
				count += 1.0;
				bool ok = true;
				foreach (SparseMatrix.Element e5 in A.Rows[m])
				{
					if (double.IsNaN(e5.value))
					{
						ok = false;
						Program.PrintText(string.Concat(new object[]
						{
							e5.i,
							":",
							e5.j,
							"\n"
						}));
					}
				}
				if (!ok)
				{
					foreach (SparseMatrix.Element e6 in A.Rows[m])
					{
						if (e6.i == e6.j)
						{
							e6.value = -1.0;
						}
						else
						{
							e6.value = 1.0 / (double)this.mesh.AdjVV[m].Length;
						}
					}
				}
			}
			for (int i2 = 0; i2 < 3; i2++)
			{
				this.lap[i2] = new double[i];
				double[] x = new double[i];
				int j2 = 0;
				int k2 = i2;
				while (j2 < i)
				{
					x[j2] = this.mesh.VertexPos[k2];
					j2++;
					k2 += 3;
				}
				A.Multiply(x, this.lap[i2]);
			}
			for (int i3 = 0; i3 < fn; i3++)
			{
				this.oldAreaRatio[i3] = areaRatio[i3];
			}
			for (int i4 = 0; i4 < i; i4++)
			{
				A.AddRow();
				A.AddElement(i4 + i, i4, this.posWeight[i4]);
			}
			for (int i5 = 0; i5 < i; i5++)
			{
				A.AddRow();
				A.AddElement(i5 + i + i, i5, this.opt.OriginalPositionalConstraintWeight);
			}
			A.SortElement();
			return A;
		}
		private CCSMatrix MultiplyATA(CCSMatrix A)
		{
			int[] last = new int[A.RowSize];
			int[] next = new int[A.NumNonZero];
			int[] colIndex = new int[A.NumNonZero];
			for (int i = 0; i < last.Length; i++)
			{
				last[i] = -1;
			}
			for (int j = 0; j < next.Length; j++)
			{
				next[j] = -1;
			}
			for (int k = 0; k < A.ColumnSize; k++)
			{
				for (int l = A.ColIndex[k]; l < A.ColIndex[k + 1]; l++)
				{
					int m = A.RowIndex[l];
					if (last[m] != -1)
					{
						next[last[m]] = l;
					}
					last[m] = l;
					colIndex[l] = k;
				}
			}
			CCSMatrix ATA = new CCSMatrix(A.ColumnSize, A.ColumnSize);
			Set<int> set = new Set<int>();
			double[] tmp = new double[A.ColumnSize];
			List<int> ATA_RowIndex = new List<int>();
			List<double> ATA_Value = new List<double>();
			for (int n = 0; n < A.ColumnSize; n++)
			{
				tmp[n] = 0.0;
			}
			for (int j2 = 0; j2 < A.ColumnSize; j2++)
			{
				for (int col = A.ColIndex[j2]; col < A.ColIndex[j2 + 1]; col++)
				{
					int arg_122_0 = A.RowIndex[col];
					double val = A.Values[col];
					int curr = col;
					while (true)
					{
						int i2 = colIndex[curr];
						set.Add(i2);
						tmp[i2] += val * A.Values[curr];
						if (next[curr] == -1)
						{
							break;
						}
						curr = next[curr];
					}
				}
				int[] s = set.ToArray();
				Array.Sort<int>(s);
				int count = 0;
				int[] array = s;
				for (int num = 0; num < array.Length; num++)
				{
					int k2 = array[num];
					if (tmp[k2] != 0.0)
					{
						ATA_RowIndex.Add(k2);
						ATA_Value.Add(tmp[k2]);
						tmp[k2] = 0.0;
						count++;
					}
				}
				ATA.ColIndex[j2 + 1] = ATA.ColIndex[j2] + count;
				set.Clear();
			}
			ATA.RowIndex = ATA_RowIndex.ToArray();
			ATA.Values = ATA_Value.ToArray();
			return ATA;
		}
		private SparseMatrix Multiply(SparseMatrix A1, SparseMatrix A2)
		{
			SparseMatrix M = new SparseMatrix(A1.RowSize, A2.ColumnSize);
			double[] tmp = new double[A2.ColumnSize];
			Set<int> marked = new Set<int>();
			for (int i = 0; i < tmp.Length; i++)
			{
				tmp[i] = 0.0;
			}
			for (int j = 0; j < A1.RowSize; j++)
			{
				marked.Clear();
				List<SparseMatrix.Element> rr = A1.Rows[j];
				foreach (SparseMatrix.Element e in rr)
				{
					List<SparseMatrix.Element> rr2 = A2.Rows[e.j];
					foreach (SparseMatrix.Element e2 in rr2)
					{
						tmp[e2.j] += e.value * e2.value;
						marked.Add(e2.j);
					}
				}
				foreach (int index in marked)
				{
					if (tmp[index] != 0.0)
					{
						M.AddElement(j, index, tmp[index]);
						tmp[index] = 0.0;
					}
				}
			}
			M.SortElement();
			return M;
		}
		private SparseMatrix Multiply1T(SparseMatrix A1, SparseMatrix A2)
		{
			SparseMatrix M = new SparseMatrix(A1.ColumnSize, A2.ColumnSize);
			double[] tmp = new double[A2.ColumnSize];
			Set<int> marked = new Set<int>();
			for (int i = 0; i < tmp.Length; i++)
			{
				tmp[i] = 0.0;
			}
			for (int j = 0; j < A1.ColumnSize; j++)
			{
				marked.Clear();
				List<SparseMatrix.Element> rr = A1.Columns[j];
				foreach (SparseMatrix.Element e in rr)
				{
					List<SparseMatrix.Element> rr2 = A2.Rows[e.i];
					foreach (SparseMatrix.Element e2 in rr2)
					{
						tmp[e2.j] += e.value * e2.value;
						marked.Add(e2.j);
					}
				}
				foreach (int index in marked)
				{
					if (tmp[index] != 0.0)
					{
						M.AddElement(j, index, tmp[index]);
						tmp[index] = 0.0;
					}
				}
			}
			M.SortElement();
			return M;
		}
		private unsafe void* Factorization(CCSMatrix C)
		{
			fixed (int* ri = C.RowIndex)
			{
				fixed (int* ci = C.ColIndex)
				{
					fixed (double* val = C.Values)
					{
						return Skeletonizer.CreaterCholeskySolver(C.ColumnSize, C.NumNonZero, ri, ci, val);
					}
				}
			}
		}
		private unsafe void* SymbolicFactorization(CCSMatrix C)
		{
			fixed (int* ri = C.RowIndex)
			{
				fixed (int* ci = C.ColIndex)
				{
					fixed (double* val = C.Values)
					{
						return Skeletonizer.CreaterSymbolicSolver(C.ColumnSize, C.NumNonZero, ri, ci, val);
					}
				}
			}
		}
		private unsafe int NumericFactorization(void* symoblicSolver, CCSMatrix C)
		{
			fixed (int* ri = C.RowIndex)
			{
				fixed (int* ci = C.ColIndex)
				{
					fixed (double* val = C.Values)
					{
						return Skeletonizer.NumericFactor(symoblicSolver, C.ColumnSize, C.NumNonZero, ri, ci, val);
					}
				}
			}
		}
		private void Simplification()
		{
			Program.PrintText("[Connectivity Surgery]");
			int vertexCount = this.mesh.VertexCount;
			int[] obj;
			Monitor.Enter(obj = this.mesh.FaceIndex);
			try
			{
				this.faceIndex = (int[])this.mesh.FaceIndex.Clone();
			}
			finally
			{
				Monitor.Exit(obj);
			}
			Skeletonizer.VertexRecord[] records = new Skeletonizer.VertexRecord[vertexCount];
			for (int j = 0; j < vertexCount; j++)
			{
				records[j] = new Skeletonizer.VertexRecord(this.mesh, j);
			}
			this.vRec = records;
			for (int k = 0; k < vertexCount; k++)
			{
				Skeletonizer.VertexRecord rec = this.vRec[k];
				Vector3d p = rec.pos;
				rec.minError = 1.7976931348623157E+308;
				rec.minIndex = -1;
				if (this.opt.UseShapeEnergy)
				{
					foreach (int l in rec.adjV)
					{
						Vector3d p2 = this.vRec[l].pos;
						Vector3d u = (p2 - p).Normalize();
						Vector3d w = u.Cross(p);
						Matrix4d m = new Matrix4d();
						m[0, 1] = -u.z;
						m[0, 2] = u.y;
						m[0, 3] = -w.x;
						m[1, 0] = u.z;
						m[1, 2] = -u.x;
						m[1, 3] = -w.y;
						m[2, 0] = -u.y;
						m[2, 1] = u.x;
						m[2, 3] = -w.z;
						rec.matrix += m.Transpose() * m;
					}
				}
				this.UpdateVertexRecords(rec);
			}
			PriorityQueue queue = new PriorityQueue(vertexCount);
			for (int n = 0; n < vertexCount; n++)
			{
				queue.Insert(this.vRec[n]);
			}
			int facesLeft = this.mesh.FaceCount;
			int vertexLeft = this.mesh.VertexCount;
			int edgeLeft = 0;
			this.remainingVertexCount = vertexLeft;
			Skeletonizer.VertexRecord[] array = this.vRec;
			for (int num = 0; num < array.Length; num++)
			{
				Skeletonizer.VertexRecord rec2 = array[num];
				foreach (int arg_266_0 in rec2.adjV)
				{
					edgeLeft++;
				}
			}
			edgeLeft /= 2;
			this.displayIntermediateMesh = true;
			while (facesLeft > 0 && vertexLeft > this.opt.TargetVertexCount && !queue.IsEmpty())
			{
				object obj2;
				Monitor.Enter(obj2 = this.myDisplayLock);
				try
				{
					Skeletonizer.VertexRecord edgeVertex1 = (Skeletonizer.VertexRecord)queue.DeleteMin();
					Skeletonizer.VertexRecord edgeVertex2 = this.vRec[edgeVertex1.minIndex];
					edgeVertex2.matrix = edgeVertex1.matrix + edgeVertex2.matrix; // secteni matic Q
					if (edgeVertex1.center) // evidentne nejen half-edge collapse
					{
						edgeVertex2.pos = (edgeVertex1.pos + edgeVertex2.pos) / 2.0;
					}

                    // slouceni seznamu
					edgeVertex2.collapseFrom.Add(edgeVertex1.vIndex);
					foreach (int index in edgeVertex1.collapseFrom)
					{
						edgeVertex2.collapseFrom.Add(index);
					}
					edgeVertex1.collapseFrom = null;

                    
					int vertexIndex1 = edgeVertex1.vIndex;
					int vertexIndex2 = edgeVertex2.vIndex;
					int count = 0;
                    // pruchod sousedicich trojuhelniku vrcholu V1 (ten slucovany do V2)
					foreach (int faceIndex in edgeVertex1.adjF)
					{
						int t = faceIndex * 3;
                        // evidentne c,c2,c3 jsou indexy vrcholu tvorici trojuhelnik faceIndex
						int c = this.faceIndex[t];
						int c2 = this.faceIndex[t + 1];
						int c3 = this.faceIndex[t + 2];
						if (c == vertexIndex2 || c2 == vertexIndex2 || c3 == vertexIndex2 || c == c2 || c2 == c3 || c3 == c)
						{
                            // hmmm... odstranime vsechny trojuhelniky, ktere jsou a) degenerovane, b) obsahuji V2
                            foreach (int index3 in edgeVertex1.adjV) // tedy - odstranime referenci na faceIndex ze vsech sousedu V1
							{
								this.vRec[index3].adjF.Remove(faceIndex);
							}
							facesLeft--; // a mame o jeden triangle mene
							count++; // pocitadlo, kolik jsme odstranili celkem
						}
						else
						{
                            // predani trojuhelniku V1 vrcholu V2
							if (c == vertexIndex1)
							{
								this.faceIndex[t] = vertexIndex2;
							}
							if (c2 == vertexIndex1)
							{
								this.faceIndex[t + 1] = vertexIndex2;
							}
							if (c3 == vertexIndex1)
							{
								this.faceIndex[t + 2] = vertexIndex2;
							}
							edgeVertex2.adjF.Add(faceIndex);
						}
					}
                    // pruchod one-ring v-sousedu V1
					foreach (int vertIndexNeighborOfV1 in edgeVertex1.adjV)
					{
						Skeletonizer.VertexRecord recAdj = this.vRec[vertIndexNeighborOfV1]; // vyzvedneme si strukturu popisujici vrchol
						if (recAdj.adjV.Contains(vertexIndex1))
						{
							recAdj.adjV.Remove(vertexIndex1); // odstranime referenci na V1
							edgeLeft--;
						}
						if (vertIndexNeighborOfV1 != vertexIndex2)
						{
							recAdj.adjV.Add(vertexIndex2);  // pridame referenci na V2, neni-li jiz zminena
                            edgeVertex2.adjV.Add(vertIndexNeighborOfV1); // ...a druhym smerem taky
						}
					}
                    // pruchod one-ring v-sousedu V2
					foreach (int vertIndexNeighborOfV2 in edgeVertex2.adjV)
					{
						this.UpdateVertexRecords(this.vRec[vertIndexNeighborOfV2]); // update zaznamu 
						queue.Update(this.vRec[vertIndexNeighborOfV2]); // ...a update ve fronte
					}

                    // update zaznamu V2 
					this.UpdateVertexRecords(edgeVertex2);
                    // ...a update ve fronte
					queue.Update(edgeVertex2);

                    // pruchod one-ring trojuhelniku okolo V2
					foreach (int faceIndexNeighborV2 in edgeVertex2.adjF)
					{
						int t2 = faceIndexNeighborV2 * 3;
						int c4 = this.faceIndex[t2];
						int c5 = this.faceIndex[t2 + 1];
						int c6 = this.faceIndex[t2 + 2];
                        // odstraneni degenerovanych trojuhelniku
						if (c4 == c5 || c5 == c6 || c6 == c4)
						{
							edgeVertex2.adjF.Remove(faceIndexNeighborV2);
                            // a odstranime i reference na trojuhelniky z vrcholu
							foreach (int vertIndexNeighborV2 in edgeVertex2.adjV)
							{
								this.vRec[vertIndexNeighborV2].adjF.Remove(faceIndexNeighborV2);
							}
							facesLeft--; // timto jsme prisli o trojuhelnik
						}
					}
					if (count == 0) // heh, nic nebylo odstraneno?
					{
						Program.PrintText("!");
					}
					vertexLeft--; // odstraneny vrchol
					this.remainingVertexCount = vertexLeft;
				}
				finally
				{
					Monitor.Exit(obj2);
				}
				if (this.opt.DisplayIntermediateMesh && facesLeft % 100 == 0)
				{
					Thread.Sleep(100);
				}
            } // while (facesLeft > 0 && vertexLeft > this.opt.TargetVertexCount && !queue.IsEmpty())

            // zbytek fronty = kostra
			this.simplifiedVertexRec = new List<Skeletonizer.VertexRecord>(512);
			while (!queue.IsEmpty())
			{
				Skeletonizer.VertexRecord rec5 = (Skeletonizer.VertexRecord)queue.DeleteMin();
				this.simplifiedVertexRec.Add(rec5);
			}
			Skeletonizer.VertexRecord[] array2 = this.vRec;
			for (int num2 = 0; num2 < array2.Length; num2++)
			{
				Skeletonizer.VertexRecord rec6 = array2[num2];
				rec6.adjF = null;
				if (rec6.collapseFrom == null)
				{
					rec6.adjV = null;
				}
			}
			this.displayIntermediateMesh = false;
			this.AssignColorIndex();
			Program.PrintText("Nodes:" + this.simplifiedVertexRec.Count.ToString());
		}
		private void RemoveFaces()
		{
			for (int i = 0; i < this.mesh.FaceCount; i++)
			{
				int indexBase = i * 3;
				int c = this.mesh.FaceIndex[indexBase];
				int c2 = this.mesh.FaceIndex[indexBase + 1];
				int c3 = this.mesh.FaceIndex[indexBase + 2];
				Vector3d v = new Vector3d(this.mesh.VertexPos, c * 3);
				Vector3d v2 = new Vector3d(this.mesh.VertexPos, c2 * 3);
				Vector3d v3 = new Vector3d(this.mesh.VertexPos, c3 * 3);
				double d = (v2 - v).Length();
				double d2 = (v3 - v2).Length();
				double d3 = (v - v3).Length();
				int u = -1;
				int u2 = -1;
				if (d > d2 && d > d3)
				{
					u = c;
					u2 = c2;
				}
				else
				{
					if (d2 > d3)
					{
						u = c2;
						u2 = c3;
					}
					else
					{
						u = c3;
						u2 = c;
					}
				}
				bool found = false;
				Skeletonizer.VertexRecord r = this.vRec[u];
				Skeletonizer.VertexRecord arg_FD_0 = this.vRec[u2];
				foreach (int faceIndex in r.adjF)
				{
					if (faceIndex != i)
					{
						int indexBase2 = faceIndex * 3;
						int cc = this.mesh.FaceIndex[indexBase2];
						int cc2 = this.mesh.FaceIndex[indexBase2 + 1];
						int cc3 = this.mesh.FaceIndex[indexBase2 + 2];
						if (cc == u2 || cc2 == u2 || cc3 == u2)
						{
							found = true;
							break;
						}
					}
				}
				if (!found)
				{
					this.vRec[u].adjV.Remove(u2);
					this.vRec[u2].adjV.Remove(u);
				}
				this.vRec[c].adjF.Remove(i);
				this.vRec[c2].adjF.Remove(i);
				this.vRec[c3].adjF.Remove(i);
			}
		}
		private void UpdateVertexRecords(Skeletonizer.VertexRecord vertexRecord) // vrchol V
		{
			Vector3d p = vertexRecord.pos;
			vertexRecord.minError = 1.7976931348623157E+308;  // evidentne budeme prepocitavat chybu
			vertexRecord.minIndex = -1;
            // mame nejake sousedni vrcholy (vic nez jeden?), mame nejake sousedni trojuhelniky?
			if (vertexRecord.adjV.Count > 1 && vertexRecord.adjF.Count > 0)
			{
                // vypocteme prumernou delku sousedni hrany k V
				double avgLength = 0.0;
				foreach (int i in vertexRecord.adjV)
				{
					Skeletonizer.VertexRecord rec2 = this.vRec[i];
					avgLength += (p - rec2.pos).Length();
				}
				avgLength /= (double)vertexRecord.adjV.Count; 

                // pruchod pres one-ring vrcholy vuci V
				foreach (int j in vertexRecord.adjV)
				{
                    // hledame referenci na vrchol (j) ve vsech one-ring trojuhelnikach (vuci V)
					bool found = false;
					foreach (int index in vertexRecord.adjF)
					{
						int t = index * 3;
						int c = this.faceIndex[t];
						int c2 = this.faceIndex[t + 1];
						int c3 = this.faceIndex[t + 2];
						if (c == j || c2 == j || c3 == j)
						{
							found = true; // nasli!
						}
					}
                    // nasli?
					if (found)
					{
                        // ziskame popisnou strukturu vrcholu J
						Skeletonizer.VertexRecord vertexJ = this.vRec[j];
						Vector3d p2 = vertexJ.pos;
                        // ma sousedni trojuhelniky?
						if (vertexJ.adjF.Count != 0)
						{
							double err = 0.0;
							if (this.opt.UseSamplingEnergy)
							{
                                err = (p - p2).Length() * avgLength; // hmmm... prvni cast chyby = delka hrany * avgLength
							}
							if (this.opt.UseShapeEnergy)
							{
								Vector4d v = new Vector4d(p2, 1.0);
								Vector4d v2 = new Vector4d((p + p2) / 2.0, 1.0); // o tomhle nejak ani nebyla v clanku rec :-P ze se snazi si vybrat uplne nejlepsi variantu...

								Matrix4d matrixQ = vertexRecord.matrix + vertexJ.matrix; // jasne... secteme si matice Q vrcholu V a vrcholu (j)
								double e = v.Dot(matrixQ * v) * this.opt.ShapeEnergyWeight;
								double e2 = v2.Dot(matrixQ * v2) * this.opt.ShapeEnergyWeight;
								if (e < e2)
								{
									err += e;
								}
								else
								{
									err += e2;
									vertexRecord.center = true;
								}
							}
                            // nove zjistena chyba je mensi nez predtim? aktualizuj minError, minIndex
							if (err < vertexRecord.minError)
							{
								vertexRecord.minError = err;
								vertexRecord.minIndex = j;
							}
						}
					}
				}
			}
            // a to je cely update vrcholu! (staci jej narvat zase do fronty)
		}
		private void AssignColorIndex()
		{
			bool[] used = new bool[3];
			Queue<Skeletonizer.VertexRecord> q = new Queue<Skeletonizer.VertexRecord>();
			foreach (Skeletonizer.VertexRecord rec in this.simplifiedVertexRec)
			{
				rec.colorIndex = -1;
			}
			foreach (Skeletonizer.VertexRecord rec2 in this.simplifiedVertexRec)
			{
				if (rec2.colorIndex == -1)
				{
					q.Enqueue(rec2);
					while (q.Count > 0)
					{
						Skeletonizer.VertexRecord rec3 = q.Dequeue();
						for (int i = 0; i < used.Length; i++)
						{
							used[i] = false;
						}
						foreach (int adj in rec3.adjV)
						{
							Skeletonizer.VertexRecord rec4 = this.vRec[adj];
							if (rec4.colorIndex != -1)
							{
								used[this.vRec[adj].colorIndex] = true;
							}
							else
							{
								q.Enqueue(rec4);
							}
						}
						for (int j = 0; j < used.Length; j++)
						{
							if (!used[j])
							{
								rec3.colorIndex = j;
							}
						}
					}
				}
			}
			foreach (Skeletonizer.VertexRecord rec5 in this.simplifiedVertexRec)
			{
				this.mesh.Flag[rec5.vIndex] = (byte)(rec5.colorIndex + 1);
				foreach (int index in rec5.collapseFrom)
				{
					this.mesh.Flag[index] = (byte)(rec5.colorIndex + 1);
				}
			}
		}
		private void PostSimplification()
		{
			bool updated = false;
			do
			{
				updated = false;
				foreach (Skeletonizer.VertexRecord rec in this.simplifiedVertexRec)
				{
					if (rec.collapseFrom != null && rec.adjV.Count > 2)
					{
						foreach (int adj in rec.adjV)
						{
							Skeletonizer.VertexRecord rec2 = this.vRec[adj];
							if (rec2.adjV.Count > 2)
							{
								double minLen = 1.7976931348623157E+308;
								double avgLen = 0.0;
								int cc = 0;
								foreach (int a in rec.adjV)
								{
									if (a != adj)
									{
										double len = this.PostSimplification_ComputeBranchLength(rec, a);
										avgLen += len;
										cc++;
										if (len < minLen)
										{
											minLen = len;
										}
									}
								}
								foreach (int a2 in rec2.adjV)
								{
									if (a2 != rec.vIndex)
									{
										double len2 = this.PostSimplification_ComputeBranchLength(rec2, a2);
										avgLen += len2;
										cc++;
										if (len2 < minLen)
										{
											minLen = len2;
										}
									}
								}
								avgLen /= (double)cc;
								double edgeLen = (rec2.pos - rec.pos).Length();
								if (edgeLen >= avgLen * 0.5)
								{
									break;
								}
								int r = rec.vIndex;
								rec2.pos = (rec2.pos + rec.pos) / 2.0;
								rec2.collapseFrom.Add(r);
								foreach (int index in rec.collapseFrom)
								{
									rec2.collapseFrom.Add(index);
								}
								rec.collapseFrom = null;
								updated = true;
								using (IEnumerator<int> enumerator6 = rec.adjV.GetEnumerator())
								{
									while (enumerator6.MoveNext())
									{
										int index2 = enumerator6.Current;
										Skeletonizer.VertexRecord recAdj = this.vRec[index2];
										if (recAdj.adjV.Contains(r))
										{
											recAdj.adjV.Remove(r);
										}
										if (index2 != adj)
										{
											recAdj.adjV.Add(adj);
											rec2.adjV.Add(index2);
										}
									}
									break;
								}
							}
						}
					}
				}
			}
			while (updated);
			List<Skeletonizer.VertexRecord> updatedVertexRec = new List<Skeletonizer.VertexRecord>(32);
			foreach (Skeletonizer.VertexRecord rec3 in this.simplifiedVertexRec)
			{
				if (rec3.collapseFrom != null)
				{
					updatedVertexRec.Add(rec3);
				}
			}
			this.simplifiedVertexRec = updatedVertexRec;
		}
		private double PostSimplification_ComputeBranchLength(Skeletonizer.VertexRecord rec, int adj)
		{
			Skeletonizer.VertexRecord rec2 = this.vRec[adj];
			double len = (rec2.pos - rec.pos).Length();
			if (rec2.adjV.Count != 2)
			{
				return len;
			}
			foreach (int adj2 in rec2.adjV)
			{
				if (adj2 != rec.vIndex)
				{
					return len + this.PostSimplification_ComputeBranchLength(rec2, adj2);
				}
			}
			return len;
		}
		private void EmbeddingImproving_old_sphere_fitting()
		{
			if (this.opt.TwoDimensionModel)
			{
				return;
			}
			if (this.opt.UseBoundaryVerticesOnly)
			{
				int vn = this.mesh.VertexCount;
				this.adjSegmentVertex = new int[vn];
				for (int i = 0; i < vn; i++)
				{
					this.adjSegmentVertex[i] = 0;
					int flag = (int)this.mesh.Flag[i];
					int[] array = this.mesh.AdjVV[i];
					for (int k = 0; k < array.Length; k++)
					{
						int j = array[k];
						if ((int)this.mesh.Flag[j] == flag)
						{
							this.adjSegmentVertex[i]++;
						}
					}
				}
			}
			this.FitNodeSphere();
			this.ComputeEmbeddingError();
			if (this.opt.PostSimplify)
			{
				this.MergeJoint();
				this.FitNodeSphere();
				this.AssignColorIndex();
				this.ComputeEmbeddingError();
			}
			this.adjSegmentVertex = null;
		}
		private void EmbeddingImproving()
		{
			Program.PrintText("[Embedding Refinement]");
			int vn = this.mesh.VertexCount;
			int[] segmentIndex = new int[vn];
			bool[] marked = new bool[vn];
			for (int i = 0; i < vn; i++)
			{
				marked[i] = false;
			}
			foreach (Skeletonizer.VertexRecord rec in this.simplifiedVertexRec)
			{
				rec.collapseFrom.Add(rec.vIndex);
				foreach (int index in rec.collapseFrom)
				{
					segmentIndex[index] = rec.vIndex;
				}
			}
			foreach (Skeletonizer.VertexRecord rec2 in this.simplifiedVertexRec)
			{
				Vector3d totDis = default(Vector3d);
				rec2.pos = new Vector3d(this.originalVertexPos, rec2.vIndex * 3);
				if (rec2.adjV.Count == 2)
				{
					foreach (int adj in rec2.adjV)
					{
						Vector3d dis = default(Vector3d);
						Vector3d q = default(Vector3d);
						Set<int> boundaryVertices = new Set<int>();
						double totLen = 0.0;
						foreach (int j in rec2.collapseFrom)
						{
							int[] array = this.mesh.AdjVV[j];
							for (int num = 0; num < array.Length; num++)
							{
								int k = array[num];
								if (segmentIndex[k] == adj)
								{
									marked[j] = true;
									boundaryVertices.Add(j);
									break;
								}
							}
						}
						foreach (int l in boundaryVertices)
						{
							Vector3d p = new Vector3d(this.originalVertexPos, l * 3);
							Vector3d p2 = new Vector3d(this.mesh.VertexPos, l * 3);
							double len = 0.0;
							int[] array2 = this.mesh.AdjVV[l];
							for (int num2 = 0; num2 < array2.Length; num2++)
							{
								int m = array2[num2];
								if (marked[m])
								{
									Vector3d u = new Vector3d(this.originalVertexPos, m * 3);
									len += (p - u).Length();
								}
							}
							q += p2 * len;
							dis += (p - p2) * len;
							totLen += len;
						}
						foreach (int n in boundaryVertices)
						{
							marked[n] = false;
						}
						Vector3d center = (q + dis) / totLen;
						if (totLen > 0.0)
						{
							totDis += center;
						}
					}
					rec2.pos = totDis / (double)rec2.adjV.Count;
				}
				else
				{
					Vector3d dis2 = default(Vector3d);
					double totLen2 = 0.0;
					foreach (int i2 in rec2.collapseFrom)
					{
						int[] array3 = this.mesh.AdjVV[i2];
						for (int num3 = 0; num3 < array3.Length; num3++)
						{
							int j2 = array3[num3];
							if (segmentIndex[j2] != rec2.vIndex)
							{
								marked[i2] = true;
							}
						}
					}
					foreach (int i3 in rec2.collapseFrom)
					{
						if (marked[i3])
						{
							Vector3d p3 = new Vector3d(this.originalVertexPos, i3 * 3);
							new Vector3d(this.mesh.VertexPos, i3 * 3);
							double len2 = 0.0;
							int[] array4 = this.mesh.AdjVV[i3];
							for (int num4 = 0; num4 < array4.Length; num4++)
							{
								int j3 = array4[num4];
								if (marked[j3])
								{
									Vector3d u2 = new Vector3d(this.originalVertexPos, j3 * 3);
									len2 += (p3 - u2).Length();
								}
							}
							dis2 += (p3 - rec2.pos) * len2;
							totLen2 += len2;
						}
					}
					if (totLen2 > 0.0)
					{
						rec2.pos += dis2 / totLen2;
					}
				}
			}
			if (this.opt.PostSimplify)
			{
				this.MergeJoint2();
				this.AssignColorIndex();
			}
		}
		private void MergeJoint2()
		{
			int vn = this.mesh.VertexCount;
			int[] segmentIndex = new int[vn];
			foreach (Skeletonizer.VertexRecord rec in this.simplifiedVertexRec)
			{
				rec.collapseFrom.Add(rec.vIndex);
				foreach (int index in rec.collapseFrom)
				{
					segmentIndex[index] = rec.vIndex;
				}
			}
			bool updated = false;
			do
			{
				foreach (Skeletonizer.VertexRecord rec2 in this.simplifiedVertexRec)
				{
					if (rec2.collapseFrom != null && rec2.adjV.Count > 2)
					{
						Vector3d p = rec2.pos;
						updated = false;
						double radius = 0.0;
						foreach (int index2 in rec2.collapseFrom)
						{
							Vector3d q = new Vector3d(this.originalVertexPos, index2 * 3);
							radius += (p - q).Length();
						}
						radius /= (double)rec2.collapseFrom.Count;
						double sd = 0.0;
						foreach (int index3 in rec2.collapseFrom)
						{
							Vector3d q2 = new Vector3d(this.originalVertexPos, index3 * 3);
							double diff = (p - q2).Length() - radius;
							sd += diff * diff;
						}
						sd /= (double)rec2.collapseFrom.Count;
						sd = Math.Sqrt(sd);
						sd /= radius;
						Vector3d minCenter = default(Vector3d);
						double minSD = 1.7976931348623157E+308;
						int minAdj = -1;
						foreach (int adj in rec2.adjV)
						{
							Skeletonizer.VertexRecord rec3 = this.vRec[adj];
							if (rec3.adjV.Count != 1)
							{
								Vector3d newCenter = default(Vector3d);
								double newRadius = 0.0;
								double newSD = 0.0;
								Vector3d dis = default(Vector3d);
								double totLen = 0.0;
								Set<int> marked = new Set<int>();
								foreach (int i in rec2.collapseFrom)
								{
									int[] array = this.mesh.AdjVV[i];
									for (int num = 0; num < array.Length; num++)
									{
										int j = array[num];
										if (segmentIndex[j] != rec2.vIndex && segmentIndex[j] != adj)
										{
											marked.Add(i);
										}
									}
								}
								foreach (int k in marked)
								{
									Vector3d p2 = new Vector3d(this.originalVertexPos, k * 3);
									new Vector3d(this.mesh.VertexPos, k * 3);
									double len = 0.0;
									int[] array = this.mesh.AdjVV[k];
									for (int num = 0; num < array.Length; num++)
									{
										int l = array[num];
										if (marked.Contains(l))
										{
											Vector3d u = new Vector3d(this.originalVertexPos, l * 3);
											len += (p2 - u).Length();
										}
									}
									dis += p2 * len;
									totLen += len;
								}
								marked.Clear();
								foreach (int m in rec3.collapseFrom)
								{
									int[] array = this.mesh.AdjVV[m];
									for (int num = 0; num < array.Length; num++)
									{
										int n = array[num];
										if (segmentIndex[n] != rec3.vIndex && segmentIndex[n] != rec2.vIndex)
										{
											marked.Add(m);
										}
									}
								}
								foreach (int i2 in marked)
								{
									Vector3d p3 = new Vector3d(this.originalVertexPos, i2 * 3);
									new Vector3d(this.mesh.VertexPos, i2 * 3);
									double len2 = 0.0;
									int[] array = this.mesh.AdjVV[i2];
									for (int num = 0; num < array.Length; num++)
									{
										int j2 = array[num];
										if (marked.Contains(j2))
										{
											Vector3d u2 = new Vector3d(this.originalVertexPos, j2 * 3);
											len2 += (p3 - u2).Length();
										}
									}
									dis += p3 * len2;
									totLen += len2;
								}
								newCenter = dis / totLen;
								foreach (int index4 in rec2.collapseFrom)
								{
									Vector3d q3 = new Vector3d(this.originalVertexPos, index4 * 3);
									newRadius += (newCenter - q3).Length();
								}
								foreach (int index5 in rec3.collapseFrom)
								{
									Vector3d q4 = new Vector3d(this.originalVertexPos, index5 * 3);
									newRadius += (newCenter - q4).Length();
								}
								newRadius /= (double)(rec2.collapseFrom.Count + rec3.collapseFrom.Count);
								foreach (int index6 in rec2.collapseFrom)
								{
									Vector3d q5 = new Vector3d(this.originalVertexPos, index6 * 3);
									double diff2 = (newCenter - q5).Length() - newRadius;
									newSD += diff2 * diff2;
								}
								foreach (int index7 in rec3.collapseFrom)
								{
									Vector3d q6 = new Vector3d(this.originalVertexPos, index7 * 3);
									double diff3 = (newCenter - q6).Length() - newRadius;
									newSD += diff3 * diff3;
								}
								newSD /= (double)(rec2.collapseFrom.Count + rec3.collapseFrom.Count);
								newSD = Math.Sqrt(newSD);
								newSD /= newRadius;
								if (newSD < minSD)
								{
									minSD = newSD;
									minCenter = newCenter;
									minAdj = adj;
								}
							}
						}
						if (minAdj != -1 && minSD < this.opt.PostSimplifyErrorRatio * sd)
						{
							int r = rec2.vIndex;
							Skeletonizer.VertexRecord rec4 = this.vRec[minAdj];
							rec4.pos = minCenter;
							rec4.collapseFrom.Add(rec2.vIndex);
							foreach (int index8 in rec2.collapseFrom)
							{
								rec4.collapseFrom.Add(index8);
							}
							rec2.collapseFrom = null;
							updated = true;
							foreach (int index9 in rec2.adjV)
							{
								Skeletonizer.VertexRecord recAdj = this.vRec[index9];
								if (recAdj.adjV.Contains(r))
								{
									recAdj.adjV.Remove(r);
								}
								if (index9 != minAdj)
								{
									recAdj.adjV.Add(minAdj);
									rec4.adjV.Add(index9);
								}
							}
						}
					}
				}
			}
			while (updated);
			List<Skeletonizer.VertexRecord> updatedVertexRec = new List<Skeletonizer.VertexRecord>(32);
			foreach (Skeletonizer.VertexRecord rec5 in this.simplifiedVertexRec)
			{
				if (rec5.collapseFrom != null)
				{
					updatedVertexRec.Add(rec5);
				}
			}
			this.simplifiedVertexRec = updatedVertexRec;
		}
		private void FitNodeSphere()
		{
			for (int iter = 0; iter < this.opt.NumOfImprovement; iter++)
			{
				foreach (Skeletonizer.VertexRecord rec in this.simplifiedVertexRec)
				{
					Vector3d c = rec.pos;
					if (rec.collapseFrom.Count <= 3)
					{
						rec.pos = default(Vector3d);
						foreach (int i in rec.collapseFrom)
						{
							rec.pos += new Vector3d(this.originalVertexPos, i * 3);
						}
						rec.pos /= (double)rec.collapseFrom.Count;
						rec.radius = 0.0;
					}
					else
					{
						double r = 0.0;
						foreach (int j in rec.collapseFrom)
						{
							Vector3d p = new Vector3d(this.originalVertexPos, j * 3);
							double len = (p - c).Length();
							r += len;
						}
						r /= (double)rec.collapseFrom.Count;
						rec.radius = r;
						double tot_w = 0.0;
						Vector3d new_c = default(Vector3d);
						foreach (int k in rec.collapseFrom)
						{
							if (!this.opt.UseBoundaryVerticesOnly || rec.adjV.Count < 2 || this.mesh.AdjVV[k].Length != this.adjSegmentVertex[k])
							{
								Vector3d p2 = new Vector3d(this.originalVertexPos, k * 3);
								double d = (p2 - c).Length();
								double w = Math.Abs(d - r);
								if (d != 0.0 && w != 0.0 && d >= r)
								{
									Vector3d target = default(Vector3d);
									target = p2 + (c - p2) * (r / d);
									tot_w += w;
									new_c += w * target;
									if (double.IsNaN(new_c.x) || double.IsNaN(new_c.y) || double.IsNaN(new_c.z))
									{
										throw new Exception();
									}
								}
							}
						}
						if (tot_w > 0.0)
						{
							new_c /= tot_w;
							rec.pos = new_c;
						}
					}
				}
			}
		}
		private void ComputeEmbeddingError()
		{
			foreach (Skeletonizer.VertexRecord rec in this.simplifiedVertexRec)
			{
				Vector3d c = rec.pos;
				double r = rec.radius;
				double err = 0.0;
				foreach (int i in rec.collapseFrom)
				{
					Vector3d p = new Vector3d(this.originalVertexPos, i * 3);
					double diff = (p - c).Length() - r;
					err += diff * diff;
				}
				err /= (double)rec.collapseFrom.Count;
				err /= r * r;
				rec.err = err;
			}
		}
		private void ComputeEmbeddingError(Skeletonizer.VertexRecord rec)
		{
			Vector3d c = rec.pos;
			double r = rec.radius;
			for (int iter = 0; iter < this.opt.NumOfImprovement; iter++)
			{
				r = 0.0;
				foreach (int i in rec.collapseFrom)
				{
					Vector3d p = new Vector3d(this.originalVertexPos, i * 3);
					double len = (p - c).Length();
					r += len;
				}
				r /= (double)rec.collapseFrom.Count;
				double tot_w = 0.0;
				Vector3d new_c = default(Vector3d);
				foreach (int j in rec.collapseFrom)
				{
					Vector3d p2 = new Vector3d(this.originalVertexPos, j * 3);
					double d = (p2 - c).Length();
					double w = Math.Abs(d - r);
					if (d != 0.0 && w != 0.0 && d >= r)
					{
						Vector3d target = default(Vector3d);
						target = p2 + (c - p2) * (r / d);
						tot_w += w;
						new_c += w * target;
						if (double.IsNaN(new_c.x) || double.IsNaN(new_c.y) || double.IsNaN(new_c.z))
						{
							throw new Exception();
						}
					}
				}
				if (tot_w > 0.0)
				{
					new_c /= tot_w;
					c = new_c;
				}
			}
			double err = 0.0;
			foreach (int k in rec.collapseFrom)
			{
				Vector3d p3 = new Vector3d(this.originalVertexPos, k * 3);
				double diff = (p3 - c).Length() - r;
				err += diff * diff;
			}
			err /= (double)rec.collapseFrom.Count;
			err /= r * r;
			rec.err = err;
		}
		private double ComputeEmbeddingError(Skeletonizer.VertexRecord rec, Skeletonizer.VertexRecord rec2)
		{
			Vector3d c = rec.pos;
			double r = rec.radius;
			for (int iter = 0; iter < this.opt.NumOfImprovement; iter++)
			{
				r = 0.0;
				foreach (int i in rec.collapseFrom)
				{
					Vector3d p = new Vector3d(this.originalVertexPos, i * 3);
					double len = (p - c).Length();
					r += len;
				}
				foreach (int j in rec2.collapseFrom)
				{
					Vector3d p2 = new Vector3d(this.originalVertexPos, j * 3);
					double len2 = (p2 - c).Length();
					r += len2;
				}
				r /= (double)(rec.collapseFrom.Count + rec2.collapseFrom.Count);
				double tot_w = 0.0;
				Vector3d new_c = default(Vector3d);
				foreach (int k in rec.collapseFrom)
				{
					Vector3d p3 = new Vector3d(this.originalVertexPos, k * 3);
					double d = (p3 - c).Length();
					double w = Math.Abs(d - r);
					if (d != 0.0 && w != 0.0 && d >= r)
					{
						Vector3d target = default(Vector3d);
						target = p3 + (c - p3) * (r / d);
						tot_w += w;
						new_c += w * target;
						if (double.IsNaN(new_c.x) || double.IsNaN(new_c.y) || double.IsNaN(new_c.z))
						{
							throw new Exception();
						}
					}
				}
				foreach (int l in rec2.collapseFrom)
				{
					Vector3d p4 = new Vector3d(this.originalVertexPos, l * 3);
					double d2 = (p4 - c).Length();
					double w2 = Math.Abs(d2 - r);
					if (d2 != 0.0 && w2 != 0.0 && d2 >= r)
					{
						Vector3d target2 = default(Vector3d);
						target2 = p4 + (c - p4) * (r / d2);
						tot_w += w2;
						new_c += w2 * target2;
						if (double.IsNaN(new_c.x) || double.IsNaN(new_c.y) || double.IsNaN(new_c.z))
						{
							throw new Exception();
						}
					}
				}
				if (tot_w > 0.0)
				{
					new_c /= tot_w;
					c = new_c;
				}
			}
			double err = 0.0;
			foreach (int m in rec.collapseFrom)
			{
				Vector3d p5 = new Vector3d(this.originalVertexPos, m * 3);
				double diff = (p5 - c).Length() - r;
				err += diff * diff;
			}
			foreach (int n in rec2.collapseFrom)
			{
				Vector3d p6 = new Vector3d(this.originalVertexPos, n * 3);
				double diff2 = (p6 - c).Length() - r;
				err += diff2 * diff2;
			}
			err /= (double)(rec.collapseFrom.Count + rec2.collapseFrom.Count);
			err /= r * r;
			return err;
		}
		private void MergeJoint()
		{
			foreach (Skeletonizer.VertexRecord rec in this.simplifiedVertexRec)
			{
				this.ComputeMergeJointCost(rec);
			}
			PriorityQueue queue = new PriorityQueue(this.simplifiedVertexRec.Count);
			using (List<Skeletonizer.VertexRecord>.Enumerator enumerator2 = this.simplifiedVertexRec.GetEnumerator())
			{
				while (enumerator2.MoveNext())
				{
					Skeletonizer.VertexRecord rec2 = enumerator2.Current;
					queue.Insert(rec2);
				}
				goto IL_24F;
			}
			IL_82:
			Skeletonizer.VertexRecord rec3 = queue.GetMin() as Skeletonizer.VertexRecord;
			if (rec3.minIndex == -1 || rec3.minError > this.opt.PostSimplifyErrorRatio)
			{
				goto IL_25A;
			}
			queue.DeleteMin();
			Skeletonizer.VertexRecord rec4 = this.vRec[rec3.minIndex];
			Program.PrintText(string.Concat(new object[]
			{
				rec3.vIndex.ToString(),
				" ==> ",
				rec4.vIndex.ToString(),
				" : ",
				rec3.minError
			}));
			rec4.collapseFrom.Add(rec3.vIndex);
			foreach (int v in rec3.collapseFrom)
			{
				rec4.collapseFrom.Add(v);
			}
			foreach (int index in rec3.adjV)
			{
				Skeletonizer.VertexRecord adjRec = this.vRec[index];
				adjRec.adjV.Remove(rec3.vIndex);
				if (index != rec4.vIndex)
				{
					rec4.adjV.Add(index);
					this.vRec[index].adjV.Add(rec4.vIndex);
				}
			}
			this.ComputeEmbeddingError(rec4);
			this.ComputeMergeJointCost(rec4);
			queue.Update(rec4);
			foreach (int index2 in rec4.adjV)
			{
				this.ComputeMergeJointCost(this.vRec[index2]);
				queue.Update(this.vRec[index2]);
			}
			IL_24F:
			if (!queue.IsEmpty())
			{
				goto IL_82;
			}
			IL_25A:
			List<Skeletonizer.VertexRecord> resultList = new List<Skeletonizer.VertexRecord>(this.simplifiedVertexRec.Count);
			while (!queue.IsEmpty())
			{
				resultList.Add((Skeletonizer.VertexRecord)queue.DeleteMin());
			}
			this.simplifiedVertexRec = resultList;
		}
		private void ComputeMergeJointCost(Skeletonizer.VertexRecord rec)
		{
			rec.minError = 1.7976931348623157E+308;
			rec.minIndex = -1;
			if (rec.adjV.Count < 3)
			{
				return;
			}
			foreach (int index in rec.adjV)
			{
				Skeletonizer.VertexRecord rec2 = this.vRec[index];
				double new_err = this.ComputeEmbeddingError(rec2, rec);
				double minError = new_err / rec.err;
				if (minError < rec.minError)
				{
					rec.minError = minError;
					rec.minIndex = rec2.vIndex;
				}
			}
		}
		private void FindRootNode()
		{
			Skeletonizer.VertexRecord maxRec = null;
			double maxLen = -1.7976931348623157E+308;
			foreach (Skeletonizer.VertexRecord rec in this.simplifiedVertexRec)
			{
				double tot = this.collapsedLength[rec.vIndex];
				foreach (int index in rec.collapseFrom)
				{
					tot += this.collapsedLength[index];
				}
				double avg = tot / (double)(rec.collapseFrom.Count + 1);
				rec.nodeSize = avg;
				if (rec.adjV.Count > 2 && avg > maxLen)
				{
					maxLen = avg;
					maxRec = rec;
				}
			}
			this.rootNode = maxRec;
		}
		public void Segmentation(Skeletonizer.SegmentationOpt opt)
		{
			Program.PrintText("[Segmentation]");
			int vn = this.mesh.VertexCount;
			bool[] marked = new bool[vn];
			int[] segmentIndex = new int[vn];
			for (int i = 0; i < vn; i++)
			{
				marked[i] = false;
			}
			foreach (Skeletonizer.VertexRecord rec in this.simplifiedVertexRec)
			{
				rec.collapseFrom.Add(rec.vIndex);
				foreach (int index in rec.collapseFrom)
				{
					segmentIndex[index] = rec.vIndex;
				}
			}
			foreach (Skeletonizer.VertexRecord rec2 in this.simplifiedVertexRec)
			{
				rec2.boundaryLength = new Dictionary<int, double>(2);
				foreach (int adj in rec2.adjV)
				{
					Set<int> boundaryVertices = new Set<int>();
					foreach (int j in rec2.collapseFrom)
					{
						int[] array = this.mesh.AdjVV[j];
						for (int num = 0; num < array.Length; num++)
						{
							int k = array[num];
							if (segmentIndex[k] == adj)
							{
								marked[j] = true;
								boundaryVertices.Add(j);
								break;
							}
						}
					}
					double totLen = 0.0;
					foreach (int l in boundaryVertices)
					{
						Vector3d p = new Vector3d(this.originalVertexPos, l * 3);
						int[] array2 = this.mesh.AdjVV[l];
						for (int num2 = 0; num2 < array2.Length; num2++)
						{
							int m = array2[num2];
							if (marked[m])
							{
								Vector3d q = new Vector3d(this.originalVertexPos, m * 3);
								totLen += (p - q).Length();
							}
						}
					}
					foreach (int n in boundaryVertices)
					{
						marked[n] = false;
					}
					rec2.boundaryLength.Add(adj, totLen);
				}
			}
			foreach (Skeletonizer.VertexRecord rec3 in this.simplifiedVertexRec)
			{
				foreach (int adj2 in rec3.adjV)
				{
					if (adj2 >= rec3.vIndex)
					{
						Skeletonizer.VertexRecord rec4 = this.vRec[adj2];
						double len = rec3.boundaryLength[adj2];
						double len2 = rec4.boundaryLength[rec3.vIndex];
						double avgLen = (len + len2) / 2.0;
						rec3.boundaryLength[adj2] = avgLen;
						rec4.boundaryLength[rec3.vIndex] = avgLen;
					}
				}
			}
			List<Skeletonizer.CutRecord> cutRecords = new List<Skeletonizer.CutRecord>();
			foreach (Skeletonizer.VertexRecord rec5 in this.simplifiedVertexRec)
			{
				if (rec5.adjV.Count != 1)
				{
					foreach (int adj3 in rec5.adjV)
					{
						if (adj3 >= rec5.vIndex)
						{
							Skeletonizer.VertexRecord rec6 = this.vRec[adj3];
							if (rec6.adjV.Count != 1)
							{
								double len3 = rec5.boundaryLength[adj3];
								double w = (rec5.pos - rec6.pos).Length();
								double len4 = 0.0;
								double len5 = 0.0;
								foreach (int adj4 in rec5.adjV)
								{
									if (adj4 != adj3 && rec5.boundaryLength[adj4] > len4)
									{
										len4 = rec5.boundaryLength[adj4];
									}
								}
								foreach (int adj5 in rec6.adjV)
								{
									if (adj5 != rec5.vIndex && rec6.boundaryLength[adj5] > len5)
									{
										len5 = rec6.boundaryLength[adj5];
									}
								}
								double c = len3 - len4;
								double c2 = len3 - len5;
								double cost = (c + c2) / w;
								cost *= len3;
								cutRecords.Add(new Skeletonizer.CutRecord(rec5.vIndex, adj3, cost));
							}
						}
					}
				}
			}
			cutRecords.Sort();
			for (int i2 = 0; i2 < vn; i2++)
			{
				this.mesh.Flag[i2] = 0;
			}
			for (int i3 = 0; i3 < opt.SegmentCount - 1; i3++)
			{
				int from = cutRecords[0].from;
				int to = cutRecords[0].to;
				foreach (int j2 in this.vRec[from].collapseFrom)
				{
					int[] array = this.mesh.AdjVV[j2];
					for (int num = 0; num < array.Length; num++)
					{
						int k2 = array[num];
						if (segmentIndex[k2] == to)
						{
							this.mesh.Flag[j2] = (byte)(i3 + 1);
						}
					}
				}
				foreach (int j3 in this.vRec[to].collapseFrom)
				{
					int[] array = this.mesh.AdjVV[j3];
					for (int num = 0; num < array.Length; num++)
					{
						int k3 = array[num];
						if (segmentIndex[k3] == from)
						{
							this.mesh.Flag[j3] = (byte)(i3 + 1);
						}
					}
				}
				if (this.vRec[from].adjV.Count <= 3)
				{
					foreach (Skeletonizer.CutRecord rec7 in cutRecords)
					{
						if ((rec7.from == from || rec7.to == from) && this.vRec[rec7.from].adjV.Count < 3 && this.vRec[rec7.to].adjV.Count < 3)
						{
							rec7.cost = 1.7976931348623157E+308;
						}
					}
				}
				if (this.vRec[to].adjV.Count <= 3)
				{
					foreach (Skeletonizer.CutRecord rec8 in cutRecords)
					{
						if ((rec8.from == to || rec8.to == to) && this.vRec[rec8.from].adjV.Count < 3 && this.vRec[rec8.to].adjV.Count < 3)
						{
							rec8.cost = 1.7976931348623157E+308;
						}
					}
				}
				cutRecords[0].cost = 1.7976931348623157E+308;
				cutRecords.Sort();
			}
		}
		private void DrawSimplifiedVertices()
		{
			foreach (Skeletonizer.VertexRecord rec in this.simplifiedVertexRec)
			{
				if (this.rootNode != null)
				{
					OpenGL.glPointSize((float)(rec.nodeSize * 200.0));
				}
				else
				{
					OpenGL.glPointSize(this.skeletonNodeSize);
				}
				if (this.rootNode == rec)
				{
					OpenGL.glColor3f(1f, 0f, 1f);
				}
				else
				{
					OpenGL.glColor3f(1f, 0f, 0f);
				}
				OpenGL.glBegin(0u);
				OpenGL.glVertex3d(rec.pos.x, rec.pos.y, rec.pos.z);
				OpenGL.glEnd();
			}
			OpenGL.glColor3f(0f, 0f, 1f);
			OpenGL.glLineWidth(2f);
			OpenGL.glBegin(1u);
			foreach (Skeletonizer.VertexRecord rec2 in this.simplifiedVertexRec)
			{
				foreach (int adj in rec2.adjV)
				{
					Skeletonizer.VertexRecord rec3 = this.vRec[adj];
					OpenGL.glVertex3d(rec2.pos.x, rec2.pos.y, rec2.pos.z);
					OpenGL.glVertex3d(rec3.pos.x, rec3.pos.y, rec3.pos.z);
				}
			}
			OpenGL.glEnd();
		}
		private void DrawNodeSphere()
		{
			OpenGL.glEnable(2896u);
			OpenGL.glEnable(2977u);
			OpenGL.glPolygonMode(1032u, 6913u);
			OpenGL.glLineWidth(0.5f);
			GLUquadric quad = GLU.gluNewQuadric();
			foreach (Skeletonizer.VertexRecord rec in this.simplifiedVertexRec)
			{
				OpenGL.glColor3f(0.3f + (float)rec.err, 0.3f, 0.3f);
				OpenGL.glTranslated(rec.pos.x, rec.pos.y, rec.pos.z);
				GLU.gluSphere(quad, rec.radius, 8, 8);
				OpenGL.glTranslated(-rec.pos.x, -rec.pos.y, -rec.pos.z);
			}
			GLU.gluDeleteQuadric(quad);
			OpenGL.glDisable(2896u);
		}
		private void DrawRootNode()
		{
		}
		private void AddNoise()
		{
			Random r = new Random(this.mesh.VertexCount);
			int i = 0;
			int j = 0;
			while (i < this.mesh.VertexCount)
			{
				Vector3d v = new Vector3d(this.mesh.VertexNormal, j) * this.opt.NoiseRatio;
				this.mesh.VertexPos[j] += (r.NextDouble() - 0.5) * v.x;
				this.mesh.VertexPos[j + 1] += (r.NextDouble() - 0.5) * v.y;
				this.mesh.VertexPos[j + 2] += (r.NextDouble() - 0.5) * v.z;
				i++;
				j += 3;
			}
			this.mesh.ComputeFaceNormal();
			this.mesh.ComputeVertexNormal();
		}
		private void DrawIntermediateMesh()
		{
			object obj;
			Monitor.Enter(obj = this.myDisplayLock);
			try
			{
				Skeletonizer.VertexRecord[] array = this.vRec;
				for (int i = 0; i < array.Length; i++)
				{
					Skeletonizer.VertexRecord rec = array[i];
					if (rec.collapseFrom != null)
					{
						OpenGL.glPointSize(4f);
						OpenGL.glColor3f(1f, 0f, 0f);
						OpenGL.glBegin(0u);
						OpenGL.glVertex3d(rec.pos.x, rec.pos.y, rec.pos.z);
						OpenGL.glEnd();
					}
				}
				OpenGL.glColor3f(0f, 0f, 1f);
				OpenGL.glLineWidth(1f);
				OpenGL.glBegin(1u);
				Skeletonizer.VertexRecord[] array2 = this.vRec;
				for (int j = 0; j < array2.Length; j++)
				{
					Skeletonizer.VertexRecord rec2 = array2[j];
					if (rec2.collapseFrom != null)
					{
						foreach (int adj in rec2.adjV)
						{
							Skeletonizer.VertexRecord rec3 = this.vRec[adj];
							OpenGL.glVertex3d(rec2.pos.x, rec2.pos.y, rec2.pos.z);
							OpenGL.glVertex3d(rec3.pos.x, rec3.pos.y, rec3.pos.z);
						}
					}
				}
				OpenGL.glEnd();
			}
			finally
			{
				Monitor.Exit(obj);
			}
		}
        public unsafe void Dispose()
		{
			if (this.solver != null)
			{
				Skeletonizer.FreeSolver(this.solver);
				this.solver = null;
			}
			if (this.symbolicSolver != null)
			{
				Skeletonizer.FreeSymbolicSolver(this.symbolicSolver);
				this.symbolicSolver = null;
			}
		}
		public override string ToString()
		{
			return "";
		}
	}
}
