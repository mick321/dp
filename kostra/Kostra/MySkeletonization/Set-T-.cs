using System;
namespace System.Collections.Generic
{
	public class Set<T> : IEnumerable<T>, IEnumerable
	{
		private Dictionary<T, byte> items;
		public int Count
		{
			get
			{
				return this.items.Count;
			}
		}
		public Set()
		{
			this.items = new Dictionary<T, byte>();
		}
		public Set(int capacity)
		{
			this.items = new Dictionary<T, byte>(capacity);
		}
		public void Add(T element)
		{
			if (!this.items.ContainsKey(element))
			{
				this.items.Add(element, 0);
			}
		}
		public bool Remove(T element)
		{
			return this.items.Remove(element);
		}
		public bool Contains(T element)
		{
			return this.items.ContainsKey(element);
		}
		public void Clear()
		{
			this.items.Clear();
		}
		public T[] ToArray()
		{
			T[] arr = new T[this.items.Keys.Count];
			this.items.Keys.CopyTo(arr, 0);
			return arr;
		}
		public IEnumerator<T> GetEnumerator()
		{
			return this.items.Keys.GetEnumerator();
		}
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.items.Keys.GetEnumerator();
		}
	}
}
