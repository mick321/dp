using CsGL.OpenGL;
using MyGeometry;
using System;
using System.Collections.Generic;
namespace IsolineEditing
{
	public class IsolineDeformer : Deformer
	{
		private class FaceRecord : IComparable
		{
			public int index;
			public int e1;
			public int e2;
			public double ratio1;
			public double ratio2;
			public Vector3d n1;
			public Vector3d n2;
			public Vector3d iso;
			public bool flag;
			public bool start;
			public FaceRecord()
			{
				this.e1 = (this.e2 = -1);
			}
			public FaceRecord(int index)
			{
				this.index = index;
			}
			public int CompareTo(object obj)
			{
				IsolineDeformer.FaceRecord rec = obj as IsolineDeformer.FaceRecord;
				return this.index - rec.index;
			}
		}
		private class ColMatrixCreator
		{
			public int m;
			public int n;
			public List<int>[] rowIndex;
			public List<double>[] values;
			public int RowSize
			{
				get
				{
					return this.m;
				}
			}
			public int ColumnSize
			{
				get
				{
					return this.n;
				}
			}
			public ColMatrixCreator(int m, int n)
			{
				this.m = m;
				this.n = n;
				this.rowIndex = new List<int>[n];
				this.values = new List<double>[n];
				for (int i = 0; i < n; i++)
				{
					this.rowIndex[i] = new List<int>();
					this.values[i] = new List<double>();
				}
			}
			public void AddValueTo(int i, int j, double value)
			{
				List<int> r = this.rowIndex[j];
				if (i >= this.m)
				{
					this.m = i + 1;
				}
				int ri = -1;
				for (int k = 0; k < r.Count; k++)
				{
					if (r[k] == i)
					{
						ri = k;
						break;
					}
				}
				if (ri == -1)
				{
					r.Add(i);
					this.values[j].Add(value);
					return;
				}
				List<double> list;
				int index;
				(list = this.values[j])[index = ri] = list[index] + value;
			}
			public IsolineDeformer.ColMatrix ToColMatrix()
			{
				IsolineDeformer.ColMatrix C = new IsolineDeformer.ColMatrix(this.m, this.n);
				for (int i = 0; i < this.n; i++)
				{
					try
					{
						C.rowIndex[i] = this.rowIndex[i].ToArray();
					}
					catch (OutOfMemoryException)
					{
						GC.Collect();
						C.rowIndex[i] = this.rowIndex[i].ToArray();
					}
					this.rowIndex[i] = null;
					try
					{
						C.values[i] = this.values[i].ToArray();
					}
					catch (OutOfMemoryException)
					{
						GC.Collect();
						C.values[i] = this.values[i].ToArray();
					}
					this.values[i] = null;
				}
				return C;
			}
		}
		private class ColMatrix
		{
			public int m;
			public int n;
			public int[][] rowIndex;
			public double[][] values;
			public int RowSize
			{
				get
				{
					return this.m;
				}
			}
			public int ColumnSize
			{
				get
				{
					return this.n;
				}
			}
			public ColMatrix(int m, int n)
			{
				this.m = m;
				this.n = n;
				this.rowIndex = new int[n][];
				this.values = new double[n][];
				for (int i = 0; i < n; i++)
				{
					this.rowIndex[i] = null;
					this.values[i] = null;
				}
			}
			public int NumOfElements()
			{
				int c = 0;
				int[][] array = this.rowIndex;
				for (int i = 0; i < array.Length; i++)
				{
					int[] r = array[i];
					c += r.Length;
				}
				return c;
			}
			public void Multiply(double[] xIn, double[] xOut)
			{
				if (xIn.Length < this.n || xOut.Length < this.m)
				{
					throw new ArgumentException();
				}
				for (int i = 0; i < this.m; i++)
				{
					xOut[i] = 0.0;
				}
				for (int j = 0; j < this.n; j++)
				{
					int[] r = this.rowIndex[j];
					double[] v = this.values[j];
					for (int k = 0; k < r.Length; k++)
					{
						int ri = r[k];
						xOut[ri] += v[k] * xIn[j];
					}
				}
			}
			public void PreMultiply(double[] xIn, double[] xOut)
			{
				if (xIn.Length < this.m || xOut.Length < this.n)
				{
					throw new ArgumentException();
				}
				for (int i = 0; i < this.n; i++)
				{
					xOut[i] = 0.0;
				}
				for (int j = 0; j < this.n; j++)
				{
					double sum = 0.0;
					int[] r = this.rowIndex[j];
					double[] v = this.values[j];
					for (int k = 0; k < r.Length; k++)
					{
						int ri = r[k];
						sum += v[k] * xIn[ri];
					}
					xOut[j] = sum;
				}
			}
			public void PreMultiply(double[] xIn, double[] xOut, int[] index)
			{
				if (xIn.Length < this.m || xOut.Length < this.n)
				{
					throw new ArgumentException();
				}
				for (int i = 0; i < this.n; i++)
				{
					xOut[i] = 0.0;
				}
				for (int l = 0; l < index.Length; l++)
				{
					int j = index[l];
					double sum = 0.0;
					int[] r = this.rowIndex[j];
					double[] v = this.values[j];
					for (int k = 0; k < r.Length; k++)
					{
						int ri = r[k];
						sum += v[k] * xIn[ri];
					}
					xOut[j] = sum;
				}
			}
			public void PreMultiplyOffset(double[] xIn, double[] xOut, int startOut, int offsetOut)
			{
				for (int i = startOut; i < this.n + offsetOut; i += offsetOut)
				{
					xOut[i] = 0.0;
				}
				int j = 0;
				int k = startOut;
				while (j < this.n)
				{
					double sum = 0.0;
					int[] r = this.rowIndex[j];
					double[] v = this.values[j];
					for (int l = 0; l < r.Length; l++)
					{
						int ri = r[l];
						sum += v[l] * xIn[ri];
					}
					xOut[k] = sum;
					j++;
					k += offsetOut;
				}
			}
			public IsolineDeformer.ColMatrix Transpose()
			{
				IsolineDeformer.ColMatrixCreator C = new IsolineDeformer.ColMatrixCreator(this.n, this.m);
				for (int i = 0; i < this.n; i++)
				{
					int[] r = this.rowIndex[i];
					double[] v = this.values[i];
					for (int j = 0; j < r.Length; j++)
					{
						try
						{
							C.rowIndex[r[j]].Add(i);
						}
						catch (OutOfMemoryException e)
						{
							Program.PrintText("Exception raised " + e.Message + "\n");
							GC.Collect();
							C.rowIndex[r[j]].Add(i);
						}
						try
						{
							C.values[r[j]].Add(v[j]);
						}
						catch (OutOfMemoryException e2)
						{
							Program.PrintText("Exception raised " + e2.Message + "\n");
							GC.Collect();
							C.values[r[j]].Add(v[j]);
						}
					}
				}
				return C;
			}
			public IsolineDeformer.ColMatrix Transpose_Destroy()
			{
				IsolineDeformer.ColMatrixCreator C = new IsolineDeformer.ColMatrixCreator(this.n, this.m);
				for (int i = 0; i < this.n; i++)
				{
					int[] r = this.rowIndex[i];
					double[] v = this.values[i];
					for (int j = 0; j < r.Length; j++)
					{
						try
						{
							C.rowIndex[r[j]].Add(i);
						}
						catch (OutOfMemoryException e)
						{
							Program.PrintText("Exception raised " + e.Message + "\n");
							GC.Collect();
							C.rowIndex[r[j]].Add(i);
						}
						try
						{
							C.values[r[j]].Add(v[j]);
						}
						catch (OutOfMemoryException e2)
						{
							Program.PrintText("Exception raised " + e2.Message + "\n");
							GC.Collect();
							C.values[r[j]].Add(v[j]);
						}
					}
					this.rowIndex[i] = null;
					this.values[i] = null;
				}
				return C;
			}
			public static implicit operator IsolineDeformer.ColMatrix(IsolineDeformer.ColMatrixCreator CC)
			{
				return CC.ToColMatrix();
			}
		}
		public enum EnumDisplayMode
		{
			None,
			Isolines,
			IsolineVertices
		}
		private Mesh mesh;
		private double[][] hf;
		private double[] isovalues;
		private int regionNum = 15;
		private IsolineDeformer.EnumDisplayMode displayMode;
		private bool filterIsolineFaces = true;
		private IsolineDeformer.FaceRecord[][][] fullFaceRec;
		private IsolineDeformer.FaceRecord[][][] faceRec;
		private int samplesPerIsoline = 10;
		private int isolineDisplayIndex;
		private IsolineDeformer.ColMatrix colMT;
		private int[][][] isolineVertices;
		private int[][] handleVertices;
		private double[] oldVertexPos;
		private Vector3d[][] isolineCenter;
		public IsolineDeformer.EnumDisplayMode DisplayMode
		{
			get
			{
				return this.displayMode;
			}
			set
			{
				this.displayMode = value;
			}
		}
		public int IsolineDisplayIndex
		{
			get
			{
				return this.isolineDisplayIndex;
			}
			set
			{
				this.isolineDisplayIndex = value;
				if (this.isolineDisplayIndex < 0)
				{
					this.isolineDisplayIndex = 0;
				}
				if (this.isolineDisplayIndex >= this.hf.Length)
				{
					this.isolineDisplayIndex = this.hf.Length - 1;
				}
			}
		}
		public IsolineDeformer(Mesh mesh)
		{
			this.mesh = mesh;
			this.oldVertexPos = (double[])mesh.VertexPos.Clone();
			Program.PrintText("Harmonic fields ... \n");
			MultigridHarmonicFieldSolver hfSolver = new MultigridHarmonicFieldSolver(mesh, 0.25, 10000, this.regionNum);
			this.hf = hfSolver.HarmonicFields;
			this.isovalues = this.GetIsovalues(0.0, (double)this.regionNum, this.regionNum);
			GC.Collect();
			Program.PrintText("isoline faces ... \n");
			this.fullFaceRec = (this.faceRec = this.LocateIsolines(this.hf, this.isovalues));
			if (this.filterIsolineFaces)
			{
				this.faceRec = this.FilterFaceRecords(this.fullFaceRec);
			}
			GC.Collect();
			Program.PrintText("isoline vertices ... \n");
			this.isolineVertices = this.FindIsolineVertices();
			this.handleVertices = this.FindHandleVertices();
			GC.Collect();
			Program.PrintText("matrix M ... \n");
			this.colMT = this.BuildMatrixMT();
			GC.Collect();
		}
		private double[] GetIsovalues(double min, double max, int steps)
		{
			double[] isovalues = new double[steps - 1];
			double s = (max - min) / (double)steps;
			for (int i = 0; i < steps - 1; i++)
			{
				isovalues[i] = s * (double)(i + 1);
			}
			return isovalues;
		}
		private IsolineDeformer.FaceRecord[][][] LocateIsolines(double[][] hf, double[] isovalue)
		{
			IsolineDeformer.FaceRecord[][][] rec = new IsolineDeformer.FaceRecord[hf.Length][][];
			for (int i = 0; i < hf.Length; i++)
			{
				double[] f = hf[i];
				rec[i] = new IsolineDeformer.FaceRecord[isovalue.Length][];
				for (int j = 0; j < isovalue.Length; j++)
				{
					double v = isovalue[j];
					List<IsolineDeformer.FaceRecord> list = new List<IsolineDeformer.FaceRecord>();
					int k = 0;
					int l = 0;
					while (k < this.mesh.FaceCount)
					{
						int c = this.mesh.FaceIndex[l];
						int c2 = this.mesh.FaceIndex[l + 1];
						int c3 = this.mesh.FaceIndex[l + 2];
						IsolineDeformer.FaceRecord r = null;
						if ((f[c] <= v && f[c2] >= v) || (f[c2] <= v && f[c] >= v))
						{
							if (r == null)
							{
								r = new IsolineDeformer.FaceRecord();
								r.index = k;
							}
							if (r.e1 == -1)
							{
								r.e1 = 0;
								r.ratio1 = (v - f[c]) / (f[c2] - f[c]);
							}
							else
							{
								r.e2 = 0;
								r.ratio2 = (v - f[c]) / (f[c2] - f[c]);
							}
						}
						if ((f[c2] <= v && f[c3] >= v) || (f[c3] <= v && f[c2] >= v))
						{
							if (r == null)
							{
								r = new IsolineDeformer.FaceRecord();
								r.index = k;
							}
							if (r.e1 == -1)
							{
								r.e1 = 1;
								r.ratio1 = (v - f[c2]) / (f[c3] - f[c2]);
							}
							else
							{
								r.e2 = 1;
								r.ratio2 = (v - f[c2]) / (f[c3] - f[c2]);
							}
						}
						if ((f[c3] <= v && f[c] >= v) || (f[c] <= v && f[c3] >= v))
						{
							if (r == null)
							{
								r = new IsolineDeformer.FaceRecord();
								r.index = k;
							}
							if (r.e1 == -1)
							{
								r.e1 = 2;
								r.ratio1 = (v - f[c3]) / (f[c] - f[c3]);
							}
							else
							{
								r.e2 = 2;
								r.ratio2 = (v - f[c3]) / (f[c] - f[c3]);
							}
						}
						if (r != null && r.e1 != -1 && r.e2 != -1)
						{
							Vector3d v2 = new Vector3d(this.mesh.VertexPos, c * 3);
							Vector3d v3 = new Vector3d(this.mesh.VertexPos, c2 * 3);
							Vector3d v4 = new Vector3d(this.mesh.VertexPos, c3 * 3);
							Vector3d n = (v2 - v3).Cross(v4 - v3).Normalize();
							Vector3d p = default(Vector3d);
							Vector3d q = default(Vector3d);
							switch (r.e1)
							{
							case 0:
								p = v3 * r.ratio1 + v2 * (1.0 - r.ratio1);
								break;
							case 1:
								p = v4 * r.ratio1 + v3 * (1.0 - r.ratio1);
								break;
							case 2:
								p = v2 * r.ratio1 + v4 * (1.0 - r.ratio1);
								break;
							}
							switch (r.e2)
							{
							case 0:
								q = v3 * r.ratio2 + v2 * (1.0 - r.ratio2);
								break;
							case 1:
								q = v4 * r.ratio2 + v3 * (1.0 - r.ratio2);
								break;
							case 2:
								q = v2 * r.ratio2 + v4 * (1.0 - r.ratio2);
								break;
							}
							r.n1 = n;
							r.iso = q - p;
							r.n2 = n.Cross(r.iso).Normalize();
							list.Add(r);
						}
						k++;
						l += 3;
					}
					rec[i][j] = list.ToArray();
				}
			}
			return rec;
		}
		private IsolineDeformer.FaceRecord[][][] FilterFaceRecords(IsolineDeformer.FaceRecord[][][] records)
		{
			IsolineDeformer.FaceRecord[][][] filteredRec = new IsolineDeformer.FaceRecord[records.Length][][];
			for (int i = 0; i < records.Length; i++)
			{
				filteredRec[i] = new IsolineDeformer.FaceRecord[records[i].Length][];
				for (int j = 0; j < records[i].Length; j++)
				{
					IsolineDeformer.FaceRecord[] list = records[i][j];
					if (list.Length <= this.samplesPerIsoline * 2)
					{
						filteredRec[i][j] = list;
					}
					else
					{
						List<IsolineDeformer.FaceRecord> outList = new List<IsolineDeformer.FaceRecord>();
						int step = list.Length / this.samplesPerIsoline;
						int nRings = 0;
						Array.Sort<IsolineDeformer.FaceRecord>(list);
						for (int k = 0; k < list.Length; k++)
						{
							IsolineDeformer.FaceRecord rec = list[k];
							if (!rec.flag)
							{
								IsolineDeformer.FaceRecord curr = rec;
								curr.start = true;
								int count = 0;
								nRings++;
								bool found;
								do
								{
									curr.flag = true;
									if (count == 0)
									{
										outList.Add(curr);
										count = step;
									}
									count--;
									found = false;
									int adj = this.mesh.AdjFF[curr.index][curr.e1];
									int nextIndex = Array.BinarySearch<IsolineDeformer.FaceRecord>(list, new IsolineDeformer.FaceRecord(adj));
									if (nextIndex >= 0 && !list[nextIndex].flag)
									{
										curr = list[nextIndex];
										found = true;
									}
									if (!found)
									{
										int adj2 = this.mesh.AdjFF[curr.index][curr.e2];
										int nextIndex2 = Array.BinarySearch<IsolineDeformer.FaceRecord>(list, new IsolineDeformer.FaceRecord(adj2));
										if (nextIndex2 >= 0 && !list[nextIndex2].flag)
										{
											curr = list[nextIndex2];
											found = true;
										}
									}
								}
								while (found);
							}
						}
						if (nRings > 1)
						{
							foreach (IsolineDeformer.FaceRecord rec2 in outList)
							{
								rec2.flag = false;
							}
						}
						filteredRec[i][j] = outList.ToArray();
					}
				}
			}
			return filteredRec;
		}
		private IsolineDeformer.ColMatrix BuildMatrixMT()
		{
			int cn = (this.regionNum + 1) * 4;
			int vn = this.mesh.VertexCount;
			int tn = this.hf.Length * cn;
			IsolineDeformer.ColMatrixCreator C = new IsolineDeformer.ColMatrixCreator(tn, vn);
			Set<int> tmpIndex = new Set<int>();
			double[] tmp = new double[tn];
			int i = 0;
			int j = 0;
			while (i < vn)
			{
				Vector3d u = new Vector3d(this.mesh.VertexPos, j);
				for (int k = 0; k < this.hf.Length; k++)
				{
					double hv = this.hf[k][i];
					if (hv < 0.0)
					{
						hv = 0.0;
					}
					if (hv > (double)this.regionNum)
					{
						hv = (double)this.regionNum;
					}
					int tranIndex = (int)Math.Floor(hv);
					if (tranIndex < 0)
					{
						tranIndex = 0;
					}
					Vector3d v = u;
					int indexBase = k * cn + tranIndex * 4;
					double ratio2 = hv - Math.Floor(hv);
					double ratio3 = 1.0 - ratio2;
					double weight = this.hf[k][i] / (double)this.regionNum;
					ratio3 *= weight;
					ratio2 *= weight;
					if (tranIndex < this.regionNum)
					{
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v.x * ratio3;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v.y * ratio3;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v.z * ratio3;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += ratio3;
						indexBase++;
						Vector3d v2 = u;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v2.x * ratio2;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v2.y * ratio2;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v2.z * ratio2;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += ratio2;
					}
					else
					{
						ratio3 = weight;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v.x * ratio3;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v.y * ratio3;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v.z * ratio3;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += ratio3;
						indexBase++;
					}
				}
				int[] t = tmpIndex.ToArray();
				Array.Sort<int>(t);
				int[] array = t;
				for (int m = 0; m < array.Length; m++)
				{
					int l = array[m];
					if (tmp[l] != 0.0)
					{
						C.rowIndex[i].Add(l);
						C.values[i].Add(tmp[l]);
						tmp[l] = 0.0;
					}
				}
				tmpIndex.Clear();
				i++;
				j += 3;
			}
			return C;
		}
		private IsolineDeformer.ColMatrix BuildMatrixMT2()
		{
			int cn = (this.regionNum + 1) * 4;
			int vn = this.mesh.VertexCount;
			int tn = this.hf.Length * cn;
			IsolineDeformer.ColMatrixCreator C = new IsolineDeformer.ColMatrixCreator(tn, vn);
			Set<int> tmpIndex = new Set<int>();
			double[] tmp = new double[tn];
			int i = 0;
			int j = 0;
			while (i < vn)
			{
				Vector3d u = new Vector3d(this.mesh.VertexPos, j);
				for (int k = 0; k < this.hf.Length; k++)
				{
					double hv = this.hf[k][i];
					if (hv < 0.0)
					{
						hv = 0.0;
					}
					if (hv > (double)this.regionNum)
					{
						hv = (double)this.regionNum;
					}
					int tranIndex = (int)Math.Floor(hv);
					if (tranIndex < 0)
					{
						tranIndex = 0;
					}
					Vector3d v = u - this.isolineCenter[k][tranIndex];
					int indexBase = k * cn + tranIndex * 4;
					double ratio2 = 0.5;
					double ratio3 = 0.5;
					double weight = this.hf[k][i] / (double)this.regionNum;
					ratio3 *= weight;
					ratio2 *= weight;
					if (tranIndex < this.regionNum)
					{
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v.x * ratio3;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v.y * ratio3;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v.z * ratio3;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += ratio3;
						indexBase++;
						Vector3d v2 = u - this.isolineCenter[k][tranIndex + 1];
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v2.x * ratio2;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v2.y * ratio2;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v2.z * ratio2;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += ratio2;
					}
					else
					{
						ratio3 = weight;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v.x * ratio3;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v.y * ratio3;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += v.z * ratio3;
						indexBase++;
						tmpIndex.Add(indexBase);
						tmp[indexBase] += ratio3;
						indexBase++;
					}
				}
				int[] t = tmpIndex.ToArray();
				Array.Sort<int>(t);
				int[] array = t;
				for (int m = 0; m < array.Length; m++)
				{
					int l = array[m];
					if (tmp[l] != 0.0)
					{
						C.rowIndex[i].Add(l);
						C.values[i].Add(tmp[l]);
						tmp[l] = 0.0;
					}
				}
				tmpIndex.Clear();
				i++;
				j += 3;
			}
			return C;
		}
		private IsolineDeformer.ColMatrix BuildMatrixMT_2handle()
		{
			int cn = (this.regionNum + 1) * 4;
			int vn = this.mesh.VertexCount;
			int tn = cn;
			IsolineDeformer.ColMatrixCreator C = new IsolineDeformer.ColMatrixCreator(tn, vn);
			Set<int> tmpIndex = new Set<int>();
			double[] tmp = new double[tn];
			int i = 0;
			int j = 0;
			while (i < vn)
			{
				Vector3d u = new Vector3d(this.mesh.VertexPos, j);
				int k = 0;
				double hv = this.hf[k][i];
				if (hv < 0.0)
				{
					hv = 0.0;
				}
				if (hv > (double)this.regionNum)
				{
					hv = (double)this.regionNum;
				}
				int tranIndex = (int)Math.Floor(hv);
				if (tranIndex < 0)
				{
					tranIndex = 0;
				}
				int indexBase = k * cn + tranIndex * 4;
				double ratio2 = hv - Math.Floor(hv);
				double ratio3 = 1.0 - ratio2;
				double arg_D7_0 = this.hf[k][i] / (double)this.regionNum;
				tmpIndex.Add(indexBase);
				tmp[indexBase] += u.x * ratio3;
				indexBase++;
				tmpIndex.Add(indexBase);
				tmp[indexBase] += u.y * ratio3;
				indexBase++;
				tmpIndex.Add(indexBase);
				tmp[indexBase] += u.z * ratio3;
				indexBase++;
				tmpIndex.Add(indexBase);
				tmp[indexBase] += ratio3;
				indexBase++;
				if (tranIndex < this.regionNum)
				{
					tmpIndex.Add(indexBase);
					tmp[indexBase] += u.x * ratio2;
					indexBase++;
					tmpIndex.Add(indexBase);
					tmp[indexBase] += u.y * ratio2;
					indexBase++;
					tmpIndex.Add(indexBase);
					tmp[indexBase] += u.z * ratio2;
					indexBase++;
					tmpIndex.Add(indexBase);
					tmp[indexBase] += ratio2;
				}
				int[] t = tmpIndex.ToArray();
				Array.Sort<int>(t);
				int[] array = t;
				for (int m = 0; m < array.Length; m++)
				{
					int l = array[m];
					if (tmp[l] != 0.0)
					{
						C.rowIndex[i].Add(l);
						C.values[i].Add(tmp[l]);
						tmp[l] = 0.0;
					}
				}
				tmpIndex.Clear();
				i++;
				j += 3;
			}
			return C;
		}
		private int[][][] FindIsolineVertices()
		{
			int[][][] isolineVertices = new int[this.hf.Length][][];
			this.isolineCenter = new Vector3d[this.hf.Length][];
			for (int i = 0; i < this.hf.Length; i++)
			{
				isolineVertices[i] = new int[this.regionNum + 1][];
				this.isolineCenter[i] = new Vector3d[this.regionNum + 1];
				List<int> vlist0 = new List<int>();
				List<int> vlist = new List<int>();
				for (int j = 0; j < this.mesh.VertexCount; j++)
				{
					if ((int)this.mesh.Flag[j] == i + 1)
					{
						int[] array = this.mesh.AdjVV[j];
						for (int num = 0; num < array.Length; num++)
						{
							int k = array[num];
							if ((int)this.mesh.Flag[k] != i + 1)
							{
								vlist.Add(j);
								this.isolineCenter[i][this.regionNum] += new Vector3d(this.mesh.VertexPos, j * 3);
								break;
							}
						}
					}
					else
					{
						if (this.mesh.Flag[j] != 0)
						{
							int[] array2 = this.mesh.AdjVV[j];
							for (int num2 = 0; num2 < array2.Length; num2++)
							{
								int l = array2[num2];
								if (this.mesh.Flag[l] != this.mesh.Flag[j])
								{
									vlist0.Add(j);
									this.isolineCenter[i][0] += new Vector3d(this.mesh.VertexPos, j * 3);
									break;
								}
							}
						}
					}
				}
				isolineVertices[i][0] = vlist0.ToArray();
				isolineVertices[i][this.regionNum] = vlist.ToArray();
				this.isolineCenter[i][0] /= (double)isolineVertices[i][0].Length;
				this.isolineCenter[i][this.regionNum] /= (double)isolineVertices[i][this.regionNum].Length;
				for (int m = 0; m < this.isovalues.Length; m++)
				{
					double[] f = this.hf[i];
					double v = this.isovalues[m];
					Set<int> vSet = new Set<int>();
					for (int n = 0; n < this.mesh.VertexCount; n++)
					{
						int[] array3 = this.mesh.AdjVV[n];
						for (int num3 = 0; num3 < array3.Length; num3++)
						{
							int k2 = array3[num3];
							if (f[n] <= v && f[k2] >= v)
							{
								vSet.Add(n);
								vSet.Add(k2);
							}
						}
					}
					isolineVertices[i][m + 1] = vSet.ToArray();
					foreach (int k3 in vSet)
					{
						this.isolineCenter[i][m + 1] += new Vector3d(this.mesh.VertexPos, k3 * 3);
					}
					this.isolineCenter[i][m + 1] /= (double)vSet.Count;
				}
			}
			return isolineVertices;
		}
		private int[][] FindHandleVertices()
		{
			Set<int>[] handleVertexSet = new Set<int>[this.hf.Length];
			for (int i = 0; i < this.hf.Length; i++)
			{
				handleVertexSet[i] = new Set<int>();
			}
			for (int j = 0; j < this.mesh.VertexCount; j++)
			{
				if (this.mesh.Flag[j] != 0)
				{
					handleVertexSet[(int)(this.mesh.Flag[j] - 1)].Add(j);
				}
			}
			int[][] handleVertices = new int[this.hf.Length][];
			for (int k = 0; k < this.hf.Length; k++)
			{
				handleVertices[k] = handleVertexSet[k].ToArray();
			}
			return handleVertices;
		}
		private Vector3d[][] FindIsolineCenter()
		{
			Vector3d[][] isolineCenter = new Vector3d[this.isolineVertices.Length][];
			for (int i = 0; i < isolineCenter.Length; i++)
			{
				isolineCenter[i] = new Vector3d[this.isolineVertices[i].Length];
				for (int j = 0; j < this.isolineVertices[i].Length; j++)
				{
					isolineCenter[i][j] = default(Vector3d);
					for (int k = 0; k < this.isolineVertices[i][j].Length; k++)
					{
						isolineCenter[i][j] += new Vector3d(this.mesh.VertexPos, this.isolineVertices[i][j][k] * 3);
					}
					isolineCenter[i][j] /= (double)this.isolineVertices[i][j].Length;
				}
			}
			return isolineCenter;
		}
		private void ComputeLocalTransformation()
		{
			this.FindIsolineCenter();
			double[][] tx = new double[3][];
			Matrix3d[][] rot = new Matrix3d[this.hf.Length][];
			Vector3d[][] tran = new Vector3d[this.hf.Length][];
			int stepSize = 1;
			for (int i = 0; i < 3; i++)
			{
				tx[i] = new double[this.hf.Length * (this.regionNum + 1) * 4];
			}
			int txIndex = 0;
			for (int j = 0; j < this.hf.Length; j++)
			{
				rot[j] = new Matrix3d[this.isolineVertices[j].Length];
				tran[j] = new Vector3d[this.isolineVertices[j].Length];
				for (int k = 0; k < this.isolineVertices[j].Length; k++)
				{
					Matrix4d UUT = new Matrix4d();
					Matrix4d VUT = new Matrix4d();
					for (int l = k - stepSize; l <= k + stepSize; l++)
					{
						if ((k != 0 || l == k) && (k != this.isolineVertices[j].Length - 1 || l == k) && l >= 0 && l < this.isolineVertices[j].Length)
						{
							int[] array = this.isolineVertices[j][l];
							for (int num = 0; num < array.Length; num++)
							{
								int index = array[num];
								int c = index * 3;
								Vector4d u = new Vector4d(this.oldVertexPos, c);
								u.w = 1.0;
								Vector4d v = new Vector4d(this.mesh.VertexPos, c);
								v.w = 1.0;
								if (k == l)
								{
									UUT += u.OuterCross(u);
									VUT += v.OuterCross(u);
								}
								else
								{
									UUT += u.OuterCross(u) * 1.0;
									VUT += v.OuterCross(u) * 1.0;
								}
							}
						}
					}
					Matrix4d T = VUT * UUT.Inverse();
					Matrix3d R = T.ToMatrix3d();
					Vector3d t = R * this.isolineCenter[j][k];
					R = R.OrthogonalFactor(0.001);
					rot[j][k] = R;
					tran[j][k] = t + new Vector3d(T[0, 3], T[1, 3], T[2, 3]);
				}
			}
			stepSize = 1;
			for (int m = 0; m < this.hf.Length; m++)
			{
				for (int n = 0; n < this.isolineVertices[m].Length; n++)
				{
					Matrix3d R2 = new Matrix3d();
					Vector3d t2 = default(Vector3d);
					int count = 0;
					for (int k2 = n - stepSize; k2 <= n + stepSize; k2++)
					{
						if ((n != 0 || k2 == n) && (n != this.isolineVertices[m].Length - 1 || k2 == n) && k2 >= 0 && k2 < this.isolineVertices[m].Length)
						{
							R2 += rot[m][k2];
							t2 += tran[m][k2] - rot[m][k2] * this.isolineCenter[m][k2];
							count++;
						}
					}
					R2 /= (double)count;
					t2 /= (double)count;
					tx[0][txIndex] = R2[0, 0];
					tx[0][txIndex + 1] = R2[0, 1];
					tx[0][txIndex + 2] = R2[0, 2];
					tx[0][txIndex + 3] = t2[0];
					tx[1][txIndex] = R2[1, 0];
					tx[1][txIndex + 1] = R2[1, 1];
					tx[1][txIndex + 2] = R2[1, 2];
					tx[1][txIndex + 3] = t2[1];
					tx[2][txIndex] = R2[2, 0];
					tx[2][txIndex + 1] = R2[2, 1];
					tx[2][txIndex + 2] = R2[2, 2];
					tx[2][txIndex + 3] = t2[2];
					txIndex += 4;
				}
			}
			double[][] x = new double[3][];
			for (int i2 = 0; i2 < 3; i2++)
			{
				x[i2] = new double[this.colMT.ColumnSize];
				this.colMT.PreMultiply(tx[i2], x[i2]);
			}
			int i3 = 0;
			int j2 = 0;
			while (i3 < this.mesh.VertexCount)
			{
				if (this.mesh.Flag[i3] == 0)
				{
					this.mesh.VertexPos[j2] = x[0][i3];
					this.mesh.VertexPos[j2 + 1] = x[1][i3];
					this.mesh.VertexPos[j2 + 2] = x[2][i3];
				}
				i3++;
				j2 += 3;
			}
		}
		private void ComputeLocalTransformation2()
		{
			double[][] tx = new double[3][];
			for (int i = 0; i < 3; i++)
			{
				tx[i] = new double[this.hf.Length * (this.regionNum + 1) * 4];
			}
			Matrix3d[][] rot = new Matrix3d[this.hf.Length][];
			Vector3d[][] tran = new Vector3d[this.hf.Length][];
			int[][] count = new int[this.hf.Length][];
			for (int j = 0; j < this.hf.Length; j++)
			{
				rot[j] = new Matrix3d[this.hf[j].Length];
				tran[j] = new Vector3d[this.hf[j].Length];
				count[j] = new int[this.hf[j].Length];
				for (int k = 0; k < this.isolineVertices[j].Length; k++)
				{
					rot[j][k] = new Matrix3d();
					tran[j][k] = default(Vector3d);
					count[j][k] = 0;
				}
				for (int l = 0; l < this.isolineVertices[j].Length; l++)
				{
					Matrix4d UUT = new Matrix4d();
					Matrix4d VUT = new Matrix4d();
					int stepSize = 1;
					for (int m = l - stepSize; m <= l + stepSize; m++)
					{
						if (m >= 0 && m < this.isolineVertices[j].Length)
						{
							int[] array = this.isolineVertices[j][m];
							for (int num = 0; num < array.Length; num++)
							{
								int index = array[num];
								int c = index * 3;
								Vector4d u = new Vector4d(this.oldVertexPos, c);
								u.w = 1.0;
								Vector4d v = new Vector4d(this.mesh.VertexPos, c);
								v.w = 1.0;
								UUT += u.OuterCross(u);
								VUT += v.OuterCross(u);
							}
						}
					}
					Matrix4d T = VUT * UUT.Inverse();
					Matrix3d R = T.ToMatrix3d();
					Vector3d t = R * this.isolineCenter[j][l] + new Vector3d(T[0, 3], T[1, 3], T[2, 3]);
					R = R.OrthogonalFactor(0.001);
					stepSize = 0;
					for (int n = l - stepSize; n <= l + stepSize; n++)
					{
						if (n >= 0 && n < this.isolineVertices[j].Length)
						{
							Matrix3d[] array2;
							IntPtr intPtr;
							(array2 = rot[j])[(int)(intPtr = (IntPtr)n)] = array2[(int)intPtr] + R;
							tran[j][n] += t;
							count[j][n]++;
						}
					}
				}
			}
			int txIndex = 0;
			for (int i2 = 0; i2 < this.hf.Length; i2++)
			{
				for (int j2 = 0; j2 < this.isolineVertices[i2].Length; j2++)
				{
					Matrix3d[] array3;
					IntPtr intPtr2;
					(array3 = rot[i2])[(int)(intPtr2 = (IntPtr)j2)] = array3[(int)intPtr2] / (double)count[i2][j2];
					tran[i2][j2] /= (double)count[i2][j2];
					tx[0][txIndex] = rot[i2][j2][0, 0];
					tx[0][txIndex + 1] = rot[i2][j2][0, 1];
					tx[0][txIndex + 2] = rot[i2][j2][0, 2];
					tx[0][txIndex + 3] = tran[i2][j2][0];
					tx[1][txIndex] = rot[i2][j2][1, 0];
					tx[1][txIndex + 1] = rot[i2][j2][1, 1];
					tx[1][txIndex + 2] = rot[i2][j2][1, 2];
					tx[1][txIndex + 3] = tran[i2][j2][1];
					tx[2][txIndex] = rot[i2][j2][2, 0];
					tx[2][txIndex + 1] = rot[i2][j2][2, 1];
					tx[2][txIndex + 2] = rot[i2][j2][2, 2];
					tx[2][txIndex + 3] = tran[i2][j2][2];
					txIndex += 4;
				}
			}
			double[][] x = new double[3][];
			for (int i3 = 0; i3 < 3; i3++)
			{
				x[i3] = new double[this.colMT.ColumnSize];
				this.colMT.PreMultiply(tx[i3], x[i3]);
			}
			int i4 = 0;
			int j3 = 0;
			while (i4 < this.mesh.VertexCount)
			{
				if (this.mesh.Flag[i4] == 0)
				{
					this.mesh.VertexPos[j3] = x[0][i4];
					this.mesh.VertexPos[j3 + 1] = x[1][i4];
					this.mesh.VertexPos[j3 + 2] = x[2][i4];
				}
				i4++;
				j3 += 3;
			}
		}
		private void DrawIsolines(int index)
		{
			OpenGL.glPushAttrib(8196u);
			OpenGL.glLineWidth(3f);
			OpenGL.glEnable(2848u);
			OpenGL.glEnable(2896u);
			OpenGL.glEnable(2977u);
			OpenGL.glPolygonMode(1032u, 6913u);
			OpenGL.glDisable(32823u);
			OpenGL.glEnable(2884u);
			OpenGL.glColor3f(1f, 0f, 0f);
			OpenGL.glBegin(1u);
			if (this.isolineDisplayIndex >= this.fullFaceRec[index].Length)
			{
				int arg_8B_0 = this.fullFaceRec[index].Length;
			}
			for (int i = this.fullFaceRec[index].Length - 1; i >= this.isolineDisplayIndex; i--)
			{
				for (int j = 0; j < this.fullFaceRec[index][i].Length; j++)
				{
					IsolineDeformer.FaceRecord rec = this.fullFaceRec[index][i][j];
					int f_index = rec.index * 3;
					int c = this.mesh.FaceIndex[f_index];
					int c2 = this.mesh.FaceIndex[f_index + 1];
					int c3 = this.mesh.FaceIndex[f_index + 2];
					Vector3d v = new Vector3d(this.mesh.VertexPos, c * 3);
					Vector3d v2 = new Vector3d(this.mesh.VertexPos, c2 * 3);
					Vector3d v3 = new Vector3d(this.mesh.VertexPos, c3 * 3);
					Vector3d normal = (v2 - v).Cross(v3 - v).Normalize();
					Vector3d p = default(Vector3d);
					Vector3d q = default(Vector3d);
					switch (rec.e1)
					{
					case 0:
						p = v2 * rec.ratio1 + v * (1.0 - rec.ratio1);
						break;
					case 1:
						p = v3 * rec.ratio1 + v2 * (1.0 - rec.ratio1);
						break;
					case 2:
						p = v * rec.ratio1 + v3 * (1.0 - rec.ratio1);
						break;
					}
					switch (rec.e2)
					{
					case 0:
						q = v2 * rec.ratio2 + v * (1.0 - rec.ratio2);
						break;
					case 1:
						q = v3 * rec.ratio2 + v2 * (1.0 - rec.ratio2);
						break;
					case 2:
						q = v * rec.ratio2 + v3 * (1.0 - rec.ratio2);
						break;
					}
					OpenGL.glNormal3d(normal.x, normal.y, normal.z);
					OpenGL.glVertex3d(p.x, p.y, p.z);
					OpenGL.glVertex3d(q.x, q.y, q.z);
				}
			}
			OpenGL.glEnd();
			OpenGL.glPopAttrib();
			OpenGL.glEnable(32823u);
			OpenGL.glEnable(2884u);
			OpenGL.glDisable(2896u);
			OpenGL.glDisable(2977u);
		}
		private unsafe void DrawIsolineVertices(int index)
		{
			int[][] isoVertex = this.isolineVertices[index];
			OpenGL.glPointSize(4f);
			OpenGL.glEnableClientState(32884u);
			fixed (double* vp = this.mesh.VertexPos)
			{
				OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
				OpenGL.glColor3f(1f, 0f, 0f);
				OpenGL.glBegin(0u);
				for (int i = 0; i < isoVertex.Length; i++)
				{
					for (int j = 0; j < isoVertex[i].Length; j++)
					{
						OpenGL.glArrayElement(isoVertex[i][j]);
					}
				}
				OpenGL.glEnd();
			}
			OpenGL.glDisableClientState(32884u);
		}
		void Deformer.Deform()
		{
			this.ComputeLocalTransformation();
		}
		void Deformer.Update()
		{
		}
		void Deformer.Display()
		{
			switch (this.displayMode)
			{
			case IsolineDeformer.EnumDisplayMode.Isolines:
				this.DrawIsolines(this.isolineDisplayIndex);
				return;
			case IsolineDeformer.EnumDisplayMode.IsolineVertices:
				this.DrawIsolineVertices(this.isolineDisplayIndex);
				return;
			default:
				return;
			}
		}
		void Deformer.Move()
		{
		}
		void Deformer.MouseDown()
		{
		}
		void Deformer.MouseUp()
		{
		}
	}
}
