using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
namespace MyGeometry
{
	public class Mesh
	{
		private int vertexCount;
		private int faceCount;
		private double[] vertexPos;
		private double[] vertexNormal;
		private int[] faceIndex;
		private double[] faceNormal;
		private double[] dualVertexPos;
		private byte[] flag;
		private bool[] isBoundary;
		private float[] color;
		private int[] singleVertexGroup;
		private int[][] adjVV;
		private int[][] adjVF;
		private int[][] adjFF;
		public int VertexCount
		{
			get
			{
				return this.vertexCount;
			}
		}
		public int FaceCount
		{
			get
			{
				return this.faceCount;
			}
		}
		public double[] VertexPos
		{
			get
			{
				return this.vertexPos;
			}
			set
			{
				if (value.Length < this.vertexCount * 3)
				{
					throw new Exception();
				}
				this.vertexPos = value;
			}
		}
		public double[] VertexNormal
		{
			get
			{
				return this.vertexNormal;
			}
		}
		public int[] FaceIndex
		{
			get
			{
				return this.faceIndex;
			}
		}
		public double[] FaceNormal
		{
			get
			{
				return this.faceNormal;
			}
		}
		public double[] DualVertexPos
		{
			get
			{
				return this.dualVertexPos;
			}
		}
		public byte[] Flag
		{
			get
			{
				return this.flag;
			}
			set
			{
				if (value.Length < this.vertexCount)
				{
					throw new Exception();
				}
				this.flag = value;
			}
		}
		public bool[] IsBoundary
		{
			get
			{
				return this.isBoundary;
			}
			set
			{
				this.isBoundary = value;
			}
		}
		public int[] SingleVertexGroup
		{
			get
			{
				return this.singleVertexGroup;
			}
		}
		public float[] Color
		{
			get
			{
				return this.color;
			}
			set
			{
				this.color = value;
			}
		}
		public int[][] AdjVV
		{
			get
			{
				return this.adjVV;
			}
			set
			{
				this.adjVV = value;
			}
		}
		public int[][] AdjVF
		{
			get
			{
				return this.adjVF;
			}
			set
			{
				this.adjVF = value;
			}
		}
		public int[][] AdjFF
		{
			get
			{
				return this.adjFF;
			}
			set
			{
				this.adjFF = value;
			}
		}
		public Mesh(StreamReader sr)
		{
			ArrayList vlist = new ArrayList();
			ArrayList flist = new ArrayList();
			char[] delimiters = new char[]
			{
				' ',
				'\t'
			};
			while (sr.Peek() > -1)
			{
				string s = sr.ReadLine();
				string[] tokens = s.Split(delimiters);
				string a;
				if ((a = tokens[0].ToLower()) != null)
				{
					if (!(a == "v"))
					{
						if (a == "f")
						{
							for (int i = 1; i < tokens.Length; i++)
							{
								if (!tokens[i].Equals(""))
								{
									string[] tokens2 = tokens[i].Split(new char[]
									{
										'/'
									});
									int index = int.Parse(tokens2[0]);
									if (index <= 0)
									{
										index = vlist.Count + index + 1;
									}
									flist.Add(index - 1);
								}
							}
						}
					}
					else
					{
						for (int j = 1; j < tokens.Length; j++)
						{
							if (!tokens[j].Equals(""))
							{
								vlist.Add(double.Parse(tokens[j]));
							}
						}
					}
				}
			}
			this.vertexCount = vlist.Count / 3;
			this.faceCount = flist.Count / 3;
			this.vertexPos = new double[this.vertexCount * 3];
			this.vertexNormal = new double[this.vertexCount * 3];
			this.faceIndex = new int[this.faceCount * 3];
			this.faceNormal = new double[this.faceCount * 3];
			this.flag = new byte[this.vertexCount];
			this.isBoundary = new bool[this.vertexCount];
			this.color = new float[this.faceCount * 3];
			for (int k = 0; k < vlist.Count; k++)
			{
				this.vertexPos[k] = (double)vlist[k];
			}
			for (int l = 0; l < flist.Count; l++)
			{
				this.faceIndex[l] = (int)flist[l];
			}
			//this.ScaleToUnitBox();
			//this.MoveToCenter();
			this.ComputeFaceNormal();
			this.ComputeVertexNormal();
			this.adjVV = this.BuildAdjacentMatrix().GetRowIndex();
			this.adjVF = this.BuildAdjacentMatrixFV().GetColumnIndex();
			this.adjFF = this.BuildAdjacentMatrixFF().GetRowIndex();
			this.FindBoundaryVertex();
			this.ComputeDualPosition();
			for (int m = 0; m < this.FaceCount; m++)
			{
				double area = this.ComputeFaceArea(m);
				if (double.IsNaN(area))
				{
					int arg_29A_0 = this.AdjFF[m].Length;
				}
			}
		}
		public Mesh(StreamReader sr, string type)
		{
			if (!type.Equals("cgal"))
			{
				throw new Exception();
			}
			char[] delimiters = new char[]
			{
				' ',
				'\t',
				'\n',
				'\r'
			};
			string[] tokens = sr.ReadToEnd().Split(delimiters);
			this.vertexCount = int.Parse(tokens[0]);
			this.faceCount = int.Parse(tokens[1]);
			this.vertexPos = new double[this.vertexCount * 3];
			this.faceIndex = new int[this.faceCount * 3];
			int i = 3;
			int j = 0;
			int k = 3;
			while (j < this.vertexCount - 1)
			{
				while (tokens[i].Equals(""))
				{
					i++;
				}
				this.vertexPos[k] = double.Parse(tokens[i++]);
				while (tokens[i].Equals(""))
				{
					i++;
				}
				this.vertexPos[k + 1] = double.Parse(tokens[i++]);
				this.vertexPos[k + 2] = 0.0;
				j++;
				k += 3;
			}
			int l = 0;
			int m = 0;
			while (l < this.faceCount)
			{
				while (tokens[i].Equals(""))
				{
					i++;
				}
				this.faceIndex[m] = int.Parse(tokens[i++]);
				while (tokens[i].Equals(""))
				{
					i++;
				}
				this.faceIndex[m + 1] = int.Parse(tokens[i++]);
				while (tokens[i].Equals(""))
				{
					i++;
				}
				this.faceIndex[m + 2] = int.Parse(tokens[i++]);
				l++;
				m += 3;
			}
			this.RemoveVertex(0);
			this.vertexNormal = new double[this.vertexCount * 3];
			this.faceNormal = new double[this.faceCount * 3];
			this.flag = new byte[this.vertexCount];
			this.isBoundary = new bool[this.vertexCount];
			this.ScaleToUnitBox();
			this.MoveToCenter();
			this.ComputeFaceNormal();
			this.ComputeVertexNormal();
			SparseMatrix adjMatrix = this.BuildAdjacentMatrix();
			SparseMatrix adjMatrixFV = this.BuildAdjacentMatrixFV();
			this.adjVV = adjMatrix.GetRowIndex();
			this.adjVF = adjMatrixFV.GetColumnIndex();
			this.FindBoundaryVertex();
			this.ComputeDualPosition();
		}
		public void Write(StreamWriter sw)
		{
			int i = 0;
			int j = 0;
			while (i < this.vertexCount)
			{
				sw.Write("v ");
				sw.Write(this.vertexPos[j].ToString() + " ");
				sw.Write(this.vertexPos[j + 1].ToString() + " ");
				sw.Write(this.vertexPos[j + 2].ToString());
				sw.WriteLine();
				i++;
				j += 3;
			}
			int k = 0;
			int l = 0;
			while (k < this.faceCount)
			{
				sw.Write("f ");
				sw.Write((this.faceIndex[l] + 1).ToString() + " ");
				sw.Write((this.faceIndex[l + 1] + 1).ToString() + " ");
				sw.Write((this.faceIndex[l + 2] + 1).ToString());
				sw.WriteLine();
				k++;
				l += 3;
			}
		}
		public void LoadSelectedVertexPositions(StreamReader sr)
		{
			new ArrayList();
			new ArrayList();
			char[] delimiters = new char[]
			{
				' ',
				'\t'
			};
			while (sr.Peek() > -1)
			{
				string s = sr.ReadLine();
				string[] tokens = s.Split(delimiters);
				int index = int.Parse(tokens[0]);
				double x = double.Parse(tokens[1]);
				double y = double.Parse(tokens[2]);
				double z = double.Parse(tokens[3]);
				if (index < this.vertexCount)
				{
					index *= 3;
					this.vertexPos[index++] = x;
					this.vertexPos[index++] = y;
					this.vertexPos[index] = z;
				}
			}
		}
		public void SaveSelectedVertexPositions(StreamWriter sw)
		{
			int i = 0;
			int j = 0;
			while (i < this.vertexCount)
			{
				if (this.flag[i] != 0)
				{
					sw.Write(i.ToString() + " ");
					sw.Write(this.vertexPos[j].ToString() + " ");
					sw.Write(this.vertexPos[j + 1].ToString() + " ");
					sw.Write(this.vertexPos[j + 2].ToString());
					sw.WriteLine();
				}
				i++;
				j += 3;
			}
		}
		public Vector3d MaxCoord()
		{
			Vector3d maxCoord = new Vector3d(-1.7976931348623157E+308, -1.7976931348623157E+308, -1.7976931348623157E+308);
			int i = 0;
			int j = 0;
			while (i < this.vertexCount)
			{
				Vector3d v = new Vector3d(this.vertexPos, j);
				maxCoord = Vector3d.Max(maxCoord, v);
				i++;
				j += 3;
			}
			return maxCoord;
		}
		public Vector3d MinCoord()
		{
			Vector3d minCoord = new Vector3d(1.7976931348623157E+308, 1.7976931348623157E+308, 1.7976931348623157E+308);
			int i = 0;
			int j = 0;
			while (i < this.vertexCount)
			{
				Vector3d v = new Vector3d(this.vertexPos, j);
				minCoord = Vector3d.Min(minCoord, v);
				i++;
				j += 3;
			}
			return minCoord;
		}
		public double Volume()
		{
			double totVolume = 0.0;
			int i = 0;
			int j = 0;
			while (i < this.faceCount)
			{
				int c = this.faceIndex[j] * 3;
				int c2 = this.faceIndex[j + 1] * 3;
				int c3 = this.faceIndex[j + 2] * 3;
				Vector3d a = new Vector3d(this.vertexPos, c);
				Vector3d b = new Vector3d(this.vertexPos, c2);
				Vector3d c4 = new Vector3d(this.vertexPos, c3);
				totVolume += a.x * b.y * c4.z - a.x * b.z * c4.y - a.y * b.x * c4.z + a.y * b.z * c4.x + a.z * b.x * c4.y - a.z * b.y * c4.x;
				i++;
				j += 3;
			}
			return totVolume;
		}
		public void MoveToCenter()
		{
			Vector3d center = (this.MaxCoord() + this.MinCoord()) / 2.0;
			int i = 0;
			int j = 0;
			while (i < this.vertexCount)
			{
				this.vertexPos[j] -= center.x;
				this.vertexPos[j + 1] -= center.y;
				this.vertexPos[j + 2] -= center.z;
				i++;
				j += 3;
			}
		}
		public void ScaleToUnitBox()
		{
			Vector3d d = this.MaxCoord() - this.MinCoord();
			double s = (d.x > d.y) ? d.x : d.y;
			s = ((s > d.z) ? s : d.z);
			if (s <= 0.0)
			{
				return;
			}
			for (int i = 0; i < this.vertexPos.Length; i++)
			{
				this.vertexPos[i] /= s;
			}
		}
		public void Transform(Matrix4d tran)
		{
			int i = 0;
			int j = 0;
			while (i < this.vertexCount)
			{
				Vector4d v = new Vector4d(this.vertexPos[j], this.vertexPos[j + 1], this.vertexPos[j + 2], 1.0);
				v = tran * v;
				this.vertexPos[j] = v.x;
				this.vertexPos[j + 1] = v.y;
				this.vertexPos[j + 2] = v.z;
				i++;
				j += 3;
			}
		}
		public void ComputeFaceNormal()
		{
			int i = 0;
			int j = 0;
			while (i < this.faceCount)
			{
				int c = this.faceIndex[j] * 3;
				int c2 = this.faceIndex[j + 1] * 3;
				int c3 = this.faceIndex[j + 2] * 3;
				Vector3d v = new Vector3d(this.vertexPos, c);
				Vector3d v2 = new Vector3d(this.vertexPos, c2);
				Vector3d v3 = new Vector3d(this.vertexPos, c3);
				Vector3d normal = (v2 - v).Cross(v3 - v).Normalize();
				this.faceNormal[j] = normal.x;
				this.faceNormal[j + 1] = normal.y;
				this.faceNormal[j + 2] = normal.z;
				i++;
				j += 3;
			}
		}
		public void ComputeVertexNormal()
		{
			Array.Clear(this.vertexNormal, 0, this.vertexNormal.Length);
			int i = 0;
			int j = 0;
			while (i < this.faceCount)
			{
				int c = this.faceIndex[j] * 3;
				int c2 = this.faceIndex[j + 1] * 3;
				int c3 = this.faceIndex[j + 2] * 3;
				this.vertexNormal[c] += this.faceNormal[j];
				this.vertexNormal[c2] += this.faceNormal[j];
				this.vertexNormal[c3] += this.faceNormal[j];
				this.vertexNormal[c + 1] += this.faceNormal[j + 1];
				this.vertexNormal[c2 + 1] += this.faceNormal[j + 1];
				this.vertexNormal[c3 + 1] += this.faceNormal[j + 1];
				this.vertexNormal[c + 2] += this.faceNormal[j + 2];
				this.vertexNormal[c2 + 2] += this.faceNormal[j + 2];
				this.vertexNormal[c3 + 2] += this.faceNormal[j + 2];
				i++;
				j += 3;
			}
			int k = 0;
			int l = 0;
			while (k < this.vertexCount)
			{
				Vector3d m = new Vector3d(this.vertexNormal, l);
				m = m.Normalize();
				this.vertexNormal[l] = m.x;
				this.vertexNormal[l + 1] = m.y;
				this.vertexNormal[l + 2] = m.z;
				k++;
				l += 3;
			}
		}
		public SparseMatrix BuildAdjacentMatrix()
		{
			SparseMatrix i = new SparseMatrix(this.vertexCount, this.vertexCount, 6);
			int j = 0;
			int k = 0;
			while (j < this.faceCount)
			{
				int c = this.faceIndex[k];
				int c2 = this.faceIndex[k + 1];
				int c3 = this.faceIndex[k + 2];
				i.AddElementIfNotExist(c, c2, 1.0);
				i.AddElementIfNotExist(c2, c3, 1.0);
				i.AddElementIfNotExist(c3, c, 1.0);
				i.AddElementIfNotExist(c2, c, 1.0);
				i.AddElementIfNotExist(c3, c2, 1.0);
				i.AddElementIfNotExist(c, c3, 1.0);
				j++;
				k += 3;
			}
			i.SortElement();
			return i;
		}
		public SparseMatrix BuildAdjacentMatrixFV()
		{
			SparseMatrix i = new SparseMatrix(this.faceCount, this.vertexCount, 6);
			int j = 0;
			int k = 0;
			while (j < this.faceCount)
			{
				i.AddElementIfNotExist(j, this.faceIndex[k], 1.0);
				i.AddElementIfNotExist(j, this.faceIndex[k + 1], 1.0);
				i.AddElementIfNotExist(j, this.faceIndex[k + 2], 1.0);
				j++;
				k += 3;
			}
			i.SortElement();
			return i;
		}
		public SparseMatrix BuildAdjacentMatrixFF()
		{
			SparseMatrix i = new SparseMatrix(this.faceCount, this.faceCount, 3);
			for (int j = 0; j < this.faceCount; j++)
			{
				int v = this.faceIndex[j * 3];
				int v2 = this.faceIndex[j * 3 + 1];
				int v3 = this.faceIndex[j * 3 + 2];
				int[] array = this.adjVF[v];
				for (int n = 0; n < array.Length; n++)
				{
					int k = array[n];
					if (k != j && this.IsContainVertex(k, v2))
					{
						i.AddElementIfNotExist(j, k, 1.0);
					}
				}
				int[] array2 = this.adjVF[v2];
				for (int num = 0; num < array2.Length; num++)
				{
					int l = array2[num];
					if (l != j && this.IsContainVertex(l, v3))
					{
						i.AddElementIfNotExist(j, l, 1.0);
					}
				}
				int[] array3 = this.adjVF[v3];
				for (int num2 = 0; num2 < array3.Length; num2++)
				{
					int m = array3[num2];
					if (m != j && this.IsContainVertex(m, v))
					{
						i.AddElementIfNotExist(j, m, 1.0);
					}
				}
			}
			return i;
		}
		public void FindBoundaryVertex()
		{
			for (int i = 0; i < this.vertexCount; i++)
			{
				int nAdjV = this.adjVV[i].Length;
				int nAdjF = this.adjVF[i].Length;
				this.isBoundary[i] = (nAdjV != nAdjF);
				if (nAdjV != nAdjF)
				{
					this.flag[i] = 1;
				}
			}
		}
		public void GroupingFlags()
		{
			for (int i = 0; i < this.flag.Length; i++)
			{
				if (this.flag[i] != 0)
				{
					this.flag[i] = 255;
				}
			}
			byte id = 0;
			Queue queue = new Queue();
			List<int> singleVertexGroupList = new List<int>();
			for (int j = 0; j < this.vertexCount; j++)
			{
				if (this.flag[j] == 255)
				{
					id += 1;
					this.flag[j] = id;
					queue.Enqueue(j);
					bool found = false;
					while (queue.Count > 0)
					{
						int curr = (int)queue.Dequeue();
						int[] array = this.adjVV[curr];
						for (int l = 0; l < array.Length; l++)
						{
							int k = array[l];
							if (this.flag[k] == 255)
							{
								this.flag[k] = id;
								queue.Enqueue(k);
								found = true;
							}
						}
					}
					if (!found)
					{
						singleVertexGroupList.Add(j);
					}
				}
			}
			this.singleVertexGroup = singleVertexGroupList.ToArray();
		}
		public void FindSingleVertexGroup()
		{
			Set<int> s = new Set<int>();
			for (int i = 0; i < this.vertexCount; i++)
			{
				if (this.flag[i] != 0)
				{
					bool found = false;
					int[] array = this.adjVV[i];
					for (int k = 0; k < array.Length; k++)
					{
						int j = array[k];
						if (this.flag[j] == this.flag[i])
						{
							found = true;
							break;
						}
					}
					if (!found)
					{
						s.Add(i);
					}
				}
			}
			int[] arr = s.ToArray();
			Array.Sort<int>(arr);
			this.singleVertexGroup = arr;
		}
		public void RemoveVertex(int index)
		{
			this._RemoveVertex(index);
			this.vertexNormal = new double[this.vertexCount * 3];
			this.faceNormal = new double[this.faceCount * 3];
			this.flag = new byte[this.vertexCount];
			this.isBoundary = new bool[this.vertexCount];
			this.ComputeFaceNormal();
			this.ComputeVertexNormal();
			SparseMatrix adjMatrix = this.BuildAdjacentMatrix();
			SparseMatrix adjMatrixFV = this.BuildAdjacentMatrixFV();
			this.adjVV = adjMatrix.GetRowIndex();
			this.adjVF = adjMatrixFV.GetColumnIndex();
			this.FindBoundaryVertex();
			this.ComputeDualPosition();
		}
		public void RemoveVertex(ArrayList indice)
		{
			for (int i = 0; i < indice.Count; i++)
			{
				this._RemoveVertex((int)indice[i] - i);
			}
			this.vertexNormal = new double[this.vertexCount * 3];
			this.faceNormal = new double[this.faceCount * 3];
			this.flag = new byte[this.vertexCount];
			this.isBoundary = new bool[this.vertexCount];
			this.ComputeFaceNormal();
			this.ComputeVertexNormal();
			SparseMatrix adjMatrix = this.BuildAdjacentMatrix();
			SparseMatrix adjMatrixFV = this.BuildAdjacentMatrixFV();
			this.adjVV = adjMatrix.GetRowIndex();
			this.adjVF = adjMatrixFV.GetColumnIndex();
			this.FindBoundaryVertex();
			this.ComputeDualPosition();
		}
		public bool IsContainVertex(int fIndex, int vIndex)
		{
			int b = fIndex * 3;
			int v = this.faceIndex[b];
			int v2 = this.faceIndex[b + 1];
			int v3 = this.faceIndex[b + 2];
			return v == vIndex || v2 == vIndex || v3 == vIndex;
		}
		public void ComputeDualPosition()
		{
			if (this.dualVertexPos == null)
			{
				this.dualVertexPos = new double[this.faceCount * 3];
			}
			for (int i = 0; i < this.dualVertexPos.Length; i++)
			{
				this.dualVertexPos[i] = 0.0;
			}
			int j = 0;
			int k = 0;
			while (j < this.vertexCount)
			{
				int[] array = this.adjVF[j];
				for (int n = 0; n < array.Length; n++)
				{
					int l = array[n];
					int b = l * 3;
					this.dualVertexPos[b] += this.vertexPos[k];
					this.dualVertexPos[b + 1] += this.vertexPos[k + 1];
					this.dualVertexPos[b + 2] += this.vertexPos[k + 2];
				}
				j++;
				k += 3;
			}
			for (int m = 0; m < this.dualVertexPos.Length; m++)
			{
				this.dualVertexPos[m] /= 3.0;
			}
		}
		public Vector3d ComputeDualPosition(int fIndex)
		{
			return new Vector3d(this.dualVertexPos, fIndex * 3);
		}
		public double ComputeFaceArea(int fIndex)
		{
			int b = fIndex * 3;
			Vector3d v = new Vector3d(this.VertexPos, this.faceIndex[b] * 3);
			Vector3d v2 = new Vector3d(this.VertexPos, this.faceIndex[b + 1] * 3);
			Vector3d v3 = new Vector3d(this.VertexPos, this.faceIndex[b + 2] * 3);
			return (v2 - v).Cross(v3 - v).Length() / 2.0;
		}
		public double AverageFaceArea()
		{
			double tot = 0.0;
			int i = 0;
			int j = 0;
			while (i < this.faceCount)
			{
				Vector3d v = new Vector3d(this.VertexPos, this.faceIndex[j] * 3);
				Vector3d v2 = new Vector3d(this.VertexPos, this.faceIndex[j + 1] * 3);
				Vector3d v3 = new Vector3d(this.VertexPos, this.faceIndex[j + 2] * 3);
				tot += (v2 - v).Cross(v3 - v).Length() / 2.0;
				i++;
				j += 3;
			}
			return tot / (double)this.faceCount;
		}
		public void SwapFlags(byte n1, byte n2)
		{
			for (int i = 0; i < this.vertexCount; i++)
			{
				if (this.flag[i] == n1)
				{
					this.flag[i] = n2;
				}
				else
				{
					if (this.flag[i] == n2)
					{
						this.flag[i] = n1;
					}
				}
			}
		}
		private void _RemoveVertex(int index)
		{
			double[] vp = new double[(this.vertexCount - 1) * 3];
			int i = 0;
			int j = 0;
			int k = 0;
			while (i < this.vertexCount)
			{
				if (i != index)
				{
					vp[k++] = this.vertexPos[j];
					vp[k++] = this.vertexPos[j + 1];
					vp[k++] = this.vertexPos[j + 2];
				}
				i++;
				j += 3;
			}
			ArrayList flist = new ArrayList(this.faceCount * 3);
			int l = 0;
			int m = 0;
			while (l < this.faceCount)
			{
				int c = this.faceIndex[m];
				int c2 = this.faceIndex[m + 1];
				int c3 = this.faceIndex[m + 2];
				if (c != index && c2 != index && c3 != index)
				{
					if (c > index)
					{
						c--;
					}
					flist.Add(c);
					if (c2 > index)
					{
						c2--;
					}
					flist.Add(c2);
					if (c3 > index)
					{
						c3--;
					}
					flist.Add(c3);
				}
				l++;
				m += 3;
			}
			this.vertexCount--;
			this.vertexPos = vp;
			this.faceCount = flist.Count / 3;
			this.faceIndex = new int[flist.Count];
			for (int n = 0; n < flist.Count; n++)
			{
				this.faceIndex[n] = (int)flist[n];
			}
		}
		private void _RemoveVertex(ArrayList indice)
		{
		}
	}
}
