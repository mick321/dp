using NumericalRecipes;
using System;
namespace MyGeometry
{
	public class Matrix3d
	{
		private const int len = 9;
		private const int row_size = 3;
		public static bool lastSVDIsFullRank;
		private double[] e = new double[9];
		public double this[int index]
		{
			get
			{
				return this.e[index];
			}
			set
			{
				this.e[index] = value;
			}
		}
		public double this[int row, int column]
		{
			get
			{
				return this.e[row * 3 + column];
			}
			set
			{
				this.e[row * 3 + column] = value;
			}
		}
		public Matrix3d()
		{
		}
		public Matrix3d(double[] arr)
		{
			for (int i = 0; i < 9; i++)
			{
				this.e[i] = arr[i];
			}
		}
		public Matrix3d(double[,] arr)
		{
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					this[i, j] = arr[i, j];
				}
			}
		}
		public Matrix3d(Matrix3d m) : this(m.e)
		{
		}
		public Matrix3d(Vector3d v1, Vector3d v2, Vector3d v3)
		{
			for (int i = 0; i < 3; i++)
			{
				this[i, 0] = v1[i];
				this[i, 1] = v2[i];
				this[i, 2] = v3[i];
			}
		}
		public void Clear()
		{
			for (int i = 0; i < 9; i++)
			{
				this.e[i] = 0.0;
			}
		}
		public double[] ToArray()
		{
			return (double[])this.e.Clone();
		}
		public double Trace()
		{
			return this.e[0] + this.e[4] + this.e[8];
		}
		public double SqNorm()
		{
			double sq = 0.0;
			for (int i = 0; i < 9; i++)
			{
				sq += this.e[i] * this.e[i];
			}
			return sq;
		}
		public Matrix3d Transpose()
		{
			Matrix3d i = new Matrix3d();
			for (int j = 0; j < 3; j++)
			{
				for (int k = 0; k < 3; k++)
				{
					i[k, j] = this[j, k];
				}
			}
			return i;
		}
		public Matrix3d Inverse()
		{
			double a = this.e[0];
			double b = this.e[1];
			double c = this.e[2];
			double d = this.e[3];
			double E = this.e[4];
			double f = this.e[5];
			double g = this.e[6];
			double h = this.e[7];
			double i = this.e[8];
			double det = a * (E * i - f * h) - b * (d * i - f * g) + c * (d * h - E * g);
			if (det == 0.0)
			{
				throw new ArithmeticException();
			}
			Matrix3d inv = new Matrix3d();
			inv[0] = (E * i - f * h) / det;
			inv[1] = (c * h - b * i) / det;
			inv[2] = (b * f - c * E) / det;
			inv[3] = (f * g - d * i) / det;
			inv[4] = (a * i - c * g) / det;
			inv[5] = (c * d - a * f) / det;
			inv[6] = (d * h - E * g) / det;
			inv[7] = (b * g - a * h) / det;
			inv[8] = (a * E - b * d) / det;
			return inv;
		}
		public Matrix3d InverseSVD()
		{
			SVD svd = new SVD(this.e, 3, 3);
			Matrix3d inv = new Matrix3d(svd.Inverse);
			Matrix3d.lastSVDIsFullRank = svd.FullRank;
			return inv;
		}
		public Matrix3d InverseTranspose()
		{
			double a = this.e[0];
			double b = this.e[1];
			double c = this.e[2];
			double d = this.e[3];
			double E = this.e[4];
			double f = this.e[5];
			double g = this.e[6];
			double h = this.e[7];
			double i = this.e[8];
			double det = a * (E * i - f * h) - b * (d * i - f * g) + c * (d * h - E * g);
			if (det == 0.0)
			{
				throw new ArithmeticException();
			}
			Matrix3d inv = new Matrix3d();
			inv[0] = (E * i - f * h) / det;
			inv[3] = (c * h - b * i) / det;
			inv[6] = (b * f - c * E) / det;
			inv[1] = (f * g - d * i) / det;
			inv[4] = (a * i - c * g) / det;
			inv[7] = (c * d - a * f) / det;
			inv[2] = (d * h - E * g) / det;
			inv[5] = (b * g - a * h) / det;
			inv[8] = (a * E - b * d) / det;
			return inv;
		}
		public Matrix3d OrthogonalFactor(double eps)
		{
			Matrix3d Q = new Matrix3d(this);
			Matrix3d Q2 = new Matrix3d();
			double err;
			do
			{
				Q2 = (Q + Q.InverseTranspose()) / 2.0;
				err = (Q2 - Q).SqNorm();
				Q = Q2;
			}
			while (err > eps);
			return Q2;
		}
		public Matrix3d OrthogonalFactorSVD()
		{
			SVD svd = new SVD(this.e, 3, 3);
			Matrix3d.lastSVDIsFullRank = svd.FullRank;
			return new Matrix3d(svd.OrthogonalFactor);
		}
		public Matrix3d OrthogonalFactorIter()
		{
			return (this + this.InverseTranspose()) / 2.0;
		}
		public static Matrix3d IdentityMatrix()
		{
			Matrix3d i = new Matrix3d();
			i[0] = (i[4] = (i[8] = 1.0));
			return i;
		}
		public static Vector3d operator *(Matrix3d m, Vector3d v)
		{
			return new Vector3d
			{
				x = m[0] * v.x + m[1] * v.y + m[2] * v.z,
				y = m[3] * v.x + m[4] * v.y + m[5] * v.z,
				z = m[6] * v.x + m[7] * v.y + m[8] * v.z
			};
		}
		public static Matrix3d operator *(Matrix3d m1, Matrix3d m2)
		{
			Matrix3d ret = new Matrix3d();
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					ret[i, j] = 0.0;
					for (int k = 0; k < 3; k++)
					{
						Matrix3d matrix3d;
						int row;
						int column;
						(matrix3d = ret)[row = i, column = j] = matrix3d[row, column] + m1[i, k] * m2[k, j];
					}
				}
			}
			return ret;
		}
		public static Matrix3d operator +(Matrix3d m1, Matrix3d m2)
		{
			Matrix3d ret = new Matrix3d();
			for (int i = 0; i < 9; i++)
			{
				ret[i] = m1[i] + m2[i];
			}
			return ret;
		}
		public static Matrix3d operator -(Matrix3d m1, Matrix3d m2)
		{
			Matrix3d ret = new Matrix3d();
			for (int i = 0; i < 9; i++)
			{
				ret[i] = m1[i] - m2[i];
			}
			return ret;
		}
		public static Matrix3d operator *(Matrix3d m, double d)
		{
			Matrix3d ret = new Matrix3d();
			for (int i = 0; i < 9; i++)
			{
				ret[i] = m[i] * d;
			}
			return ret;
		}
		public static Matrix3d operator /(Matrix3d m, double d)
		{
			Matrix3d ret = new Matrix3d();
			for (int i = 0; i < 9; i++)
			{
				ret[i] = m[i] / d;
			}
			return ret;
		}
		public override string ToString()
		{
			return string.Concat(new string[]
			{
				this.e[0].ToString("F5"),
				" ",
				this.e[1].ToString("F5"),
				" ",
				this.e[2].ToString("F5"),
				this.e[3].ToString("F5"),
				" ",
				this.e[4].ToString("F5"),
				" ",
				this.e[5].ToString("F5"),
				this.e[6].ToString("F5"),
				" ",
				this.e[7].ToString("F5"),
				" ",
				this.e[8].ToString("F5")
			});
		}
	}
}
