using System;
using System.Collections.Generic;
using System.IO;
namespace MyGeometry
{
	public class CCSMatrix
	{
		private int m;
		private int n;
		private int[] rowIndex;
		private int[] colIndex;
		private double[] values;
		public int RowSize
		{
			get
			{
				return this.m;
			}
		}
		public int ColumnSize
		{
			get
			{
				return this.n;
			}
		}
		public int[] RowIndex
		{
			get
			{
				return this.rowIndex;
			}
			set
			{
				this.rowIndex = value;
			}
		}
		public int[] ColIndex
		{
			get
			{
				return this.colIndex;
			}
		}
		public double[] Values
		{
			get
			{
				return this.values;
			}
			set
			{
				this.values = value;
			}
		}
		public int NumNonZero
		{
			get
			{
				return this.values.Length;
			}
		}
		public CCSMatrix(SparseMatrix matrix)
		{
			this.m = matrix.RowSize;
			this.n = matrix.ColumnSize;
			int nnz = 0;
			foreach (List<SparseMatrix.Element> col in matrix.Columns)
			{
				nnz += col.Count;
			}
			this.rowIndex = new int[nnz];
			this.colIndex = new int[this.n + 1];
			this.values = new double[nnz];
			int index = 0;
			int index2 = 0;
			this.colIndex[0] = 0;
			foreach (List<SparseMatrix.Element> col2 in matrix.Columns)
			{
				foreach (SparseMatrix.Element e in col2)
				{
					this.rowIndex[index] = e.i;
					this.values[index] = e.value;
					index++;
				}
				this.colIndex[++index2] = index;
			}
		}
		public CCSMatrix(SparseMatrix matrix, bool transponse)
		{
			this.m = matrix.ColumnSize;
			this.n = matrix.RowSize;
			int nnz = 0;
			foreach (List<SparseMatrix.Element> col in matrix.Columns)
			{
				nnz += col.Count;
			}
			this.rowIndex = new int[nnz];
			this.colIndex = new int[this.n + 1];
			this.values = new double[nnz];
			int index = 0;
			int index2 = 0;
			this.colIndex[0] = 0;
			foreach (List<SparseMatrix.Element> row in matrix.Rows)
			{
				foreach (SparseMatrix.Element e in row)
				{
					this.rowIndex[index] = e.j;
					this.values[index] = e.value;
					index++;
				}
				this.colIndex[++index2] = index;
			}
		}
		public CCSMatrix(double[,] matrix)
		{
			this.m = matrix.GetLength(0);
			this.n = matrix.GetLength(1);
			int nnz = 0;
			for (int i = 0; i < this.m; i++)
			{
				for (int j = 0; j < this.n; j++)
				{
					if (matrix[i, j] != 0.0)
					{
						nnz++;
					}
				}
			}
			this.rowIndex = new int[nnz];
			this.colIndex = new int[this.n + 1];
			this.values = new double[nnz];
			int index = 0;
			int index2 = 0;
			this.colIndex[0] = 0;
			for (int k = 0; k < this.n; k++)
			{
				for (int l = 0; l < this.n; l++)
				{
					if (matrix[l, k] != 0.0)
					{
						this.rowIndex[index] = l;
						this.values[index] = matrix[l, k];
						index++;
					}
				}
				this.colIndex[++index2] = index;
			}
		}
		public CCSMatrix(int m, int n)
		{
			this.m = m;
			this.n = n;
			this.colIndex = new int[n + 1];
			this.colIndex[0] = 0;
		}
		public CCSMatrix FastMultiply(CCSMatrix B)
		{
			CCSMatrix C = new CCSMatrix(this.m, B.n);
			Set<int> tmpIndex = new Set<int>();
			double[] tmp = new double[this.m];
			List<int> C_RowIndex = new List<int>();
			List<double> C_Value = new List<double>();
			for (int i = 0; i < this.m; i++)
			{
				tmp[i] = 0.0;
			}
			for (int j = 0; j < B.n; j++)
			{
				for (int col = B.colIndex[j]; col < B.colIndex[j + 1]; col++)
				{
					int k = B.rowIndex[col];
					double valB = B.values[col];
					if (k < this.ColumnSize)
					{
						for (int col2 = this.colIndex[k]; col2 < this.colIndex[k + 1]; col2++)
						{
							int k2 = this.rowIndex[col2];
							double valA = this.values[col2];
							tmpIndex.Add(k2);
							tmp[k2] += valA * valB;
						}
					}
				}
				int[] t = tmpIndex.ToArray();
				int count = 0;
				Array.Sort<int>(t);
				int[] array = t;
				for (int m = 0; m < array.Length; m++)
				{
					int l = array[m];
					if (tmp[l] != 0.0)
					{
						C_RowIndex.Add(l);
						C_Value.Add(tmp[l]);
						tmp[l] = 0.0;
						count++;
					}
				}
				C.colIndex[j + 1] = C.colIndex[j] + count;
				tmpIndex.Clear();
			}
			C.rowIndex = C_RowIndex.ToArray();
			C.values = C_Value.ToArray();
			return C;
		}
		public CCSMatrix Transpose()
		{
			CCSMatrix C = new CCSMatrix(this.n, this.m);
			int[] rowCount = new int[this.m];
			for (int i = 0; i < rowCount.Length; i++)
			{
				rowCount[i] = 0;
			}
			for (int j = 0; j < this.rowIndex.Length; j++)
			{
				rowCount[this.rowIndex[j]]++;
			}
			C.ColIndex[0] = 0;
			for (int k = 0; k < this.m; k++)
			{
				C.ColIndex[k + 1] = C.ColIndex[k] + rowCount[k];
				rowCount[k] = C.ColIndex[k];
			}
			C.values = new double[this.NumNonZero];
			C.rowIndex = new int[this.NumNonZero];
			for (int l = 0; l < this.n; l++)
			{
				for (int m = this.colIndex[l]; m < this.colIndex[l + 1]; m++)
				{
					int n = this.rowIndex[m];
					C.values[rowCount[n]] = this.values[m];
					C.rowIndex[rowCount[n]] = l;
					rowCount[n]++;
				}
			}
			return C;
		}
		public void Multiply(double[] xIn, double[] xOut)
		{
			if (xIn.Length < this.n || xOut.Length < this.m)
			{
				throw new ArgumentException();
			}
			for (int i = 0; i < this.m; i++)
			{
				xOut[i] = 0.0;
			}
			for (int j = 0; j < this.n; j++)
			{
				for (int k = this.colIndex[j]; k < this.colIndex[j + 1]; k++)
				{
					int r = this.rowIndex[k];
					xOut[r] += this.values[k] * xIn[j];
				}
			}
		}
		public void PreMultiply(double[] xIn, double[] xOut)
		{
			if (xIn.Length < this.m || xOut.Length < this.n)
			{
				throw new ArgumentException();
			}
			for (int i = 0; i < this.n; i++)
			{
				xOut[i] = 0.0;
			}
			for (int j = 0; j < this.n; j++)
			{
				double sum = 0.0;
				for (int k = this.colIndex[j]; k < this.colIndex[j + 1]; k++)
				{
					int r = this.rowIndex[k];
					sum += this.values[k] * xIn[r];
				}
				xOut[j] = sum;
			}
		}
		public void PreMultiply(double[] xIn, double[] xOut, int startIndex, bool init)
		{
			if (xIn.Length < this.m || xOut.Length < this.n)
			{
				throw new ArgumentException();
			}
			if (init)
			{
				for (int i = 0; i < this.n; i++)
				{
					xOut[i] = 0.0;
				}
			}
			for (int j = 0; j < this.n; j++)
			{
				double sum = 0.0;
				for (int k = this.colIndex[j]; k < this.colIndex[j + 1]; k++)
				{
					int r = this.rowIndex[k];
					sum += this.values[k] * xIn[r + startIndex];
				}
				xOut[j] += sum;
			}
		}
		public void PreMultiply(double[] xIn, double[] xOut, int inStart, int outStart, bool init)
		{
			if (xIn.Length < this.m || xOut.Length < this.n)
			{
				throw new ArgumentException();
			}
			if (init)
			{
				for (int i = 0; i < this.n; i++)
				{
					xOut[i + outStart] = 0.0;
				}
			}
			for (int j = 0; j < this.n; j++)
			{
				double sum = 0.0;
				for (int k = this.colIndex[j]; k < this.colIndex[j + 1]; k++)
				{
					int r = this.rowIndex[k];
					sum += this.values[k] * xIn[r + inStart];
				}
				xOut[j + outStart] += sum;
			}
		}
		public void PreMultiply(double[] xIn, double[] xOut, int[] index)
		{
			if (xIn.Length < this.m || xOut.Length < this.n)
			{
				throw new ArgumentException();
			}
			for (int l = 0; l < index.Length; l++)
			{
				int i = index[l];
				xOut[i] = 0.0;
			}
			for (int m = 0; m < index.Length; m++)
			{
				int j = index[m];
				double sum = 0.0;
				for (int k = this.colIndex[j]; k < this.colIndex[j + 1]; k++)
				{
					int r = this.rowIndex[k];
					sum += this.values[k] * xIn[r];
				}
				xOut[j] = sum;
			}
		}
		public void PreMultiplyOffset(double[] xIn, double[] xOut, int startOut, int offsetOut)
		{
			for (int i = startOut; i < this.n + offsetOut; i += offsetOut)
			{
				xOut[i] = 0.0;
			}
			int j = 0;
			int k = startOut;
			while (j < this.n)
			{
				double sum = 0.0;
				for (int l = this.colIndex[j]; l < this.colIndex[j + 1]; l++)
				{
					int r = this.rowIndex[l];
					sum += this.values[l] * xIn[r];
				}
				xOut[k] = sum;
				j++;
				k += offsetOut;
			}
		}
		public bool Check(CCSMatrix B)
		{
			if (this.rowIndex.Length != B.rowIndex.Length)
			{
				throw new Exception();
			}
			if (this.colIndex.Length != B.colIndex.Length)
			{
				throw new Exception();
			}
			if (this.values.Length != B.values.Length)
			{
				throw new Exception();
			}
			for (int i = 0; i < this.rowIndex.Length; i++)
			{
				if (this.rowIndex[i] != B.rowIndex[i])
				{
					throw new Exception();
				}
			}
			for (int j = 0; j < this.colIndex.Length; j++)
			{
				if (this.colIndex[j] != B.colIndex[j])
				{
					throw new Exception();
				}
			}
			for (int k = 0; k < this.values.Length; k++)
			{
				if (this.values[k] != B.values[k])
				{
					throw new Exception();
				}
			}
			return true;
		}
		public void CG(double[] x, double[] b, double eps, int maxIter)
		{
			double[] inv = new double[this.m];
			double[] r = new double[this.m];
			double[] d = new double[this.m];
			double[] q = new double[this.m];
			double[] s = new double[this.m];
			double errNew = 0.0;
			for (int i = 0; i < this.n; i++)
			{
				for (int j = this.colIndex[i]; j < this.colIndex[i + 1]; j++)
				{
					int row = this.rowIndex[j];
					if (i == row)
					{
						inv[i] = 1.0 / this.values[j];
					}
				}
			}
			int iter = 0;
			this.PreMultiply(x, r);
			for (int k = 0; k < this.m; k++)
			{
				r[k] = b[k] - r[k];
			}
			for (int l = 0; l < this.m; l++)
			{
				d[l] = inv[l] * r[l];
			}
			for (int m = 0; m < this.m; m++)
			{
				errNew += r[m] * d[m];
			}
			double err = errNew;
			while (iter < maxIter && errNew > eps * err)
			{
				this.PreMultiply(d, q);
				double alpha = 0.0;
				for (int n = 0; n < this.m; n++)
				{
					alpha += d[n] * q[n];
				}
				alpha = errNew / alpha;
				for (int i2 = 0; i2 < this.m; i2++)
				{
					x[i2] += alpha * d[i2];
				}
				if (iter % 500 == 0)
				{
					this.PreMultiply(x, r);
					for (int i3 = 0; i3 < this.m; i3++)
					{
						r[i3] = b[i3] - r[i3];
					}
				}
				else
				{
					for (int i4 = 0; i4 < this.m; i4++)
					{
						r[i4] -= alpha * q[i4];
					}
				}
				for (int i5 = 0; i5 < this.m; i5++)
				{
					s[i5] = inv[i5] * r[i5];
				}
				double errOld = errNew;
				errNew = 0.0;
				for (int i6 = 0; i6 < this.m; i6++)
				{
					errNew += r[i6] * s[i6];
				}
				double beta = errNew / errOld;
				for (int i7 = 0; i7 < this.m; i7++)
				{
					d[i7] = s[i7] + beta * d[i7];
				}
				iter++;
			}
		}
		public void PCG(double[] x, double[] b, double[] inv, double eps, int maxIter)
		{
			double[] r = new double[this.m];
			double[] d = new double[this.m];
			double[] q = new double[this.m];
			double[] s = new double[this.m];
			for (int i = 0; i < this.m; i++)
			{
				r[i] = b[i];
			}
			for (int j = 0; j < this.n; j++)
			{
				for (int k = this.colIndex[j]; k < this.colIndex[j + 1]; k++)
				{
					r[this.rowIndex[k]] -= this.values[k] * x[j];
				}
			}
			double new_err = 0.0;
			for (int l = 0; l < this.m; l++)
			{
				d[l] = inv[l] * r[l];
				new_err += d[l] * r[l];
			}
			double err = new_err;
			int iter = 0;
			while (iter < maxIter && new_err > eps * eps * err)
			{
				this.Multiply(d, q);
				double tmp = 0.0;
				for (int m = 0; m < this.m; m++)
				{
					tmp += d[m] * q[m];
				}
				double alpha = new_err / tmp;
				for (int n = 0; n < this.m; n++)
				{
					x[n] += alpha * d[n];
				}
				for (int i2 = 0; i2 < this.m; i2++)
				{
					if (double.IsNaN(x[i2]))
					{
						throw new Exception();
					}
				}
				if (iter % 50 == 0)
				{
					for (int i3 = 0; i3 < this.m; i3++)
					{
						r[i3] = b[i3];
					}
					for (int i4 = 0; i4 < this.n; i4++)
					{
						for (int j2 = this.colIndex[i4]; j2 < this.colIndex[i4 + 1]; j2++)
						{
							r[this.rowIndex[j2]] -= this.values[j2] * x[i4];
						}
					}
				}
				else
				{
					for (int i5 = 0; i5 < this.m; i5++)
					{
						r[i5] -= alpha * q[i5];
					}
				}
				for (int i6 = 0; i6 < this.m; i6++)
				{
					s[i6] = inv[i6] * r[i6];
				}
				double old_err = new_err;
				new_err = 0.0;
				for (int i7 = 0; i7 < this.m; i7++)
				{
					new_err += r[i7] * s[i7];
				}
				double beta = new_err / old_err;
				for (int i8 = 0; i8 < this.m; i8++)
				{
					d[i8] = s[i8] + beta * d[i8];
				}
				iter++;
			}
		}
		public void Write(StreamWriter sw)
		{
			for (int i = 0; i < this.n; i++)
			{
				for (int j = this.colIndex[i]; j < this.colIndex[i + 1]; j++)
				{
					sw.WriteLine(string.Concat(new object[]
					{
						this.rowIndex[j] + 1,
						" ",
						i + 1,
						" ",
						this.values[j].ToString()
					}));
					if (i != this.rowIndex[j])
					{
						sw.WriteLine(string.Concat(new object[]
						{
							i + 1,
							" ",
							this.rowIndex[j] + 1,
							" ",
							this.values[j].ToString()
						}));
					}
				}
			}
		}
	}
}
