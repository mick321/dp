using System;
namespace MyGeometry
{
	public struct Vector3d
	{
		public static Vector3d MinValue = new Vector3d(-1.7976931348623157E+308, -1.7976931348623157E+308, -1.7976931348623157E+308);
		public static Vector3d MaxValue = new Vector3d(1.7976931348623157E+308, 1.7976931348623157E+308, 1.7976931348623157E+308);
		public double x;
		public double y;
		public double z;
		public double this[int index]
		{
			get
			{
				if (index == 0)
				{
					return this.x;
				}
				if (index == 1)
				{
					return this.y;
				}
				if (index == 2)
				{
					return this.z;
				}
				throw new ArgumentException();
			}
			set
			{
				if (index == 0)
				{
					this.x = value;
				}
				if (index == 1)
				{
					this.y = value;
				}
				if (index == 2)
				{
					this.z = value;
				}
			}
		}
		public Vector3d(double x, double y, double z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}
		public Vector3d(Vector2d v)
		{
			this.x = v.x;
			this.y = v.y;
			this.z = 0.0;
		}
		public Vector3d(Vector2d v, double z)
		{
			this.x = v.x;
			this.y = v.y;
			this.z = z;
		}
		public Vector3d(double[] arr, int index)
		{
			this.x = arr[index];
			this.y = arr[index + 1];
			this.z = arr[index + 2];
		}
		public double Dot(Vector3d v)
		{
			return this.x * v.x + this.y * v.y + this.z * v.z;
		}
		public double Length()
		{
			return Math.Sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
		}
		public Vector3d Cross(Vector3d v)
		{
			return new Vector3d(this.y * v.z - v.y * this.z, this.z * v.x - v.z * this.x, this.x * v.y - v.x * this.y);
		}
		public Vector3d Normalize()
		{
			return this / this.Length();
		}
		public Matrix3d OuterCross(Vector3d v)
		{
			Matrix3d i = new Matrix3d();
			i[0, 0] = this.x * v.x;
			i[0, 1] = this.x * v.y;
			i[0, 2] = this.x * v.z;
			i[1, 0] = this.y * v.x;
			i[1, 1] = this.y * v.y;
			i[1, 2] = this.y * v.z;
			i[2, 0] = this.z * v.x;
			i[2, 1] = this.z * v.y;
			i[2, 2] = this.z * v.z;
			return i;
		}
		public override string ToString()
		{
			return string.Concat(new string[]
			{
				this.x.ToString(),
				" ",
				this.y.ToString(),
				" ",
				this.z.ToString()
			});
		}
		public static Vector3d Max(Vector3d v1, Vector3d v2)
		{
			return new Vector3d((v1.x > v2.x) ? v1.x : v2.x, (v1.y > v2.y) ? v1.y : v2.y, (v1.z > v2.z) ? v1.z : v2.z);
		}
		public static Vector3d Min(Vector3d v1, Vector3d v2)
		{
			return new Vector3d((v1.x < v2.x) ? v1.x : v2.x, (v1.y < v2.y) ? v1.y : v2.y, (v1.z < v2.z) ? v1.z : v2.z);
		}
		public static Vector3d operator +(Vector3d v1, Vector3d v2)
		{
			return new Vector3d(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
		}
		public static Vector3d operator -(Vector3d v1, Vector3d v2)
		{
			return new Vector3d(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
		}
		public static Vector3d operator *(Vector3d v, double s)
		{
			return new Vector3d(v.x * s, v.y * s, v.z * s);
		}
		public static Vector3d operator *(double s, Vector3d v)
		{
			return new Vector3d(v.x * s, v.y * s, v.z * s);
		}
		public static Vector3d operator /(Vector3d v, double s)
		{
			return new Vector3d(v.x / s, v.y / s, v.z / s);
		}
	}
}
