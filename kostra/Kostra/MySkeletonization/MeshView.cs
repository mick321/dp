using CsGL.OpenGL;
using MyGeometry;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
namespace IsolineEditing
{
	public class MeshView : OpenGLControl
	{
		private class FaceDepth : IComparable
		{
			public int index;
			public double depth;
			public FaceDepth(int index, double depth)
			{
				this.index = index;
				this.depth = depth;
			}
			public int CompareTo(object obj)
			{
				MeshView.FaceDepth f = obj as MeshView.FaceDepth;
				if (this.depth < f.depth)
				{
					return -1;
				}
				if (this.depth > f.depth)
				{
					return 1;
				}
				return 0;
			}
		}
		private MeshRecord currMeshRecord;
		private Matrix4d currTransformation = Matrix4d.IdentityMatrix();
		private Vector2d currMousePosition = default(Vector2d);
		private Trackball ball;
		private double scaleRatio;
		private int[] faceDepth;
		private Vector2d mouseDownPosition = default(Vector2d);
		private bool isMouseDown;
		private Trackball movingBall = new Trackball(200.0, 200.0);
		private Vector3d projectedCenter = default(Vector3d);
		private Vector4d handleCenter = default(Vector4d);
		private List<int> handleIndex = new List<int>();
		private List<Vector3d> oldHandlePos = new List<Vector3d>();
		private int handleFlag = -1;
		private bool initFont;
		public uint fontBase;
		private IContainer components;
		public Matrix4d CurrTransformation
		{
			get
			{
				return this.currTransformation;
			}
			set
			{
				this.currTransformation = value;
			}
		}
		[DllImport("opengl32")]
		public static extern bool wglUseFontBitmaps(IntPtr hDC, [MarshalAs(UnmanagedType.U4)] uint first, [MarshalAs(UnmanagedType.U4)] uint count, [MarshalAs(UnmanagedType.U4)] uint listBase);
		[DllImport("GDI32.DLL")]
		public static extern IntPtr SelectObject([In] IntPtr hDC, [In] IntPtr font);
		public MeshView()
		{
			this.InitializeComponent();
			this.ball = new Trackball((double)base.Width * 1.0, (double)base.Height * 1.0);
		}
		~MeshView()
		{
			this.ReleaseFont();
		}
		private void BuildFont(PaintEventArgs pe)
		{
			IntPtr dc = pe.Graphics.GetHdc();
			IntPtr oldFontH = IntPtr.Zero;
			Font font = new Font("Verdana", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.fontBase = OpenGL.glGenLists(128);
			IntPtr fontH = font.ToHfont();
			oldFontH = MeshView.SelectObject(dc, fontH);
			bool ret = MeshView.wglUseFontBitmaps(dc, 0u, 128u, this.fontBase);
			MeshView.SelectObject(dc, oldFontH);
			pe.Graphics.ReleaseHdc(dc);
			if (!ret)
			{
				throw new Exception();
			}
		}
		private void ReleaseFont()
		{
			OpenGL.glDeleteLists(this.fontBase, 255);
		}
		protected override void InitGLContext()
		{
			base.InitGLContext();
			Color c = SystemColors.Control;
			OpenGL.glClearColor((float)c.R / 255f, (float)c.G / 255f, (float)c.B / 255f, 0f);
			OpenGL.glShadeModel(7425u);
			OpenGL.glClearDepth(1.0);
			OpenGL.glEnable(2929u);
			OpenGL.glDepthFunc(515u);
			OpenGL.glHint(3152u, 4354u);
			OpenGL.glEnable(2884u);
			OpenGL.glPolygonOffset(1f, 1f);
			OpenGL.glBlendFunc(770u, 771u);
			OpenGL.glEnable(2832u);
			OpenGL.glEnable(2848u);
			OpenGL.glEnable(3042u);
			float[] LightDiffuse = new float[]
			{
				1f,
				1f,
				1f,
				0f
			};
			float[] LightSpecular = new float[]
			{
				0.7f,
				0.7f,
				0.7f,
				1f
			};
			float[] SpecularRef = new float[]
			{
				0.5f,
				0.5f,
				0.5f,
				1f
			};
			GL.glLightfv(16384u, 4609u, LightDiffuse);
			GL.glLightfv(16384u, 4610u, LightSpecular);
			OpenGL.glEnable(16384u);
			OpenGL.glEnable(2903u);
			OpenGL.glColorMaterial(1028u, 4609u);
			GL.glMaterialfv(1028u, 4610u, SpecularRef);
			OpenGL.glMateriali(1028u, 5633u, 128);
		}
		protected override void OnSizeChanged(EventArgs e)
		{
			base.OnSizeChanged(e);
			if (base.Width == 0 || base.Height == 0)
			{
				return;
			}
			this.scaleRatio = (double)((base.Width > base.Height) ? base.Height : base.Width);
			this.InitMatrix();
			this.ball.SetBounds((double)base.Width * 1.0, (double)base.Height * 1.0);
		}
		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.OnMouseDown(e);
			this.currMousePosition = new Vector2d((double)e.X, (double)(base.Height - e.Y));
			this.mouseDownPosition = this.currMousePosition;
			this.isMouseDown = true;
			switch (Program.currentMode)
			{
			case Program.EnumOperationMode.Viewing:
			{
				MouseButtons button = e.Button;
				if (button == MouseButtons.Left)
				{
					this.ball.Click(this.currMousePosition, Trackball.MotionType.Rotation);
					return;
				}
				if (button == MouseButtons.Right)
				{
					this.ball.Click(this.currMousePosition, Trackball.MotionType.Scale);
					return;
				}
				if (button != MouseButtons.Middle)
				{
					return;
				}
				this.ball.Click(this.currMousePosition / this.scaleRatio, Trackball.MotionType.Pan);
				return;
			}
			case Program.EnumOperationMode.Selection:
				break;
			case Program.EnumOperationMode.Moving:
				if (this.StartMoving())
				{
					Vector2d p = this.mouseDownPosition - new Vector2d(this.projectedCenter.x, this.projectedCenter.y);
					p.x += 100.0;
					p.y += 100.0;
					MouseButtons button2 = e.Button;
					if (button2 != MouseButtons.Left)
					{
						if (button2 != MouseButtons.Right)
						{
							if (button2 == MouseButtons.Middle)
							{
								this.movingBall.Click(p, Trackball.MotionType.Scale);
							}
						}
						else
						{
							this.movingBall.Click(p, Trackball.MotionType.Rotation);
						}
					}
					else
					{
						this.movingBall.Click(p / this.scaleRatio, Trackball.MotionType.Pan);
					}
					if (this.currMeshRecord.Deformer != null)
					{
						this.currMeshRecord.Deformer.MouseDown();
					}
					this.Refresh();
				}
				break;
			default:
				return;
			}
		}
		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);
			this.currMousePosition = new Vector2d((double)e.X, (double)(base.Height - e.Y));
			switch (Program.currentMode)
			{
			case Program.EnumOperationMode.Viewing:
			{
				MouseButtons button = e.Button;
				if (button != MouseButtons.Left)
				{
					if (button != MouseButtons.Right)
					{
						if (button == MouseButtons.Middle)
						{
							this.ball.Drag(this.currMousePosition / this.scaleRatio);
						}
					}
					else
					{
						this.ball.Drag(this.currMousePosition);
					}
				}
				else
				{
					this.ball.Drag(this.currMousePosition);
				}
				this.Refresh();
				return;
			}
			case Program.EnumOperationMode.Selection:
				if (this.isMouseDown)
				{
					this.Refresh();
					return;
				}
				break;
			case Program.EnumOperationMode.Moving:
				if (this.isMouseDown)
				{
					Vector2d p = this.currMousePosition - new Vector2d(this.projectedCenter.x, this.projectedCenter.y);
					int d = 0;
					p.x += 100.0;
					p.y += 100.0;
					MouseButtons button2 = e.Button;
					if (button2 != MouseButtons.Left)
					{
						if (button2 != MouseButtons.Right)
						{
							if (button2 == MouseButtons.Middle)
							{
								this.movingBall.Drag(p);
							}
						}
						else
						{
							this.movingBall.Drag(p);
						}
					}
					else
					{
						this.movingBall.Drag(p / this.scaleRatio);
						d = 1;
					}
					Matrix4d currInverseTransformation = this.currTransformation.Inverse();
					Matrix4d tran = currInverseTransformation * this.movingBall.GetMatrix() * this.currTransformation;
					double[] pos = this.currMeshRecord.Mesh.VertexPos;
					for (int i = 0; i < this.handleIndex.Count; i++)
					{
						int j = this.handleIndex[i] * 3;
						Vector4d q = new Vector4d(this.oldHandlePos[i], (double)d);
						q = tran * (q - this.handleCenter) + this.handleCenter;
						pos[j] = q.x;
						pos[j + 1] = q.y;
						pos[j + 2] = q.z;
					}
					if (this.currMeshRecord.Deformer != null)
					{
						this.currMeshRecord.Deformer.Move();
						this.currMeshRecord.Deformer.Deform();
						this.currMeshRecord.Deformer.Update();
					}
					this.Refresh();
				}
				break;
			default:
				return;
			}
		}
		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.OnMouseUp(e);
			this.currMousePosition = new Vector2d((double)e.X, (double)(base.Height - e.Y));
			this.isMouseDown = false;
			switch (Program.currentMode)
			{
			case Program.EnumOperationMode.Viewing:
			{
				if (this.currMousePosition == this.mouseDownPosition)
				{
					return;
				}
				Matrix4d i = this.ball.GetMatrix();
				this.currTransformation = i * this.currTransformation;
				this.ball.End();
				this.Refresh();
				return;
			}
			case Program.EnumOperationMode.Selection:
				switch (Program.toolsProperty.SelectionMethod)
				{
				case ToolsProperty.EnumSelectingMethod.Rectangle:
					this.SelectVertexByRect();
					break;
				case ToolsProperty.EnumSelectingMethod.Point:
					this.SelectVertexByPoint();
					break;
				}
				this.currMeshRecord.Mesh.GroupingFlags();
				this.Refresh();
				return;
			case Program.EnumOperationMode.Moving:
				if (this.currMeshRecord.Deformer != null)
				{
					this.currMeshRecord.Deformer.MouseUp();
					this.currMeshRecord.Deformer.Deform();
				}
				this.currMeshRecord.Mesh.ComputeFaceNormal();
				this.currMeshRecord.Mesh.ComputeVertexNormal();
				this.movingBall.End();
				this.handleFlag = -1;
				this.Refresh();
				return;
			default:
				return;
			}
		}
		protected override void OnPaint(PaintEventArgs pe)
		{
			base.OnPaint(pe);
			if (!this.initFont)
			{
				this.BuildFont(pe);
				this.initFont = true;
			}
		}
		public override void glDraw()
		{
			base.glDraw();
			OpenGL.glClear(16384u);
			if (base.DesignMode)
			{
				return;
			}
			if (this.currMeshRecord == null)
			{
				return;
			}
			Matrix4d i = this.ball.GetMatrix() * this.currTransformation;
			OpenGL.glClear(16640u);
			OpenGL.glMatrixMode(5888u);
			GL.glLoadMatrixd(i.Transpose().ToArray());
			switch (Program.displayProperty.MeshDisplayMode)
			{
			case DisplayProperty.EnumMeshDisplayMode.Points:
				this.DrawPoints();
				break;
			case DisplayProperty.EnumMeshDisplayMode.Wireframe:
				this.DrawWireframe();
				break;
			case DisplayProperty.EnumMeshDisplayMode.FlatShaded:
				this.DrawFlatShaded();
				break;
			case DisplayProperty.EnumMeshDisplayMode.SmoothShaded:
				this.DrawSmoothShaded();
				break;
			case DisplayProperty.EnumMeshDisplayMode.FlatShadedHiddenLine:
				this.DrawFlatHiddenLine();
				break;
			case DisplayProperty.EnumMeshDisplayMode.SmoothShadedHiddenLine:
				this.DrawSmoothHiddenLine();
				break;
			case DisplayProperty.EnumMeshDisplayMode.TransparentSmoothShaded:
				if (this.isMouseDown)
				{
					this.DrawSmoothShaded();
				}
				else
				{
					this.DrawTransparentSmoothShaded();
				}
				break;
			case DisplayProperty.EnumMeshDisplayMode.TransparentFlatShaded:
				if (this.isMouseDown)
				{
					this.DrawFlatShaded();
				}
				else
				{
					this.DrawTransparentFlatShaded();
				}
				break;
			}
			if (this.currMeshRecord.Deformer != null)
			{
				this.currMeshRecord.Deformer.Display();
			}
			if (this.currMeshRecord.Skeletonizer != null)
			{
				this.currMeshRecord.Skeletonizer.Display();
			}
			Program.EnumOperationMode currentMode = Program.currentMode;
			if (currentMode == Program.EnumOperationMode.Selection && this.isMouseDown)
			{
				this.DrawSelectionRect();
			}
			if (Program.displayProperty.DisplaySelectedVertices)
			{
				this.DrawSelectedVertice_ByPoint();
			}
		}
		private void InitMatrix()
		{
			double w = (double)base.Size.Width;
			double h = (double)base.Size.Height;
			OpenGL.glMatrixMode(5889u);
			OpenGL.glLoadIdentity();
			if (w > h)
			{
				double ratio = w / h / 2.0;
				OpenGL.glOrtho(-ratio, ratio, -0.5, 0.5, -100.0, 100.0);
			}
			else
			{
				double ratio2 = h / w / 2.0;
				OpenGL.glOrtho(-0.5, 0.5, -ratio2, ratio2, -100.0, 100.0);
			}
			OpenGL.glMatrixMode(5888u);
			OpenGL.glLoadIdentity();
		}
		private unsafe void DrawPoints()
		{
			Mesh i = this.currMeshRecord.Mesh;
			OpenGL.glPointSize(Program.displayProperty.PointSize);
			Color c = Program.displayProperty.PointColor;
			OpenGL.glColor3ub(c.R, c.G, c.B);
			OpenGL.glEnableClientState(32884u);
			fixed (double* vp = i.VertexPos)
			{
				OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
				OpenGL.glDrawArrays(0u, 0, i.VertexCount);
			}
			OpenGL.glDisableClientState(32884u);
		}
		private unsafe void DrawWireframe()
		{
			OpenGL.glPolygonMode(1032u, 6913u);
			OpenGL.glDisable(2884u);
			Color c = Program.displayProperty.LineColor;
			OpenGL.glColor3ub(c.R, c.G, c.B);
			Mesh i = this.currMeshRecord.Mesh;
			OpenGL.glLineWidth(Program.displayProperty.LineWidth);
			OpenGL.glEnableClientState(32884u);
			fixed (double* vp = i.VertexPos)
			{
				fixed (int* index = i.FaceIndex)
				{
					OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
					OpenGL.glDrawElements(4u, i.FaceCount * 3, 5125u, (void*)index);
				}
			}
			OpenGL.glDisableClientState(32884u);
			OpenGL.glEnable(2884u);
		}
		private unsafe void DrawFlatShaded()
		{
			OpenGL.glShadeModel(7424u);
			OpenGL.glPolygonMode(1032u, 6914u);
			OpenGL.glEnable(2896u);
			OpenGL.glEnable(2977u);
			Mesh i = this.currMeshRecord.Mesh;
			Color c = Program.displayProperty.MeshColor;
			OpenGL.glColor3ub(c.R, c.G, c.B);
			OpenGL.glEnableClientState(32884u);
			fixed (double* vp = i.VertexPos)
			{
				fixed (double* np = i.FaceNormal)
				{
					OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
					OpenGL.glBegin(4u);
					int j = 0;
					int k = 0;
					while (j < i.FaceCount)
					{
						OpenGL.glNormal3dv(np + k);
						OpenGL.glArrayElement(i.FaceIndex[k]);
						OpenGL.glArrayElement(i.FaceIndex[k + 1]);
						OpenGL.glArrayElement(i.FaceIndex[k + 2]);
						j++;
						k += 3;
					}
					OpenGL.glEnd();
				}
			}
			OpenGL.glDisableClientState(32884u);
			OpenGL.glDisable(2896u);
		}
		private unsafe void DrawSmoothShaded()
		{
			OpenGL.glShadeModel(7425u);
			OpenGL.glPolygonMode(1032u, 6914u);
			OpenGL.glEnable(2896u);
			OpenGL.glEnable(2977u);
			Mesh i = this.currMeshRecord.Mesh;
			Color c = Program.displayProperty.MeshColor;
			OpenGL.glColor3ub(c.R, c.G, c.B);
			OpenGL.glEnableClientState(32884u);
			OpenGL.glEnableClientState(32885u);
			fixed (double* vp = i.VertexPos)
			{
				fixed (double* np = i.VertexNormal)
				{
					fixed (int* index = i.FaceIndex)
					{
						OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
						OpenGL.glNormalPointer(5130u, 0, (void*)np);
						OpenGL.glDrawElements(4u, i.FaceCount * 3, 5125u, (void*)index);
					}
				}
			}
			OpenGL.glDisableClientState(32884u);
			OpenGL.glDisableClientState(32885u);
			OpenGL.glDisable(2896u);
		}
		private unsafe void DrawSmoothShaded2()
		{
			Mesh i = this.currMeshRecord.Mesh;
			OpenGL.glShadeModel(7425u);
			OpenGL.glPolygonMode(1032u, 6914u);
			OpenGL.glEnable(2896u);
			OpenGL.glEnable(2977u);
			OpenGL.glEnableClientState(32884u);
			OpenGL.glEnableClientState(32885u);
			fixed (double* vp = i.VertexPos)
			{
				fixed (double* np = i.VertexNormal)
				{
					OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
					OpenGL.glNormalPointer(5130u, 0, (void*)np);
					OpenGL.glBegin(4u);
					for (int j = 0; j < i.FaceCount * 3; j++)
					{
						int k = i.FaceIndex[j];
						if (i.Flag[k] != 0)
						{
							switch (i.Flag[k] % 6)
							{
							case 0:
								OpenGL.glColor3f(1f, 1f, 0f);
								break;
							case 1:
								OpenGL.glColor3f(1f, 0f, 0f);
								break;
							case 2:
								OpenGL.glColor3f(0f, 0.7f, 0f);
								break;
							case 3:
								OpenGL.glColor3f(1f, 0f, 0f);
								break;
							case 4:
								OpenGL.glColor3f(0f, 1f, 1f);
								break;
							case 5:
								OpenGL.glColor3f(1f, 0f, 1f);
								break;
							}
							OpenGL.glArrayElement(k);
						}
					}
					OpenGL.glEnd();
				}
			}
			OpenGL.glDisableClientState(32884u);
			OpenGL.glDisableClientState(32885u);
			OpenGL.glDisable(2896u);
		}
		private void DrawSmoothHiddenLine()
		{
			OpenGL.glEnable(32823u);
			this.DrawSmoothShaded();
			this.DrawWireframe();
		}
		private void DrawFlatHiddenLine()
		{
			OpenGL.glEnable(32823u);
			this.DrawFlatShaded();
			this.DrawWireframe();
		}
		private void DrawSelectionRect()
		{
			OpenGL.glMatrixMode(5889u);
			OpenGL.glPushMatrix();
			OpenGL.glLoadIdentity();
			GLU.gluOrtho2D(0.0, (double)base.Width, 0.0, (double)base.Height);
			OpenGL.glMatrixMode(5888u);
			OpenGL.glPushMatrix();
			OpenGL.glLoadIdentity();
			OpenGL.glPolygonMode(1032u, 6913u);
			OpenGL.glDisable(2884u);
			OpenGL.glDisable(2929u);
			OpenGL.glColor3f(0f, 0f, 0f);
			OpenGL.glRectd(this.mouseDownPosition.x, this.mouseDownPosition.y, this.currMousePosition.x, this.currMousePosition.y);
			OpenGL.glEnable(2884u);
			OpenGL.glEnable(2929u);
			OpenGL.glMatrixMode(5889u);
			OpenGL.glPopMatrix();
			OpenGL.glMatrixMode(5888u);
			OpenGL.glPopMatrix();
		}
		private unsafe void DrawSelectedVertice_ByPoint()
		{
			Mesh i = this.currMeshRecord.Mesh;
			OpenGL.glColor3f(1f, 0.4f, 0.4f);
			OpenGL.glPointSize(Program.displayProperty.PointSize);
			OpenGL.glEnableClientState(32884u);
			fixed (double* vp = i.VertexPos)
			{
				OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
				OpenGL.glBegin(0u);
				for (int j = 0; j < i.VertexCount; j++)
				{
					if (i.Flag[j] != 0)
					{
						switch (i.Flag[j] % 6)
						{
						case 0:
							OpenGL.glColor3f(1f, 1f, 0f);
							break;
						case 1:
							OpenGL.glColor3f(0f, 1f, 0f);
							break;
						case 2:
							OpenGL.glColor3f(0f, 0f, 1f);
							break;
						case 3:
							OpenGL.glColor3f(1f, 0f, 0f);
							break;
						case 4:
							OpenGL.glColor3f(0f, 1f, 1f);
							break;
						case 5:
							OpenGL.glColor3f(1f, 0f, 1f);
							break;
						}
						OpenGL.glArrayElement(j);
					}
				}
				OpenGL.glEnd();
			}
			OpenGL.glDisableClientState(32884u);
		}
		private unsafe void DrawTransparentSmoothShaded()
		{
			Mesh i = this.currMeshRecord.Mesh;
			this.SortFaces();
			OpenGL.glShadeModel(7425u);
			OpenGL.glPolygonMode(1032u, 6914u);
			OpenGL.glEnable(2896u);
			OpenGL.glEnable(2977u);
			Color c = Program.displayProperty.MeshColor;
			OpenGL.glColor4ub(c.R, c.G, c.B, 128);
			OpenGL.glBlendFunc(770u, 771u);
			OpenGL.glDisable(2929u);
			OpenGL.glDisable(2884u);
			OpenGL.glEnableClientState(32884u);
			OpenGL.glEnableClientState(32885u);
			fixed (double* vp = i.VertexPos)
			{
				fixed (double* np = i.VertexNormal)
				{
					int[] faceIndex;
					if ((faceIndex = i.FaceIndex) != null)
					{
						int arg_ED_0 = faceIndex.Length;
					}
					int[] array;
					if ((array = this.faceDepth) != null)
					{
						int arg_FD_0 = array.Length;
					}
					OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
					OpenGL.glNormalPointer(5130u, 0, (void*)np);
					OpenGL.glBegin(4u);
					for (int j = 0; j < i.FaceCount; j++)
					{
						int k = this.faceDepth[j] * 3;
						OpenGL.glArrayElement(i.FaceIndex[k]);
						OpenGL.glArrayElement(i.FaceIndex[k + 1]);
						OpenGL.glArrayElement(i.FaceIndex[k + 2]);
					}
					OpenGL.glEnd();
				}
			}
			OpenGL.glDisableClientState(32884u);
			OpenGL.glDisableClientState(32885u);
			OpenGL.glDisable(2896u);
			OpenGL.glBlendFunc(770u, 771u);
			OpenGL.glEnable(2929u);
			OpenGL.glEnable(2884u);
		}
		private unsafe void DrawTransparentSmoothShaded(byte alpha)
		{
			Mesh i = this.currMeshRecord.Mesh;
			this.SortFaces();
			OpenGL.glShadeModel(7425u);
			OpenGL.glPolygonMode(1032u, 6914u);
			OpenGL.glEnable(2896u);
			OpenGL.glEnable(2977u);
			Color c = Program.displayProperty.MeshColor;
			OpenGL.glColor4ub(c.R, c.G, c.B, alpha);
			OpenGL.glBlendFunc(770u, 771u);
			OpenGL.glDisable(2929u);
			OpenGL.glDisable(2884u);
			OpenGL.glEnableClientState(32884u);
			OpenGL.glEnableClientState(32885u);
			fixed (double* vp = i.VertexPos)
			{
				fixed (double* np = i.VertexNormal)
				{
					int[] faceIndex;
					if ((faceIndex = i.FaceIndex) != null)
					{
						int arg_E9_0 = faceIndex.Length;
					}
					int[] array;
					if ((array = this.faceDepth) != null)
					{
						int arg_F9_0 = array.Length;
					}
					OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
					OpenGL.glNormalPointer(5130u, 0, (void*)np);
					OpenGL.glBegin(4u);
					for (int j = 0; j < i.FaceCount; j++)
					{
						int k = this.faceDepth[j] * 3;
						OpenGL.glArrayElement(i.FaceIndex[k]);
						OpenGL.glArrayElement(i.FaceIndex[k + 1]);
						OpenGL.glArrayElement(i.FaceIndex[k + 2]);
					}
					OpenGL.glEnd();
				}
			}
			OpenGL.glDisableClientState(32884u);
			OpenGL.glDisableClientState(32885u);
			OpenGL.glDisable(2896u);
			OpenGL.glBlendFunc(770u, 771u);
			OpenGL.glEnable(2929u);
			OpenGL.glEnable(2884u);
		}
		private void DrawTransparentSmoothShaded2()
		{
			if (this.currMeshRecord == null)
			{
				return;
			}
			if (this.currMeshRecord.Mesh == null)
			{
				return;
			}
			if (this.currMeshRecord.Skeletonizer == null)
			{
				return;
			}
			this.currMeshRecord.Skeletonizer.DisplayOriginalMesh = false;
			this.DrawFlatHiddenLine();
			this.currMeshRecord.Skeletonizer.DisplayOriginalMesh = true;
			this.DrawTransparentSmoothShaded(64);
		}
		private unsafe void DrawTransparentFlatShaded()
		{
			Mesh i = this.currMeshRecord.Mesh;
			this.SortFaces();
			OpenGL.glShadeModel(7425u);
			OpenGL.glPolygonMode(1032u, 6914u);
			OpenGL.glEnable(2896u);
			OpenGL.glEnable(2977u);
			Color c = Program.displayProperty.MeshColor;
			OpenGL.glColor4ub(c.R, c.G, c.B, 180);
			OpenGL.glBlendFunc(770u, 771u);
			OpenGL.glDisable(2929u);
			OpenGL.glDisable(2884u);
			OpenGL.glEnableClientState(32884u);
			fixed (double* vp = i.VertexPos)
			{
				int[] faceIndex;
				if ((faceIndex = i.FaceIndex) != null)
				{
					int arg_C4_0 = faceIndex.Length;
				}
				int[] array;
				if ((array = this.faceDepth) != null)
				{
					int arg_D4_0 = array.Length;
				}
				fixed (double* np = i.FaceNormal)
				{
					OpenGL.glVertexPointer(3, 5130u, 0, (void*)vp);
					OpenGL.glNormalPointer(5130u, 0, (void*)np);
					OpenGL.glBegin(4u);
					for (int j = 0; j < i.FaceCount; j++)
					{
						int k = this.faceDepth[j] * 3;
						OpenGL.glNormal3dv(np + k);
						OpenGL.glArrayElement(i.FaceIndex[k]);
						OpenGL.glArrayElement(i.FaceIndex[k + 1]);
						OpenGL.glArrayElement(i.FaceIndex[k + 2]);
					}
					OpenGL.glEnd();
				}
			}
			OpenGL.glDisableClientState(32884u);
			OpenGL.glDisable(2896u);
			OpenGL.glBlendFunc(770u, 771u);
			OpenGL.glEnable(2929u);
			OpenGL.glEnable(2884u);
		}
		private void SelectVertexByRect()
		{
			Vector2d minV = Vector2d.Min(this.mouseDownPosition, this.currMousePosition);
			Vector2d size = Vector2d.Max(this.mouseDownPosition, this.currMousePosition) - minV;
			Rectangle rect = new Rectangle((int)minV.x, (int)minV.y, (int)size.x, (int)size.y);
			Rectangle viewport = new Rectangle(0, 0, base.Width, base.Height);
			OpenGLProjector projector = new OpenGLProjector();
			Mesh i = this.currMeshRecord.Mesh;
			bool laser = Program.toolsProperty.Laser;
			double eps = Program.toolsProperty.DepthTolerance;
			if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift)
			{
				int j = 0;
				int k = 0;
				while (j < i.VertexCount)
				{
					Vector3d v = projector.Project(i.VertexPos, k);
					if (viewport.Contains((int)v.x, (int)v.y))
					{
						bool flag = rect.Contains((int)v.x, (int)v.y);
						flag &= (laser || projector.GetDepthValue((int)v.x, (int)v.y) - v.z >= eps);
						if (flag)
						{
							i.Flag[j] = 1;
						}
					}
					j++;
					k += 3;
				}
				return;
			}
			if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
			{
				int l = 0;
				int m = 0;
				while (l < i.VertexCount)
				{
					Vector3d v2 = projector.Project(i.VertexPos, m);
					if (viewport.Contains((int)v2.x, (int)v2.y))
					{
						bool flag2 = rect.Contains((int)v2.x, (int)v2.y);
						flag2 &= (laser || projector.GetDepthValue((int)v2.x, (int)v2.y) - v2.z >= eps);
						if (flag2)
						{
							i.Flag[l] = 0;
						}
					}
					l++;
					m += 3;
				}
				return;
			}
			int n = 0;
			int j2 = 0;
			while (n < i.VertexCount)
			{
				Vector3d v3 = projector.Project(i.VertexPos, j2);
				if (viewport.Contains((int)v3.x, (int)v3.y))
				{
					bool flag3 = rect.Contains((int)v3.x, (int)v3.y);
					flag3 &= (laser || projector.GetDepthValue((int)v3.x, (int)v3.y) - v3.z >= eps);
					i.Flag[n] = (byte)(flag3 ? 1 : 0);
				}
				n++;
				j2 += 3;
			}
		}
		private void SelectVertexByPoint()
		{
			Rectangle viewport = new Rectangle(0, 0, base.Width, base.Height);
			OpenGLProjector projector = new OpenGLProjector();
			Mesh i = this.currMeshRecord.Mesh;
			bool laser = Program.toolsProperty.Laser;
			double eps = Program.toolsProperty.DepthTolerance;
			double minDis = 1.7976931348623157E+308;
			int minIndex = -1;
			int j = 0;
			int k = 0;
			while (j < i.VertexCount)
			{
				Vector3d v = projector.Project(i.VertexPos, k);
				Vector2d u = new Vector2d(v.x, v.y);
				if (viewport.Contains((int)v.x, (int)v.y) && (laser || projector.GetDepthValue((int)v.x, (int)v.y) - v.z >= eps))
				{
					double dis = (u - this.currMousePosition).Length();
					if (dis < minDis)
					{
						minIndex = j;
						minDis = dis;
					}
				}
				j++;
				k += 3;
			}
			if (minIndex == -1)
			{
				return;
			}
			if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift)
			{
				i.Flag[minIndex] = 1;
				return;
			}
			if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
			{
				i.Flag[minIndex] = 0;
				return;
			}
			for (int l = 0; l < i.VertexCount; l++)
			{
				i.Flag[l] = 0;
			}
			i.Flag[minIndex] = 1;
		}
		private bool StartMoving()
		{
			base.GrabContext();
			OpenGLProjector projector = new OpenGLProjector();
			Mesh i = this.currMeshRecord.Mesh;
			Deformer arg_23_0 = this.currMeshRecord.Deformer;
			Rectangle viewport = new Rectangle(0, 0, base.Width, base.Height);
			double eps = Program.toolsProperty.DepthTolerance;
			double minDis = 1.7976931348623157E+308;
			int minIndex = -1;
			int j = 0;
			int k = 0;
			while (j < i.VertexCount)
			{
				if (i.Flag[j] != 0)
				{
					Vector3d v3d = projector.Project(i.VertexPos, k);
					Vector2d v = new Vector2d(v3d.x, v3d.y);
					if (viewport.Contains((int)v.x, (int)v.y) && projector.GetDepthValue((int)v.x, (int)v.y) - v3d.z >= eps)
					{
						double dis = (v - this.mouseDownPosition).Length();
						if (dis < minDis)
						{
							minDis = dis;
							minIndex = j;
						}
					}
				}
				j++;
				k += 3;
			}
			if (minIndex == -1)
			{
				this.handleFlag = -1;
				this.handleIndex.Clear();
				this.oldHandlePos.Clear();
				return false;
			}
			int flag = (int)i.Flag[minIndex];
			this.handleFlag = flag;
			this.handleIndex.Clear();
			this.oldHandlePos.Clear();
			Vector3d c = new Vector3d(0.0, 0.0, 0.0);
			int count = 0;
			for (int l = 0; l < i.VertexCount; l++)
			{
				if ((int)i.Flag[l] == flag)
				{
					Vector3d p = new Vector3d(i.VertexPos, l * 3);
					c += p;
					this.handleIndex.Add(l);
					this.oldHandlePos.Add(p);
					count++;
				}
			}
			c /= (double)count;
			this.handleCenter = new Vector4d(c, 0.0);
			this.projectedCenter = projector.Project(this.handleCenter.x, this.handleCenter.y, this.handleCenter.z);
			return true;
		}
		private void SortFaces()
		{
			Mesh i = this.currMeshRecord.Mesh;
			Matrix4d tran = this.ball.GetMatrix() * this.currTransformation;
			MeshView.FaceDepth[] d = new MeshView.FaceDepth[i.FaceCount];
			for (int j = 0; j < i.FaceCount; j++)
			{
				Vector4d v = new Vector4d(new Vector3d(i.DualVertexPos, j * 3), 1.0);
				v = tran * v;
				d[j] = new MeshView.FaceDepth(j, v.z);
			}
			Array.Sort<MeshView.FaceDepth>(d);
			this.faceDepth = new int[i.FaceCount];
			for (int k = 0; k < i.FaceCount; k++)
			{
				this.faceDepth[k] = d[k].index;
			}
		}
		public void SetModel(MeshRecord rec)
		{
			if (this.currMeshRecord != null)
			{
				this.currMeshRecord.ModelViewMatrix = this.currTransformation;
			}
			if (rec != null)
			{
				this.currTransformation = rec.ModelViewMatrix;
			}
			this.currMeshRecord = rec;
			this.Refresh();
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}
		private void InitializeComponent()
		{
			this.components = new Container();
		}
	}
}
