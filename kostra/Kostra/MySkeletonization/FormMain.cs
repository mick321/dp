using CsGL.OpenGL;
using MyGeometry;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;
namespace IsolineEditing
{
	public class FormMain : Form
	{
		private delegate void SetTextCallback(string text);
		private IContainer components;
		private ToolStrip toolStrip1;
		private SplitContainer splitContainer1;
		private SplitContainer splitContainer2;
		private TabControl tabControl1;
		private TabPage tabPageModel;
		private TabPage tabPageDisplay;
		private PropertyGrid propertyGridModel;
		private PropertyGrid propertyGridDisplay;
		private TextBox textBoxOutput;
		private Button buttonShowHideOutput;
		private Button buttonShowHideProperty;
		private ToolStripSeparator toolStripSeparator1;
		private ToolStripButton toolStripButtonViewingTool;
		private ToolStripButton toolStripButtonSelectionTool;
		private ToolStripButton toolStripButtonMovingTool;
		private OpenFileDialog openFileDialog1;
		private Button buttonClearOutputText;
		private TabControl tabControlModelList;
		private MeshView meshView1;
		private ToolStripSeparator toolStripSeparator3;
		private ToolStripContainer toolStripContainer1;
		private StatusStrip statusStrip1;
		private ToolStripSplitButton toolStripSplitButtonCreateDeformer;
		private Button buttonCloseTab;
		private System.Windows.Forms.Timer timer1;
		private ToolStripButton toolStripButtonAutoUpdate;
		private ToolStripSeparator toolStripSeparator2;
		private ToolStripButton toolStripButtonAbout;
		private ToolStripButton toolStripButtonExit;
		private ToolStripSplitButton toolStripSplitButtonSkeletonization;
		private ToolStripSeparator toolStripSeparator4;
		private SaveFileDialog saveFileDialog1;
		private ToolStripMenuItem saveSegmentationToolStripMenuItem;
		private ToolStripMenuItem createDualLaplacianDeformerToolStripMenuItem;
		private ToolStripMenuItem createGeneralReducedDeformerToolStripMenuItem;
		private ToolStripMenuItem createIsolineDeformerToolStripMenuItem;
		private ToolStripButton toolStripButtonSegmentation;
		private Label labelVertexCount;
		private ToolStripButton toolStripButton1;
		private ToolStripSplitButton toolStripSplitButtonOpen;
		private ToolStripMenuItem openMeshFileToolStripMenuItem;
		private ToolStripMenuItem openSelectionFileToolStripMenuItem;
		private ToolStripMenuItem openCameraFileToolStripMenuItem;
		private ToolStripSplitButton toolStripSplitButtonSaveFile;
		private ToolStripMenuItem saveMeshFileToolStripMenuItem;
		private ToolStripMenuItem saveSelectionFileToolStripMenuItem;
		private ToolStripMenuItem saveCameraFileToolStripMenuItem;
		private ToolStripButton toolStripButton2;
		private ToolStripStatusLabel toolStripStatusLabel1;
		private List<MeshRecord> meshes = new List<MeshRecord>();
		private MeshRecord currentMeshRecord;
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}
		private void InitializeComponent()
		{
			this.components = new Container();
			ComponentResourceManager resources = new ComponentResourceManager(typeof(FormMain));
			Matrix4d matrix4d = new Matrix4d();
			this.toolStrip1 = new ToolStrip();
			this.toolStripButton1 = new ToolStripButton();
			this.toolStripSplitButtonOpen = new ToolStripSplitButton();
			this.openMeshFileToolStripMenuItem = new ToolStripMenuItem();
			this.openSelectionFileToolStripMenuItem = new ToolStripMenuItem();
			this.openCameraFileToolStripMenuItem = new ToolStripMenuItem();
			this.toolStripSplitButtonSaveFile = new ToolStripSplitButton();
			this.saveMeshFileToolStripMenuItem = new ToolStripMenuItem();
			this.saveSelectionFileToolStripMenuItem = new ToolStripMenuItem();
			this.saveCameraFileToolStripMenuItem = new ToolStripMenuItem();
			this.toolStripSeparator1 = new ToolStripSeparator();
			this.toolStripButtonViewingTool = new ToolStripButton();
			this.toolStripButtonSelectionTool = new ToolStripButton();
			this.toolStripButtonMovingTool = new ToolStripButton();
			this.toolStripSeparator3 = new ToolStripSeparator();
			this.toolStripSplitButtonCreateDeformer = new ToolStripSplitButton();
			this.createDualLaplacianDeformerToolStripMenuItem = new ToolStripMenuItem();
			this.createGeneralReducedDeformerToolStripMenuItem = new ToolStripMenuItem();
			this.createIsolineDeformerToolStripMenuItem = new ToolStripMenuItem();
			this.toolStripButtonAutoUpdate = new ToolStripButton();
			this.toolStripSeparator2 = new ToolStripSeparator();
			this.toolStripButton2 = new ToolStripButton();
			this.toolStripSplitButtonSkeletonization = new ToolStripSplitButton();
			this.saveSegmentationToolStripMenuItem = new ToolStripMenuItem();
			this.toolStripButtonSegmentation = new ToolStripButton();
			this.toolStripSeparator4 = new ToolStripSeparator();
			this.toolStripButtonAbout = new ToolStripButton();
			this.toolStripButtonExit = new ToolStripButton();
			this.splitContainer1 = new SplitContainer();
			this.splitContainer2 = new SplitContainer();
			this.labelVertexCount = new Label();
			this.buttonCloseTab = new Button();
			this.buttonShowHideProperty = new Button();
			this.buttonShowHideOutput = new Button();
			this.tabControlModelList = new TabControl();
			this.buttonClearOutputText = new Button();
			this.textBoxOutput = new TextBox();
			this.tabControl1 = new TabControl();
			this.tabPageModel = new TabPage();
			this.propertyGridModel = new PropertyGrid();
			this.tabPageDisplay = new TabPage();
			this.propertyGridDisplay = new PropertyGrid();
			this.openFileDialog1 = new OpenFileDialog();
			this.toolStripContainer1 = new ToolStripContainer();
			this.statusStrip1 = new StatusStrip();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.saveFileDialog1 = new SaveFileDialog();
			this.toolStripStatusLabel1 = new ToolStripStatusLabel();
			this.meshView1 = new MeshView();
			this.toolStrip1.SuspendLayout();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPageModel.SuspendLayout();
			this.tabPageDisplay.SuspendLayout();
			this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
			this.toolStripContainer1.ContentPanel.SuspendLayout();
			this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
			this.toolStripContainer1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			base.SuspendLayout();
			this.toolStrip1.Dock = DockStyle.None;
			this.toolStrip1.Items.AddRange(new ToolStripItem[]
			{
				this.toolStripButton1,
				this.toolStripSplitButtonOpen,
				this.toolStripSplitButtonSaveFile,
				this.toolStripSeparator1,
				this.toolStripButtonViewingTool,
				this.toolStripButtonSelectionTool,
				this.toolStripButtonMovingTool,
				this.toolStripSeparator3,
				this.toolStripSplitButtonCreateDeformer,
				this.toolStripButtonAutoUpdate,
				this.toolStripSeparator2,
				this.toolStripButton2,
				this.toolStripSplitButtonSkeletonization,
				this.toolStripButtonSegmentation,
				this.toolStripSeparator4,
				this.toolStripButtonAbout,
				this.toolStripButtonExit
			});
			this.toolStrip1.Location = new Point(3, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new Size(246, 25);
			this.toolStrip1.TabIndex = 2;
			this.toolStrip1.Text = "toolStrip1";
			//this.toolStripButton1.Image = (Image)resources.GetObject("toolStripButton1.Image");
			this.toolStripButton1.ImageTransparentColor = Color.Magenta;
			this.toolStripButton1.Name = "toolStripButton1";
			this.toolStripButton1.Size = new Size(98, 22);
			this.toolStripButton1.Text = "Open Mesh File";
			this.toolStripButton1.Click += new EventHandler(this.toolStripSplitButtonOpen_ButtonClick);
			this.toolStripSplitButtonOpen.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.toolStripSplitButtonOpen.DropDownItems.AddRange(new ToolStripItem[]
			{
				this.openMeshFileToolStripMenuItem,
				this.openSelectionFileToolStripMenuItem,
				this.openCameraFileToolStripMenuItem
			});
			//this.toolStripSplitButtonOpen.Image = (Image)resources.GetObject("toolStripSplitButtonOpen.Image");
			this.toolStripSplitButtonOpen.ImageTransparentColor = Color.Magenta;
			this.toolStripSplitButtonOpen.Name = "toolStripSplitButtonOpen";
			this.toolStripSplitButtonOpen.Size = new Size(32, 22);
			this.toolStripSplitButtonOpen.Text = "Open File";
			this.toolStripSplitButtonOpen.Visible = false;
			this.toolStripSplitButtonOpen.ButtonClick += new EventHandler(this.toolStripSplitButtonOpen_ButtonClick);
			this.openMeshFileToolStripMenuItem.ImageScaling = ToolStripItemImageScaling.None;
			this.openMeshFileToolStripMenuItem.Name = "openMeshFileToolStripMenuItem";
			this.openMeshFileToolStripMenuItem.Size = new Size(141, 22);
			this.openMeshFileToolStripMenuItem.Text = "Mesh File...";
			this.openMeshFileToolStripMenuItem.Click += new EventHandler(this.openMeshFileToolStripMenuItem_Click);
			this.openSelectionFileToolStripMenuItem.Name = "openSelectionFileToolStripMenuItem";
			this.openSelectionFileToolStripMenuItem.Size = new Size(141, 22);
			this.openSelectionFileToolStripMenuItem.Text = "Selection File...";
			this.openSelectionFileToolStripMenuItem.Click += new EventHandler(this.openSelectionFileToolStripMenuItem_Click);
			this.openCameraFileToolStripMenuItem.Name = "openCameraFileToolStripMenuItem";
			this.openCameraFileToolStripMenuItem.Size = new Size(141, 22);
			this.openCameraFileToolStripMenuItem.Text = "Camera File...";
			this.openCameraFileToolStripMenuItem.Click += new EventHandler(this.openCameraFileToolStripMenuItem_Click);
			this.toolStripSplitButtonSaveFile.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.toolStripSplitButtonSaveFile.DropDownItems.AddRange(new ToolStripItem[]
			{
				this.saveMeshFileToolStripMenuItem,
				this.saveSelectionFileToolStripMenuItem,
				this.saveCameraFileToolStripMenuItem
			});
			//this.toolStripSplitButtonSaveFile.Image = (Image)resources.GetObject("toolStripSplitButtonSaveFile.Image");
			this.toolStripSplitButtonSaveFile.ImageTransparentColor = Color.Magenta;
			this.toolStripSplitButtonSaveFile.Name = "toolStripSplitButtonSaveFile";
			this.toolStripSplitButtonSaveFile.Size = new Size(32, 22);
			this.toolStripSplitButtonSaveFile.Text = "Save File";
			this.toolStripSplitButtonSaveFile.Visible = false;
			//this.saveMeshFileToolStripMenuItem.Image = (Image)resources.GetObject("saveMeshFileToolStripMenuItem.Image");
			this.saveMeshFileToolStripMenuItem.Name = "saveMeshFileToolStripMenuItem";
			this.saveMeshFileToolStripMenuItem.Size = new Size(141, 22);
			this.saveMeshFileToolStripMenuItem.Text = "Mesh File...";
			this.saveMeshFileToolStripMenuItem.Click += new EventHandler(this.saveMeshFileToolStripMenuItem_Click);
			//this.saveSelectionFileToolStripMenuItem.Image = (Image)resources.GetObject("saveSelectionFileToolStripMenuItem.Image");
			this.saveSelectionFileToolStripMenuItem.Name = "saveSelectionFileToolStripMenuItem";
			this.saveSelectionFileToolStripMenuItem.Size = new Size(141, 22);
			this.saveSelectionFileToolStripMenuItem.Text = "Selection File...";
			this.saveSelectionFileToolStripMenuItem.Click += new EventHandler(this.saveSelectionFileToolStripMenuItem_Click);
			//this.saveCameraFileToolStripMenuItem.Image = (Image)resources.GetObject("saveCameraFileToolStripMenuItem.Image");
			this.saveCameraFileToolStripMenuItem.Name = "saveCameraFileToolStripMenuItem";
			this.saveCameraFileToolStripMenuItem.Size = new Size(141, 22);
			this.saveCameraFileToolStripMenuItem.Text = "Camera File...";
			this.saveCameraFileToolStripMenuItem.Click += new EventHandler(this.saveCameraFileToolStripMenuItem_Click);
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new Size(6, 25);
			this.toolStripButtonViewingTool.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.toolStripButtonViewingTool.Image = (Image)resources.GetObject("toolStripButtonViewingTool.Image");
			this.toolStripButtonViewingTool.ImageTransparentColor = Color.Magenta;
			this.toolStripButtonViewingTool.Name = "toolStripButtonViewingTool";
			this.toolStripButtonViewingTool.Size = new Size(23, 22);
			this.toolStripButtonViewingTool.Text = "Viewing Tool";
			this.toolStripButtonViewingTool.Visible = false;
			this.toolStripButtonViewingTool.Click += new EventHandler(this.toolStripButtonViewingTool_Click);
			this.toolStripButtonSelectionTool.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.toolStripButtonSelectionTool.Image = (Image)resources.GetObject("toolStripButtonSelectionTool.Image");
			this.toolStripButtonSelectionTool.ImageScaling = ToolStripItemImageScaling.None;
			this.toolStripButtonSelectionTool.ImageTransparentColor = Color.Magenta;
			this.toolStripButtonSelectionTool.Name = "toolStripButtonSelectionTool";
			this.toolStripButtonSelectionTool.Size = new Size(23, 22);
			this.toolStripButtonSelectionTool.Text = "Selection Tool";
			this.toolStripButtonSelectionTool.Visible = false;
			this.toolStripButtonSelectionTool.Click += new EventHandler(this.toolStripButtonSelectionTool_Click);
			this.toolStripButtonMovingTool.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.toolStripButtonMovingTool.Image = (Image)resources.GetObject("toolStripButtonMovingTool.Image");
			this.toolStripButtonMovingTool.ImageScaling = ToolStripItemImageScaling.None;
			this.toolStripButtonMovingTool.ImageTransparentColor = Color.Magenta;
			this.toolStripButtonMovingTool.Name = "toolStripButtonMovingTool";
			this.toolStripButtonMovingTool.Size = new Size(23, 22);
			this.toolStripButtonMovingTool.Text = "Moving Tool";
			this.toolStripButtonMovingTool.Visible = false;
			this.toolStripButtonMovingTool.Click += new EventHandler(this.toolStripButtonMovingTool_Click);
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new Size(6, 25);
			this.toolStripSeparator3.Visible = false;
			this.toolStripSplitButtonCreateDeformer.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.toolStripSplitButtonCreateDeformer.DropDownItems.AddRange(new ToolStripItem[]
			{
				this.createDualLaplacianDeformerToolStripMenuItem,
				this.createGeneralReducedDeformerToolStripMenuItem,
				this.createIsolineDeformerToolStripMenuItem
			});
			//this.toolStripSplitButtonCreateDeformer.Image = (Image)resources.GetObject("toolStripSplitButtonCreateDeformer.Image");
			this.toolStripSplitButtonCreateDeformer.ImageTransparentColor = Color.Magenta;
			this.toolStripSplitButtonCreateDeformer.Name = "toolStripSplitButtonCreateDeformer";
			this.toolStripSplitButtonCreateDeformer.Size = new Size(32, 22);
			this.toolStripSplitButtonCreateDeformer.Text = "Create Deformer";
			this.toolStripSplitButtonCreateDeformer.Visible = false;
			this.toolStripSplitButtonCreateDeformer.ButtonClick += new EventHandler(this.toolStripSplitButtonCreateDeformer_ButtonClick);
			this.createDualLaplacianDeformerToolStripMenuItem.Name = "createDualLaplacianDeformerToolStripMenuItem";
			this.createDualLaplacianDeformerToolStripMenuItem.Size = new Size(240, 22);
			this.createDualLaplacianDeformerToolStripMenuItem.Text = "Create Dual Laplacian Deformer";
			this.createDualLaplacianDeformerToolStripMenuItem.Click += new EventHandler(this.createDualLaplacianDeformerToolStripMenuItem_Click);
			this.createGeneralReducedDeformerToolStripMenuItem.Name = "createGeneralReducedDeformerToolStripMenuItem";
			this.createGeneralReducedDeformerToolStripMenuItem.Size = new Size(240, 22);
			this.createGeneralReducedDeformerToolStripMenuItem.Text = "Create General Reduced Deformer...";
			this.createGeneralReducedDeformerToolStripMenuItem.Click += new EventHandler(this.createGeneralReducedDeformerToolStripMenuItem_Click);
			this.createIsolineDeformerToolStripMenuItem.Name = "createIsolineDeformerToolStripMenuItem";
			this.createIsolineDeformerToolStripMenuItem.Size = new Size(240, 22);
			this.createIsolineDeformerToolStripMenuItem.Text = "Create Isoline Deformer";
			this.createIsolineDeformerToolStripMenuItem.Click += new EventHandler(this.createIsolineDeformerToolStripMenuItem_Click);
			this.toolStripButtonAutoUpdate.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.toolStripButtonAutoUpdate.Image = (Image)resources.GetObject("toolStripButtonAutoUpdate.Image");
			this.toolStripButtonAutoUpdate.ImageTransparentColor = Color.Magenta;
			this.toolStripButtonAutoUpdate.Name = "toolStripButtonAutoUpdate";
			this.toolStripButtonAutoUpdate.Size = new Size(23, 22);
			this.toolStripButtonAutoUpdate.Text = "Auto Update";
			this.toolStripButtonAutoUpdate.Visible = false;
			this.toolStripButtonAutoUpdate.Click += new EventHandler(this.toolStripButtonAutoUpdate_Click);
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new Size(6, 25);
			this.toolStripSeparator2.Visible = false;
			//this.toolStripButton2.Image = (Image)resources.GetObject("toolStripButton2.Image");
			this.toolStripButton2.ImageTransparentColor = Color.Magenta;
			this.toolStripButton2.Name = "toolStripButton2";
			this.toolStripButton2.Size = new Size(101, 22);
			this.toolStripButton2.Text = "Extract Skeleton";
			this.toolStripButton2.Click += new EventHandler(this.toolStripSplitButtonSkeletonization_ButtonClick);
			this.toolStripSplitButtonSkeletonization.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.toolStripSplitButtonSkeletonization.DropDownItems.AddRange(new ToolStripItem[]
			{
				this.saveSegmentationToolStripMenuItem
			});
			//this.toolStripSplitButtonSkeletonization.Image = (Image)resources.GetObject("toolStripSplitButtonSkeletonization.Image");
			this.toolStripSplitButtonSkeletonization.ImageTransparentColor = Color.Magenta;
			this.toolStripSplitButtonSkeletonization.Name = "toolStripSplitButtonSkeletonization";
			this.toolStripSplitButtonSkeletonization.Size = new Size(32, 22);
			this.toolStripSplitButtonSkeletonization.Text = "Skeletonization";
			this.toolStripSplitButtonSkeletonization.Visible = false;
			this.toolStripSplitButtonSkeletonization.ButtonClick += new EventHandler(this.toolStripSplitButtonSkeletonization_ButtonClick);
			this.saveSegmentationToolStripMenuItem.Name = "saveSegmentationToolStripMenuItem";
			this.saveSegmentationToolStripMenuItem.Size = new Size(167, 22);
			this.saveSegmentationToolStripMenuItem.Text = "Save Segmentation...";
			this.saveSegmentationToolStripMenuItem.Click += new EventHandler(this.saveSegmentationToolStripMenuItem_Click);
			this.toolStripButtonSegmentation.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.toolStripButtonSegmentation.Image = (Image)resources.GetObject("toolStripButtonSegmentation.Image");
			this.toolStripButtonSegmentation.ImageTransparentColor = Color.Magenta;
			this.toolStripButtonSegmentation.Name = "toolStripButtonSegmentation";
			this.toolStripButtonSegmentation.Size = new Size(23, 22);
			this.toolStripButtonSegmentation.Text = "Segmentation";
			this.toolStripButtonSegmentation.Visible = false;
			this.toolStripButtonSegmentation.Click += new EventHandler(this.toolStripButtonSegmentation_Click);
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new Size(6, 25);
			this.toolStripButtonAbout.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.toolStripButtonAbout.Image = (Image)resources.GetObject("toolStripButtonAbout.Image");
			this.toolStripButtonAbout.ImageTransparentColor = Color.Magenta;
			this.toolStripButtonAbout.Name = "toolStripButtonAbout";
			this.toolStripButtonAbout.Size = new Size(23, 22);
			this.toolStripButtonAbout.Text = "About";
			this.toolStripButtonAbout.Visible = false;
			this.toolStripButtonAbout.Click += new EventHandler(this.toolStripButtonAbout_Click);
			this.toolStripButtonExit.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.toolStripButtonExit.Image = (Image)resources.GetObject("toolStripButtonExit.Image");
			this.toolStripButtonExit.ImageTransparentColor = Color.Magenta;
			this.toolStripButtonExit.Name = "toolStripButtonExit";
			this.toolStripButtonExit.Size = new Size(23, 22);
			this.toolStripButtonExit.Text = "Exit";
			this.toolStripButtonExit.Click += new EventHandler(this.toolStripButtonExit_Click);
			this.splitContainer1.Dock = DockStyle.Fill;
			this.splitContainer1.Location = new Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
			this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
			this.splitContainer1.Size = new Size(802, 411);
			this.splitContainer1.SplitterDistance = 526;
			this.splitContainer1.TabIndex = 3;
			this.splitContainer2.Dock = DockStyle.Fill;
			this.splitContainer2.Location = new Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			this.splitContainer2.Orientation = Orientation.Horizontal;
			this.splitContainer2.Panel1.Controls.Add(this.labelVertexCount);
			this.splitContainer2.Panel1.Controls.Add(this.buttonCloseTab);
			this.splitContainer2.Panel1.Controls.Add(this.buttonShowHideProperty);
			this.splitContainer2.Panel1.Controls.Add(this.buttonShowHideOutput);
			this.splitContainer2.Panel1.Controls.Add(this.tabControlModelList);
			this.splitContainer2.Panel1.Controls.Add(this.meshView1);
			this.splitContainer2.Panel2.Controls.Add(this.buttonClearOutputText);
			this.splitContainer2.Panel2.Controls.Add(this.textBoxOutput);
			this.splitContainer2.Panel2Collapsed = true;
			this.splitContainer2.Size = new Size(526, 411);
			this.splitContainer2.SplitterDistance = 338;
			this.splitContainer2.TabIndex = 0;
			this.labelVertexCount.AutoSize = true;
			this.labelVertexCount.BackColor = SystemColors.Control;
			this.labelVertexCount.Font = new Font("Arial", 27.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.labelVertexCount.Location = new Point(12, 33);
			this.labelVertexCount.Name = "labelVertexCount";
			this.labelVertexCount.Size = new Size(163, 42);
			this.labelVertexCount.TabIndex = 5;
			this.labelVertexCount.Text = "Vertex #:";
			this.buttonCloseTab.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
			//this.buttonCloseTab.Image = (Image)resources.GetObject("buttonCloseTab.Image");
			this.buttonCloseTab.Location = new Point(487, 23);
			this.buttonCloseTab.Name = "buttonCloseTab";
			this.buttonCloseTab.Size = new Size(36, 22);
			this.buttonCloseTab.TabIndex = 4;
			this.buttonCloseTab.UseVisualStyleBackColor = true;
			this.buttonCloseTab.Click += new EventHandler(this.buttonCloseTab_Click);
			this.buttonShowHideProperty.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			//this.buttonShowHideProperty.Image = (Image)resources.GetObject("buttonShowHideProperty.Image");
			this.buttonShowHideProperty.Location = new Point(487, 387);
			this.buttonShowHideProperty.Name = "buttonShowHideProperty";
			this.buttonShowHideProperty.Size = new Size(36, 22);
			this.buttonShowHideProperty.TabIndex = 0;
			this.buttonShowHideProperty.UseVisualStyleBackColor = true;
			this.buttonShowHideProperty.Visible = false;
			this.buttonShowHideProperty.Click += new EventHandler(this.buttonShowHideProperty_Click);
			this.buttonShowHideOutput.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			//this.buttonShowHideOutput.Image = (Image)resources.GetObject("buttonShowHideOutput.Image");
			this.buttonShowHideOutput.Location = new Point(445, 387);
			this.buttonShowHideOutput.Name = "buttonShowHideOutput";
			this.buttonShowHideOutput.Size = new Size(36, 22);
			this.buttonShowHideOutput.TabIndex = 1;
			this.buttonShowHideOutput.UseVisualStyleBackColor = true;
			this.buttonShowHideOutput.Visible = false;
			this.buttonShowHideOutput.Click += new EventHandler(this.buttonShowHideOutput_Click);
			this.tabControlModelList.Dock = DockStyle.Top;
			this.tabControlModelList.Location = new Point(0, 0);
			this.tabControlModelList.Name = "tabControlModelList";
			this.tabControlModelList.SelectedIndex = 0;
			this.tabControlModelList.Size = new Size(526, 21);
			this.tabControlModelList.TabIndex = 2;
			this.tabControlModelList.Selected += new TabControlEventHandler(this.tabControlModelList_Selected);
			this.buttonClearOutputText.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			//this.buttonClearOutputText.Image = (Image)resources.GetObject("buttonClearOutputText.Image");
			this.buttonClearOutputText.Location = new Point(1193, 57);
			this.buttonClearOutputText.Name = "buttonClearOutputText";
			this.buttonClearOutputText.Size = new Size(36, 22);
			this.buttonClearOutputText.TabIndex = 1;
			this.buttonClearOutputText.UseVisualStyleBackColor = true;
			this.buttonClearOutputText.Click += new EventHandler(this.buttonClearOutputText_Click);
			this.textBoxOutput.BackColor = SystemColors.Window;
			this.textBoxOutput.Dock = DockStyle.Fill;
			this.textBoxOutput.Location = new Point(0, 0);
			this.textBoxOutput.Multiline = true;
			this.textBoxOutput.Name = "textBoxOutput";
			this.textBoxOutput.ReadOnly = true;
			this.textBoxOutput.ScrollBars = ScrollBars.Vertical;
			this.textBoxOutput.Size = new Size(150, 46);
			this.textBoxOutput.TabIndex = 0;
			this.tabControl1.Controls.Add(this.tabPageModel);
			this.tabControl1.Controls.Add(this.tabPageDisplay);
			this.tabControl1.Dock = DockStyle.Fill;
			this.tabControl1.Location = new Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new Size(272, 411);
			this.tabControl1.TabIndex = 0;
			this.tabPageModel.Controls.Add(this.propertyGridModel);
			this.tabPageModel.Location = new Point(4, 21);
			this.tabPageModel.Name = "tabPageModel";
			this.tabPageModel.Padding = new Padding(3);
			this.tabPageModel.Size = new Size(264, 386);
			this.tabPageModel.TabIndex = 0;
			this.tabPageModel.Text = "Model";
			this.tabPageModel.UseVisualStyleBackColor = true;
			this.propertyGridModel.CommandsDisabledLinkColor = Color.FromArgb(224, 224, 224);
			this.propertyGridModel.Dock = DockStyle.Fill;
			this.propertyGridModel.HelpVisible = false;
			this.propertyGridModel.Location = new Point(3, 3);
			this.propertyGridModel.Name = "propertyGridModel";
			this.propertyGridModel.Size = new Size(258, 380);
			this.propertyGridModel.TabIndex = 0;
			this.tabPageDisplay.Controls.Add(this.propertyGridDisplay);
			this.tabPageDisplay.Location = new Point(4, 21);
			this.tabPageDisplay.Name = "tabPageDisplay";
			this.tabPageDisplay.Padding = new Padding(3);
			this.tabPageDisplay.Size = new Size(264, 386);
			this.tabPageDisplay.TabIndex = 1;
			this.tabPageDisplay.Text = "Display";
			this.tabPageDisplay.UseVisualStyleBackColor = true;
			this.propertyGridDisplay.Dock = DockStyle.Fill;
			this.propertyGridDisplay.HelpVisible = false;
			this.propertyGridDisplay.Location = new Point(3, 3);
			this.propertyGridDisplay.Name = "propertyGridDisplay";
			this.propertyGridDisplay.Size = new Size(258, 380);
			this.propertyGridDisplay.TabIndex = 0;
			this.openFileDialog1.FileName = "openFileDialog1";
			this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
			this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
			this.toolStripContainer1.ContentPanel.Size = new Size(802, 411);
			this.toolStripContainer1.Dock = DockStyle.Fill;
			this.toolStripContainer1.LeftToolStripPanelVisible = false;
			this.toolStripContainer1.Location = new Point(0, 0);
			this.toolStripContainer1.Name = "toolStripContainer1";
			this.toolStripContainer1.RightToolStripPanelVisible = false;
			this.toolStripContainer1.Size = new Size(802, 458);
			this.toolStripContainer1.TabIndex = 4;
			this.toolStripContainer1.Text = "toolStripContainer1";
			this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
			this.statusStrip1.Dock = DockStyle.None;
			this.statusStrip1.Items.AddRange(new ToolStripItem[]
			{
				this.toolStripStatusLabel1
			});
			this.statusStrip1.Location = new Point(0, 0);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new Size(802, 22);
			this.statusStrip1.TabIndex = 5;
			this.statusStrip1.Text = "statusStrip1";
			this.timer1.Interval = 20;
			this.timer1.Tick += new EventHandler(this.timer1_Tick);
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new Size(43, 17);
			this.toolStripStatusLabel1.Text = "[Ready]";
			Matrix4d arg_1A3F_0 = matrix4d;
			double[] element = new double[16];
			arg_1A3F_0.Element = element;
			this.meshView1.CurrTransformation = matrix4d;
			this.meshView1.Dock = DockStyle.Fill;
			this.meshView1.Location = new Point(0, 0);
			this.meshView1.Name = "meshView1";
			this.meshView1.Size = new Size(526, 411);
			this.meshView1.TabIndex = 3;
			this.meshView1.Text = "meshView1";
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(802, 458);
			base.Controls.Add(this.toolStripContainer1);
			base.KeyPreview = true;
			base.Name = "FormMain";
			this.Text = "Skeleton Extraction Demo";
			base.KeyDown += new KeyEventHandler(this.FormMain_KeyDown);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel1.PerformLayout();
			this.splitContainer2.Panel2.ResumeLayout(false);
			this.splitContainer2.Panel2.PerformLayout();
			this.splitContainer2.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPageModel.ResumeLayout(false);
			this.tabPageDisplay.ResumeLayout(false);
			this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
			this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
			this.toolStripContainer1.ContentPanel.ResumeLayout(false);
			this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
			this.toolStripContainer1.TopToolStripPanel.PerformLayout();
			this.toolStripContainer1.ResumeLayout(false);
			this.toolStripContainer1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			base.ResumeLayout(false);
		}
		public FormMain()
		{
			this.InitializeComponent();
			this.propertyGridDisplay.SelectedObject = Program.displayProperty;
			this.toolStripButtonViewingTool.Checked = true;
			this.toolStripButtonSelectionTool.Checked = false;
			this.toolStripButtonMovingTool.Checked = false;
			this.labelVertexCount.Text = "";
		}
		public void PrintText(string s)
		{
			if (this.textBoxOutput.InvokeRequired)
			{
				FormMain.SetTextCallback d = new FormMain.SetTextCallback(this.PrintText);
				base.Invoke(d, new object[]
				{
					s
				});
				return;
			}
			this.textBoxOutput.AppendText(s + Environment.NewLine);
			this.toolStripStatusLabel1.Text = s;
		}
		public void Print3DText(Vector3d pos, string s)
		{
			OpenGL.glRasterPos3d(pos.x, pos.y, pos.z);
			OpenGL.glPushAttrib(131072u);
			OpenGL.glListBase(this.meshView1.fontBase);
			GL.glCallLists(s.Length, 5123u, s);
			OpenGL.glPopAttrib();
		}
		public void OpenMeshFile()
		{
			this.openFileDialog1.FileName = "";
			this.openFileDialog1.Filter = "Mesh files (*.obj)|*.obj";
			this.openFileDialog1.CheckFileExists = true;
			DialogResult ret = this.openFileDialog1.ShowDialog(this);
			if (ret == DialogResult.OK)
			{
				StreamReader sr = new StreamReader(this.openFileDialog1.FileName);
				Mesh i = new Mesh(sr);
				sr.Close();
				MeshRecord rec = new MeshRecord(this.openFileDialog1.FileName, i);
				this.meshes.Add(rec);
				this.currentMeshRecord = rec;
				TabPage page = new TabPage(rec.ToString());
				page.Tag = rec;
				this.tabControlModelList.TabPages.Add(page);
				this.tabControlModelList.SelectedTab = page;
				this.meshView1.SetModel(rec);
				this.propertyGridModel.SelectedObject = rec;
				this.PrintText("Loaded mesh " + this.openFileDialog1.FileName);
			}
		}
		public void SaveMeshFile()
		{
			if (this.currentMeshRecord == null || this.currentMeshRecord.Mesh == null)
			{
				return;
			}
			this.saveFileDialog1.FileName = "";
			this.saveFileDialog1.Filter = "Mesh files (*.obj)|*.obj";
			this.saveFileDialog1.OverwritePrompt = true;
			DialogResult ret = this.saveFileDialog1.ShowDialog(this);
			if (ret == DialogResult.OK)
			{
				StreamWriter sw = new StreamWriter(this.saveFileDialog1.FileName);
				this.currentMeshRecord.Mesh.Write(sw);
				sw.Close();
				this.PrintText("Saved mesh " + this.saveFileDialog1.FileName + "\n");
			}
		}
		public void OpenCameraFile()
		{
			this.openFileDialog1.FileName = "";
			this.openFileDialog1.Filter = "Camera files (*.camera)|*.camera";
			this.openFileDialog1.CheckFileExists = true;
			DialogResult ret = this.openFileDialog1.ShowDialog(this);
			if (ret == DialogResult.OK)
			{
				StreamReader sr = new StreamReader(this.openFileDialog1.FileName);
				XmlSerializer xs = new XmlSerializer(typeof(Matrix4d));
				this.meshView1.CurrTransformation = (Matrix4d)xs.Deserialize(sr);
				sr.Close();
				this.PrintText("Loaded camera " + this.openFileDialog1.FileName + "\n");
			}
		}
		public void SaveCameraFile()
		{
			this.saveFileDialog1.FileName = "";
			this.saveFileDialog1.Filter = "Camera files (*.camera)|*.camera";
			this.saveFileDialog1.OverwritePrompt = true;
			DialogResult ret = this.saveFileDialog1.ShowDialog(this);
			if (ret == DialogResult.OK)
			{
				StreamWriter sw = new StreamWriter(this.saveFileDialog1.FileName);
				XmlSerializer xs = new XmlSerializer(typeof(Matrix4d));
				xs.Serialize(sw, this.meshView1.CurrTransformation);
				sw.Close();
				this.PrintText("Saved camera " + this.saveFileDialog1.FileName + "\n");
			}
		}
		public void OpenSelectionFile()
		{
			if (this.currentMeshRecord == null || this.currentMeshRecord.Mesh == null)
			{
				return;
			}
			this.openFileDialog1.FileName = "";
			this.openFileDialog1.Filter = "Selection files (*.sel)|*.sel";
			this.openFileDialog1.CheckFileExists = true;
			DialogResult ret = this.openFileDialog1.ShowDialog(this);
			if (ret == DialogResult.OK)
			{
				StreamReader sr = new StreamReader(this.openFileDialog1.FileName);
				XmlSerializer xs = new XmlSerializer(typeof(byte[]));
				this.currentMeshRecord.Mesh.Flag = (byte[])xs.Deserialize(sr);
				sr.Close();
				this.PrintText("Loaded selection " + this.openFileDialog1.FileName + "\n");
			}
		}
		public void SaveSelectionFile()
		{
			if (this.currentMeshRecord == null || this.currentMeshRecord.Mesh == null)
			{
				return;
			}
			this.saveFileDialog1.FileName = "";
			this.saveFileDialog1.Filter = "Selection files (*.sel)|*.sel";
			this.saveFileDialog1.OverwritePrompt = true;
			DialogResult ret = this.saveFileDialog1.ShowDialog(this);
			if (ret == DialogResult.OK)
			{
				StreamWriter sw = new StreamWriter(this.saveFileDialog1.FileName);
				XmlSerializer xs = new XmlSerializer(typeof(byte[]));
				xs.Serialize(sw, this.currentMeshRecord.Mesh.Flag);
				sw.Close();
				this.PrintText("Saved selection " + this.saveFileDialog1.FileName + "\n");
			}
		}
		public void CreateIsolineDeformer()
		{
			if (this.currentMeshRecord != null)
			{
				this.currentMeshRecord.Deformer = new IsolineDeformer(this.currentMeshRecord.Mesh);
				this.propertyGridModel.SelectedObject = this.currentMeshRecord;
				this.propertyGridModel.Refresh();
			}
		}
		public void CreateDualLaplacianDeformer()
		{
			if (this.currentMeshRecord != null)
			{
				this.currentMeshRecord.Deformer = new DualLaplacianDeformer(this.currentMeshRecord.Mesh);
				this.propertyGridModel.SelectedObject = this.currentMeshRecord;
				this.propertyGridModel.Refresh();
			}
		}
		public void CreateGeneralReducedDeformer()
		{
			if (this.currentMeshRecord != null)
			{
				this.openFileDialog1.FileName = "";
				this.openFileDialog1.Filter = "Mesh segmentation (*.seg)|*.seg";
				this.openFileDialog1.CheckFileExists = true;
				DialogResult ret = this.openFileDialog1.ShowDialog(this);
				if (ret == DialogResult.OK)
				{
					StreamReader sr = new StreamReader(this.openFileDialog1.FileName);
					MeshSegmentation seg = new MeshSegmentation(sr);
					sr.Close();
					this.PrintText("Loaded mesh segmentation " + this.openFileDialog1.FileName + "\n");
					GeneralReducedDeformer.Option opt = new GeneralReducedDeformer.Option();
					FormInput f = new FormInput(opt);
					DialogResult ret2 = f.ShowDialog(this);
					if (seg != null && ret2 == DialogResult.OK)
					{
						this.currentMeshRecord.Deformer = new GeneralReducedDeformer(this.currentMeshRecord.Mesh, seg, opt);
						this.propertyGridModel.SelectedObject = this.currentMeshRecord;
						this.propertyGridModel.Refresh();
					}
				}
			}
		}
		public void CloseTab()
		{
			if (this.tabControlModelList.SelectedTab != null)
			{
				this.tabControlModelList.TabPages.Remove(this.tabControlModelList.SelectedTab);
			}
		}
		private void toolStripSplitButtonOpen_ButtonClick(object sender, EventArgs e)
		{
			this.OpenMeshFile();
		}
		private void toolStripButtonViewingTool_Click(object sender, EventArgs e)
		{
			this.toolStripButtonViewingTool.Checked = true;
			this.toolStripButtonSelectionTool.Checked = false;
			this.toolStripButtonMovingTool.Checked = false;
			Program.currentMode = Program.EnumOperationMode.Viewing;
		}
		private void toolStripButtonSelectionTool_Click(object sender, EventArgs e)
		{
			this.toolStripButtonViewingTool.Checked = false;
			this.toolStripButtonSelectionTool.Checked = true;
			this.toolStripButtonMovingTool.Checked = false;
			Program.currentMode = Program.EnumOperationMode.Selection;
		}
		private void toolStripButtonMovingTool_Click(object sender, EventArgs e)
		{
			this.toolStripButtonViewingTool.Checked = false;
			this.toolStripButtonSelectionTool.Checked = false;
			this.toolStripButtonMovingTool.Checked = true;
			Program.currentMode = Program.EnumOperationMode.Moving;
		}
		private void toolStripSplitButtonCreateDeformer_ButtonClick(object sender, EventArgs e)
		{
			this.CreateGeneralReducedDeformer();
		}
		private void toolStripButtonAutoUpdate_Click(object sender, EventArgs e)
		{
			this.toolStripButtonAutoUpdate.Checked = !this.toolStripButtonAutoUpdate.Checked;
			this.timer1.Enabled = this.toolStripButtonAutoUpdate.Checked;
		}
		private void toolStripButtonAbout_Click(object sender, EventArgs e)
		{
			AboutBox1 box = new AboutBox1();
			box.ShowDialog(this);
		}
		private void toolStripButtonExit_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}
		private void toolStripSplitButtonSkeletonization_ButtonClick(object sender, EventArgs e)
		{
			if (this.currentMeshRecord != null)
			{
				Skeletonizer.Options opt = new Skeletonizer.Options();
				opt.LaplacianConstraintWeight = 1.0 / (10.0 * Math.Sqrt(this.currentMeshRecord.Mesh.AverageFaceArea()));
				FormInput f = new FormInput(opt);
				DialogResult ret = f.ShowDialog(this);
				if (ret == DialogResult.OK)
				{
					this.currentMeshRecord.Skeletonizer = new Skeletonizer(this.currentMeshRecord.Mesh, opt);
					ParameterizedThreadStart s = new ParameterizedThreadStart(this.ThreadStartSkeletonization);
					new Thread(s)
					{
						Priority = ThreadPriority.Lowest
					}.Start(this.currentMeshRecord.Skeletonizer);
				}
			}
		}
		private void ThreadStartSkeletonization(object data)
		{
			Skeletonizer s = data as Skeletonizer;
			if (s == null)
			{
				return;
			}
			s.Start();
		}
		private void openMeshFileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.OpenMeshFile();
		}
		private void openCameraFileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.OpenCameraFile();
		}
		private void openSelectionFileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.OpenSelectionFile();
		}
		private void saveMeshFileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.SaveMeshFile();
		}
		private void saveCameraFileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.SaveCameraFile();
		}
		private void saveSelectionFileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.SaveSelectionFile();
		}
		private void saveSegmentationToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (this.currentMeshRecord == null || this.currentMeshRecord.Mesh == null || this.currentMeshRecord.Skeletonizer == null)
			{
				return;
			}
			this.saveFileDialog1.FileName = "";
			this.saveFileDialog1.Filter = "Mesh segmentation (*.seg)|*.seg";
			this.saveFileDialog1.OverwritePrompt = true;
			DialogResult ret = this.saveFileDialog1.ShowDialog(this);
			if (ret == DialogResult.OK)
			{
				StreamWriter sw = new StreamWriter(this.saveFileDialog1.FileName);
				this.currentMeshRecord.Skeletonizer.WriteSegmentation(sw);
				sw.Close();
				this.PrintText("Saved mesh segmentation " + this.saveFileDialog1.FileName + "\n");
			}
		}
		private void createDualLaplacianDeformerToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.CreateDualLaplacianDeformer();
		}
		private void createGeneralReducedDeformerToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.CreateGeneralReducedDeformer();
		}
		private void createIsolineDeformerToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.CreateIsolineDeformer();
		}
		private void buttonShowHideOutput_Click(object sender, EventArgs e)
		{
			this.splitContainer2.Panel2Collapsed = !this.splitContainer2.Panel2Collapsed;
		}
		private void buttonShowHideProperty_Click(object sender, EventArgs e)
		{
			this.splitContainer1.Panel2Collapsed = !this.splitContainer1.Panel2Collapsed;
		}
		private void buttonClearOutputText_Click(object sender, EventArgs e)
		{
			this.textBoxOutput.Clear();
		}
		private void buttonCloseTab_Click(object sender, EventArgs e)
		{
			this.CloseTab();
		}
		private void tabControlModelList_Selected(object sender, TabControlEventArgs e)
		{
			if (this.tabControlModelList.SelectedTab != null)
			{
				MeshRecord rec = (MeshRecord)this.tabControlModelList.SelectedTab.Tag;
				this.meshView1.SetModel(rec);
				this.propertyGridModel.SelectedObject = rec;
				this.currentMeshRecord = rec;
				return;
			}
			this.meshView1.SetModel(null);
			this.propertyGridModel.SelectedObject = null;
			this.currentMeshRecord = null;
		}
		private void FormMain_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Control)
			{
				Keys keyCode = e.KeyCode;
				if (keyCode != Keys.O)
				{
					return;
				}
				this.OpenMeshFile();
				return;
			}
			else
			{
				Keys keyCode2 = e.KeyCode;
				switch (keyCode2)
				{
				case Keys.F1:
					this.OpenMeshFile();
					return;
				case Keys.F2:
					Program.currentMode = Program.EnumOperationMode.Viewing;
					return;
				case Keys.F3:
					Program.currentMode = Program.EnumOperationMode.Selection;
					return;
				case Keys.F4:
					Program.currentMode = Program.EnumOperationMode.Moving;
					return;
				case Keys.F5:
					this.CreateIsolineDeformer();
					return;
				default:
					if (keyCode2 != Keys.F12)
					{
						return;
					}
					if (this.currentMeshRecord != null && this.currentMeshRecord.Deformer != null)
					{
						this.currentMeshRecord.Deformer.Deform();
						this.meshView1.Refresh();
					}
					return;
				}
			}
		}
		private void timer1_Tick(object sender, EventArgs e)
		{
			if (Program.currentMode == Program.EnumOperationMode.Moving && this.toolStripButtonAutoUpdate.Checked && this.currentMeshRecord != null && this.currentMeshRecord.Deformer != null)
			{
				this.currentMeshRecord.Deformer.Deform();
				this.currentMeshRecord.Deformer.Update();
			}
			if (this.currentMeshRecord != null && this.currentMeshRecord.Skeletonizer != null && this.currentMeshRecord.Skeletonizer.NodeCount != 0)
			{
				this.labelVertexCount.Text = "Vertex #: " + this.currentMeshRecord.Skeletonizer.NodeCount;
			}
			this.meshView1.Refresh();
		}
		private void toolStripButtonSegmentation_Click(object sender, EventArgs e)
		{
			if (this.currentMeshRecord != null && this.currentMeshRecord.Mesh != null && this.currentMeshRecord.Skeletonizer != null)
			{
				Skeletonizer.SegmentationOpt opt = new Skeletonizer.SegmentationOpt();
				FormInput f = new FormInput(opt);
				DialogResult ret = f.ShowDialog(this);
				if (ret == DialogResult.OK)
				{
					this.currentMeshRecord.Skeletonizer.Segmentation(opt);
				}
			}
		}
	}
}
