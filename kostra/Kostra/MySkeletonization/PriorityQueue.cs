using System;
namespace System.Collections.Generic
{
	public class PriorityQueue
	{
		public List<PriorityQueueElement> data;
		public PriorityQueue()
		{
			this.data = new List<PriorityQueueElement>();
		}
		public PriorityQueue(int initSize)
		{
			this.data = new List<PriorityQueueElement>(initSize);
		}
		public bool IsEmpty()
		{
			return this.data.Count == 0;
		}
		public void Clear()
		{
			this.data.Clear();
		}
		public void Insert(PriorityQueueElement obj)
		{
			int hole = this.data.Count;
			this.data.Add(obj);
			while (hole > 0 && obj.CompareTo(this.data[(hole - 1) / 2]) < 0)
			{
				this.data[hole] = this.data[(hole - 1) / 2];
				this.data[hole].PQIndex = hole;
				hole = (hole - 1) / 2;
			}
			this.data[hole] = obj;
			obj.PQIndex = hole;
		}
		public PriorityQueueElement DeleteMin()
		{
			if (this.IsEmpty())
			{
				throw new Exception();
			}
			PriorityQueueElement obj = this.data[0];
			this.data[0] = this.data[this.data.Count - 1];
			this.data.RemoveAt(this.data.Count - 1);
			if (this.data.Count > 0)
			{
				this.data[0].PQIndex = 0;
				this.PercolateDown(0);
			}
			return obj;
		}
		public PriorityQueueElement GetMin()
		{
			if (this.IsEmpty())
			{
				throw new Exception();
			}
			return this.data[0];
		}
		public void Update(PriorityQueueElement obj)
		{
			this.PercolateUp(obj.PQIndex);
			this.PercolateDown(obj.PQIndex);
		}
		private void PercolateUp(int hole)
		{
			PriorityQueueElement obj = this.data[hole];
			while (hole > 0 && obj.CompareTo(this.data[(hole - 1) / 2]) < 0)
			{
				int parent = (hole - 1) / 2;
				PriorityQueueElement parentObj = this.data[parent];
				this.data[hole] = this.data[parent];
				parentObj.PQIndex = hole;
				hole = parent;
			}
			this.data[hole] = obj;
			obj.PQIndex = hole;
		}
		private void PercolateDown(int hole)
		{
			PriorityQueueElement obj = this.data[hole];
			while (hole * 2 + 1 < this.data.Count)
			{
				int child = hole * 2 + 1;
				if (child != this.data.Count - 1 && this.data[child + 1].CompareTo(this.data[child]) < 0)
				{
					child++;
				}
				PriorityQueueElement childObj = this.data[child];
				if (childObj.CompareTo(obj) >= 0)
				{
					break;
				}
				this.data[hole] = childObj;
				childObj.PQIndex = hole;
				hole = child;
			}
			this.data[hole] = obj;
			obj.PQIndex = hole;
		}
	}
}
