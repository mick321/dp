using System;
namespace MyGeometry
{
	public struct Quaternion
	{
		private const double eps = 1E-05;
		public double s;
		public Vector3d v;
		public double this[int index]
		{
			get
			{
				if (index == 0)
				{
					return this.s;
				}
				return this.v[index - 1];
			}
			set
			{
				if (index == 0)
				{
					this.s = value;
				}
				this.v[index - 1] = value;
			}
		}
		public Quaternion(double s, Vector3d v)
		{
			this.s = s;
			this.v = v;
		}
		public Quaternion(double a, double b, double c, double d)
		{
			this.s = a;
			this.v.x = b;
			this.v.y = c;
			this.v.z = d;
		}
		public Quaternion(Matrix3d m)
		{
			double T = 1.0 + m.Trace();
			double X;
			double Y;
			double Z;
			double W;
			if (T > 1E-05)
			{
				double S = Math.Sqrt(T) * 2.0;
				X = (m[5] - m[7]) / S;
				Y = (m[6] - m[2]) / S;
				Z = (m[1] - m[3]) / S;
				W = 0.25 * S;
			}
			else
			{
				if (m[0] > m[4] && m[0] > m[8])
				{
					double S = Math.Sqrt(1.0 + m[0] - m[4] - m[8]) * 2.0;
					X = 0.25 * S;
					Y = (m[1] + m[3]) / S;
					Z = (m[6] + m[2]) / S;
					W = (m[5] - m[7]) / S;
				}
				else
				{
					if (m[4] > m[8])
					{
						double S = Math.Sqrt(1.0 + m[4] - m[0] - m[8]) * 2.0;
						X = (m[1] + m[3]) / S;
						Y = 0.25 * S;
						Z = (m[5] + m[7]) / S;
						W = (m[6] - m[2]) / S;
					}
					else
					{
						double S = Math.Sqrt(1.0 + m[8] - m[0] - m[4]) * 2.0;
						X = (m[6] + m[2]) / S;
						Y = (m[5] + m[7]) / S;
						Z = 0.25 * S;
						W = (m[1] - m[3]) / S;
					}
				}
			}
			this.s = W;
			this.v = new Vector3d(X, Y, Z);
		}
		public Matrix3d ToMatrix3d()
		{
			double xx = this.v.x * this.v.x;
			double xy = this.v.x * this.v.y;
			double xz = this.v.x * this.v.z;
			double xw = this.v.x * this.s;
			double yy = this.v.y * this.v.y;
			double yz = this.v.y * this.v.z;
			double yw = this.v.y * this.s;
			double zz = this.v.z * this.v.z;
			double zw = this.v.z * this.s;
			Matrix3d i = new Matrix3d();
			i[0] = 1.0 - 2.0 * (yy + zz);
			i[1] = 2.0 * (xy + zw);
			i[2] = 2.0 * (xz - yw);
			i[3] = 2.0 * (xy - zw);
			i[4] = 1.0 - 2.0 * (xx + zz);
			i[5] = 2.0 * (yz + xw);
			i[6] = 2.0 * (xz + yw);
			i[7] = 2.0 * (yz - xw);
			i[8] = 1.0 - 2.0 * (xx + yy);
			return i;
		}
		public Quaternion Conjugate()
		{
			return new Quaternion(this.s, -this.v.x, -this.v.y, -this.v.z);
		}
		public Quaternion Inverse()
		{
			double b = this.s * this.s + this.v.Dot(this.v);
			if (b != 0.0)
			{
				return new Quaternion(this.s / b, -this.v.x / b, -this.v.y / b, -this.v.z / b);
			}
			throw new DivideByZeroException();
		}
		public double Norm()
		{
			return Math.Sqrt(this.s * this.s + this.v.Dot(this.v));
		}
		public static Quaternion operator +(Quaternion a, Quaternion b)
		{
			return new Quaternion(a.s + b.s, a.v + b.v);
		}
		public static Quaternion operator -(Quaternion a, Quaternion b)
		{
			return new Quaternion(a.s - b.s, a.v - b.v);
		}
		public static Quaternion operator *(Quaternion a, Quaternion b)
		{
			return new Quaternion(a.s * b.s - a.v.Dot(b.v), a.s * b.v + b.s * a.v + a.v.Cross(b.v));
		}
		public static Quaternion operator *(Quaternion q, double d)
		{
			return new Quaternion(q.s * d, q.v * d);
		}
		public static Quaternion operator /(Quaternion q, double d)
		{
			return new Quaternion(q.s / d, q.v / d);
		}
		public static Quaternion slerp(Quaternion a, Quaternion b, double t)
		{
			double half_angle = Math.Acos(a.s * b.s + a.v.Dot(b.v));
			double sin_theta = Math.Sin(half_angle);
			double w = Math.Sin((1.0 - t) * half_angle) / sin_theta;
			double w2 = Math.Sin(t * half_angle) / sin_theta;
			return a * w + b * w2;
		}
	}
}
