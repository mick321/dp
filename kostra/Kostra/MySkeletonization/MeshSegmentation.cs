using MyGeometry;
using System;
using System.Collections.Generic;
using System.IO;
namespace IsolineEditing
{
	public class MeshSegmentation
	{
		private List<int>[] segments;
		private List<int>[] adjIndex;
		private int[] segmentIndex;
		private Vector3d[] nodePosition;
		public List<int>[] Segments
		{
			get
			{
				return this.segments;
			}
		}
		public List<int>[] AdjIndex
		{
			get
			{
				return this.adjIndex;
			}
		}
		public int[] SegmentIndex
		{
			get
			{
				return this.segmentIndex;
			}
		}
		public Vector3d[] NodePosition
		{
			get
			{
				return this.nodePosition;
			}
		}
		public MeshSegmentation(StreamReader sr)
		{
			char[] delimiters = new char[]
			{
				' ',
				'\t'
			};
			string s = sr.ReadLine();
			int maxIndex = int.Parse(s);
			s = sr.ReadLine();
			int vn = int.Parse(s);
			this.segmentIndex = new int[vn];
			this.segments = new List<int>[maxIndex];
			this.adjIndex = new List<int>[maxIndex];
			this.nodePosition = new Vector3d[maxIndex];
			for (int i = 0; i < maxIndex; i++)
			{
				this.segments[i] = new List<int>();
				this.adjIndex[i] = new List<int>();
				s = sr.ReadLine();
				string[] tokens = s.Split(delimiters);
				for (int j = 0; j < 3; j++)
				{
					if (tokens[j].Equals(""))
					{
						j--;
					}
					else
					{
						this.nodePosition[i][j] = double.Parse(tokens[j]);
					}
				}
				for (int k = 3; k < tokens.Length; k++)
				{
					if (!tokens[k].Equals(""))
					{
						int index = int.Parse(tokens[k]);
						this.adjIndex[i].Add(index);
					}
				}
			}
			for (int l = 0; l < vn; l++)
			{
				s = sr.ReadLine();
				int index2 = int.Parse(s);
				this.segmentIndex[l] = index2;
				this.segments[index2 - 1].Add(l);
			}
		}
	}
}
