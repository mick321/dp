using System;
using System.Collections.Generic;
namespace MyGeometry
{
	public class MatrixNd
	{
		private int m;
		private int n;
		private double[] e;
		public double this[int row, int column]
		{
			get
			{
				return this.e[row * this.n + column];
			}
			set
			{
				this.e[row * this.n + column] = value;
			}
		}
		public int RowSize
		{
			get
			{
				return this.m;
			}
		}
		public int ColumnSize
		{
			get
			{
				return this.n;
			}
		}
		public MatrixNd(int m, int n)
		{
			this.m = m;
			this.n = n;
			this.e = new double[m * n];
			for (int i = 0; i < m * n; i++)
			{
				this.e[i] = 0.0;
			}
		}
		public MatrixNd(SparseMatrix right) : this(right.RowSize, right.ColumnSize)
		{
			int b = 0;
			foreach (List<SparseMatrix.Element> row in right.Rows)
			{
				foreach (SparseMatrix.Element element in row)
				{
					this.e[b + element.j] = element.value;
				}
				b += this.n;
			}
		}
		public MatrixNd(SparseMatrix right, bool transpose) : this(right.ColumnSize, right.RowSize)
		{
			int b = 0;
			foreach (List<SparseMatrix.Element> col in right.Columns)
			{
				foreach (SparseMatrix.Element element in col)
				{
					this.e[b + element.i] = element.value;
				}
				b += this.n;
			}
		}
		public void Multiply(double[] xIn, double[] xOut)
		{
			if (xIn.Length < this.n || xOut.Length < this.m)
			{
				throw new ArgumentException();
			}
			int i = 0;
			int b = 0;
			while (i < this.m)
			{
				double sum = 0.0;
				for (int j = 0; j < this.n; j++)
				{
					sum += this.e[b + j] * xIn[j];
				}
				xOut[i] = sum;
				i++;
				b += this.n;
			}
		}
	}
}
