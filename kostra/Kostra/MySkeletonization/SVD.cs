using System;
namespace NumericalRecipes
{
	public class SVD
	{
		private int m;
		private int n;
		private bool state;
		private bool fullRank;
		private double[] w;
		private double[,] u;
		private double[,] v;
		private double[,] inverse;
		public int RowSize
		{
			get
			{
				return this.m;
			}
		}
		public int ColumnSize
		{
			get
			{
				return this.n;
			}
		}
		public bool State
		{
			get
			{
				return this.state;
			}
		}
		public bool FullRank
		{
			get
			{
				return this.fullRank;
			}
		}
		public double[,] Inverse
		{
			get
			{
				if (this.state && this.m == this.n && this.inverse == null)
				{
					this.inverse = new double[this.n, this.n];
					for (int i = 1; i <= this.n; i++)
					{
						for (int j = 1; j <= this.n; j++)
						{
							this.inverse[i - 1, j - 1] = 0.0;
							for (int k = 1; k <= this.n; k++)
							{
								if (this.w[k] != 0.0)
								{
									this.inverse[i - 1, j - 1] += this.v[i, k] * this.u[j, k] * (1.0 / this.w[k]);
								}
							}
						}
					}
				}
				return this.inverse;
			}
		}
		public double[,] OrthogonalFactor
		{
			get
			{
				if (this.state)
				{
					double[,] rot = new double[this.m, this.n];
					for (int i = 1; i <= this.m; i++)
					{
						for (int j = 1; j <= this.n; j++)
						{
							rot[i - 1, j - 1] = 0.0;
							for (int k = 1; k <= this.n; k++)
							{
								rot[i - 1, j - 1] += this.u[i, k] * this.v[j, k];
							}
						}
					}
					return rot;
				}
				return null;
			}
		}
		public SVD(double[,] A)
		{
			this.m = A.GetLength(0);
			this.n = A.GetLength(1);
			this.u = SVD.dmatrix(1, this.m, 1, this.n);
			this.w = SVD.dvector(1, this.n);
			this.v = SVD.dmatrix(1, this.n, 1, this.n);
			this.fullRank = true;
			for (int i = 0; i < this.m; i++)
			{
				for (int j = 0; j < this.n; j++)
				{
					this.u[i + 1, j + 1] = A[i, j];
				}
			}
			this.state = SVD.dsvdcmp(this.u, this.m, this.n, this.w, this.v);
			double max = 0.0;
			for (int i = 1; i <= this.n; i++)
			{
				if (this.w[i] > max)
				{
					max = this.w[i];
				}
			}
			double min = max * 1E-06;
			for (int i = 1; i <= this.n; i++)
			{
				if (this.w[i] < min)
				{
					this.w[i] = 0.0;
					this.fullRank = false;
				}
			}
		}
		public SVD(double[] A, int m, int n)
		{
			this.m = m;
			this.n = n;
			if (A.GetLength(0) < m * n)
			{
				throw new ArgumentException();
			}
			this.u = SVD.dmatrix(1, m, 1, n);
			this.w = SVD.dvector(1, n);
			this.v = SVD.dmatrix(1, n, 1, n);
			this.fullRank = true;
			for (int i = 0; i < m; i++)
			{
				for (int j = 0; j < n; j++)
				{
					this.u[i + 1, j + 1] = A[i * n + j];
				}
			}
			this.state = SVD.dsvdcmp(this.u, m, n, this.w, this.v);
			double max = 0.0;
			for (int i = 1; i <= n; i++)
			{
				if (this.w[i] > max)
				{
					max = this.w[i];
				}
			}
			double min = max * 1E-06;
			for (int i = 1; i <= n; i++)
			{
				if (this.w[i] < min)
				{
					this.w[i] = 0.0;
					this.fullRank = false;
				}
			}
		}
		public bool SolveX(double[] B, double[] X)
		{
			if (!this.state)
			{
				return false;
			}
			if (B.GetLength(0) < this.m)
			{
				return false;
			}
			if (X.GetLength(0) < this.n)
			{
				return false;
			}
			double[] b2 = SVD.dvector(1, this.m);
			double[] x2 = SVD.dvector(1, this.n);
			for (int i = 0; i < this.m; i++)
			{
				b2[i + 1] = B[i];
			}
			for (int i = 0; i < this.n; i++)
			{
				x2[i + 1] = X[i];
			}
			SVD.dsvbksb(this.u, this.w, this.v, this.m, this.n, b2, x2);
			for (int i = 0; i < this.n; i++)
			{
				X[i] = x2[i + 1];
			}
			return true;
		}
		public static bool Solve(double[,] a, double[] b, double[] x)
		{
			int i = a.GetLength(0);
			int j = a.GetLength(1);
			double[,] u = SVD.dmatrix(1, i, 1, j);
			double[] w = SVD.dvector(1, j);
			double[,] v = SVD.dmatrix(1, j, 1, j);
			double[] b2 = SVD.dvector(1, i);
			double[] x2 = SVD.dvector(1, j);
			for (int k = 0; k < i; k++)
			{
				for (int l = 0; l < j; l++)
				{
					u[k + 1, l + 1] = a[k, l];
				}
			}
			for (int k = 0; k < i; k++)
			{
				b2[k + 1] = b[k];
			}
			for (int k = 0; k < j; k++)
			{
				x2[k + 1] = x[k];
			}
			bool ok = SVD.dsvdcmp(u, i, j, w, v);
			if (ok)
			{
				double max = 0.0;
				for (int k = 1; k <= j; k++)
				{
					if (w[k] > max)
					{
						max = w[k];
					}
				}
				double min = max * 1E-12;
				for (int k = 1; k <= j; k++)
				{
					if (w[k] < min)
					{
						w[k] = 0.0;
					}
				}
				SVD.dsvbksb(u, w, v, i, j, b2, x2);
			}
			for (int k = 0; k < j; k++)
			{
				x[k] = x2[k + 1];
			}
			return ok;
		}
		private static double dpythag(double a, double b)
		{
			double absa = Math.Abs(a);
			double absb = Math.Abs(b);
			double tmp;
			if (absa > absb)
			{
				tmp = absb / absa;
				return absa * Math.Sqrt(1.0 + tmp * tmp);
			}
			tmp = absa / absb;
			if (absb != 0.0)
			{
				return absb * Math.Sqrt(1.0 + tmp * tmp);
			}
			return 0.0;
		}
		private static void dsvbksb(double[,] u, double[] w, double[,] v, int m, int n, double[] b, double[] x)
		{
			double[] tmp = SVD.dvector(1, n);
			for (int i = 1; i <= n; i++)
			{
				double s = 0.0;
				if (w[i] != 0.0)
				{
					for (int j = 1; j <= m; j++)
					{
						s += u[j, i] * b[j];
					}
					s /= w[i];
				}
				tmp[i] = s;
			}
			for (int i = 1; i <= n; i++)
			{
				double s = 0.0;
				for (int jj = 1; jj <= n; jj++)
				{
					s += v[i, jj] * tmp[jj];
				}
				x[i] = s;
			}
		}
		private static bool dsvdcmp(double[,] a, int m, int n, double[] w, double[,] v)
		{
			int i;
			int nm = i = 0;
			double[] rv = SVD.dvector(1, n);
			double anorm;
			double g;
			double scale = g = (anorm = 0.0);
			for (int j = 1; j <= n; j++)
			{
				i = j + 1;
				rv[j] = scale * g;
				double s = g = (scale = 0.0);
				if (j <= m)
				{
					for (int k = j; k <= m; k++)
					{
						scale += Math.Abs(a[k, j]);
					}
					if (scale != 0.0)
					{
						for (int k = j; k <= m; k++)
						{
							a[k, j] /= scale;
							s += a[k, j] * a[k, j];
						}
						double f = a[j, j];
						g = -SVD.Sign(Math.Sqrt(s), f);
						double h = f * g - s;
						a[j, j] = f - g;
						for (int l = i; l <= n; l++)
						{
							s = 0.0;
							for (int k = j; k <= m; k++)
							{
								s += a[k, j] * a[k, l];
							}
							f = s / h;
							for (int k = j; k <= m; k++)
							{
								a[k, l] += f * a[k, j];
							}
						}
						for (int k = j; k <= m; k++)
						{
							a[k, j] *= scale;
						}
					}
				}
				w[j] = scale * g;
				s = (g = (scale = 0.0));
				if (j <= m && j != n)
				{
					for (int k = i; k <= n; k++)
					{
						scale += Math.Abs(a[j, k]);
					}
					if (scale != 0.0)
					{
						for (int k = i; k <= n; k++)
						{
							a[j, k] /= scale;
							s += a[j, k] * a[j, k];
						}
						double f = a[j, i];
						g = -SVD.Sign(Math.Sqrt(s), f);
						double h = f * g - s;
						a[j, i] = f - g;
						for (int k = i; k <= n; k++)
						{
							rv[k] = a[j, k] / h;
						}
						for (int l = i; l <= m; l++)
						{
							s = 0.0;
							for (int k = i; k <= n; k++)
							{
								s += a[l, k] * a[j, k];
							}
							for (int k = i; k <= n; k++)
							{
								a[l, k] += s * rv[k];
							}
						}
						for (int k = i; k <= n; k++)
						{
							a[j, k] *= scale;
						}
					}
				}
				anorm = Math.Max(anorm, Math.Abs(w[j]) + Math.Abs(rv[j]));
			}
			for (int j = n; j >= 1; j--)
			{
				if (j < n)
				{
					if (g != 0.0)
					{
						for (int l = i; l <= n; l++)
						{
							v[l, j] = a[j, l] / a[j, i] / g;
						}
						for (int l = i; l <= n; l++)
						{
							double s = 0.0;
							for (int k = i; k <= n; k++)
							{
								s += a[j, k] * v[k, l];
							}
							for (int k = i; k <= n; k++)
							{
								v[k, l] += s * v[k, j];
							}
						}
					}
					for (int l = i; l <= n; l++)
					{
						v[j, l] = (v[l, j] = 0.0);
					}
				}
				v[j, j] = 1.0;
				g = rv[j];
				i = j;
			}
			for (int j = Math.Min(m, n); j >= 1; j--)
			{
				i = j + 1;
				g = w[j];
				for (int l = i; l <= n; l++)
				{
					a[j, l] = 0.0;
				}
				if (g != 0.0)
				{
					g = 1.0 / g;
					for (int l = i; l <= n; l++)
					{
						double s = 0.0;
						for (int k = i; k <= m; k++)
						{
							s += a[k, j] * a[k, l];
						}
						double f = s / a[j, j] * g;
						for (int k = j; k <= m; k++)
						{
							a[k, l] += f * a[k, j];
						}
					}
					for (int l = j; l <= m; l++)
					{
						a[l, j] *= g;
					}
				}
				else
				{
					for (int l = j; l <= m; l++)
					{
						a[l, j] = 0.0;
					}
				}
				a[j, j] += 1.0;
			}
			for (int k = n; k >= 1; k--)
			{
				int its = 1;
				while (its <= 30)
				{
					int flag = 1;
					for (i = k; i >= 1; i--)
					{
						nm = i - 1;
						if (Math.Abs(rv[i]) + anorm == anorm)
						{
							flag = 0;
							break;
						}
						if (Math.Abs(w[nm]) + anorm == anorm)
						{
							break;
						}
					}
					double z;
					if (flag != 0)
					{
						double c = 0.0;
						double s = 1.0;
						for (int j = i; j <= k; j++)
						{
							double f = s * rv[j];
							rv[j] = c * rv[j];
							if (Math.Abs(f) + anorm == anorm)
							{
								break;
							}
							g = w[j];
							double h = SVD.dpythag(f, g);
							w[j] = h;
							h = 1.0 / h;
							c = g * h;
							s = -f * h;
							for (int l = 1; l <= m; l++)
							{
								double y = a[l, nm];
								z = a[l, j];
								a[l, nm] = y * c + z * s;
								a[l, j] = z * c - y * s;
							}
						}
					}
					z = w[k];
					if (i == k)
					{
						if (z < 0.0)
						{
							w[k] = -z;
							for (int l = 1; l <= n; l++)
							{
								v[l, k] = -v[l, k];
							}
							break;
						}
						break;
					}
					else
					{
						if (its == 30)
						{
							return false;
						}
						double x = w[i];
						nm = k - 1;
						double y = w[nm];
						g = rv[nm];
						double h = rv[k];
						double f = ((y - z) * (y + z) + (g - h) * (g + h)) / (2.0 * h * y);
						g = SVD.dpythag(f, 1.0);
						f = ((x - z) * (x + z) + h * (y / (f + SVD.Sign(g, f)) - h)) / x;
						double c;
						double s = c = 1.0;
						for (int l = i; l <= nm; l++)
						{
							int j = l + 1;
							g = rv[j];
							y = w[j];
							h = s * g;
							g = c * g;
							z = SVD.dpythag(f, h);
							rv[l] = z;
							c = f / z;
							s = h / z;
							f = x * c + g * s;
							g = g * c - x * s;
							h = y * s;
							y *= c;
							for (int jj = 1; jj <= n; jj++)
							{
								x = v[jj, l];
								z = v[jj, j];
								v[jj, l] = x * c + z * s;
								v[jj, j] = z * c - x * s;
							}
							z = SVD.dpythag(f, h);
							w[l] = z;
							if (z != 0.0)
							{
								z = 1.0 / z;
								c = f * z;
								s = h * z;
							}
							f = c * g + s * y;
							x = c * y - s * g;
							for (int jj = 1; jj <= m; jj++)
							{
								y = a[jj, l];
								z = a[jj, j];
								a[jj, l] = y * c + z * s;
								a[jj, j] = z * c - y * s;
							}
						}
						rv[i] = 0.0;
						rv[k] = f;
						w[k] = x;
						its++;
					}
				}
			}
			return true;
		}
		private static double[] dvector(int nl, int nh)
		{
			return new double[nh + 1];
		}
		private static double[,] dmatrix(int nrl, int nrh, int ncl, int nch)
		{
			return new double[nrh + 1, nch + 1];
		}
		private static double Sign(double a, double b)
		{
			if (b < 0.0)
			{
				return -Math.Abs(a);
			}
			return Math.Abs(a);
		}
	}
}
