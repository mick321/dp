using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace IsolineEditing
{
	public class FormInput : Form
	{
		private IContainer components;
		private PropertyGrid propertyGrid;
		private Button buttonOK;
		private Button buttonCancel;
		public FormInput()
		{
			this.InitializeComponent();
		}
		public FormInput(object obj)
		{
			this.InitializeComponent();
			this.Text = obj.ToString();
			this.propertyGrid.SelectedObject = obj;
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}
		private void InitializeComponent()
		{
			this.propertyGrid = new PropertyGrid();
			this.buttonOK = new Button();
			this.buttonCancel = new Button();
			base.SuspendLayout();
			this.propertyGrid.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.propertyGrid.HelpVisible = false;
			this.propertyGrid.Location = new Point(0, 0);
			this.propertyGrid.Name = "propertyGrid";
			this.propertyGrid.Size = new Size(292, 238);
			this.propertyGrid.TabIndex = 0;
			this.buttonOK.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.buttonOK.DialogResult = DialogResult.OK;
			this.buttonOK.Location = new Point(124, 244);
			this.buttonOK.Name = "buttonOK";
			this.buttonOK.Size = new Size(75, 23);
			this.buttonOK.TabIndex = 1;
			this.buttonOK.Text = "&OK";
			this.buttonOK.UseVisualStyleBackColor = true;
			this.buttonCancel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.buttonCancel.DialogResult = DialogResult.Cancel;
			this.buttonCancel.Location = new Point(205, 244);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new Size(75, 23);
			this.buttonCancel.TabIndex = 2;
			this.buttonCancel.Text = "&Cancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			base.AcceptButton = this.buttonOK;
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.CancelButton = this.buttonCancel;
			base.ClientSize = new Size(292, 279);
			base.Controls.Add(this.buttonCancel);
			base.Controls.Add(this.buttonOK);
			base.Controls.Add(this.propertyGrid);
			base.FormBorderStyle = FormBorderStyle.SizableToolWindow;
			base.Name = "FormInput";
			base.StartPosition = FormStartPosition.CenterParent;
			this.Text = "FormInput";
			base.ResumeLayout(false);
		}
	}
}
