///
/// @file SkinnedMesh.h
/// @author Michal Zak
/// @brief Contains implementation and interface of mesh-skinning.
///

#pragma once
#include "MeshEmptyFilter.h"
#include "Assignment.h"
#include "Utils.h"

#define BONES_PER_VERTEX 4

/// Skinned mesh is specialization of vtkPolyDataAlgorithm class, it deforms given mesh accordingly to assigned skeleton.
class CSkinnedMesh : public vtkPolyDataAlgorithm
{
public:
private:
  /// Not implemented.
  CSkinnedMesh(const CSkinnedMesh&) : vtkPolyDataAlgorithm() { }
  /// Not implemented.
  void operator=(const CSkinnedMesh&) { }

  typedef unsigned short int UINT16;
  typedef unsigned int UINT32;

  /// Structure describing one vertex.
  struct TMeshVertex
  {
    double x, y, z;
    UINT16 boneIndices[BONES_PER_VERTEX];
    double boneWeights[BONES_PER_VERTEX];
    /// Default constructor.
    TMeshVertex() { memset(this, 0, sizeof(TMeshVertex)); }
  };

  CSkeleton skeletonRestPose;        //!< rest pose of skeleton
  CSkeleton skeletonCurrPose;        //!< current pose of skeleton
  std::vector<TMeshVertex> vertices; //!< array of vertices
  std::vector<UINT32> indices;       //!< array of indices

protected:
  /// Execution method.
  virtual void Execute(vtkPolyData* toPolydata);

  /// Default constructor.
  CSkinnedMesh(void);
  /// Default destructor.
  virtual ~CSkinnedMesh(void);
  /// Overridden function, this method does the process.
  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
public:
  bool ColorizeSegmentation = false; //!< flag: if set to true, output polydata will be colorized by their most important bone

  /// Copy skeleton to internal storage.
  inline void SetSkeletonRestPose(const CSkeleton& skeletonRef) { skeletonRestPose = skeletonRef; }
  /// Copy skeleton to internal storage.
  inline void SetSkeletonCurrPose(const CSkeleton& skeletonRef) { skeletonCurrPose = skeletonRef; }
  /// Copy mesh data to internal storage for future rendering.
  void SetMeshData(vtkPolyData* meshData);
  /// Compute skinning weights - must be called after SetSkeletonRestPose. Best: pass mesh data shrinked by Laplace smoothing.
  /// Returns true on success.
  bool ComputeWeights(vtkPolyData* meshData_LaplaceSmoothed);

  /// VTK macro.
  vtkTypeMacro(CSkinnedMesh, vtkPolyDataAlgorithm);
  /// Print info about this object.
  void PrintSelf(ostream& os, vtkIndent indent);
  /// Create new instance.
  static CSkinnedMesh* New();

  /// Get vertex count of this mesh.
  inline const int GetVertexCount() const { return (int)vertices.size(); }
  /// Get triangle count of this mesh.
  inline const int GetTriangleCount() const { return (int)indices.size() / 3; }
};

