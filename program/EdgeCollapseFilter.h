///
/// @file EdgeCollapseFilter.h
/// @author Michal Zak
/// @brief EdgeCollapseFilter class, filter implementing edge collapse (decimating mesh).
///

#pragma once
#include "meshemptyfilter.h"
#include "PriorityQueue.h"
#include "Assignment.h"

// ------------------------------------------------------------------------------
/// Filter implementing edge collapse (decimating mesh).
//
// Warning - we use only positions and faces, edges in structures are not used nor updated during process
class EdgeCollapseFilter : public MeshEmptyFilter
{
private:
  std::vector<std::vector<int> > mergeList; ///< in each edge collapse, we remember which all vertices are collapsed in one

  CPriorityQueue<double, int> queue; ///< priority queue
  
  std::vector<Eigen::Matrix4d, aligned_allocator<Eigen::Matrix4d, 16> > vertMatrixQ; ///< shape cost matrix for each vertex
  std::vector<int>             vertMinErrIndex; ///< array of least significant edges - from [i] to v[i]

  /// Compute shape cost matrix for vertex[index].
  void ComputeVertMatrixQ(const int index);
  /// Compute priority of vertex edge (determined by its least valuable edge).
  double ComputeVertexPriority(const int index);

  /// Move vertices of skeleton to centroids of original vertex groups.
  void MoveVertsToCentroids();

protected:
  CSkeleton skeleton;  //!< generated skeleton

  /// Default constructor.
  EdgeCollapseFilter(void);
  /// Default destructor.
  ~EdgeCollapseFilter(void);

  /// Copy data from internal structures.
  virtual void DoneMesh(vtkPolyData* toPolydata);
  /// Execution of edge collapse filter.
  virtual void Execute();
public:
  std::vector<CVertex>* originalVertexes; ///< pointer to vertexes before Laplace transform

  bool refinement;  ///< flag: enable final refinement

  /// VTK macro.
  vtkTypeMacro(EdgeCollapseFilter, MeshEmptyFilter);
  /// Print info about this object.
  void PrintSelf(ostream& os, vtkIndent indent);
  /// Create new instance.
  static EdgeCollapseFilter *New();

  /// Obtain internally stored skeleton.
  const CSkeleton& GetSkeleton() { return skeleton; }
};


// ------------------------------------------------------------------------------