#pragma once
#include "stdafx.h"

/// Structure defining application settings.
struct TAppSettings
{
  std::string inputFilename;       //!< file name containing mesh to be processed
  int maxitercount = 6;            //!< maximum number of iterations
  double poscoeff = 0.5;           //!< laplacian smoothing - factor of staying at place
  double areacoef = 1.0;           //!< laplacian smoothing - factor of influence of initial triangle areas
  double sL = 2.5;                 //!< laplacian smoothing - multiplication of smoothing strength after each iteration
  double volumeCoef = 1e-5;        //!< laplacian smoothing - minimal voume stopping computation
  double keep_faces_perces = 0;    //!< edge collapse debugging - keep given percent of faces (0-1)

  double edgeCollapse_weightShapeCost = 0.1;       ///< determines penalizing collapse with "wrong shape", default = 0.1
  double edgeCollapse_weightSamplingCost = 1;    ///< determines penalizing collapse with long edges, default = 1

  double matchCoeffPos = 0.01;     //!< skeleton leaf matching - err weight of position differences
  double matchCoeffDWT = 1;        //!< skeleton leaf matching - err weight of DWT result matching geodesic distances
  double matchCoeffDegreeDiff = 0.1;   //!< skeleton internal nodes matching - err weight of degree differences
  double matchCoeffInterPos = 0;       //!< skeleton internal nodes matching - err weight of position differences
  double matchCoeffExcludedCost = 10;  //!< skeleton internal nodes matching - err weight of excluded nodes
  
  double templateSkeletonScalingFactor = 1; //!< template skeleton additional resizement, should be "1"

  int ik_maxitercount = 300;        //!< inverse kinematics - maximum number of iterations
  bool ik_allowStretching = false;  //!< inverse kinematics - allow mode with stretching

  /// Default constructor.
  inline TAppSettings() : inputFilename("../data/human.obj") { }
};

// Application settings (all values in one structure).
extern TAppSettings settings;
/// Default address of template skeleton.
extern const char* skeletonDefaultAddr;

// ------------------------------------------------------------------------------

//! Clamp given value to fit in inverval <minimum, maximum>.
template <typename T>
inline const T clamp(const T& value, const T& minimum, const T& maximum)
{
  if (value < minimum)
    return minimum;
  else if (value > maximum)
    return maximum;
  else
    return value;
}

//! Helper function finding element in vector.
template <typename T>
inline bool isInVector(const std::vector<T>& vec, const T& value)
{
  return std::find(vec.begin(), vec.end(), value) != vec.end();
}

//! Helper function adding element to vector only if it is not present yet.
template <typename T>
inline bool addToVectorIfMissing(std::vector<T>& vec, const T& value)
{
  if (!isInVector(vec, value))
  {
    vec.push_back(value);
    return true;
  }
  return false;
}

/// Helper function removing element from vector.
/// @return  returns true on success, false if elements is not in vector
template <typename T>
inline bool removeFromVector(std::vector<T>& vec, const T& value)
{
  size_t lastsize = vec.size();
  vec.erase(std::remove(vec.begin(), vec.end(), value), vec.end());
  return lastsize != vec.size();
}

/// Squared distance of point from line segment.
inline double distanceFromLineSegmentSq(const Eigen::Vector3d& point, const Eigen::Vector3d& linePointA, const Eigen::Vector3d& linePointB)
{
  Eigen::Vector3d capsuleDirVecN = linePointB - linePointA;
  float capsuleDirLen = capsuleDirVecN.norm();
  capsuleDirVecN /= capsuleDirLen;
  float t = capsuleDirVecN.dot(point - linePointA);
  if (t < 0.0)
    t = 0.0;
  else if (t > capsuleDirLen)
    t = capsuleDirLen;

  Eigen::Vector3d projectedPoint = linePointA + capsuleDirVecN * t;
  return (projectedPoint - point).squaredNorm();
}

/// Distance of point from line segment.
inline double distanceFromLineSegment(const Eigen::Vector3d& point, const Eigen::Vector3d& linePointA, const Eigen::Vector3d& linePointB)
{
  return sqrt(distanceFromLineSegmentSq(point, linePointA, linePointB));
}

/// Axis oriented bounding box.
class CBoundBox
{
public:
  double minx, miny, minz;
  double maxx, maxy, maxz;

  /// Default constructor.
  inline CBoundBox()
  {
    minx = miny = minz = DBL_MAX;
    maxx = maxy = maxz = DBL_MIN;
  }

  /// Add point to bounding box volume, resize bounding box if necessary.
  inline void Join(double x, double y, double z)
  {
    minx = std::min(x, minx);
    maxx = std::max(x, maxx);
    miny = std::min(y, miny);
    maxy = std::max(y, maxy);
    minz = std::min(z, minz);
    maxz = std::max(z, maxz);
  }

  /// Obtain center of bouding box.
  inline void GetCenter(double& x, double& y, double& z) const
  {
    x = 0.5 * (minx + maxx);
    y = 0.5 * (miny + maxy);
    z = 0.5 * (minz + maxz);
  }
};

/**
* Allocator for aligned data.
*
* Modified from the Mallocator from Stephan T. Lavavej.
* <http://blogs.msdn.com/b/vcblog/archive/2008/08/28/the-mallocator.aspx>
*/
template <typename T, std::size_t Alignment>
class aligned_allocator
{
public:
  // The following will be the same for virtually all allocators.
  typedef T * pointer;
  typedef const T * const_pointer;
  typedef T& reference;
  typedef const T& const_reference;
  typedef T value_type;
  typedef std::size_t size_type;
  typedef ptrdiff_t difference_type;
  T * address(T& r) const
  {
    return &r;
  }
  const T * address(const T& s) const
  {
    return &s;
  }
  std::size_t max_size() const
  {
    // The following has been carefully written to be independent of
    // the definition of size_t and to avoid signed/unsigned warnings.
    return (static_cast<std::size_t>(0) - static_cast<std::size_t>(1)) / sizeof(T);
  }
  // The following must be the same for all allocators.
  template <typename U>
  struct rebind
  {
    typedef aligned_allocator<U, Alignment> other;
  };
  bool operator!=(const aligned_allocator& other) const
  {
    return !(*this == other);
  }
  void construct(T * const p, const T& t) const
  {
    void * const pv = static_cast<void *>(p);
    new (pv)T(t);
  }
  void destroy(T * const p) const
  {
    p->~T();
  }
  // Returns true if and only if storage allocated from *this
  // can be deallocated from other, and vice versa.
  // Always returns true for stateless allocators.
  bool operator==(const aligned_allocator& other) const
  {
    return true;
  }
  // Default constructor, copy constructor, rebinding constructor, and destructor.
  // Empty for stateless allocators.
  aligned_allocator() { }
  aligned_allocator(const aligned_allocator&) { }
  template <typename U> aligned_allocator(const aligned_allocator<U, Alignment>&) { }
  ~aligned_allocator() { }
  // The following will be different for each allocator.
  T * allocate(const std::size_t n) const
  {
    // The return value of allocate(0) is unspecified.
    // Mallocator returns NULL in order to avoid depending
    // on malloc(0)'s implementation-defined behavior
    // (the implementation can define malloc(0) to return NULL,
    // in which case the bad_alloc check below would fire).
    // All allocators can return NULL in this case.
    if (n == 0) {
      return NULL;
    }
    // All allocators should contain an integer overflow check.
    // The Standardization Committee recommends that std::length_error
    // be thrown in the case of integer overflow.
    if (n > max_size())
    {
      throw std::length_error("aligned_allocator<T>::allocate() - Integer overflow.");
    }
    // Mallocator wraps malloc().
    void * const pv = _mm_malloc(n * sizeof(T), Alignment);
    // Allocators should throw std::bad_alloc in the case of memory allocation failure.
    if (pv == NULL)
    {
      throw std::bad_alloc();
    }
    return static_cast<T *>(pv);
  }
  void deallocate(T * const p, const std::size_t n) const
  {
    _mm_free(p);
  }
  // The following will be the same for all allocators that ignore hints.
  template <typename U>
  T * allocate(const std::size_t n, const U * /* const hint */) const
  {
    return allocate(n);
  }
  // Allocators are not required to be assignable, so
  // all allocators should have a private unimplemented
  // assignment operator. Note that this will trigger the
  // off-by-default (enabled under /Wall) warning C4626
  // "assignment operator could not be generated because a
  // base class assignment operator is inaccessible" within
  // the STL headers, but that warning is useless.
private:
  aligned_allocator& operator=(const aligned_allocator&);
};

/// Color pallete used by visualization of segmentation.
static unsigned char colortable[] = {
  255, 0, 0,
  255, 125, 0,
  255, 255, 0,
  0, 255, 0,
  0, 255, 255,
  0, 125, 255,
  0, 0, 255,
  125, 0, 255,
  255, 0, 255,
  255, 0, 125,
  0, 0, 0,
  60, 60, 60,
  125, 125, 125,
  190, 190, 190,
  255, 255, 255,
  255, 60, 60,
  255, 125, 60,
  255, 255, 60,
  125, 255, 60,
  60, 255, 60,
  60, 255, 125,
  60, 255, 255,
  60, 125, 255,
  60, 60, 255,
  125, 60, 255,
  255, 60, 255,
  255, 60, 125,
};