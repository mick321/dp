///
/// @file Assignment.cpp
/// @author Michal Zak
///

#include "stdafx.h"
#include "Assignment.h"
#include "Utils.h"
#include "hungarian.h"

// --------------------------------------------------------------------------------------------------------------------

/// Helper structure, indices of nodes of one bone.
struct boneindices
{
  int headindex = -1;    //!< node index of bone head
  int tailindex = -1;    //!< node index of bone tail
};

// --------------------------------------------------------------------------------------------------------------------
bool CSkeleton::Load(const char* filename)
{
  std::vector<boneindices> bi;
  Clear();

  FILE* f = fopen(filename, "r");
  if (!f)
    return false;

  int bonecount;
  if (fscanf(f, "%d", &bonecount) != 1)
  {
    fclose(f);
    return false;
  }

  bi.resize(bonecount);
  this->nodes.reserve(bonecount + 1); // tree, v = e + 1  (does work for only one root node)
  for (int i = 0; i < bonecount; i++)
  {
    int parentBoneIndex;
    double headx, heady, headz;
    double tailx, taily, tailz;
    if (fscanf(f, "%d%lf%lf%lf%lf%lf%lf", &parentBoneIndex, &headx, &heady, &headz, &tailx, &taily, &tailz) != 7)
    {
      this->Clear();
      fclose(f);
      return false;
    }

    CSkeletonJointConstraint constraint;
    if (fscanf(f, "%lf%lf%lf%lf%lf%lf", &constraint.minx, &constraint.maxx, &constraint.miny, &constraint.maxy, &constraint.minz, &constraint.maxz) != 6)
    {
      this->Clear();
      fclose(f);
      return false;
    }

    Eigen::Matrix4d boneMatrix;
    for (int rowi = 0; rowi < 4; rowi++)
      for (int coli = 0; coli < 4; coli++)
      {
        double val;
        if (fscanf(f, "%lf", &val) != 1)
        {
          this->Clear();
          fclose(f);
          return false;
        }
        boneMatrix.coeffRef(rowi, coli) = val;
      }

    if (parentBoneIndex >= i)
    {
      this->Clear();
      fclose(f);
      return false;
    }

    if (parentBoneIndex == -1) 
    {
      // ignore root node!
      bi[i].headindex = -1;
      rootIndex = (int)this->nodes.size();
    }
    else
    {
      bi[i].headindex = bi[parentBoneIndex].tailindex;
    }

    bi[i].tailindex = (int)this->nodes.size();
    this->nodes.emplace_back();
    CSkeletonJoint& tailJoint = this->nodes[bi[i].tailindex];
    tailJoint.index = bi[i].tailindex;
    tailJoint.x = tailx;
    tailJoint.y = taily;
    tailJoint.z = tailz;

    tailJoint.matrixWorld_Parent = boneMatrix;
    tailJoint.constraint_Parent = constraint;

    boundbox.Join(tailx, taily, tailz);

    if (bi[i].headindex != -1)
    {
      CSkeletonJoint& headJoint = this->nodes[bi[i].headindex];
      headJoint.links.push_back(bi[i].tailindex);
      tailJoint.links.push_back(bi[i].headindex);
    }
  }

  fclose(f);

  MakeRootNode(rootIndex);

  return true;
}

// --------------------------------------------------------------------------------------------------------------------
void CSkeleton::ComputeDistanceMatrix()
{
  // Floyd-Warshall - initialize
  longestPath = 0;
  int matrixSize = (int)nodes.size();
  distanceMatrix.resize(matrixSize, matrixSize);
  distanceMatrix.fill(DBL_MAX);
  for (int i = 0; i < matrixSize; i++)
  {
    distanceMatrix.coeffRef(i, i) = 0;
    for (int j : nodes[i].links)
    {
      double dx = nodes[j].x - nodes[i].x;
      double dy = nodes[j].y - nodes[i].y;
      double dz = nodes[j].z - nodes[i].z;
      distanceMatrix.coeffRef(i, j) = sqrt(dx * dx + dy * dy + dz * dz);
      if (longestPath < distanceMatrix.coeff(i, j))
        longestPath = distanceMatrix.coeff(i, j);
    }
  }

  // Floyd-Warshall - process!
  for (int k = 0; k < matrixSize; k++)
    for (int i = 0; i < matrixSize; i++)
      for (int j = 0; j < matrixSize; j++)
        if (distanceMatrix.coeffRef(i, j) > distanceMatrix.coeffRef(i, k) + distanceMatrix.coeffRef(k, j))
        {
          distanceMatrix.coeffRef(i, j) = distanceMatrix.coeffRef(i, k) + distanceMatrix.coeffRef(k, j);
          if (longestPath < distanceMatrix.coeff(i, j))
            longestPath = distanceMatrix.coeff(i, j);
        }

  // finished, okay!
}

// --------------------------------------------------------------------------------------------------------------------
int CSkeleton::AddNode(double x, double y, double z)
{
  nodes.emplace_back();
  nodes.back().index = nodes.size() - 1;
  nodes.back().x = x;
  nodes.back().y = y;
  nodes.back().z = z;

  boundbox.Join(x, y, z);

  return nodes.size() - 1;
}

// --------------------------------------------------------------------------------------------------------------------
void CSkeleton::Connect(int node1, int node2)
{
  assert(node1 >= 0 && node2 >= 0 && node1 < (int)nodes.size() && node2 < (int)nodes.size());
  addToVectorIfMissing(nodes[node1].links, node2);
  addToVectorIfMissing(nodes[node2].links, node1);
}

// --------------------------------------------------------------------------------------------------------------------
double CSkeleton::ComputeValueDWT(Eigen::MatrixXd& mat1, Eigen::MatrixXd& mat2, int row1, int row2)
{
  std::vector<double> row;
  std::vector<double> rowLast;

  int length1 = mat1.cols();
  int length2 = mat2.cols();

  // initialize
  rowLast.resize(length1 + 1, DBL_MAX);
  row.resize(length1 + 1, DBL_MAX);
  rowLast[0] = 0;

  // process
  for (int j = 1; j <= length2; j++)
  {
    for (int i = 1; i <= length1; i++)
    {
      row[i] = abs(mat1.coeff(i - 1) - mat2.coeff(j - 1)) 
        + std::min(row[i - 1], std::min(rowLast[i - 1], rowLast[i]));
    }
    row.swap(rowLast); // new line (row)
    row[0] = DBL_MAX;
    rowLast[0] = DBL_MAX;
  }
  return rowLast.back(); // see above
}

// --------------------------------------------------------------------------------------------------------------------
CSkeleton CSkeleton::Match(const CSkeleton& skeletonGenerated, const CSkeleton& skeletonTemplate)
{
  if (skeletonTemplate.nodes.empty() || skeletonGenerated.nodes.empty())
    return CSkeleton();

  printf("Matching...\n");

  CSkeleton result = skeletonTemplate;
  std::vector<int> assignmentTemplateToGenerated;
  assignmentTemplateToGenerated.resize(result.nodes.size()); 

  // === match leaves === 
  // collect leaves
  std::vector<int> generatedLeafs;
  generatedLeafs.reserve(20);
  for (int i = 0; i < (int)skeletonGenerated.nodes.size(); i++)
    if (skeletonGenerated.nodes[i].links.size() == 1 /*&& skeletonGenerated.headIndex != i*/)
      generatedLeafs.push_back(i);
  std::vector<int> templateLeafs;
  templateLeafs.reserve(20);
  for (int i = 0; i < (int)skeletonTemplate.nodes.size(); i++)
    if (skeletonTemplate.nodes[i].links.size() == 1 /*&& skeletonTemplate.headIndex != i*/)
      templateLeafs.push_back(i);

  // prepare matrices for DWT
  Eigen::MatrixXd mDwtGenerated;
  mDwtGenerated.resize(generatedLeafs.size(), generatedLeafs.size());
  Eigen::MatrixXd mDwtTemplate;
  mDwtTemplate.resize(templateLeafs.size(), templateLeafs.size());

  for (int i = 0; i < (int)generatedLeafs.size(); i++)
  {
    int indexI = generatedLeafs[i];
    for (int j = 0; j < (int)generatedLeafs.size(); j++)
    {
      int indexJ = generatedLeafs[j];
      mDwtGenerated.coeffRef(i, j) = skeletonGenerated.distanceMatrix.coeff(indexI, indexJ);
    }
  }
  for (int i = 0; i < (int)templateLeafs.size(); i++)
  {
    int indexI = templateLeafs[i];
    for (int j = 0; j < (int)templateLeafs.size(); j++)
    {
      int indexJ = templateLeafs[j];
      mDwtTemplate.coeffRef(i, j) = skeletonTemplate.distanceMatrix.coeff(indexI, indexJ);
    }
  }
  printf("Leaf count in template skeleton: %d\n", (int)templateLeafs.size());
  // DWT to compute matrix for munkres 
  Eigen::MatrixXd matrixErr;
  matrixErr.resize(generatedLeafs.size(), templateLeafs.size());
  double absValMax = 0;
  for (int i = 0; i < (int)generatedLeafs.size(); i++)
  {
    for (int j = 0; j < (int)templateLeafs.size(); j++)
    {
      if (result.headIndex == templateLeafs[j] && skeletonGenerated.headIndex == generatedLeafs[i])
      {
        matrixErr.coeffRef(i, j) = 0;
        continue;
      }

      const CSkeletonJoint& jointTemplate = result.nodes[templateLeafs[j]];
      const CSkeletonJoint& jointGenerated = skeletonGenerated.nodes[generatedLeafs[i]];

      double value = ComputeValueDWT(mDwtGenerated, mDwtTemplate, i, j);
      
      double valuePos = 0;
      valuePos += (jointTemplate.x - jointGenerated.x) * (jointTemplate.x - jointGenerated.x);
      valuePos += (jointTemplate.y - jointGenerated.y) * (jointTemplate.y - jointGenerated.y);
      valuePos += (jointTemplate.z - jointGenerated.z) * (jointTemplate.z - jointGenerated.z);

      value = sqrt(settings.matchCoeffDWT * value * value + settings.matchCoeffPos * valuePos);
      
      matrixErr.coeffRef(i, j) = value;
      absValMax = abs(value) > absValMax ? abs(value) : absValMax;
    }
  }

  // -- hungarian method --
  double munkresRescaleToInt = 0.0001 * INT_MAX / absValMax;
  // allocate & copy prepared values
  int** distanceMatrix = (int**) new int[generatedLeafs.size()];
  for (int i = 0; i < (int)generatedLeafs.size(); i++)
  {
    distanceMatrix[i] = (int*) new int[templateLeafs.size()];
    for (int j = 0; j < (int)templateLeafs.size(); j++)
      distanceMatrix[i][j] = (int)(munkresRescaleToInt * matrixErr.coeff(i, j));
  }

  // libhungarian's turn
  hungarian_problem_t prob;
  hungarian_init(&prob, distanceMatrix, generatedLeafs.size(), templateLeafs.size(), HUNGARIAN_MODE_MINIMIZE_COST);
  hungarian_solve(&prob);

  std::vector<std::pair<int, int> > matchingLeaves;
  // taking the solution
  for (int i = 0; i < (int)generatedLeafs.size(); i++)
  {
    for (int j = 0; j < (int)templateLeafs.size(); j++)
    {
      if (prob.assignment[i][j] == HUNGARIAN_ASSIGNED)
      {
        matchingLeaves.push_back(std::make_pair(generatedLeafs[i], templateLeafs[j]));
        CSkeletonJoint& jointResult = result.nodes[templateLeafs[j]];
        const CSkeletonJoint& jointGenerated = skeletonGenerated.nodes[generatedLeafs[i]];
        jointResult.x = jointGenerated.x;
        jointResult.y = jointGenerated.y;
        jointResult.z = jointGenerated.z;

        if (templateLeafs[j] == skeletonTemplate.rootIndex) // update root
          result.rootIndex = templateLeafs[j];

        assignmentTemplateToGenerated[templateLeafs[j]] = generatedLeafs[i];

        break;
      }
    }
  }

  // cleanup
  hungarian_free(&prob);
  for (int i = 0; i < (int)generatedLeafs.size(); i++)
  {
    delete[] distanceMatrix[i];
  }
  delete[] distanceMatrix;

  // === prune tree === 
  std::vector<bool> generatedNodesExcluded;
  generatedNodesExcluded.resize(skeletonGenerated.nodes.size(), true);
  for (int k = 0; k < (int)matchingLeaves.size(); k++)
  {
    int currentIndex = matchingLeaves[k].first;
    printf("LEAF %d -> ", currentIndex);
    while (currentIndex != skeletonGenerated.headIndex)
    {
      double currentDistToHead = skeletonGenerated.distanceMatrix.coeff(currentIndex, skeletonGenerated.headIndex);

      generatedNodesExcluded[currentIndex] = false;
      int nextIndex = -1;
      double nextIndexVal = DBL_MAX;
      for (int i : skeletonGenerated.nodes[currentIndex].links)
      {
        double distToHead = skeletonGenerated.distanceMatrix.coeff(i, skeletonGenerated.headIndex);
        if (distToHead < nextIndexVal)
        {
          nextIndexVal = distToHead;
          nextIndex = i;
        }
      }

      if (currentDistToHead < nextIndexVal || nextIndex == -1)
        break;

      currentIndex = nextIndex;
      printf("%d -> ", currentIndex);
    }
    printf("\n");
  }

  // === match internal nodes === 
  std::vector<bool> alreadyAssignedGenerated;
  alreadyAssignedGenerated.resize(skeletonGenerated.nodes.size());
  for (int j = (int)skeletonTemplate.nodes.size() - 1; j >= 0; j--) // array is sorted by distance from head
  {
    if (isInVector(templateLeafs, j))
      continue;

    double errVal = DBL_MAX;
    int errI = 0;

    const CSkeletonJoint& jointTemplate = skeletonTemplate.nodes[j];
    CSkeletonJoint& jointResult = result.nodes[j];
    for (int i = 0; i < (int)skeletonGenerated.nodes.size(); i++)
    {
      if (alreadyAssignedGenerated[i])
        continue;

      const CSkeletonJoint& jointGenerated = skeletonGenerated.nodes[i];
      double currentErr = 0;

      // distance to leaves penalization
      for (int k = 0; k < (int)matchingLeaves.size(); k++)
      {
        double q1 = skeletonGenerated.distanceMatrix.coeff(i, matchingLeaves[k].first);
        double q2 = skeletonTemplate.distanceMatrix.coeff(j, matchingLeaves[k].second);
        currentErr += (q2 - q1) * (q2 - q1);
      }

      // degree difference penalization
      int degreeDiff = (int)jointGenerated.links.size() - (int)jointTemplate.links.size();
      currentErr += settings.matchCoeffDegreeDiff * degreeDiff * degreeDiff;

      // position penalization
      double valuePos = 0;
      valuePos += (jointTemplate.x - jointGenerated.x) * (jointTemplate.x - jointGenerated.x);
      valuePos += (jointTemplate.y - jointGenerated.y) * (jointTemplate.y - jointGenerated.y);
      valuePos += (jointTemplate.z - jointGenerated.z) * (jointTemplate.z - jointGenerated.z);
      currentErr += valuePos * settings.matchCoeffInterPos;

      // penalization of excluded points
      currentErr += generatedNodesExcluded[i] ? settings.matchCoeffExcludedCost : 0;

      //currentErr = sqrt(currentErr);
      if (currentErr < errVal)
      {
        errVal = currentErr;
        errI = i;
      }
    }

    const CSkeletonJoint& jointGenerated = skeletonGenerated.nodes[errI];
    printf("Inner node: matching j=%d to i=%d\n", j, errI);
    jointResult.x = jointGenerated.x;
    jointResult.y = jointGenerated.y;
    jointResult.z = jointGenerated.z;
    alreadyAssignedGenerated[errI] = true;

    assignmentTemplateToGenerated[j] = errI;
  }

  // find best root node (max degree)
  size_t bestRootIndex = 0;
  size_t maxNeighbors = 0;
  for (size_t i = 0; i < result.nodes.size(); i++)
  {
    if (result.nodes[i].links.size() > maxNeighbors)
    {
      maxNeighbors = result.nodes[i].links.size();
      bestRootIndex = i;
    }
  }
  result.MakeRootNode(bestRootIndex);

  // "copy" matrices from template
  for (int j = 0; j < (int)result.nodes.size(); j++)
  {
    int resultParentIndex = result.nodes[j].parentIndex;
    if (resultParentIndex == -1)
      continue;
    // translation part
    result.nodes[j].matrixWorld_Parent.coeffRef(0, 3) = result.nodes[resultParentIndex].x;
    result.nodes[j].matrixWorld_Parent.coeffRef(1, 3) = result.nodes[resultParentIndex].y;
    result.nodes[j].matrixWorld_Parent.coeffRef(2, 3) = result.nodes[resultParentIndex].z;

    // copy Y direction
    Eigen::Vector3d yDirResult(result.nodes[j].x - result.nodes[resultParentIndex].x,
      result.nodes[j].y - result.nodes[resultParentIndex].y,
      result.nodes[j].z - result.nodes[resultParentIndex].z);
    yDirResult.normalize();

    // compute Z direction as Z = cross(X, Y)
    Eigen::Vector3d xDirTemplate(skeletonTemplate.nodes[j].matrixWorld_Parent.coeffRef(0, 0),
      skeletonTemplate.nodes[j].matrixWorld_Parent.coeffRef(1, 0),
      skeletonTemplate.nodes[j].matrixWorld_Parent.coeffRef(2, 0));

    Eigen::Vector3d zDirResult = xDirTemplate.cross(yDirResult);

    // compute X direction as X = cross(Y, Z)
    Eigen::Vector3d xDirResult = yDirResult.cross(zDirResult);

    // set matrix rotation part
    result.nodes[j].matrixWorld_Parent.coeffRef(0, 0) = xDirResult.x();
    result.nodes[j].matrixWorld_Parent.coeffRef(1, 0) = xDirResult.y();
    result.nodes[j].matrixWorld_Parent.coeffRef(2, 0) = xDirResult.z();

    result.nodes[j].matrixWorld_Parent.coeffRef(0, 1) = yDirResult.x();
    result.nodes[j].matrixWorld_Parent.coeffRef(1, 1) = yDirResult.y();
    result.nodes[j].matrixWorld_Parent.coeffRef(2, 1) = yDirResult.z();

    result.nodes[j].matrixWorld_Parent.coeffRef(0, 2) = zDirResult.x();
    result.nodes[j].matrixWorld_Parent.coeffRef(1, 2) = zDirResult.y();
    result.nodes[j].matrixWorld_Parent.coeffRef(2, 2) = zDirResult.z();

    // fill rest of matrix
    result.nodes[j].matrixWorld_Parent.coeffRef(3, 0) = 0;
    result.nodes[j].matrixWorld_Parent.coeffRef(3, 1) = 0;
    result.nodes[j].matrixWorld_Parent.coeffRef(3, 2) = 0;
    result.nodes[j].matrixWorld_Parent.coeffRef(3, 3) = 1;

    // copy constraint
    result.nodes[j].constraint_Parent = skeletonTemplate.nodes[j].constraint_Parent;
  }

  return result;
}

// --------------------------------------------------------------------------------------------------------------------
int CSkeleton::FindHeadAndReorder()
{
  // find head index
  int headIndex = 0;
  for (int i = 0; i < (int)nodes.size(); i++)
    if (nodes[i].y > nodes[headIndex].y)
      headIndex = i;
  this->headIndex = headIndex;

  // sort
  std::sort(nodes.begin(), nodes.end(), 
    [&](const CSkeletonJoint& lhs, const CSkeletonJoint& rhs) 
  {
    double lhsDistToHead = distanceMatrix.coeff(lhs.index, headIndex);
    double rhsDistToHead = distanceMatrix.coeff(rhs.index, headIndex);
    return lhsDistToHead < rhsDistToHead;
  });

  // reindex
  std::vector<int> reindexationTable;
  reindexationTable.resize(nodes.size());
  for (int i = 0; i < (int)nodes.size(); i++)
    reindexationTable[nodes[i].index] = i;

  for (int i = 0; i < (int)nodes.size(); i++)
  {
    nodes[i].index = i;
    if (nodes[i].parentIndex != -1)
      nodes[i].parentIndex = reindexationTable[nodes[i].parentIndex];
    for (auto& linkIndex : nodes[i].links)
      linkIndex = reindexationTable[linkIndex];
  }
  this->headIndex = reindexationTable[headIndex];

  ComputeDistanceMatrix();
  MakeRootNode(headIndex);
  return headIndex;
}

// --------------------------------------------------------------------------------------------------------------------
void CSkeleton::Rescale(double scalingFactor, double newCenterX, double newCenterY, double newCenterZ, bool centerIsNodeHavingMaxDegree)
{
  double oldCenterX, oldCenterY, oldCenterZ;
  
  if (centerIsNodeHavingMaxDegree)
  {
    size_t maxdegreevalue = 0;
    size_t maxdegreeindex = 0;
    for (CSkeletonJoint& node : nodes)
      if (node.links.size() > maxdegreevalue)
      {
        maxdegreevalue = node.links.size();
        maxdegreeindex = node.index;
      }

    oldCenterX = nodes[maxdegreeindex].x;
    oldCenterY = nodes[maxdegreeindex].y;
    oldCenterZ = nodes[maxdegreeindex].z;
  }
  else
  {
    boundbox.GetCenter(oldCenterX, oldCenterY, oldCenterZ);
  }

  for (CSkeletonJoint& node : nodes)
  {
    node.x -= oldCenterX;
    node.y -= oldCenterY;
    node.z -= oldCenterZ;

    node.matrixWorld_Parent(0, 3) -= oldCenterX;
    node.matrixWorld_Parent(1, 3) -= oldCenterY;
    node.matrixWorld_Parent(2, 3) -= oldCenterZ;
  }


  for (CSkeletonJoint& node : nodes)
  {
    node.x *= scalingFactor;
    node.y *= scalingFactor;
    node.z *= scalingFactor;

    node.matrixWorld_Parent(0, 3) *= scalingFactor;
    node.matrixWorld_Parent(1, 3) *= scalingFactor;
    node.matrixWorld_Parent(2, 3) *= scalingFactor;
  }

  for (int i = 0; i < (int)nodes.size(); i++)
    for (int j = 0; j < (int)nodes.size(); j++)
      distanceMatrix.coeffRef(i, j) *= scalingFactor;

  longestPath *= scalingFactor;


  boundbox = CBoundBox();
  for (CSkeletonJoint& node : nodes)
  {
    node.x += newCenterX;
    node.y += newCenterY;
    node.z += newCenterZ;

    node.matrixWorld_Parent(0, 3) += newCenterX;
    node.matrixWorld_Parent(1, 3) += newCenterY;
    node.matrixWorld_Parent(2, 3) += newCenterZ;

    boundbox.Join(node.x, node.y, node.z);
  }
}

// --------------------------------------------------------------------------------------------------------------------
void CSkeleton::PrepareBones()
{
  bones.clear();
  std::vector<size_t> nodeToBone;
  nodeToBone.resize(nodes.size(), -1);

  for (size_t i = 0; i < nodes.size(); i++)
  {
    const CSkeletonJoint& node = nodes[i];
    if (node.parentIndex != -1)
    {
      CSkeletonBone newBone;
      nodeToBone[i] = bones.size();
      
      newBone.skeleton = this;
      newBone.worldTransform = node.matrixWorld_Parent;
      newBone.worldTransformInverted = newBone.worldTransform.inverse();
      newBone.nodeHead = node.parentIndex;
      newBone.nodeTail = i;
      bones.push_back(newBone);
    }
  }

  for (size_t i = 0; i < bones.size(); i++)
  {
    CSkeletonBone& bone = bones[i];
    size_t parentBoneIndex = nodeToBone[bone.nodeHead];
    if (parentBoneIndex == -1)
    {
      //bone.localTransformToParent = bone.worldTransform;
      bone.parentBone = -1;
    }
    else
    {
      //bone.localTransformToParent = bone.worldTransform * bones[parentBoneIndex].worldTransformInverted;
      bone.parentBone = parentBoneIndex;
    }
  }
}

// --------------------------------------------------------------------------------------------------------------------
void CSkeleton::MakeRootNode(int index)
{
  rootIndex = index;
  for (CSkeletonJoint& node : nodes)
    node.parentIndex = -1;

  Traverse(index, -1);
}

// --------------------------------------------------------------------------------------------------------------------
void CSkeleton::Traverse(int currIndex, int cameFrom)
{
  nodes[currIndex].parentIndex = cameFrom;
  for (int neighbor : nodes[currIndex].links)
    if (neighbor != cameFrom)
      Traverse(neighbor, currIndex);
}

// --------------------------------------------------------------------------------------------------------------------
Eigen::Matrix4d CSkeleton::RelativeRestPose(const CSkeleton& skeletonMeshPose, const CSkeleton& skeletonMeshRest, const int boneIndex)
{
  const CSkeletonBone& restBone = skeletonMeshRest.bones[boneIndex];
  const CSkeletonBone& poseBone = skeletonMeshPose.bones[boneIndex];

  if (poseBone.parentBone == -1)
  {
    return restBone.worldTransform;
  }
  else
  {
    const CSkeletonBone& parentRestBone = skeletonMeshRest.bones[restBone.parentBone];
    const CSkeletonBone& parentPoseBone = skeletonMeshPose.bones[poseBone.parentBone];

    Eigen::Matrix3d rotDeltaRest = restBone.worldTransform.block(0, 0, 3, 3) * parentRestBone.worldTransformInverted.block(0, 0, 3, 3);
    Eigen::Matrix3d finalRotation = rotDeltaRest * parentPoseBone.worldTransform.block(0, 0, 3, 3);

    Eigen::Matrix4d result(Eigen::Matrix4d::Identity());
    result(0, 3) = poseBone.worldTransform(0, 3);
    result(1, 3) = poseBone.worldTransform(1, 3);
    result(2, 3) = poseBone.worldTransform(2, 3);
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
        result(i, j) = finalRotation(i, j);

    return result;
  }
}


// ---------- CSkeletonSource -----------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------
CSkeletonSource::CSkeletonSource(void)
{
  this->SetNumberOfInputPorts(0);
  this->SetNumberOfOutputPorts(1);
}

// --------------------------------------------------------------------------------------------------------------------
CSkeletonSource::~CSkeletonSource(void)
{
}

// --------------------------------------------------------------------------------------------------------------------
int CSkeletonSource::RequestData(vtkInformation* vtkNotUsed(request), vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{
  // get the info objects
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the ouptut
  vtkPolyData *output = vtkPolyData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  if (output)
    Execute(output);
  return 1;
}

// --------------------------------------------------------------------------------------------------------------------
void CSkeletonSource::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

// --------------------------------------------------------------------------------------------------------------------
void CSkeletonSource::Execute(vtkPolyData* toPolydata)
{
  assert(toPolydata);
  // set up polydata object and data arrays
  const int nodeCount = (int)skeleton.nodes.size();
  const int boneCount = (int)skeleton.bones.size();
  vtkSmartPointer<vtkPoints> points = toPolydata->GetPoints() ? toPolydata->GetPoints() : vtkSmartPointer<vtkPoints>::New();
  points->Allocate(nodeCount);
  for (int i = 0; i < nodeCount; i++)
  {
    points->InsertPoint(i, skeleton.nodes[i].x, 
      skeleton.nodes[i].y, 
      skeleton.nodes[i].z);
  }

  toPolydata->SetPoints(points);

  toPolydata->DeleteCells();
  toPolydata->Allocate((int)skeleton.nodes.size() + 32);
  for (int i = 0; i < boneCount; i++)
  {
    const CSkeletonBone& bones = skeleton.bones[i];

    vtkIdType pts[2];
    pts[0] = (vtkIdType)bones.nodeHead;
    pts[1] = (vtkIdType)bones.nodeTail;

    toPolydata->InsertNextCell(VTK_LINE, 2, pts);
    toPolydata->InsertNextCell(VTK_VERTEX, 2, pts);
  }
}

// --------------------------------------------------------------------------------------------------------------------
vtkStandardNewMacro(CSkeletonSource);