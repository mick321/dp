# Semestral project generating skeleton from mesh


This source can be built by CMake and depends on:

+ Eigen, math library
+ VTK 6.0.0, visualization framework

Eigen is header-only library and is already included in this repositary, no building is necessary.

## Build instructions:

1. Go to page [vtk.org](http://www.vtk.org/), download version 6.0.0

2. Use CMake to generate Visual studio project of VTK.

3. Build VTK framework in Visual Studio.

4. Clone this repository.

5. Use CMake to generate Visual Studio project for these sources.

6. Build generated project.
