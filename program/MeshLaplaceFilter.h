///
/// @file MeshLaplaceFilter.h
/// @author Michal Zak
/// @brief MeshLaplaceFilter class, implementation of weighted iterative Laplace transformation.
///

#pragma once
#include "meshemptyfilter.h"

/// Implementation of weighted iterative Laplace transformation.
class MeshLaplaceFilter : public MeshEmptyFilter
{
private:
  int IterationCount;     //!< specifies maximum number of iterations
  double sL;              //!< initial multiplying constant of Laplacian
  double avgFaceAreaCoef; //!< multiplying constant for initial weights (triangle areas)
  double volumeCoef;      //!< process stop if initialVolume (volumeCoef * initialvolume) is greater than currentVolume
  double initialWHCoef;   //!< initial weights for positions
  bool loggingEnabled;
  
  Eigen::SparseMatrix<double> matrixL; //!< Laplacian matrix
  Eigen::VectorXd matrixW_L; /* diagonal matrix W_L */
  Eigen::VectorXd matrixW_H; /* diagonal matrix W_H */
  std::vector<double> faceArea;
  std::vector<double> faceAreaInitial;

  /// Not implemented.
  MeshLaplaceFilter(const MeshLaplaceFilter&) : MeshEmptyFilter() { }
  /// Not implemented.
  void operator=(const MeshLaplaceFilter&) { }
  
  /// Compute area of each face.
  void ComputeFaceAreas(bool initial);
  /// Compute laplace operator "matrixL".
  void ComputeLaplaceOperator();
  /// Compute weight matrices "matrixW_L" nad "matrixW_H".
  void ComputeWeights(bool initial);
  /// Compute new vertex positions.
  void ComputeNewVertexes();
  /// Compute volume of mesh.
  double ComputeVolume();

  /// Static method for computing angle between vectors BA and BC.
  inline static double ComputeAngle(const CVertex A, const CVertex& B, const CVertex& C)
  {
    Eigen::Vector3d v1(A.dCoord[0] - B.dCoord[0], A.dCoord[1] - B.dCoord[1], A.dCoord[2] - B.dCoord[2]);
    Eigen::Vector3d v2(C.dCoord[0] - B.dCoord[0], C.dCoord[1] - B.dCoord[1], C.dCoord[2] - B.dCoord[2]);
    v1.normalize();
    v2.normalize();
    return acos(v1.dot(v2));
  }

  /// Static method for computing area of triangle ABC.
  inline static double ComputeArea(const CVertex& A, const CVertex& B, const CVertex& C)
  {
    Eigen::Vector3d v1(A.dCoord[0] - B.dCoord[0], A.dCoord[1] - B.dCoord[1], A.dCoord[2] - B.dCoord[2]);
    Eigen::Vector3d v2(C.dCoord[0] - B.dCoord[0], C.dCoord[1] - B.dCoord[1], C.dCoord[2] - B.dCoord[2]);
    const double rtn = 0.5 * (v1.cross(v2)).norm();
    return rtn;
  }

protected:
  MeshLaplaceFilter(void);
  ~MeshLaplaceFilter(void);
  virtual void Execute();
public:
  std::vector<CVertex> originalVertexes; ///< vertexes before Laplace transform

  /// Set filter constants.
  inline void SetConstants(int maxIterationCount,  ///< maximum iterations
    double sL,              ///< initial multiplying constant of Laplacian
    double avgFaceAreaCoef, ///< multiplying constant for initial weights (triangle areas)
    double volumeCoef,      ///< process stop if initialVolume (volumeCoef * initialvolume) > currentVolume
    double initialWHCoef)   ///< initial weights for positions
  {
    this->IterationCount = maxIterationCount;
    this->sL = sL;
    this->avgFaceAreaCoef = avgFaceAreaCoef;
    this->volumeCoef = volumeCoef;
    this->initialWHCoef = initialWHCoef;
  }

  /// VTK macro.
  vtkTypeMacro(MeshLaplaceFilter, MeshEmptyFilter);
  /// Print info about this object.
  void PrintSelf(ostream& os, vtkIndent indent);
  /// Create new instance.
  static MeshLaplaceFilter *New();
};