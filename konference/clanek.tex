\documentclass[a4paper,twoside]{article}

\usepackage{epsfig}
\usepackage{subfigure}
\usepackage{calc}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{multicol}
\usepackage{pslatex}
\usepackage{apalike}
\usepackage{SCITEPRESS}     % Please add other packages that you may need BEFORE the SCITEPRESS.sty package.

\subfigtopskip=0pt
\subfigcapskip=0pt
\subfigbottomskip=0pt

\begin{document}

\title{Generating Skeleton of Humanoid Objects Represented~by~Triangle~Meshes}

\author{\authorname{Michal \v{Z}\'{a}k\sup{}, Josef Kohout\sup{1}}
\affiliation{\sup{1}Department of Computer Science and Engineering, University of West Bohemia, Univerzitn\'{i} 8, Plze\v{n}, Czech Republic}
\email{mick321@gmail.com, besoft@kiv.zcu.cz}
}

\keywords{Skeleton Generation, Animation Skeleton, Humanoid Objects, VTK.}

\abstract{Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tempus sem risus, at suscipit nunc sodales id. Vestibulum ligula risus, ornare sit amet nunc ut, volutpat tempus libero. Praesent sagittis sed ex vel dapibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. In felis nisi, iaculis ut nunc et, tempus sodales justo. Mauris porta mollis sagittis. Vivamus eleifend convallis justo nec consequat. Maecenas ac leo elementum, efficitur est ac, pharetra metus. Nulla facilisi. }

\onecolumn \maketitle \normalsize \vfill

\section{\uppercase{Introduction}}
\label{sec:introduction}

\noindent Skeletal animation is a~technique commonly used for animating characters represented by triangle meshes. 
Vertices are controlled through hierarchical 1D structure of bones called skeleton. 
Each bone stores unique local transformation and each vertex stores binding weights scaling influence
of particular bone transformation.

The skeleton is usually created manually by 3D artist. This process contains also defining bone weights in each
mesh vertex. After short observation we expect that similar meshes would have similar skeletons
and therefore we can automatize generation of skeleton for certain types of meshes.
Our method produces simplified skeleton for meshes representing humanoids, resulting skeleton
is enhanced by joint movement limits afterwards.

\subsection{Existing Methods}

\noindent This section provides quick view on existing methods for generating skeletons. The input of methods
differs; geometric and topologic methods use meshes, volumetric methods use voxelized representation.

Amenta~et.~al~\cite{Amenta00} propose method computing piecewise linear approximation of medial axis of given input mesh.
However, medial axis is topologically complex and generation is greatly influenced by noise.

In \cite{Au2008}, authors invented a~method that iteratively shrinks mesh using discrete Laplace operator.
When reaching volume near zero, edge-collapse algorithm is performed repeatedly until mesh contains no faces left.
By this point, we have got 1D skeleton structure with final topology -- only positions of skeleton vertices are refined.
Method is noise-proof and performs well on various meshes. On the other hand, due to solving of Laplacian transformation,
the algorithm has cubic complexity.

Straka~et.~al~\cite{Straka2011} were scanning human with several cameras and they were able to create his simplified skeleton in real time. 
Approximate volumetric represenation of human was gathered and then transformed to 1D skeleton using method \textit{volume scooping}\cite{Rodriguez2009}.
However, this generated skeleton has too much vertices for further use. Straka~et~al. prepared template skeleton having immutable count of vertices and topological structure, each vertex
has also its semantical meaning (head, left hand etc.).
After volume scooping step, vertices of template and generated skeleton are paired using Hungarian method. 
Big advantage of this method is that resulting topology is very simple, in fact, we can configure
it by template skeleton topology. Second advantage lies in quick computational time. Method expects the topmost vertex to be always a~head, in other situations it must be marked manually.

In \cite{Ma2014}, they created skeleton by finding isocountours and joining their centers. 
At first, feature points are found in the mesh which are locally most distant vertices
to other vertices (they have no neighbor having larger distance from any chosen vertex).
We start famous Dijkstra's algorithm in these points to find isocontours (their linear approximation).
By joining centers of isocontours, we get an~animation skeleton. Similar isocontours are joined during the process, root joint is added separately at the end. Big disadvantage of this method is that final skeleton has a~star-like shape and therefore is unsuitable for animating humanoids. 

\section{\uppercase{Method Overview}}

We had several requirements on our method that would generate humanoid skeletons:

\begin{itemize}
\item The skeleton matches topology of the humanoid, it is emplaced in all limbs and head. We expect
    humanoid having two legs and arms, chest bones are not a~part of final skeleton.
\item The skeleton must have low count of bones. If it is to the contrary, the animation of such character 
    becomes more complex.
\item The skeleton lies at least approximatelly inside the mesh representing the humanoid.
\item Joint movement constraints are included.
\end{itemize}

None of explored generic methods fulfills our expectations. Therefore, we created method
which utilizes fact that input model represents a~humanoid:

\begin{enumerate}
\item We prepare reference skeleton of humanoid which already contains demanded joint movement constraints.
\item We generate temporary skeleton for input mesh using method from \cite{Au2008}. This skeleton has many
    short bones and defines no constraints.
\item Joints of temporary and reference skeleton are matched using
    \textit{dynamic time warping} and \textit{hungarian method}.
\item Reference skeleton can now be inserted into input mesh. Position of joints is determined by
    position of matching joints in temporary skeleton.
\item Constraints are simply copied, we expect that input mesh is in rest pose (outstretched arms, palms towards).
\end{enumerate}

\section{\uppercase{Reference Skeleton}}

Reference skeleton are the bones in rest pose. Each bone contains position coordinates, rotation specified 
by local frame, length and link to parent bone. Joint constraints between current and 
parent bone are embedded too.

\section{\uppercase{Temporary Skeleton}}

As it was told before, temporary skeleton is generated using method from \cite{Au2008}.
Let us present creation of temporary skeleton in a~nutshell.

First of all, the input mesh is repeatedly contracted by laplacian smoothing until we reach
threshold volume or run out of iterations. Laplacian smoothing removes details and moves vertices
in the opposite direction of computed surface normal. Each iteration step is done by solving
overdetermined system:

\begin{equation}
\label{eq:me-smrsteni}
\begin{bmatrix} \mathbf W_L \mathbf L \\ \mathbf W_H \end{bmatrix} \mathbf V' 
\;=\;
\begin{bmatrix} 0 \\ \mathbf W_H V \end{bmatrix}
\end{equation}

where $\mathbf V $ stands for present vertex positions, $\mathbf V'$ is the unknown variable representing
positions of vertices in next iteration. Diagonal weighting matrix $\mathbf W_L$ amplifies contraction,
$\mathbf W_H$ forces vertices to stay on same place. Matrix $\mathbf L$ defines discrete Laplace smoothing
against direction of normal and its values are defined as follows:

\begin{equation}
\mathbf L_{ij} \;=\;
\left\{
\begin{array}{l l}
  \omega_{ij} = cotg \, \alpha_{ij}  + cotg \, \beta_{ij} & \text{if } (i,j) \text{ is edge} \\
      & \text{between vertices } \\ 
      & v_i \text{ and } v_j \\
  \sum_{(i,k) \in E} -\omega_{ik} & \text{if } i = j \\
  0 & \text{otherwise}
\end{array} 
\right.
\end{equation}

where $\alpha_{ij}$ and $\beta_{ij}$ are angles opposing edge $(i,j)$. Mesh contraction is illustrated
on picture \ref{fig:me-contraction}.

\begin{figure}[!htp]
\centering
\includegraphics[width=6.5cm]{img/me-smrsteni.png}
\caption{Visualization of mesh contraction.}
\label{fig:me-contraction}
\end{figure}

At this point, we perform half-edge collapse technique to convert shrinked 2D mesh to 1D skeleton. 
We choose the least important edge in mesh and remove it. The process is stopped after all triangles
of model had been removed. Importance of edge is defined as a~weighted sum of two functions:

\begin{equation}
\begin{array}{l}
F(i,j) \; = \; w_a F_a(i,j) + w_b F_b(i,j) \\ \quad \text{ recommended: } w_a = 1,\; w_b = 0.1
\end{array}
\end{equation}

Function $F_a(i,j)$ represents error in shape that will arise after removing edge $(i,j)$:

\begin{equation}
\begin{array}{l}
F_a(i,j) \; = \; F(\mathbf v_j, i) + F(\mathbf v_j, j) \\[12pt]
F(\mathbf v_j, i) \; = \; \mathbf v_{j}^T \sum_{(i,j) \in E} \; (\mathbf K_{ij}^T \mathbf K_{ij}) \mathbf v_j \\[12pt]
\mathbf K_{ij}  \; = \; 
\begin{bmatrix}
0 & -a_z & a_y & -b_x \\
a_z & 0 & -a_x & -b_y \\
-a_y & a_x & 0 & -b_z
\end{bmatrix}
\end{array}
\end{equation}

\noindent
where $\mathbf a$ is normalized vector of edge $(i,j)$ a~$\mathbf b = \mathbf a \times \mathbf v_i$. 
Variable $E$ stands for the set of all edges in the input mesh. We expect that $\mathbf v_i$ is non-zero.

Function $F_b(i,j)$ represents penalty caused by length of edge $(i,j)$:

\begin{equation}
F_b(i,j) \; = \; \| \mathbf v_i -  \mathbf v_j \| \sum_{(i,k) \in E} \; \| \mathbf v_i -  \mathbf v_k \|
\end{equation}

At this time, we have strongly connected 1D structure in 3D space. Previous steps may have led to invalid 
vertex positions, so the last step is correction of positions. We compute new position of vertex 
as a~centroid of all original vertices which collapsed into it.

\section{\uppercase{Matching Vertices}}

After creating temporary skeleton, we have to match vertices from both reference and temporary skeleton.
This step was inspired by part of method from \cite{Straka2011}. Before the matching, we must preprocess
both skeletons.

\subsection{\uppercase{Preprocessing}}

We expect that vertex representing the head is the topmost one. This allows us to determine head
position automatically and in a~very simply way, however, this recognition can be replaced with
more sophisticated method or manual selection.

From now on, we treat both skeletons as graphs and compute geodesic distances between all possible
vertex pairs in both graphs. Next step is to sort vertices accordingly to their geodesic distance from
vertex marked as head. Sorting is necessary for continuation.

Longest path in graph serves as a~skeleton scale. Afterwards, we rescale reference skeleton to match the temporary one:

\begin{equation}
s \;=\; \dfrac{L_{temp}}{L_{ref}},
\end{equation}

\noindent where $L_{temp}$ stands for temporary and $L_{ref}$ for reference skeleton scale. Precomputed 
length of geodesic distances between vertices of reference skeleton are multiplied by factor $s$, too.

Last step of preprocessing is to align centers of bounding boxes of both skeleton 
($B_{temp}$ and~$B_{ref}$). We translate reference skeleton by vector $\mathbf T(x,y,z)$:

\begin{equation}
\mathbf T(x,y,z) \;=\; \begin{bmatrix} B_{temp}^{maxx} - B_{temp}^{minx} - B_{ref}^{maxx} + B_{ref}^{minx} \\[12pt] 
                               B_{temp}^{maxy} - B_{temp}^{miny} - B_{ref}^{maxy} + B_{ref}^{miny} \\[12pt]
                              B_{temp}^{maxz} - B_{temp}^{minz} - B_{ref}^{maxz} + B_{ref}^{minz} \\ \end{bmatrix}.
\end{equation}

\subsection{\uppercase{Leaves}}

At this point we match leaf nodes from reference skeleton to temporary one. The leaf node representing
head has been already matched and so it is excluded from following process. We denote leaf nodes of
reference skeleton as $L_{ref} = (r_1, r_2, \,\dots\, ,r_n)$, leaves from temporary skeleton as 
$L_{temp} = (t_1, t_2, \,\dots\, ,t_m)$. We expect that $m \geq n$, in other words, that temporary
skeleton has more nodes.

Leaf nodes from both graphs are matched using hungarian method which minimalizes price of assignment.
We define price $E_{i,j}$ for assignment of pair $(t_i, r_j)$  as follows:

\begin{align}
E_{i,j} \;=\; c_{DWT} \cdot DWT(d_{t_i}, d_{r_j})^2 + c_{dist} \cdot dist(t_i, r_j)^2,
\end{align}

Parameters $c_{DWT}$ and $c_{dist}$ serve as constant weights, function $dist$ computes euclidean 
distance between nodes $t_i$ (from temporary skeleton) and $r_i$ (from reference skeleton). Function $DWT$ 
is a~result value of \textit{dynamic time warping} for two sequences $d_{t_i}$ and $d_{r_j}$ which contain 
geodesic distances of all leaves from vertex $t_i$ or $r_i$.

After this step, we remove all vertices belonging to branches of temporary skeleton which end with 
an unmatched leaf (see picture \ref{fig:ppm-removing-branches}). This helps us to get cleaner results
in the following step -- matching inner vertices.

\begin{figure}[!htp]
\centering
\includegraphics[width=6.5cm]{img/ppm-odstraneni-vetvi.pdf}
\caption{Cutting of branches containing unmatched vertices $L_2$, $L_3$, $L_4$.}
\label{fig:ppm-removing-branches}
\end{figure}

\subsection{\uppercase{Inner vertices}}

Inner vertices are matched using local optimization -- independently on penalization 
of other vertices. Each vertex can be present in matching pair only once, algorithm
can use it only once.

We denote inner vertices of temporary skeleton as $u_i$, vertices of reference skeleton
as $v_j$. For each $u_i$, we go through all yet unassigned vertices $v_j$ and seach for
the least penalized assignment.

Penalization of assignment pair $(u_i, v_j)$ is composed of these parts:

\begin{enumerate}
\item \textbf{Difference of geodesic distances to leaves.} We already know assignment of leaves
and geodesic distance (denoted as $d(u_i, t)$ or $d(v_j, r)$) to each one of them. First part of penalization
is then defined as follows:

\begin{align}
E_L \;=\; \sum_{k=0}^{N}\;(d(u_i, t_0) - d(v_j, r_0))^2
\end{align}

\noindent
where $N$ is number of matched leaves.

\item \textbf{Difference of vertex degree}. Second part $E_D$ is defined as squared difference
between degrees of vertices $u_i$ and $v_j$.

\item \textbf{Distance of vertices}. Third part $E_P$ is equal to squared euclidean distance
between vertices $u_i$ and $v_j$.

\end{enumerate}

Resulting penalization is defined as weighted sum of particular penalizations:

\begin{align}
E \;=\; c_{E_L} \cdot E_L + c_{E_D} \cdot E_D + c_{E_P} \cdot E_P.
\end{align}

\noindent
where $c_{E_L}$, $c_{E_D}$ and $c_{E_P}$ are constants chosen by user. We recommend following settings 
for most input meshes: $c_{E_L} = 1$, $c_{E_D} = 0.1$ and $c_{E_P} = 0$.

\subsection{\uppercase{Local frames and constraints}}

We have successfully matched vertices of reference and temporary skeleton. At this point we 
set positions of reference vertices to be same as matching temporary ones. We created final
animation skeleton, however, local frames are still missing. Local frames (see picture \ref{fig:local-frames}) 
are necessary for practical use of skeleton animations (e.g. in skinning or inverse kinematics).

If the axis $y$ of local frame is aligned with orientation of bone $\mathbf{B}$ then
we can compute local frames in final skeleton from these equations:

\begin{equation}
\begin{array}{l}
\mathbf A_y \;=\; \dfrac{\overrightarrow{\mathbf B}}{\|\overrightarrow{\mathbf B}\|} \\
\mathbf A_z \;=\; \mathbf R_x \times \mathbf A_y \\
\mathbf A_x \;=\; \mathbf A_y \times \mathbf A_z
\end{array}
\end{equation}

\noindent
where $\mathbf A_x, \mathbf A_y, \mathbf A_z$ are coordinates of local frame in final skeleton 
and $\mathbf R_x, \mathbf R_y, \mathbf R_z$ in reference skeleton. We assume that both
skeletons are similar due to required rest pose of character.

This assumption allows us to copy definitions of joint movement constraints after assignment of local frames.

\begin{figure}[!htp]
\centering
\includegraphics[width=6.5cm]{img/ppm-lokalni-system.PNG}
\caption{Local frames of bones in the skeleton.}
\label{fig:local-frames}
\end{figure}

\section{\uppercase{Experiments \& Results}}

\noindent \dots

\section{\uppercase{Discussion}}
\label{sec:conclusion}

\noindent \dots

\section{\uppercase{Conclusions}}
\label{sec:conclusion}

\noindent \dots

\section*{\uppercase{Acknowledgements}}

\noindent \dots

\vfill
\bibliographystyle{apalike}
{\small
\bibliography{clanek}}


\section*{\uppercase{Appendix}}

\noindent \dots ?

\vfill
\end{document}

